//
//  MIHomeViewController.h
//  AMHP
//
//  Created by S12 Solutions on 7/25/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"

@interface MIAMHPHomeViewController : ParentViewController<MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet UITextField *txtSearchBar;
    IBOutlet UIButton *btnSort;
    IBOutlet UIButton *btnFilter;
    
    IBOutlet UIView *vSegment;
    IBOutlet UIView *vSegmentBack;
    IBOutlet UIButton *btnAddress;
    IBOutlet UIButton *btnLocation;
    IBOutlet NSLayoutConstraint *cnXViewSegment;
    
    IBOutlet UIView *vListMap;
    IBOutlet UIButton *btnMapView;
    IBOutlet UIButton *btnListView;
    
    IBOutlet UITableView *tblAddressBook;
    IBOutlet UITableView *tblVLocation;
    
    IBOutlet MKMapView *mapViewHome;
    
    IBOutlet UILabel *lblNoData;
}
@end
