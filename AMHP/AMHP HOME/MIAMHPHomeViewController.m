//
//  MIHomeViewController.m
//  AMHP
//
//  Created by S12 Solutions on 7/25/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPHomeViewController.h"
#import "MIDoctorDetailViewController.h"
#import "MIAddressBookTableViewCell.h"
#import "MILocationListTableViewCell.h"
#import "MIFilterViewController.h"
#import "MISortViewController.h"

#import "MKCustomAnnotation.h"
#import "CustomAnnotationView.h"
#import "CustomMapAnnotation.h"

#import "AFNetworkReachabilityManager.h"

@interface MIAMHPHomeViewController ()
{
    NSMutableArray *arrAddressBook;
    NSMutableArray *arrLocationSearch;
    NSMutableArray *arrSelectedDoctors;
    
    CustomMapAnnotation *customAnnotation;
    
    NSDictionary *dictFilterAddressBook;
    NSDictionary *dictSortByAddressBook;
    
    NSDictionary *dictFilterLocation;
    NSDictionary *dictSortByLocation;
    
    NSMutableArray *arrLocalAuthority;
    NSMutableArray *arrSpecialties;
    NSMutableArray *arrLanguageSpoken;
    NSMutableArray *arrAutoSuggesion;
    
    NSURLSessionDataTask *sessionTask;
    NSURLSessionDataTask *sessionTaskFavourite;
    
    double selectedLocationLatitude;
    double selectedLocationLongitude;
    
    NSString *strLocationMapSearch;
    NSString *strAddressBookSearch;
    NSString *strListSearch;
    
    int iOffsetAddress;
    int iOffsetLocation;
    
    UIRefreshControl *refreshControl;
    
    BOOL isFirstTimeLocation;
}

@end

@implementation MIAMHPHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self.navigationController navigationBar] setShadowImage:[UIImage new]];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    //..... Set segment frame
    [vSegment.layer setCornerRadius:CViewHeight(vSegment)/2.0f];
    [vSegmentBack.layer setCornerRadius:CViewHeight(vSegmentBack)/2.0f];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self.navigationController navigationBar] setShadowImage:nil]; //..... HIDE NAVIGATION LINE
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}






# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"AMHP Home";
    
    isFirstTimeLocation = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReachAbilityChange) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    [self handleReachAbilityChange];
    
    if ([AFNetworkReachabilityManager sharedManager].isReachable)
    {
        [CUserDefaults removeObjectForKey:UserDefaultAddressBook];
        [CUserDefaults synchronize];
    }
    
    // SET SHADOW
    vSegment.layer.cornerRadius = 5;
    vSegment.layer.shadowColor = ColorBlack.CGColor;
    vSegment.layer.shadowOffset = CGSizeZero;
    vSegment.layer.shadowRadius = 3;
    vSegment.layer.shadowOpacity = 0.1;
    vSegment.layer.masksToBounds = NO;
    
    arrAddressBook = [NSMutableArray new];
    arrLocationSearch = [NSMutableArray new];
    arrAutoSuggesion = [NSMutableArray new];
    
    // SEARCH BAR CONFIGURATION
    [self searchBarConfiguration];
    
    
    // MAPVIEW CONFIGURATION
    [self mapViewConfiguration];
    
    
    [tblAddressBook setContentInset:UIEdgeInsetsMake(13, 0, 0, 0)];
    [tblAddressBook registerNib:[UINib nibWithNibName:@"MIAddressBookTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAddressBookTableViewCell"];
    [tblAddressBook registerNib:[UINib nibWithNibName:@"MILocationListTableViewCell" bundle:nil] forCellReuseIdentifier:@"MILocationListTableViewCell"];
    
    //..... BAR BUTTON
    [self setEditBtn];
    
    
    //..... UIREFRESH CONTROL
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = ColorGreen_47C0B6;
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblAddressBook addSubview:refreshControl];
    
    
    if (Is_iPhone_4 || Is_iPhone_5)
    {
        NSString *fontName = btnAddress.titleLabel.font.fontName;
        CGFloat fontSize = btnAddress.titleLabel.font.pointSize;
        btnAddress.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 2)];
        btnLocation.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 2)];
    }
    
    arrLocalAuthority = [NSMutableArray new];
    arrSpecialties = [NSMutableArray new];
    arrLanguageSpoken = [NSMutableArray new];
    arrSelectedDoctors = [NSMutableArray new];
    
    [self getLocalAuthorityListFromServer];
    [self getSpecialtiesListFromServer];
    [self getLanguageListFromServer];
    
    [btnAddress setSelected:NO];
    [btnLocation setSelected:NO];
    [btnAddress sendActionsForControlEvents:UIControlEventTouchUpInside];
    [cnXViewSegment setConstant:CViewX(btnAddress)];
    
    [btnMapView setSelected:NO];
    [btnListView setSelected:NO];
    
    [self checkLocationService];
}

- (void)searchBarConfiguration
{
    [txtSearchBar.layer setBorderWidth:1];
    [txtSearchBar.layer setCornerRadius:15];
    [txtSearchBar.layer setMasksToBounds:YES];
    [txtSearchBar.layer setBorderColor:CRGB(228, 228, 228).CGColor];
    [txtSearchBar setFont:Is_iPhone_5 ? CFontAvenirLTStd55Roman(11) : CFontAvenirLTStd55Roman(13)];
    [txtSearchBar setPlaceHolderColor:ColorDarkGray_ededf1];
    
    UIButton *btnClear = [txtSearchBar valueForKey:@"_clearButton"];
    UIImage *imageNormal = [btnClear imageForState:UIControlStateNormal];
    [btnClear setImage:imageNormal forState:UIControlStateHighlighted];
    
    [txtSearchBar addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [txtSearchBar setLeftImage:[UIImage imageNamed:@"search"] withSize:CGSizeMake(30, 16)];
    [txtSearchBar setReturnKeyType:UIReturnKeySearch];
}

- (void)checkLocationService
{
    if ((![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]  == kCLAuthorizationStatusDenied))
    {
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CLocationServiceMap firstButton:COpenAppSetting firstHandler:^(UIAlertAction *action)
        {
            [appDelegate openSettings];
        }
        secondButton:CCancel secondHandler:nil inView:self];
    }
}

- (void)addressEditing
{
    if (self.editing)
    {
        if ([arrSelectedDoctors count] > 0)
        {
            dispatch_async(GCDMainThread, ^{
                
                if([MFMessageComposeViewController canSendText])
                {
                    if ([arrSelectedDoctors count] > 0)
                    {
                        [appDelegate contactDoctor:[[arrSelectedDoctors valueForKeyPath:@"doctor_id"] componentsJoinedByString:@","] andCotactType:CContactSms];
//                        [appDelegate trackTheEventWithName:CEventMultiTextCreated properties:@{@"doctor_id":[arrSelectedDoctors valueForKeyPath:@"doctor_id"],
//                                                                                               @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO],
//                            @"number_of_doctors":[NSNumber numberWithInteger:arrSelectedDoctors.count]}];
                    }
                    
                    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init]; // Create message VC
                    messageController.messageComposeDelegate = self; // Set delegate to current instance
                    
                    messageController.recipients = [arrSelectedDoctors valueForKeyPath:@"mobile_number"]; // Set the recipients of the message to the created array
                    
                    messageController.body = [NSString stringWithFormat:@"I am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]. Are you available to attend? If so, please reply to this email or phone [number] for more information.\n\nKind regards,\n\n[Name]"];
                    
                    [self presentViewController:messageController animated:YES completion:NULL];
                }
                else
                    [CustomAlertView iOSAlert:@"" withMessage:CMessageSMSNotSupport onView:self];
            });
        }
        
        [super setEditing:NO animated:NO];
        [tblAddressBook setEditing:NO animated:NO];
        [tblAddressBook reloadData];
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"group_message"]];
        [self.navigationItem.rightBarButtonItem setTitle:nil];
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
    }
    else
    {
        [super setEditing:YES animated:YES];
        [tblAddressBook setEditing:YES animated:YES];
        [tblAddressBook reloadData];
        [self.navigationItem.rightBarButtonItem setImage:nil];
        [self.navigationItem.rightBarButtonItem setTitle:@"Done"];
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
    }
}

- (void)handleReachAbilityChange
{
    if ([AFNetworkReachabilityManager sharedManager].reachable) //..... Internet Reachable
    {
        NSLog(@"AFNetworkReachabilityManager reachable");
        txtSearchBar.userInteractionEnabled = YES;
    }
    else    //..... Internet Not Reachable
    {
        NSLog(@"AFNetworkReachabilityManager not reachable");
        txtSearchBar.userInteractionEnabled = NO;
    }
}






# pragma mark
# pragma mark - Filter & Sort Selection

- (void)setAddressBookFilterIcon
{
    if (dictFilterAddressBook.allKeys.count > 0)
        [btnFilter setImage:[UIImage imageNamed:@"filter_selected"] forState:UIControlStateNormal];
    else
        [btnFilter setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
}

- (void)setLocationSearchFilterIcon
{
    if (dictFilterLocation.allKeys.count > 0)
        [btnFilter setImage:[UIImage imageNamed:@"filter_selected"] forState:UIControlStateNormal];
    else
        [btnFilter setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
}

- (void)setAddressBookSortByIcon
{
    if (dictSortByAddressBook.allKeys.count > 0)
        [btnSort setImage:[UIImage imageNamed:@"sort_by_selected"] forState:UIControlStateNormal];
    else
        [btnSort setImage:[UIImage imageNamed:@"sortby"] forState:UIControlStateNormal];
}

- (void)setLocationSearchSortByIcon
{
    if (dictSortByLocation.allKeys.count > 0)
        [btnSort setImage:[UIImage imageNamed:@"sort_by_selected"] forState:UIControlStateNormal];
    else
        [btnSort setImage:[UIImage imageNamed:@"sortby"] forState:UIControlStateNormal];
}

- (void)filterAndSortSelection
{
    if (btnAddress.selected)
    {
        [self setAddressBookFilterIcon];
        [self setAddressBookSortByIcon];
    }
    else if (btnLocation.selected)
    {
        if (btnMapView.selected)
        {
            [self setLocationSearchFilterIcon];
        }
        else if (btnListView.selected)
        {
            [self setLocationSearchFilterIcon];
            [self setLocationSearchSortByIcon];
        }
    }
}








# pragma mark
# pragma mark - API Methods

- (void)pullToRefresh
{
    if (btnLocation.selected)
    {
        if ([AFNetworkReachabilityManager sharedManager].reachable)
        {
            NSLog(@"AFNetworkReachabilityManager reachable");
            isFirstTimeLocation = NO;
            iOffsetLocation = 0;
            [CUserDefaults removeObjectForKey:UserDefaultLocationSearch];
            [CUserDefaults removeObjectForKey:UserDefaultMapSearch];
            [self locationAPIAccordingToLatLong];
        }
        else
            [refreshControl endRefreshing];
    }
    else if (btnAddress.selected)
    {
        if ([AFNetworkReachabilityManager sharedManager].reachable)
        {
            iOffsetAddress = 0;
            [CUserDefaults removeObjectForKey:UserDefaultAddressBook];
            [self addressBookAPIAccordingToLatLong];
        }
        else
            [refreshControl endRefreshing];
    }
}

- (void)loadAddressBookDataFromLocal
{
    NSArray *arrData = [CUserDefaults valueForKey:UserDefaultAddressBook];
    [arrAddressBook removeAllObjects];
    [arrAddressBook addObjectsFromArray:arrData];
    [tblAddressBook reloadData];
}

- (void)loadLocationSearchDataFromLocal
{
    NSArray *arrData = [CUserDefaults valueForKey:UserDefaultLocationSearch];
    [arrLocationSearch removeAllObjects];
    [arrLocationSearch addObjectsFromArray:arrData];
}

- (void)addressBookAPIAccordingToLatLong
{
    if ((![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]  == kCLAuthorizationStatusDenied))
    {
        [self getHomeDataFromServerWithLatitude:CDefaultLatitude andLongitude:CDefaultLongitude];
    }
    else
    {
        [self getHomeDataFromServerWithLatitude:appDelegate.current_lat andLongitude:appDelegate.current_long];
    }
}

- (void)locationAPIAccordingToLatLong
{
    if (selectedLocationLatitude && selectedLocationLongitude)
    {
        [self getHomeDataFromServerWithLatitude:selectedLocationLatitude andLongitude:selectedLocationLongitude];
    }
    else if ((![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]  == kCLAuthorizationStatusDenied))
    {
        [self getHomeDataFromServerWithLatitude:CDefaultLatitude andLongitude:CDefaultLongitude];
    }
    else
    {
        [self getHomeDataFromServerWithLatitude:appDelegate.current_lat andLongitude:appDelegate.current_long];
    }
}

- (void)cancelRequest
{
    if (sessionTask && sessionTask.state == NSURLSessionTaskStateRunning)
        [sessionTask cancel];
}

- (void)getHomeDataFromServerWithLatitude:(double)latitude andLongitude:(double)longitude
{
    NSString *strFilterAvailability;
    NSString *strFilterWeeklyDays;
    NSString *strFilterTimeSlot;
    
    int iFilterLocalAuthority = 0;
    int iFilterDistance = 0;
    int iFilterEmployer = 0;
    int iFilterSpecialty = 0;
    int iFilterGender = 0;
    int iFilterLanguage = 0;
    
    int iSortByAvailability = 0;
    int iSortByEmployer = 0;
    int iSortByDistance = 0;
    int iSortByName = 0;
    
    if (btnAddress.selected)
    {
        strFilterAvailability = [[dictFilterAddressBook stringValueForJSON:@"Availability"] isBlankValidationPassed] ? [self availabilitySelected:[[dictFilterAddressBook valueForKey:@"Availability"] componentsSeparatedByString:CComponentJoinedString]] : @"";
        strFilterWeeklyDays = [[dictFilterAddressBook stringValueForJSON:CWeeklyAvailability] isBlankValidationPassed] ? [self weekDaysSelected:[[dictFilterAddressBook valueForKey:CWeeklyAvailability] componentsSeparatedByString:CComponentJoinedString]] : @"";
        strFilterTimeSlot = [[dictFilterAddressBook stringValueForJSON:CTimeSlot] isBlankValidationPassed] ? [self timeSlotSelected:[[dictFilterAddressBook valueForKey:CTimeSlot] componentsSeparatedByString:CComponentJoinedString]] : @"";
        
        iFilterLocalAuthority = [dictFilterAddressBook intForKey:@"filter_local_authority"];
        iFilterDistance = [dictFilterAddressBook intForKey:@"filter_distance"];
        iFilterEmployer = [dictFilterAddressBook intForKey:@"filter_employer"];
        iFilterSpecialty = [dictFilterAddressBook intForKey:@"filter_specialty"];
        iFilterGender = [dictFilterAddressBook intForKey:@"filter_gender"];
        iFilterLanguage = [dictFilterAddressBook intForKey:@"filter_language"];
        
        iSortByAvailability = [dictSortByAddressBook intForKey:@"sort_availability"];
        iSortByEmployer = [dictSortByAddressBook intForKey:@"sort_employer"];
        iSortByDistance = [dictSortByAddressBook intForKey:@"sort_distance"];
        iSortByName = [dictSortByAddressBook intForKey:@"sort_name"];
    }
    else if (btnLocation.selected)
    {
        strFilterAvailability = [[dictFilterLocation stringValueForJSON:@"Availability"] isBlankValidationPassed] ? [self availabilitySelected:[[dictFilterLocation valueForKey:@"Availability"] componentsSeparatedByString:CComponentJoinedString]] : @"";
        
        strFilterWeeklyDays = [[dictFilterLocation stringValueForJSON:CWeeklyAvailability] isBlankValidationPassed] ? [self weekDaysSelected:[[dictFilterLocation valueForKey:CWeeklyAvailability] componentsSeparatedByString:CComponentJoinedString]] : @"";
        
        strFilterTimeSlot = [[dictFilterLocation stringValueForJSON:CTimeSlot] isBlankValidationPassed] ? [self timeSlotSelected:[[dictFilterLocation valueForKey:CTimeSlot] componentsSeparatedByString:CComponentJoinedString]] : @"";
        
        iFilterLocalAuthority = [dictFilterLocation intForKey:@"filter_local_authority"];
        iFilterDistance = [dictFilterLocation intForKey:@"filter_distance"];
        iFilterEmployer = [dictFilterLocation intForKey:@"filter_employer"];
        iFilterSpecialty = [dictFilterLocation intForKey:@"filter_specialty"];
        iFilterGender = [dictFilterLocation intForKey:@"filter_gender"];
        iFilterLanguage = [dictFilterLocation intForKey:@"filter_language"];
        
        iSortByAvailability = [dictSortByLocation intForKey:@"sort_availability"];
        iSortByEmployer = [dictSortByLocation intForKey:@"sort_employer"];
        iSortByDistance = [dictSortByLocation intForKey:@"sort_distance"];
        iSortByName = [dictSortByLocation intForKey:@"sort_name"];
    }
    
    [self cancelRequest];
    
    int offset = 0;
    
    if (btnAddress.selected)
        offset = iOffsetAddress;
    else if (btnLocation.selected)
        offset = iOffsetLocation;
    
    if (refreshControl.isRefreshing) offset = 0;
    
    sessionTask = [[APIRequest request] amhpHomeWithType:btnAddress.selected ? 1 : 2 andOffset:offset andSearchTxt:btnMapView.selected ? @"" : txtSearchBar.text andFilterAvailability:strFilterAvailability ? strFilterAvailability : @"0" andFilterLocalAuthority:iFilterLocalAuthority ? iFilterLocalAuthority : 0 andFilterDistance:iFilterDistance ? iFilterDistance : 0 andFilterEmployer:iFilterEmployer ? iFilterEmployer : 0 andFilterSpecialty:iFilterSpecialty ? iFilterSpecialty : 0 andFilterGender:iFilterGender ? iFilterGender : 0 andFilterLanguage:iFilterLanguage ? iFilterLanguage : 0 andSortbyAvailability:iSortByAvailability ? iSortByAvailability : 0 andSortbyEmployer:iSortByEmployer andSortbyDistance:iSortByDistance ? iSortByDistance : 0 andSortbyName:iSortByName andLatitude:latitude andlongitude:longitude andWeekDays:strFilterWeeklyDays andTimeSlot:strFilterTimeSlot completed:^(id responseObject, NSError *error)
    {
        [refreshControl endRefreshing];
       
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            [self filterAndSortSelection];
            
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            if (btnAddress.selected)
            {
                if (iOffsetAddress == 0)
                    [arrAddressBook removeAllObjects];
                
                if (arrData.count > 0)
                {
                    iOffsetAddress = [[responseObject valueForKey:CJsonMeta] intForKey:CJsonNewOffset];
                    [arrAddressBook addObjectsFromArray:arrData];
                    
                    [CUserDefaults setValue:arrAddressBook forKey:UserDefaultAddressBook];
                    [CUserDefaults synchronize];
                    
                    [self loadAddressBookDataFromLocal];
                }
                else
                {
                    [CUserDefaults setValue:arrAddressBook forKey:UserDefaultAddressBook];
                    [CUserDefaults synchronize];
                }
                
            }
            else if (btnLocation.selected)
            {
                if (iOffsetLocation == 0)
                    [arrLocationSearch removeAllObjects];
                
                if (btnMapView.selected)
                {
                    [CUserDefaults setValue:txtSearchBar.text forKey:UserDefaultMapSearch];
                    [CUserDefaults synchronize];
                }
                
                if (arrData.count > 0)
                {
                    iOffsetLocation = [[responseObject valueForKey:CJsonMeta] intForKey:CJsonNewOffset];
                    [arrLocationSearch addObjectsFromArray:arrData];
                    
                    [CUserDefaults setValue:arrLocationSearch forKey:UserDefaultLocationSearch];
                    [CUserDefaults synchronize];
                    
                    [self loadLocationSearchDataFromLocal];
                    
                    if (btnListView.selected)
                        [tblAddressBook reloadData];
                }
            }
            
            if (btnMapView.selected)
            {
                [mapViewHome removeAnnotations:mapViewHome.annotations];
                [self mapViewConfiguration];
            }
        }
       
        if (btnAddress.selected)
        {
            [self displayNoData:arrAddressBook];
        }
        else if (btnLocation.selected)
        {
            [self displayNoData:arrLocationSearch];
        }
        
    }];
}

- (void)displayNoData:(NSArray *)arrData
{
    if (arrData.count == 0)
    {
        [lblNoData setHidden:btnMapView.selected ? YES : NO];
        [tblVLocation setHidden:YES];
    }
    else
    {
        [lblNoData setHidden:YES];
        [tblAddressBook setHidden:NO];
    }
}

- (void)getLocalAuthorityListFromServer
{
    [[APIRequest request] localAuthorityListWithLoginType:appDelegate.loginUserAMHP.login_type andType:CLAAll andRegionID:@"" completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             NSArray *arrData = [responseObject valueForKey:CJsonData];
             
             if (arrData.count > 0)
             {
                 [arrLocalAuthority removeAllObjects];
                 [arrLocalAuthority addObjectsFromArray:arrData];
             }
         }
     }];
}

- (void)getSpecialtiesListFromServer
{
    [[APIRequest request] specialtiesList:appDelegate.loginUserAMHP.login_type completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             NSArray *arrData = [responseObject valueForKey:CJsonData];
             
             if (arrData.count > 0)
             {
                 [arrSpecialties removeAllObjects];
                 [arrSpecialties addObjectsFromArray:arrData];
             }
         }
     }];
}

- (void)getLanguageListFromServer
{
    [[APIRequest request] languageList:appDelegate.loginUserAMHP.login_type completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             NSArray *arrData = [responseObject valueForKey:CJsonData];
             
             if (arrData.count > 0)
             {
                 [arrLanguageSpoken removeAllObjects];
                 [arrLanguageSpoken addObjectsFromArray:arrData];
             }
         }
     }];
}

- (void)clearDataForAddressBook
{
    iOffsetAddress = 0;
    [arrAddressBook removeAllObjects];
    [tblAddressBook reloadData];
}

- (void)clearDataForLocation
{
    iOffsetLocation = 0;
    [arrLocationSearch removeAllObjects];
    if (btnListView.selected)
        [tblAddressBook reloadData];
}





# pragma mark
# pragma mark - Action Event

- (IBAction)btnSegmentClicked:(UIButton *)sender
{
    if (sender.selected) return;
    
    [appDelegate resignKeyboard];

    [btnAddress setSelected:NO];
    [btnLocation setSelected:NO];
    [sender setSelected:YES];
    
    [self filterAndSortSelection];
    
    tblAddressBook.hidden = mapViewHome.hidden = tblVLocation.hidden = YES;
    
    [arrSelectedDoctors removeAllObjects];
    [tblAddressBook setEditing:NO animated:YES];
    [tblAddressBook reloadData];
    self.editing = NO;
    self.navigationItem.rightBarButtonItem = nil;
    [self setEditBtn];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [txtSearchBar setPlaceholder: sender.tag == 0 ? CMessageAddressBookPlaceholder : CMessageSearchPlaceholder];
    
    [UIView transitionWithView:tblAddressBook
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         [cnXViewSegment setConstant:CViewX(sender)];
         [vSegment layoutIfNeeded];
         [sender setSelected:YES];
         [vListMap hideByHeight:!sender.tag];
         
         if (sender.tag == 0)
         {
             //..... ADDRESS BOOK
             
             if (![AFNetworkReachabilityManager sharedManager].reachable)
                 [appDelegate toastAlertWithMessage:CMessageOffline showDone:NO];
             
             [self loadAddressBookDataFromLocal];
             txtSearchBar.text = strAddressBookSearch;
             btnListView.selected = NO;
             btnMapView.selected = NO;
             tblAddressBook.hidden = NO;
             [btnSort hideByWidth:NO];
             
             if ([arrAddressBook count] == 0)
                 [self addressBookAPIAccordingToLatLong];
             
             [tblAddressBook reloadData];
             [self displayNoData:arrAddressBook];
         }
         else
         {
             //..... LOCATION SEARCH
             
             mapViewHome.hidden = NO;
             
             if ([AFNetworkReachabilityManager sharedManager].reachable && isFirstTimeLocation)
             {
                 NSLog(@"AFNetworkReachabilityManager reachable");
                 isFirstTimeLocation = NO;
                 [self clearDataForLocation];
                 txtSearchBar.text = strLocationMapSearch;
                 
                 [CUserDefaults removeObjectForKey:UserDefaultLocationSearch];
                 [CUserDefaults removeObjectForKey:UserDefaultMapSearch];
                 
                 [self locationAPIAccordingToLatLong];
             }
             else
             {
                 txtSearchBar.text = [CUserDefaults valueForKey:UserDefaultMapSearch];
                 if (arrLocationSearch.count == 0)
                     [self locationAPIAccordingToLatLong];
             }
             
             [btnMapView sendActionsForControlEvents:UIControlEventTouchUpInside];
         }
         
     } completion:^(BOOL finished)
     {
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
     }];
}

- (IBAction)btnMapListClicked:(UIButton *)sender
{
    if (sender.selected) return;
    
    [appDelegate resignKeyboard];
    
    [btnMapView setSelected:NO];
    [btnListView setSelected:NO];
    [sender setSelected:YES];
    [btnSort hideByWidth:!sender.tag];
    [tblAddressBook setHidden:!sender.tag];
    [mapViewHome setHidden:sender.tag];
    
    [self filterAndSortSelection];

    [txtSearchBar setPlaceholder: sender.tag == 0 ? CMessageSearchPlaceholder : CMessageAddressBookPlaceholder];
    
    switch (sender.tag)
    {
        case 0:
        {
            //..
            [self handleReachAbilityChange];
            self.navigationItem.rightBarButtonItem = nil;
            txtSearchBar.text = [CUserDefaults valueForKey:UserDefaultMapSearch];
            [lblNoData setHidden:YES];
            [self mapViewConfiguration];
            break;
        }
            
        case 1:
        {
            if (![AFNetworkReachabilityManager sharedManager].reachable)
                [appDelegate toastAlertWithMessage:CMessageOffline showDone:NO];
            
            [arrSelectedDoctors removeAllObjects];
            [tblAddressBook setEditing:NO animated:YES];
            self.editing = NO;
            self.navigationItem.rightBarButtonItem = nil;
            [self setEditBtn];
            [tblVLocation setHidden:YES];
            txtSearchBar.text = strListSearch;
            [tblAddressBook reloadData];
            
            [self displayNoData:arrLocationSearch];
            
            break;
        }
    }
    
    [self loadLocationSearchDataFromLocal];
}

- (IBAction)btnSortClicked:(UIButton *)sender
{
    MISortViewController *sortBy = [[MISortViewController alloc] initWithNibName:@"MISortViewController" bundle:nil];
    [sortBy setIObject:btnAddress.selected ? dictSortByAddressBook : dictSortByLocation];
    [sortBy setMISortType:btnAddress.selected ? MISortTypeAddressBook : MISortTypeLocation];
    [sortBy setBlock:^(id object, NSError *error)
    {
        if (object)
        {
            if ([AFNetworkReachabilityManager sharedManager].reachable)
            {
                [[MILoader sharedInstance] startAnimation];
                
                if (btnAddress.selected)
                {
                    if (arrAddressBook.count > 0)
                    {
                        iOffsetAddress = 0;
                        dictSortByAddressBook  = object;
                        [self addressBookAPIAccordingToLatLong];
                    }
                    else
                    {
                        [[MILoader sharedInstance] stopAnimation];
                        [CustomAlertView iOSAlert:@"" withMessage:CMessageNoDoctorForSort onView:self];
                    }
                }
                else if (btnLocation.selected)
                {
                    if (arrLocationSearch.count > 0)
                    {
                        iOffsetLocation = 0;
                        dictSortByLocation = object;
                        [self locationAPIAccordingToLatLong];
                    }
                    else
                    {
                        [[MILoader sharedInstance] stopAnimation];
                        [CustomAlertView iOSAlert:@"" withMessage:CMessageNoDoctorForSort onView:self];
                    }
                }
            }
        }
        
        [self filterAndSortSelection];
        
    }];
    
    [self presentViewController:[UINavigationController navigationControllerWithRootViewController:sortBy] animated:YES completion:nil];
}

- (IBAction)btnFilterClicked:(UIButton *)sender
{
    MIFilterViewController *filter = [[MIFilterViewController alloc] initWithNibName:@"MIFilterViewController" bundle:nil];
    
    [filter setIObject:btnLocation.selected ? dictFilterLocation : dictFilterAddressBook];
    [filter setMIFilterType:!btnLocation.selected ? MIFilterTypeNormal : MIFilterTypeMap];
    
    filter.arrFilterLanguage = arrLanguageSpoken;
    filter.arrFilterSpecialties = arrSpecialties;
    filter.arrFilterLocalAuthority = arrLocalAuthority;
    
    [filter setBlock:^(id object, NSError *error)
     {
         if (object)
         {
             if ([AFNetworkReachabilityManager sharedManager].reachable)
             {
                 [[MILoader sharedInstance] startAnimation];
                 
                 if (btnLocation.selected)
                 {
                     [self checkLocationService];
                     
                     dictFilterLocation = object;
                     [self clearDataForLocation];
                     [self locationAPIAccordingToLatLong];
                 }
                 else if (btnAddress.selected)
                 {
                     dictFilterAddressBook = object;
                     [self clearDataForAddressBook];
                     [self addressBookAPIAccordingToLatLong];
                 }
             }
         }
         
         [self filterAndSortSelection];
     }];

    [self presentViewController:[UINavigationController navigationControllerWithRootViewController:filter] animated:YES completion:nil];
}

- (void)setEditBtn
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"group_message"] style: UIBarButtonItemStylePlain target:self action:@selector(addressEditing)];
}

- (NSString *)availabilitySelected:(NSArray *)arrAvailable
{
    NSMutableArray *arrAddress = [[NSMutableArray alloc] init];
    
    if ([arrAvailable containsObject:@"Available"])
        [arrAddress addObject:@"1"];
    
    if ([arrAvailable containsObject:@"May be available"])
        [arrAddress addObject:@"2"];
    
    if ([arrAvailable containsObject:@"Unavailable"])
        [arrAddress addObject:@"3"];
    
    if ([arrAvailable containsObject:@"Unknown"])
        [arrAddress addObject:@"4"];
    
    return [arrAddress componentsJoinedByString:CComponentJoinedString];
}

- (NSString *)weekDaysSelected:(NSArray *)arrWeekDays
{
    NSMutableArray *arrAddress = [[NSMutableArray alloc] init];
    
    if ([arrWeekDays containsObject:@"Mon"])
        [arrAddress addObject:@"1"];
    
    if ([arrWeekDays containsObject:@"Tue"])
        [arrAddress addObject:@"2"];
    
    if ([arrWeekDays containsObject:@"Wed"])
        [arrAddress addObject:@"3"];
    
    if ([arrWeekDays containsObject:@"Thu"])
        [arrAddress addObject:@"4"];
    
    if ([arrWeekDays containsObject:@"Fri"])
        [arrAddress addObject:@"5"];
    
    if ([arrWeekDays containsObject:@"Sat"])
        [arrAddress addObject:@"6"];
    
    if ([arrWeekDays containsObject:@"Sun"])
        [arrAddress addObject:@"7"];
    
    return [arrAddress componentsJoinedByString:CComponentJoinedString];
}

- (NSString *)timeSlotSelected:(NSArray *)arrTimeSlot
{
    NSMutableArray *arrAddress = [[NSMutableArray alloc] init];
    
    if ([arrTimeSlot containsObject:@"Morning"])
        [arrAddress addObject:@"1"];
    
    if ([arrTimeSlot containsObject:@"Lunch"])
        [arrAddress addObject:@"2"];
    
    if ([arrTimeSlot containsObject:@"Afternoon"])
        [arrAddress addObject:@"3"];
    
    if ([arrTimeSlot containsObject:@"Evening"])
        [arrAddress addObject:@"4"];
    
    if ([arrTimeSlot containsObject:@"Night"])
        [arrAddress addObject:@"5"];
    
    return [arrAddress componentsJoinedByString:CComponentJoinedString];
}




# pragma mark
# pragma mark - UITextField Delegate

- (void)textFieldDidChange:(UITextField *)textField
{
    if ([AFNetworkReachabilityManager sharedManager].reachable)
    {
        textField.userInteractionEnabled = YES;
    }
    else
    {
        textField.userInteractionEnabled = NO;
        return;
    }
    
    if (btnMapView.selected)
    {
        if (textField.text.length != 0)
        {
            if (sessionTask && sessionTask.state == NSURLSessionTaskStateRunning)
                [sessionTask cancel];
            
            sessionTask = [[APIRequest request] LoadGooglePlacesList:textField.text completed:^(id responseObject, NSError *error)
                           {
                               if (responseObject && !error)
                               {
                                   [arrAutoSuggesion removeAllObjects];
                                   
                                   NSArray *arr = [responseObject valueForKey:@"predictions"];
                                   
                                   if (arr.count > 0)
                                   {
                                       [arrAutoSuggesion addObjectsFromArray:[arr valueForKey:@"description"]];
                                       [tblVLocation reloadData];
                                   }
                               }
                               
                               if (arrAutoSuggesion.count == 0 || ![textField.text isBlankValidationPassed])
                                   [tblVLocation setHidden:YES];
                               else
                                   [tblVLocation setHidden:NO];
                           }];
        }
    }
    
    if (![textField.text isBlankValidationPassed])
    {
        if (btnAddress.selected)
        {
            [self clearDataForAddressBook];
            [self addressBookAPIAccordingToLatLong];
        }
        else if (btnListView.selected || btnMapView.selected)
        {
            selectedLocationLatitude = selectedLocationLongitude = 0;
            [tblVLocation setHidden:YES];
            
            if (btnListView.selected)
                strListSearch = @"";
            else
                strLocationMapSearch = @"";
            
            [self clearDataForLocation];
            [self locationAPIAccordingToLatLong];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [appDelegate resignKeyboard];
    
    if (![textField.text isBlankValidationPassed])
    {
        [CustomAlertView iOSAlert:@"" withMessage:@"Please enter a keyword." onView:self];
        return NO;
    }
    else if (btnAddress.selected)
    {
        strAddressBookSearch = textField.text;
        [CUserDefaults removeObjectForKey:UserDefaultAddressBook];
        [self clearDataForAddressBook];
        [self addressBookAPIAccordingToLatLong];
    }
    else if (btnListView.selected)
    {
        if ([AFNetworkReachabilityManager sharedManager].reachable)
        {
            NSLog(@"AFNetworkReachabilityManager reachable");
            [self clearDataForLocation];
            [tblAddressBook reloadData];
            isFirstTimeLocation = NO;
            strListSearch = textField.text;
            [CUserDefaults removeObjectForKey:UserDefaultLocationSearch];
            [CUserDefaults removeObjectForKey:UserDefaultMapSearch];
            [self locationAPIAccordingToLatLong];
        }
        else
        {
            strListSearch = textField.text;
            [self clearDataForLocation];
            [self locationAPIAccordingToLatLong];
        }
    }
    
    return YES;
}





# pragma mark
# pragma mark - Map view configuration

- (void)mapViewConfiguration
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.015;
    span.longitudeDelta = 0.015;
    
    CLLocationCoordinate2D location;
    
    if (selectedLocationLatitude && selectedLocationLongitude)
    {
        location = CLLocationCoordinate2DMake(selectedLocationLatitude, selectedLocationLongitude);
    }
    else if ((![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]  == kCLAuthorizationStatusDenied))
    {
        location = CLLocationCoordinate2DMake(CDefaultLatitude, CDefaultLongitude);
    }
    else
    {
        location = CLLocationCoordinate2DMake(appDelegate.current_lat, appDelegate.current_long);
    }
    
    region.center = location;
    region.span =  span;
    
    [mapViewHome setRegion:region animated:YES];
    [self mapViewConfigurationWithIndex:0];
}

-(void)mapViewConfigurationWithIndex:(NSInteger)index
{
    for (; index < arrLocationSearch.count; index++)
    {
        NSDictionary *dictMapview = arrLocationSearch [index];
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([dictMapview doubleForKey:@"latitude"], [dictMapview doubleForKey:@"longitude"]);
        
        MKCustomAnnotation *pinAnnotation = [[MKCustomAnnotation alloc] initWithCoordinate:coordinate];
        [pinAnnotation setObject:[NSIndexPath indexPathForItem:index inSection:0] forKey:@"tag"];

        if (![mapViewHome.annotations containsObject:pinAnnotation])
            [mapViewHome addAnnotation:pinAnnotation];
    }
}





# pragma mark
# pragma mark - MKMapView Delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation) return nil; //..... Hide current location pin
    
    if ([annotation isKindOfClass:[MKCustomAnnotation class]])
    {
        static NSString *identifier = @"annotationView";
        
        MKAnnotationView *annotationView;
        
        if (annotationView == nil)
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        
        MKCustomAnnotation *annnotation = annotationView.annotation;
        
        NSIndexPath *indexPath = [annnotation objectForKey:@"tag"];
        
        if (arrLocationSearch.count > 0)
        {
            NSDictionary *dictMapView = arrLocationSearch [indexPath.row];
            
            NSInteger status = [dictMapView intForKey:@"availability_status"];
            
            switch (status)
            {
                case 1:
                    annotationView.image = [UIImage imageNamed:@"available_pin"];
                    break;
                    
                case 2:
                    annotationView.image = [UIImage imageNamed:@"mayBeAvailable_pin"];
                    break;
                    
                case 3:
                    annotationView.image = [UIImage imageNamed:@"unavailable_pin"];
                    break;
                    
                case 4:
                    annotationView.image = [UIImage imageNamed:@"unKnown_pin"];
                    break;
            }
        }
        else
        {
            [mapView removeAnnotations:mapView.annotations];
        }
        
        return annotationView;
    }
    else if ([annotation isKindOfClass:[CustomMapAnnotation class]])
    {
        CustomAnnotationView *annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomAnnotationView"];
        
        NSIndexPath *indexPath = [customAnnotation objectForKey:@"tag"];
        
        NSDictionary *dictLocationSearch;
        
        if (arrLocationSearch.count > 0)
        {
            dictLocationSearch = arrLocationSearch [indexPath.row];
            
            annotationView.lblTitle.text = [dictLocationSearch stringValueForJSON:@"doctor_name"];
            [annotationView.btnFavourite setSelected:[dictLocationSearch booleanForKey:@"favourite_status"]];
            annotationView.lblEmployerType.text = [dictLocationSearch stringValueForJSON:@"employer_name"];
            annotationView.lblStatusDescription.text = [[dictLocationSearch stringValueForJSON:@"availability_message"] isBlankValidationPassed] ? [dictLocationSearch stringValueForJSON:@"availability_message"] : @"N/A";
            [annotationView configureStatus:[dictLocationSearch intForKey:@"availability_status"]];
            annotationView.lblStatusUpdateTime.text = ([dictLocationSearch intForKey:@"availability_status"] == 4)?@"":[NSDate timeAgoFromTimestamp:[dictLocationSearch stringValueForJSON:@"updated_status_timestamp"]];
            
//            [appDelegate trackTheEventWithName:CEventLocationMarkerClicked properties:@{@"doctor_id":[arrSelectedDoctors valueForKeyPath:@"doctor_id"]}];
        }
        else
        {
            [mapView removeAnnotations:mapView.annotations];
        }
        
        //..... Click Events
        
        [annotationView.btnSelf touchUpInsideClicked:^{
           
            if ([AFNetworkReachabilityManager sharedManager].reachable)
            {
                MIDoctorDetailViewController *doctorDetails = [[MIDoctorDetailViewController alloc] initWithNibName:@"MIDoctorDetailViewController" bundle:nil];
                doctorDetails.dictDoctor = dictLocationSearch;
                [self.navigationController pushViewController:doctorDetails animated:YES];
            }
           
        }];
        
        
        [annotationView.btnFavourite touchUpInsideClicked:^{
            
//            int status = [[arrLocationSearch [indexPath.row] numberForJson:@"favourite_status"] isEqual:@1] ? 0 : 1;
//
//            if (status == 1)
//                [appDelegate trackTheEventWithName:CEventDoctorUnfavouriteClicked properties:@{@"doctor_id":[dictLocationSearch stringValueForJSON:@"doctor_id"]}];
//            else
//                [appDelegate trackTheEventWithName:CEventDoctorFavouriteClicked properties:@{@"doctor_id":[dictLocationSearch stringValueForJSON:@"doctor_id"]}];
            
            
            if ([AFNetworkReachabilityManager sharedManager].reachable)
            {
                if (sessionTaskFavourite &&  sessionTaskFavourite.state == NSURLSessionTaskStateRunning)
                    [sessionTaskFavourite cancel];
                
                int status = [[arrLocationSearch [indexPath.row] numberForJson:@"favourite_status"] isEqual:@1] ? 0 : 1;
                
                annotationView.btnFavourite.selected = !annotationView.btnFavourite.selected;
                
                NSMutableDictionary *dictReplace = [arrLocationSearch [indexPath.row] mutableCopy];
                
                if ([dictReplace intForKey:@"favourite_status"] == 1)
                    [dictReplace setValue:@0 forKey:@"favourite_status"];
                else
                    [dictReplace setValue:@1 forKey:@"favourite_status"];
                
                [arrLocationSearch replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                
                [annotationView.btnFavourite setSelected:[arrLocationSearch [indexPath.row] booleanForKey:@"favourite_status"]];
                
                if (dictLocationSearch)
                {
                    sessionTaskFavourite = [[APIRequest request] favouriteUnFavouriteDoctorWithStatus:status andDoctorID:[dictLocationSearch stringValueForJSON:@"doctor_id"] completed:nil];
                }
            }
        }];
        
        
        CGRect frame = annotationView.frame;
        annotationView.frame = frame;
        annotationView.btnSelf.frame = frame;
        annotationView.centerOffset = CGPointMake(0, - 58);
        
//        [annotationView bringSubviewToFront:annotationView];
        return annotationView;
    }

    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)annotationView
{
    if ([annotationView.annotation isKindOfClass:[MKCustomAnnotation class]])
    {
        if (customAnnotation == nil)
        {
            customAnnotation = [[CustomMapAnnotation alloc] initWithLatitude:annotationView.annotation.coordinate.latitude andLongitude:annotationView.annotation.coordinate.longitude];
        }
        else
        {
            customAnnotation.latitude = annotationView.annotation.coordinate.latitude;
            customAnnotation.longitude = annotationView.annotation.coordinate.longitude;
        }
        
        [customAnnotation setObject:[((MKCustomAnnotation *)annotationView.annotation) objectForKey:@"tag"] forKey:@"tag"];
        
        [mapView addAnnotation:customAnnotation];
        [mapView setCenterCoordinate:customAnnotation.coordinate animated:YES];
        [annotationView.superview bringSubviewToFront:annotationView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)annotationView
{
    if (customAnnotation && ![annotationView isKindOfClass:[CustomAnnotationView class]])
    {
        if (customAnnotation.coordinate.latitude == annotationView.annotation.coordinate.latitude&&
            customAnnotation.coordinate.longitude == annotationView.annotation.coordinate.longitude)
        {
            [mapView removeAnnotation:customAnnotation];
            customAnnotation = nil;
        }
    }
}




# pragma mark
# pragma mark - UITableview Delegate And Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableView isEqual:tblVLocation] ? arrAutoSuggesion.count : btnListView.selected ? arrLocationSearch.count : arrAddressBook.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView isEqual:tblVLocation] ? 44 : btnAddress.selected ? 65 : 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblVLocation])
    {
        static NSString *simpleTableIdentifier = @"UITableViewCell";
        UITableViewCell *cell = (UITableViewCell *)[tblVLocation dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.text = [arrAutoSuggesion objectAtIndex:indexPath.row];
        cell.textLabel.font = CFontAvenirLTStd55Roman(14);
        cell.textLabel.textColor = [UIColor darkGrayColor];
        
        return cell;
    }
    else
    {
        if (btnListView.selected)
        {
            static NSString *identifier = @"MILocationListTableViewCell";
            MILocationListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            NSDictionary *dictLocationSearch = arrLocationSearch [indexPath.row];
            
            if (self.editing)
            {
                if (![[dictLocationSearch stringValueForJSON:@"mobile_number"] isBlankValidationPassed])
                    [self setDisableCell:cell];
                else
                    [self setEnableCell:cell];
            }
            else
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.contentView.alpha = 1;
            }
            
            [cell.imgVFavourite setHidden:[[dictLocationSearch numberForJson:@"favourite_status"] isEqual:@1] ? NO : YES];
            
            [cell.lblTitle setText:[dictLocationSearch stringValueForJSON:@"doctor_name"]];
            [cell.btnFavourite setSelected:[dictLocationSearch booleanForKey:@"favourite_status"]];
            [cell configureStatus:[dictLocationSearch integerForKey:@"availability_status"]];
            [cell.lblDistance setText:[NSString stringWithFormat:@"%@ mi", [dictLocationSearch stringValueForJSON:@"distance"]]];
            
            [cell.lblStatusUpdatedTime setText:([dictLocationSearch intForKey:@"availability_status"] == 4)?@"":([[dictLocationSearch stringValueForJSON:@"updated_status_timestamp"] isBlankValidationPassed] ? [NSDate timeAgoFromTimestamp:[dictLocationSearch stringValueForJSON:@"updated_status_timestamp"]] : CNotAvailable)];
            
            
            //..... Click Events
            
            [cell.btnFavourite touchUpInsideClicked:^{
                
//                int status = [[arrLocationSearch [indexPath.row] numberForJson:@"favourite_status"] isEqual:@1] ? 0 : 1;
//
//                if (status == 1)
//                    [appDelegate trackTheEventWithName:CEventDoctorUnfavouriteClicked properties:@{@"doctor_id":[dictLocationSearch stringValueForJSON:@"doctor_id"]}];
//                else
//                    [appDelegate trackTheEventWithName:CEventDoctorFavouriteClicked properties:@{@"doctor_id":[dictLocationSearch stringValueForJSON:@"doctor_id"]}];
                
                [UIAlertController alertControllerWithFourButtonsWithStyleWithCancelAction:UIAlertControllerStyleActionSheet title:@"" message:[dictLocationSearch stringValueForJSON:@"doctor_name"] firstButton:[dictLocationSearch booleanForKey:@"favourite_status"] == 0 ? CMarkAsFavourite : CMarkAsUnFavourite firstHandler:^(UIAlertAction *action)
                 {
                     if ([AFNetworkReachabilityManager sharedManager].reachable)
                     {
                         if (sessionTaskFavourite &&  sessionTaskFavourite.state == NSURLSessionTaskStateRunning)
                             [sessionTaskFavourite cancel];
                         
                         int status = [[dictLocationSearch numberForJson:@"favourite_status"] isEqual:@1] ? 0 : 1;
                         
                         cell.btnFavourite.selected = !cell.btnFavourite.selected;
                         
                         NSMutableDictionary *dictReplace = [dictLocationSearch mutableCopy];
                         
                         if ([[dictReplace numberForJson:@"favourite_status"] isEqual:@1])  //..... Unfavourite
                             [dictReplace setValue:@0 forKey:@"favourite_status"];
                         else
                             [dictReplace setValue:@1 forKey:@"favourite_status"];
                         
                         [arrLocationSearch replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                         [cell.btnFavourite setSelected:[dictLocationSearch booleanForKey:@"favourite_status"]];
                         [tblAddressBook reloadData];
                         
                         sessionTaskFavourite = [[APIRequest request] favouriteUnFavouriteDoctorWithStatus:status andDoctorID:[dictLocationSearch stringValueForJSON:@"doctor_id"] completed:nil];
                     }
                     
                 } secondButton:CCallDoctor secondHandler:^(UIAlertAction *action)
                 {
                     [self callDoctor:dictLocationSearch];
                     
                 } thirdButton:CSMSDoctor thirdHandler:^(UIAlertAction *action)
                 {
                     [self smsDoctor:dictLocationSearch];
                     
                 } fourthButton:CEmailDoctor fourthHandler:^(UIAlertAction *action)
                 {
                     [self mailToDoctor:dictLocationSearch];
                     
                 } inView:self];
                
            }];
            
            
            //..... LOCATION SEARCH LOAD MORE DATA
            
            if ([AFNetworkReachabilityManager sharedManager].reachable)
            {
                if (indexPath.row == (arrLocationSearch.count - 1))
                    [self locationAPIAccordingToLatLong];
            }
            
            return cell;
        }
        else
        {
            static NSString *identifier = @"MIAddressBookTableViewCell";
            MIAddressBookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            
            NSDictionary *dictAddressBook = arrAddressBook[indexPath.row];
            
            if (self.editing)
            {
                if (![[dictAddressBook stringValueForJSON:@"mobile_number"] isBlankValidationPassed])
                    [self setDisableCell:cell];
                else
                    [self setEnableCell:cell];
            }
            else
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.contentView.alpha = 1;
            }
            
            [cell.imgVFavourite setHidden:[[dictAddressBook numberForJson:@"favourite_status"] isEqual:@1] ? NO : YES];
            
            [cell.lblTitle setText:[dictAddressBook stringValueForJSON:@"doctor_name"]];
            [cell.btnFavourite setSelected:[dictAddressBook booleanForKey:@"favourite_status"]];
            [cell configureStatus:[dictAddressBook integerForKey:@"availability_status"]];
            
            [cell.lblStatusUpdatedTime setText:([dictAddressBook intForKey:@"availability_status"] == 4)?@"":([[dictAddressBook stringValueForJSON:@"updated_status_timestamp"] isBlankValidationPassed] ? [NSDate timeAgoFromTimestamp:[dictAddressBook stringValueForJSON:@"updated_status_timestamp"]] : CNotAvailable)];
            
            
            //..... FAVOURITE / UNFAVOURITE
            
            [cell.btnFavourite touchUpInsideClicked:^{
                
//                int status = [[arrLocationSearch [indexPath.row] numberForJson:@"favourite_status"] isEqual:@1] ? 0 : 1;
//
//                if (status == 1)
//                    [appDelegate trackTheEventWithName:CEventDoctorUnfavouriteClicked properties:@{@"doctor_id":[dictAddressBook stringValueForJSON:@"doctor_id"]}];
//                else
//                    [appDelegate trackTheEventWithName:CEventDoctorFavouriteClicked properties:@{@"doctor_id":[dictAddressBook stringValueForJSON:@"doctor_id"]}];
                
                [UIAlertController alertControllerWithFourButtonsWithStyleWithCancelAction:UIAlertControllerStyleActionSheet title:@"" message:[dictAddressBook stringValueForJSON:@"doctor_name"] firstButton:[dictAddressBook booleanForKey:@"favourite_status"] == 0 ? CMarkAsFavourite : CMarkAsUnFavourite firstHandler:^(UIAlertAction *action)
                 {
                     if ([AFNetworkReachabilityManager sharedManager].reachable)
                     {
                         if (sessionTaskFavourite &&  sessionTaskFavourite.state == NSURLSessionTaskStateRunning)
                             [sessionTaskFavourite cancel];
                         
                         int status = [[dictAddressBook numberForJson:@"favourite_status"] isEqual:@1] ? 0 : 1;
                         
                         cell.btnFavourite.selected = !cell.btnFavourite.selected;
                         
                         NSMutableDictionary *dictReplace = [dictAddressBook mutableCopy];
                         
                         if ([[dictReplace numberForJson:@"favourite_status"] isEqual:@1])
                         {
                             //..... Unfavourite
                             [dictReplace setValue:@0 forKey:@"favourite_status"];
                         }
                         else
                         {
                             //..... Favourite
                             [dictReplace setValue:@1 forKey:@"favourite_status"];
                         }
                         
                         [arrAddressBook replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                         
                         [CUserDefaults setValue:arrAddressBook forKey:UserDefaultAddressBook];
                         [CUserDefaults synchronize];
                         
                         [cell.btnFavourite setSelected:[dictAddressBook booleanForKey:@"favourite_status"]];
                         [tblAddressBook reloadData];
                         
                         sessionTaskFavourite = [[APIRequest request] favouriteUnFavouriteDoctorWithStatus:status andDoctorID:[dictAddressBook stringValueForJSON:@"doctor_id"] completed:nil];
                     }
                     
                 } secondButton:CCallDoctor secondHandler:^(UIAlertAction *action)
                 {
                     [self callDoctor:dictAddressBook];
                     
                 } thirdButton:CSMSDoctor thirdHandler:^(UIAlertAction *action)
                 {
                     [self smsDoctor:dictAddressBook];
                     
                 } fourthButton:CEmailDoctor fourthHandler:^(UIAlertAction *action)
                 {
                     [self mailToDoctor:dictAddressBook];
                     
                 } inView:self];
                
            }];
            
    
            //..... ADDRESS BOOK LOAD MORE DATA
        
            if ([AFNetworkReachabilityManager sharedManager].reachable)
            {
                if (indexPath.row == (arrAddressBook.count - 1))
                    [self addressBookAPIAccordingToLatLong];
            }
            
            return cell;
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [appDelegate resignKeyboard];
    
    if ([tableView isEqual:tblVLocation])
    {
        txtSearchBar.text = strLocationMapSearch = arrAutoSuggesion [indexPath.row];
        tblVLocation.hidden = YES;
        
        [CUserDefaults setValue:strLocationMapSearch forKey:UserDefaultMapSearch];
        [CUserDefaults synchronize];
        
        
        CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
        
        [geoCoder geocodeAddressString:arrAutoSuggesion [indexPath.row] completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if(!error)
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 NSLog(@"%f",placemark.location.coordinate.latitude);
                 NSLog(@"%f",placemark.location.coordinate.longitude);
                 NSLog(@"%@",[NSString stringWithFormat:@"%@",[placemark description]]);
                 
                 CLLocationCoordinate2D location = CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
                 
                 [mapViewHome setCenterCoordinate:location animated:YES];
                 [self clearDataForLocation];
                 
                 selectedLocationLatitude = location.latitude;
                 selectedLocationLongitude = location.longitude;
                 [self locationAPIAccordingToLatLong];
             }
             else
             {
                 NSLog(@"There was a forward geocoding error\n%@",[error localizedDescription]);
             }
         }
         ];
        
    }
    else
    {
        if (self.editing)
        {
            NSDictionary *dictEditing;
            
            if (btnListView.selected)
            {
                dictEditing = arrLocationSearch [indexPath.row];
                
                if (![[dictEditing stringValueForJSON:@"mobile_number"] isBlankValidationPassed])
                    return;
            }
            else
            {
                dictEditing = arrAddressBook [indexPath.row];
                
                if (![[dictEditing stringValueForJSON:@"mobile_number"] isBlankValidationPassed])
                    return;
            }
            
            [arrSelectedDoctors addObject:@{@"mobile_number":[dictEditing stringValueForJSON:@"mobile_number"], @"doctor_id":[dictEditing stringValueForJSON:@"doctor_id"]}];
        }
        else
        {
            if ([AFNetworkReachabilityManager sharedManager].reachable)
            {
                MIDoctorDetailViewController *objDoctorDetailsVC = [[MIDoctorDetailViewController alloc] initWithNibName:@"MIDoctorDetailViewController" bundle:nil];
                objDoctorDetailsVC.dictDoctor = btnListView.selected ? arrLocationSearch [indexPath.row] : arrAddressBook [indexPath.row];
                [self.navigationController pushViewController:objDoctorDetailsVC animated:YES];
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblAddressBook])
    {
        if (self.editing)
        {
            NSDictionary *dictEditing;
            
            if (btnListView.selected)
            {
                dictEditing = arrLocationSearch [indexPath.row];
                
                if (![[dictEditing stringValueForJSON:@"mobile_number"] isBlankValidationPassed])
                    return;
            }
            else
            {
                dictEditing = arrAddressBook [indexPath.row];
                
                if (![[dictEditing stringValueForJSON:@"mobile_number"] isBlankValidationPassed])
                    return;
            }
            
//            [arrSelectedDoctors removeObject:[dictEditing stringValueForJSON:@"mobile_number"]];
            
            [arrSelectedDoctors removeObject:@{@"mobile_number":[dictEditing stringValueForJSON:@"mobile_number"], @"doctor_id":[dictEditing stringValueForJSON:@"doctor_id"]}];
            
            NSLog(@"%@", arrSelectedDoctors);
        }
    }
}





# pragma mark
# pragma mark - Enable Disable Cell For Group SMS

- (void)setEnableCell:(UITableViewCell *)cell
{
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.contentView.alpha = 1;
}

- (void)setDisableCell:(UITableViewCell *)cell
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.alpha = 0.3;
}





# pragma mark
# pragma mark - CALL || SMS || EMAIL

- (void)callDoctor:(NSDictionary *)dictDoctor
{
    if ([[dictDoctor stringValueForJSON:@"mobile_number"] isBlankValidationPassed] && [[dictDoctor stringValueForJSON:@"landline_number"] isBlankValidationPassed])
    {
        // When Both Numbers Available
        
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleActionSheet title:@"" message:CChooseOption firstButton:CCallMobile firstHandler:^(UIAlertAction *action)
         {
             if ([[dictDoctor stringValueForJSON:@"doctor_id"] isBlankValidationPassed])
             {
                 [appDelegate contactDoctor:[dictDoctor stringValueForJSON:@"doctor_id"] andCotactType:CContactMobile];
//                 [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":[dictDoctor stringValueForJSON:@"doctor_id"], @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
             }
             [appDelegate callMobileNo:[dictDoctor stringValueForJSON:@"mobile_number"]];
             
         } secondButton:CCallLandline secondHandler:^(UIAlertAction *action)
         {
             if ([[dictDoctor stringValueForJSON:@"doctor_id"] isBlankValidationPassed])
             {
                 [appDelegate contactDoctor:[dictDoctor stringValueForJSON:@"doctor_id"] andCotactType:CContactLandline];
//                 [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":[dictDoctor stringValueForJSON:@"doctor_id"], @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
             }
             [appDelegate callMobileNo:[dictDoctor stringValueForJSON:@"landline_number"]];
             
         } inView:self];
    }
    else if ([[dictDoctor stringValueForJSON:@"mobile_number"] isBlankValidationPassed] && ![[dictDoctor stringValueForJSON:@"landline_number"] isBlankValidationPassed])
    {
        // When Only Mobile Number Available
        
        if ([[dictDoctor stringValueForJSON:@"doctor_id"] isBlankValidationPassed])
        {
            [appDelegate contactDoctor:[dictDoctor stringValueForJSON:@"doctor_id"] andCotactType:CContactMobile];
//            [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":[dictDoctor stringValueForJSON:@"doctor_id"], @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
        }
        [appDelegate callMobileNo:[dictDoctor stringValueForJSON:@"mobile_number"]];
    }
    else if (![[dictDoctor stringValueForJSON:@"mobile_number"] isBlankValidationPassed] && [[dictDoctor stringValueForJSON:@"landline_number"] isBlankValidationPassed])
    {
        // When Only Landline Number Available
        
        if ([[dictDoctor stringValueForJSON:@"doctor_id"] isBlankValidationPassed])
        {
            [appDelegate contactDoctor:[dictDoctor stringValueForJSON:@"doctor_id"] andCotactType:CContactLandline];
//            [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":[dictDoctor stringValueForJSON:@"doctor_id"], @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
        }
        [appDelegate callMobileNo:[dictDoctor stringValueForJSON:@"landline_number"]];
    }
    else if (![[dictDoctor stringValueForJSON:@"mobile_number"] isBlankValidationPassed] && ![[dictDoctor stringValueForJSON:@"landline_number"] isBlankValidationPassed])
    {
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankContactNumber onView:self];
    }
}

- (void)smsDoctor:(NSDictionary *)dictDoctor
{
    if (![[dictDoctor stringValueForJSON:@"mobile_number"] isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankMobileNumber onView:self];
    else
    {
        dispatch_async(GCDMainThread, ^{
            
            if ([MFMessageComposeViewController canSendText])
            {
                if ([[dictDoctor stringValueForJSON:@"doctor_id"] isBlankValidationPassed])
                {
                    [appDelegate contactDoctor:[dictDoctor stringValueForJSON:@"doctor_id"] andCotactType:CContactSms];
//                    [appDelegate trackTheEventWithName:CEventDoctorSMSButtonClicked properties:@{@"doctor_id":[dictDoctor stringValueForJSON:@"doctor_id"], @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
                }
                
                MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init]; // Create message VC
                messageController.messageComposeDelegate = self; // Set delegate to current instance
                
                NSMutableArray *recipients = [[NSMutableArray alloc] init]; // Create an array to hold the recipients
                [recipients addObject:[dictDoctor stringValueForJSON:@"mobile_number"]]; // Append example phone number to array
                messageController.recipients = recipients; // Set the recipients of the message to the created array
                
                messageController.body = [NSString stringWithFormat:@"Dear Dr %@\n\nI am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]. Are you available to attend? If so, please reply to this email or phone [number] for more information.\n\nKind regards,\n\n[Name]", [dictDoctor stringValueForJSON:@"doctor_name"]];
                
                [self presentViewController:messageController animated:YES completion:NULL];
                
            }
            else
                [CustomAlertView iOSAlert:@"" withMessage:CMessageSMSNotSupport onView:self];
        });
    }
}

- (void)mailToDoctor:(NSDictionary *)dictDoctor
{
    dispatch_async(GCDMainThread, ^{
        
        if ([MFMailComposeViewController canSendMail])
        {
            if ([[dictDoctor stringValueForJSON:@"doctor_id"] isBlankValidationPassed])
            {
                [appDelegate contactDoctor:[dictDoctor stringValueForJSON:@"doctor_id"] andCotactType:CContactEmail];
//                [appDelegate trackTheEventWithName:CEventDoctorEmailButtonClicked properties:@{@"doctor_id":[dictDoctor stringValueForJSON:@"doctor_id"], @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
            }
            
            MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setSubject:CDoctorSmsSubject];
            [controller setMessageBody:[NSString stringWithFormat:@"Dear Dr %@\n\nI am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]. Are you available to attend? If so, please reply to this email or phone [number] for more information.\n\nKind regards,\n\n[Name]", [dictDoctor stringValueForJSON:@"doctor_name"]] isHTML:NO];
            
            [controller setToRecipients:[NSArray arrayWithObjects:[dictDoctor stringValueForJSON:@"email"],nil]];
            
            [self presentViewController:controller animated:YES completion:NULL];
        }
        else
            [CustomAlertView iOSAlert:@"" withMessage:CMessageMailNotSupport onView:self];
        
    });
}





# pragma mark
# pragma mark - MFMailComposeViewController Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}





# pragma mark
# pragma mark - MFMessageComposeViewController Delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [arrSelectedDoctors removeAllObjects];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
