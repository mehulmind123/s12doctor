//
//  MIDoctorDetailViewController.h
//  AMHP
//
//  Created by S12 Solutions on 7/27/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"

@interface MIDoctorDetailViewController : ParentViewController<MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet MIGenericLabel *lblDoctorName;
    IBOutlet MIGenericLabel *lblCurrentAvailability;
    IBOutlet MIGenericLabel *lblStatusUpdatedTime;
    IBOutlet MIGenericLabel *lblCurrentStatus;
    IBOutlet MIGenericLabel *lblMobileNumber;
    IBOutlet MIGenericLabel *lblLandlineNumber;
    IBOutlet MIGenericLabel *lblEmail;
    IBOutlet MIGenericLabel *lblGeneralAvailability;
    IBOutlet MIGenericLabel *lblSpecialties;
    IBOutlet MIGenericLabel *lblLanguageSpoken;
    IBOutlet MIGenericLabel *lblGmcReferenceNumber;
    IBOutlet MIGenericLabel *lblGender;
    IBOutlet MIGenericLabel *lblEmployer;
    IBOutlet MIGenericLabel *lblOfficeBasePostCode;
    IBOutlet MIGenericLabel *lblOfficeBaseTeam;
    IBOutlet MIGenericLabel *lblLocalAuthorities;
    
    
    IBOutlet UIButton *btnAmMon;
    IBOutlet UIButton *btnAmTue;
    IBOutlet UIButton *btnAmWed;
    IBOutlet UIButton *btnAmThu;
    IBOutlet UIButton *btnAmFri;
    IBOutlet UIButton *btnAmSat;
    IBOutlet UIButton *btnAmSun;
    
    IBOutlet UIButton *btnLunchMon;
    IBOutlet UIButton *btnLunchTue;
    IBOutlet UIButton *btnLunchWed;
    IBOutlet UIButton *btnLunchThu;
    IBOutlet UIButton *btnLunchFri;
    IBOutlet UIButton *btnLunchSat;
    IBOutlet UIButton *btnLunchSun;
    
    IBOutlet UIButton *btnPmMon;
    IBOutlet UIButton *btnPmTue;
    IBOutlet UIButton *btnPmWed;
    IBOutlet UIButton *btnPmThu;
    IBOutlet UIButton *btnPmFri;
    IBOutlet UIButton *btnPmSat;
    IBOutlet UIButton *btnPmSun;
    
    IBOutlet UIButton *btnEveningMon;
    IBOutlet UIButton *btnEveningTue;
    IBOutlet UIButton *btnEveningWed;
    IBOutlet UIButton *btnEveningThu;
    IBOutlet UIButton *btnEveningFri;
    IBOutlet UIButton *btnEveningSat;
    IBOutlet UIButton *btnEveningSun;
    
    IBOutlet UIButton *btnNightMon;
    IBOutlet UIButton *btnNightTue;
    IBOutlet UIButton *btnNightWed;
    IBOutlet UIButton *btnNightThu;
    IBOutlet UIButton *btnNightFri;
    IBOutlet UIButton *btnNightSat;
    IBOutlet UIButton *btnNightSun;
}

@property (nonatomic, strong) NSDictionary *dictDoctor;

@end
