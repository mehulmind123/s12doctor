//
//  MIDoctorDetailViewController.m
//  AMHP
//
//  Created by S12 Solutions on 7/27/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIDoctorDetailViewController.h"

@interface MIDoctorDetailViewController ()
{
    NSString *strMobileNumber;
    NSString *strLandlineNumber;
    NSString *strDoctorID;
}
@end

@implementation MIDoctorDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Doctor Details";
    
//    if (_dictDoctor)    //..... Event Doctor Details Loaded
//    {
//        [appDelegate trackTheEventWithName:CEventDoctorDetailsLoaded properties:@{@"doctor_id":[_dictDoctor stringValueForJSON:@"doctor_id"]}];
//    }
    
    
    //..... API CALL
    [self loadDoctorDetailsFromServer];
}





# pragma mark
# pragma mark - API Methods

- (void)loadDoctorDetailsFromServer
{
    if (_dictDoctor)
    {
        [self startLoadingAnimationInView:self.view];
        
        [[APIRequest request] doctorDetailWithDoctorID:[_dictDoctor stringValueForJSON:@"doctor_id"] completed:^(id responseObject, NSError *error)
        {
            [self stopLoadingAnimationInView:self.view];
            
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                [self setDataFromServer:[responseObject valueForKey:CJsonData]];
        }];
    }
}

- (void)setDataFromServer:(NSDictionary *)dictData
{
    if ([[dictData allKeys] count] > 0)
    {
        lblDoctorName.text          = [[dictData stringValueForJSON:@"doctor_name"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"doctor_name"] : CNotAvailable;
        
        strDoctorID                 = [dictData stringValueForJSON:@"doctor_id"];
        
        lblCurrentStatus.text       = [[dictData stringValueForJSON:@"availability_message"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"availability_message"] : CNotAvailable;
        
        lblStatusUpdatedTime.text   = [[dictData stringValueForJSON:@"updated_status_timestamp"] isBlankValidationPassed] ? [NSDate timeAgoFromTimestamp:[dictData stringValueForJSON:@"updated_status_timestamp"]] : @"";
        
        
        switch ([dictData intForKey:@"availability_status"])        //..... AVAILABILITY STATUS
        {
            case 1: // Available
            {
                lblCurrentAvailability.text = @"Available";
                lblCurrentAvailability.textColor = ColorAvaiable_00d800;
                break;
            }
            case 2: // May be available
            {
                lblCurrentAvailability.text = @"May be available";
                lblCurrentAvailability.textColor = ColorMayBeAvaialble_ffba00;
                break;
            }
            case 3: // Unavailable
            {
                lblCurrentAvailability.text = @"Unavailable";
                lblCurrentAvailability.textColor = ColorUnavailable_ffba00;
                break;
            }
            case 4: // Unknown
            {
                lblCurrentAvailability.text = @"Unknown";
                lblCurrentAvailability.textColor = ColorUnknown_00d800;
                lblStatusUpdatedTime.text = @"";
                break;
            }
                
            default:
            {
                lblCurrentAvailability.text = CNotAvailable;
                lblCurrentAvailability.textColor = ColorGreen_47C0B6;
                break;
            }
        }
        
        lblMobileNumber.text        = [[dictData stringValueForJSON:@"mobile_number"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"mobile_number"] : CNotAvailable;
        lblLandlineNumber.text      = [[dictData stringValueForJSON:@"landline_number"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"landline_number"] : CNotAvailable;
        
        strMobileNumber = [[dictData stringValueForJSON:@"mobile_number"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"mobile_number"] : @"";
        
        strLandlineNumber      = [[dictData stringValueForJSON:@"landline_number"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"landline_number"] : @"";
        
        lblEmail.text               = [[dictData stringValueForJSON:@"email"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"email"] : CNotAvailable;
        lblGeneralAvailability.text = [[dictData stringValueForJSON:@"general_availability"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"general_availability"] : CNotAvailable;
        
        NSArray *arrSpeicalties     = [dictData valueForKey:@"specialties"];
        lblSpecialties.text         = [arrSpeicalties count] > 0 ?  [[arrSpeicalties valueForKeyPath:@"specialties_name"] componentsJoinedByString:@", "] : CNotAvailable;
        
        NSArray *arrLanguage        = [dictData valueForKey:@"languages_spoken"];
        lblLanguageSpoken.text      = [arrLanguage count] > 0 ? [[arrLanguage valueForKeyPath:@"language_name"] componentsJoinedByString:@", "] : CNotAvailable;
        
        NSArray *arrLocalAuthority  = [dictData valueForKey:@"local_authorities"];
        lblLocalAuthorities.text    = [arrLocalAuthority count] > 0 ? [[arrLocalAuthority valueForKeyPath:@"authority_name"] componentsJoinedByString:@", "] : CNotAvailable;
        
        NSArray *arrEmployer        = [dictData valueForKey:@"employer"];
        lblEmployer.text            = [arrEmployer count] > 0 ? [[arrEmployer valueForKeyPath:@"employer_name"] componentsJoinedByString:@", "] : CNotAvailable;
        
        lblGmcReferenceNumber.text  = [[dictData stringValueForJSON:@"gmc_referance_number"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"gmc_referance_number"] : CNotAvailable;
        
        switch ([dictData intForKey:@"gender"])
        {
            case 1:
                lblGender.text      = @"Prefer not to say";
            break;
            
            case 2:
                lblGender.text      = @"Male";
            break;
            
            case 3:
                lblGender.text      = @"Female";
            break;
        }
        
        lblOfficeBasePostCode.text  = [[dictData stringValueForJSON:@"office_base_postcode"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"office_base_postcode"] : CNotAvailable;
        lblOfficeBaseTeam.text      = [[dictData stringValueForJSON:@"office_base_team"] isBlankValidationPassed] ? [dictData stringValueForJSON:@"office_base_team"] : CNotAvailable;
        
        
        //..... Weekly Availability
        
        NSDictionary *dictStatus = [dictData valueForKey:@"weekly_availability_status"];
        
        NSString *strAM = [dictStatus valueForKey:@"am"];
        NSString *strLunch = [dictStatus valueForKey:@"lunch"];
        NSString *strPM = [dictStatus valueForKey:@"pm"];
        NSString *strEvening = [dictStatus valueForKey:@"evening"];
        NSString *strNight = [dictStatus valueForKey:@"night"];
        
        NSArray *arrAM = [strAM componentsSeparatedByString:@","];
        NSArray *arrLunch = [strLunch componentsSeparatedByString:@","];
        NSArray *arrPM = [strPM componentsSeparatedByString:@","];
        NSArray *arrEvening = [strEvening componentsSeparatedByString:@","];
        NSArray *arrNight = [strNight componentsSeparatedByString:@","];
        
        if ([strAM isBlankValidationPassed])
        {
            [btnAmMon setImage:[self setWeeklyAvailabilityStatus:[[arrAM objectAtIndex:0] intValue] andSender:btnAmMon] forState:UIControlStateNormal];
            [btnAmTue setImage:[self setWeeklyAvailabilityStatus:[[arrAM objectAtIndex:1] intValue] andSender:btnAmTue] forState:UIControlStateNormal];
            [btnAmWed setImage:[self setWeeklyAvailabilityStatus:[[arrAM objectAtIndex:2] intValue] andSender:btnAmWed] forState:UIControlStateNormal];
            [btnAmThu setImage:[self setWeeklyAvailabilityStatus:[[arrAM objectAtIndex:3] intValue] andSender:btnAmThu] forState:UIControlStateNormal];
            [btnAmFri setImage:[self setWeeklyAvailabilityStatus:[[arrAM objectAtIndex:4] intValue] andSender:btnAmFri] forState:UIControlStateNormal];
            [btnAmSat setImage:[self setWeeklyAvailabilityStatus:[[arrAM objectAtIndex:5] intValue] andSender:btnAmSat] forState:UIControlStateNormal];
            [btnAmSun setImage:[self setWeeklyAvailabilityStatus:[[arrAM objectAtIndex:6] intValue] andSender:btnAmSun] forState:UIControlStateNormal];
        }
        
        if ([strLunch isBlankValidationPassed])
        {
            [btnLunchMon setImage:[self setWeeklyAvailabilityStatus:[[arrLunch objectAtIndex:0] intValue] andSender:btnLunchMon] forState:UIControlStateNormal];
            [btnLunchTue setImage:[self setWeeklyAvailabilityStatus:[[arrLunch objectAtIndex:1] intValue] andSender:btnLunchTue] forState:UIControlStateNormal];
            [btnLunchWed setImage:[self setWeeklyAvailabilityStatus:[[arrLunch objectAtIndex:2] intValue] andSender:btnLunchWed] forState:UIControlStateNormal];
            [btnLunchThu setImage:[self setWeeklyAvailabilityStatus:[[arrLunch objectAtIndex:3] intValue] andSender:btnLunchThu] forState:UIControlStateNormal];
            [btnLunchFri setImage:[self setWeeklyAvailabilityStatus:[[arrLunch objectAtIndex:4] intValue] andSender:btnLunchFri] forState:UIControlStateNormal];
            [btnLunchSat setImage:[self setWeeklyAvailabilityStatus:[[arrLunch objectAtIndex:5] intValue] andSender:btnLunchSat] forState:UIControlStateNormal];
            [btnLunchSun setImage:[self setWeeklyAvailabilityStatus:[[arrLunch objectAtIndex:6] intValue] andSender:btnLunchSun] forState:UIControlStateNormal];
        }
        
        
        if ([strPM isBlankValidationPassed])
        {
            [btnPmMon setImage:[self setWeeklyAvailabilityStatus:[[arrPM objectAtIndex:0] intValue] andSender:btnPmMon] forState:UIControlStateNormal];
            [btnPmTue setImage:[self setWeeklyAvailabilityStatus:[[arrPM objectAtIndex:1] intValue] andSender:btnPmTue] forState:UIControlStateNormal];
            [btnPmWed setImage:[self setWeeklyAvailabilityStatus:[[arrPM objectAtIndex:2] intValue] andSender:btnPmWed] forState:UIControlStateNormal];
            [btnPmThu setImage:[self setWeeklyAvailabilityStatus:[[arrPM objectAtIndex:3] intValue] andSender:btnPmThu] forState:UIControlStateNormal];
            [btnPmFri setImage:[self setWeeklyAvailabilityStatus:[[arrPM objectAtIndex:4] intValue] andSender:btnPmFri] forState:UIControlStateNormal];
            [btnPmSat setImage:[self setWeeklyAvailabilityStatus:[[arrPM objectAtIndex:5] intValue] andSender:btnPmSat] forState:UIControlStateNormal];
            [btnPmSun setImage:[self setWeeklyAvailabilityStatus:[[arrPM objectAtIndex:6] intValue] andSender:btnPmSun] forState:UIControlStateNormal];
        }
        
        
        
        if ([strEvening isBlankValidationPassed])
        {
            [btnEveningMon setImage:[self setWeeklyAvailabilityStatus:[[arrEvening objectAtIndex:0] intValue] andSender:btnEveningMon] forState:UIControlStateNormal];
            [btnEveningTue setImage:[self setWeeklyAvailabilityStatus:[[arrEvening objectAtIndex:1] intValue] andSender:btnEveningTue] forState:UIControlStateNormal];
            [btnEveningWed setImage:[self setWeeklyAvailabilityStatus:[[arrEvening objectAtIndex:2] intValue] andSender:btnEveningWed] forState:UIControlStateNormal];
            [btnEveningThu setImage:[self setWeeklyAvailabilityStatus:[[arrEvening objectAtIndex:3] intValue] andSender:btnEveningThu] forState:UIControlStateNormal];
            [btnEveningFri setImage:[self setWeeklyAvailabilityStatus:[[arrEvening objectAtIndex:4] intValue] andSender:btnEveningFri] forState:UIControlStateNormal];
            [btnEveningSat setImage:[self setWeeklyAvailabilityStatus:[[arrEvening objectAtIndex:5] intValue] andSender:btnEveningSat] forState:UIControlStateNormal];
            [btnEveningSun setImage:[self setWeeklyAvailabilityStatus:[[arrEvening objectAtIndex:6] intValue] andSender:btnEveningSun] forState:UIControlStateNormal];
        }
        
        
        
        if ([strNight isBlankValidationPassed])
        {
            [btnNightMon setImage:[self setWeeklyAvailabilityStatus:[[arrNight objectAtIndex:0] intValue] andSender:btnNightMon] forState:UIControlStateNormal];
            [btnNightTue setImage:[self setWeeklyAvailabilityStatus:[[arrNight objectAtIndex:1] intValue] andSender:btnNightTue] forState:UIControlStateNormal];
            [btnNightWed setImage:[self setWeeklyAvailabilityStatus:[[arrNight objectAtIndex:2] intValue] andSender:btnNightWed] forState:UIControlStateNormal];
            [btnNightThu setImage:[self setWeeklyAvailabilityStatus:[[arrNight objectAtIndex:3] intValue] andSender:btnNightThu] forState:UIControlStateNormal];
            [btnNightFri setImage:[self setWeeklyAvailabilityStatus:[[arrNight objectAtIndex:4] intValue] andSender:btnNightFri] forState:UIControlStateNormal];
            [btnNightSat setImage:[self setWeeklyAvailabilityStatus:[[arrNight objectAtIndex:5] intValue] andSender:btnNightSat] forState:UIControlStateNormal];
            [btnNightSun setImage:[self setWeeklyAvailabilityStatus:[[arrNight objectAtIndex:6] intValue] andSender:btnNightSun] forState:UIControlStateNormal];
        }
        
        
    }
}

- (UIImage *)setWeeklyAvailabilityStatus:(int)status andSender:(UIButton *)sender
{
    UIImage *image;
    
    switch (status)
    {
        case 1:
        {
            image = [UIImage imageNamed:@"weekly_selected"];
        }
        break;
            
        default:
        {
            image = [UIImage imageNamed:@""];
            [sender setTitle:CNotAvailable forState:UIControlStateNormal];
        }
        break;
    }
    
    return image;
}






# pragma mark
# pragma mark - Action Event

-(IBAction)btnCallClicked:(UIButton*)sender
{
    dispatch_async(GCDMainThread, ^{
        
        if ([strMobileNumber isBlankValidationPassed] && [strLandlineNumber isBlankValidationPassed])
        {
            //..... WHEN BOTH NUMBER AVAILABLE
            
            [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleActionSheet title:@"" message:CChooseOption firstButton:CCallMobile firstHandler:^(UIAlertAction *action)
             {
                 if ([strDoctorID isBlankValidationPassed])
                 {
                     [appDelegate contactDoctor:strDoctorID andCotactType:CContactMobile];
//                     [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":strDoctorID, @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
                 }
                 [appDelegate callMobileNo:lblMobileNumber.text];
                 
             } secondButton:CCallLandline secondHandler:^(UIAlertAction *action)
             {
                 if ([strDoctorID isBlankValidationPassed])
                 {
                     [appDelegate contactDoctor:strDoctorID andCotactType:CContactLandline];
//                     [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":strDoctorID, @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
                 }
                 [appDelegate callMobileNo:lblLandlineNumber.text];
                 
             } inView:self];
        }
        else if ([strMobileNumber isBlankValidationPassed] && ![strLandlineNumber isBlankValidationPassed])
        {
            //..... WHEN ONLY MOBILE NUMBER AVAILABLE
            if ([strDoctorID isBlankValidationPassed])
            {
                [appDelegate contactDoctor:strDoctorID andCotactType:CContactMobile];
//                [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":strDoctorID, @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
            }
            [appDelegate callMobileNo:lblMobileNumber.text];
        }
        else if (![strMobileNumber isBlankValidationPassed] && [strLandlineNumber isBlankValidationPassed])
        {
            //..... WHEN ONLY LANDLINE NUMBER AVAILABLE
            
            if ([strDoctorID isBlankValidationPassed])
            {
                [appDelegate contactDoctor:strDoctorID andCotactType:CContactLandline];
//                [appDelegate trackTheEventWithName:CEventDoctorCallButtonClicked properties:@{@"doctor_id":strDoctorID, @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
            }
            [appDelegate callMobileNo:lblLandlineNumber.text];
        }
        else if (![strMobileNumber isBlankValidationPassed] && ![strLandlineNumber isBlankValidationPassed])
        {
            [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankContactNumber onView:self];
        }
        
    });
}

- (IBAction)btnSmsClicked:(UIButton*)sender
{
    if (![strMobileNumber isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankMobileNumber onView:self];
    else
    {
        dispatch_async(GCDMainThread, ^{
            
            if ([MFMessageComposeViewController canSendText])
            {
                if ([strDoctorID isBlankValidationPassed])
                {
                    [appDelegate contactDoctor:strDoctorID andCotactType:CContactSms];
//                    [appDelegate trackTheEventWithName:CEventDoctorSMSButtonClicked properties:@{@"doctor_id":strDoctorID, @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
                }
                
                MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init]; // Create message VC
                messageController.messageComposeDelegate = self; // Set delegate to current instance
                
                NSMutableArray *recipients = [[NSMutableArray alloc] init]; // Create an array to hold the recipients
                [recipients addObject:lblMobileNumber.text]; // Append example phone number to array
                messageController.recipients = recipients; // Set the recipients of the message to the created array
                
                messageController.body = [NSString stringWithFormat:@"Dear Dr %@\n\nI am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]. Are you available to attend? If so, please reply to this email or phone [number] for more information.\n\nKind regards,\n\n[Name]", lblDoctorName.text];
                
                [self presentViewController:messageController animated:YES completion:NULL];
                
            }
            else
                [CustomAlertView iOSAlert:@"" withMessage:CMessageSMSNotSupport onView:self];
        });
    }
}

- (IBAction)btnEmailClicked:(UIButton*)sender
{
    if (![lblEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankDoctorEmail onView:self];
    else
    {
        dispatch_async(GCDMainThread, ^{
            
            if ([MFMailComposeViewController canSendMail])
            {
                if ([strDoctorID isBlankValidationPassed])
                {
                    [appDelegate contactDoctor:strDoctorID andCotactType:CContactEmail];
//                    [appDelegate trackTheEventWithName:CEventDoctorEmailButtonClicked properties:@{@"doctor_id":strDoctorID, @"internet_available":[AFNetworkReachabilityManager sharedManager].reachable ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]}];
                }
                
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setSubject:CDoctorSmsSubject];
                [controller setMessageBody:[NSString stringWithFormat:@"Dear Dr %@\n\nI am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]. Are you available to attend? If so, please reply to this email or phone [number] for more information.\n\nKind regards,\n\n[Name]", lblDoctorName.text] isHTML:NO];
                [controller setToRecipients:[NSArray arrayWithObjects:lblEmail.text,nil]];
                [self presentViewController:controller animated:YES completion:NULL];
            }
            else
                [CustomAlertView iOSAlert:@"" withMessage:CMessageMailNotSupport onView:self];
        });
    }
}





# pragma mark
# pragma mark - MFMailComposeViewController Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}





# pragma mark
# pragma mark - MFMessageComposeViewController Delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
