//
//  MIFilterViewController.h
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"

typedef enum : NSUInteger
{
    MIFilterTypeNormal,
    MIFilterTypeMap,
} MIFilterType;

@interface MIFilterViewController : ParentViewController
{
    IBOutlet UITableView *tblFilter;
}

@property (nonatomic, assign) MIFilterType MIFilterType;
@property (nonatomic, strong) NSMutableArray *arrFilterLocalAuthority;
@property (nonatomic, strong) NSMutableArray *arrFilterSpecialties;
@property (nonatomic, strong) NSMutableArray *arrFilterLanguage;

@end
