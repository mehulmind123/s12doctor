//
//  MIFilterViewController.m
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIFilterViewController.h"
#import "MIFilterOptionTableViewCell.h"
#import "MIFilterSelectOptionTableViewCell.h"
#import "MIFilterSliderTableViewCell.h"

@interface MIFilterViewController ()
{
    NSMutableArray *arrFilter;
    
    NSMutableDictionary *dictFilter;
    NSMutableDictionary *dictFilterSelected;
}
@end

@implementation MIFilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [tblFilter performSelector:@selector(reloadData) withObject:nil afterDelay:0.1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Filter";
    
    dictFilter = [[NSMutableDictionary alloc] init];
    dictFilterSelected = [NSMutableDictionary new];
    
    if (self.iObject)
        [dictFilter addEntriesFromDictionary:self.iObject];
    
    
    
    //..... BAR BUTTON
    
    UIBarButtonItem *reset = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(btnResetClicked:)];

    UIBarButtonItem *apply = [[UIBarButtonItem alloc] initWithTitle:@"Apply" style:UIBarButtonItemStylePlain target:self action:@selector(btnApplyClicked:)];

    self.navigationItem.leftBarButtonItem = reset;
    self.navigationItem.rightBarButtonItem = apply;
    
    
    
    
    //..... REGISTER CELL
    
    [tblFilter registerNib:[UINib nibWithNibName:@"MIFilterOptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIFilterOptionTableViewCell"];
    
    [tblFilter registerNib:[UINib nibWithNibName:@"MIFilterSelectOptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIFilterSelectOptionTableViewCell"];
    
    [tblFilter registerNib:[UINib nibWithNibName:@"MIFilterSliderTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIFilterSliderTableViewCell"];
    
    [tblFilter setRowHeight:UITableViewAutomaticDimension];
    [tblFilter setEstimatedRowHeight:90];
    
    
    
    //..... SET DATA
    
    [self configureFilterData];
}

- (void)configureFilterData
{
    NSArray *arrAvailability =  @[@"Available",@"May be available",@"Unavailable",@"Unknown"];
    NSArray *arrWeekDays =      @[@"Mon",@"Tue",@"Wed",@"Thu",@"Fri",@"Sat",@"Sun"];
    NSArray *arrTimeSlot =      @[@"Morning",@"Lunch",@"Afternoon",@"Evening",@"Night"];
    NSArray *arrEmployer =      @[@"Trust",@"Independent"];
    NSArray *arrGender =        @[@"Female",@"Male"];
    
    if (self.MIFilterType == MIFilterTypeNormal)
    {
        arrFilter = @[@{@"title":@"Availability",
                        @"selected":[[dictFilter allKeys] containsObject:@"Availability"] ? [dictFilter stringValueForJSON:@"Availability"] : @"",
                        @"options":arrAvailability},
                      
                      @{@"title":CWeeklyAvailability,
                        @"selected":[[dictFilter allKeys] containsObject:CWeeklyAvailability] ? [dictFilter stringValueForJSON:CWeeklyAvailability] : @"",
                        @"options":arrWeekDays},
                      
                      @{@"title":CTimeSlot,
                      @"selected":[[dictFilter allKeys] containsObject:CTimeSlot] ? [dictFilter stringValueForJSON:CTimeSlot] : @"",
                      @"options":arrTimeSlot},
                      
                      @{@"title":@"Define the area you'd like to search",
                        @"selected":[[dictFilter allKeys] containsObject:@"Define the area you'd like to search"] ? [dictFilter stringValueForJSON:@"Define the area you'd like to search"] : @"",
                        @"options":[_arrFilterLocalAuthority valueForKeyPath:@"authority_name"]},
                      
                      @{@"title":@"Employer",   
                        @"selected":[[dictFilter allKeys] containsObject:@"Employer"] ? [dictFilter stringValueForJSON:@"Employer"] : @"",
                        @"options":arrEmployer},
                      
                      @{@"title":@"Specialty",
                        @"selected":[[dictFilter allKeys] containsObject:@"Specialty"] ? [dictFilter stringValueForJSON:@"Specialty"] : @"",
                        @"options":[_arrFilterSpecialties valueForKeyPath:@"specialties_name"]},
                      
                      @{@"title":@"Gender",
                        @"selected":[[dictFilter allKeys] containsObject:@"Gender"] ? [dictFilter stringValueForJSON:@"Gender"] : @"",
                        @"options":arrGender},
                      
                      @{@"title":@"Language Spoken",
                        @"selected":[[dictFilter allKeys] containsObject:@"Language Spoken"] ? [dictFilter stringValueForJSON:@"Language Spoken"] : @"",
                        @"options":[_arrFilterLanguage valueForKeyPath:@"language_name"]}].mutableCopy;
    }
    else
    {
        arrFilter = @[@{@"title":@"Availability",
                        @"selected":[[dictFilter allKeys] containsObject:@"Availability"]? [dictFilter stringValueForJSON:@"Availability"] : @"",
                        @"options":arrAvailability},
                      
                      @{@"title":CWeeklyAvailability,
                        @"selected":[[dictFilter allKeys] containsObject:CWeeklyAvailability] ? [dictFilter stringValueForJSON:CWeeklyAvailability] : @"",
                        @"options":arrWeekDays},
                      
                      @{@"title":CTimeSlot,
                        @"selected":[[dictFilter allKeys] containsObject:CTimeSlot] ? [dictFilter stringValueForJSON:CTimeSlot] : @"",
                        @"options":arrTimeSlot},
                      
                      @{@"title":@"Define the area you'd like to search",
                        @"selected":[[dictFilter allKeys] containsObject:@"Define the area you'd like to search"] ? [dictFilter stringValueForJSON:@"Define the area you'd like to search"] : @"",
                        @"options":[_arrFilterLocalAuthority valueForKeyPath:@"authority_name"]},
                      
                      @{@"title":@"Select Distance",
                        @"selected":[[dictFilter allKeys] containsObject:@"Select Distance"] ? [dictFilter stringValueForJSON:@"Select Distance"] : @"",
                        @"options":@""},
                      
                      @{@"title":@"Employer",
                        @"selected":[[dictFilter allKeys] containsObject:@"Employer"] ? [dictFilter stringValueForJSON:@"Employer"] : @"",
                        @"options":arrEmployer},
                      
                      @{@"title":@"Specialty",
                        @"selected":[[dictFilter allKeys] containsObject:@"Specialty"] ? [dictFilter stringValueForJSON:@"Specialty"] : @"",
                        @"options":[_arrFilterSpecialties valueForKeyPath:@"specialties_name"]},
                      
                      @{@"title":@"Gender",
                        @"selected":[[dictFilter allKeys] containsObject:@"Gender"] ? [dictFilter stringValueForJSON:@"Gender"] : @"",
                        @"options":arrGender},
                      
                      @{@"title":@"Language Spoken",
                        @"selected":[[dictFilter allKeys] containsObject:@"Language Spoken"] ? [dictFilter stringValueForJSON:@"Language Spoken"] : @"",
                        @"options":[_arrFilterLanguage valueForKeyPath:@"language_name"]}].mutableCopy;
    }
}





# pragma mark
# pragma mark - Action Event

- (void)btnResetClicked:(UIBarButtonItem*)sender
{
    [dictFilter removeAllObjects];
    
    [self configureFilterData];
    
    [tblFilter reloadData];
}

- (void)btnApplyClicked:(UIBarButtonItem*)sender
{
    if ([[dictFilter valueForKey:CWeeklyAvailability] isBlankValidationPassed])
    {
        if (![[dictFilter valueForKey:CTimeSlot] isBlankValidationPassed])
        {
            [CustomAlertView iOSAlert:@"" withMessage:CSelectTimeSlot onView:self];
            return;
        }
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
        if (self.block)
            self.block(dictFilter,nil);
    }];
}





# pragma mark
# pragma mark - UITableview Delegate And Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrFilter.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.MIFilterType == MIFilterTypeMap)   //..... MAP FILTER
    {
        if (indexPath.row == 4)
        {
            //..... SELECT DISTANCE
            
            static NSString *identifier = @"MIFilterSliderTableViewCell";
            MIFilterSliderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            [cell configureData:arrFilter[indexPath.row] handler:^(NSString *selectedOption)
             {
                 NSMutableDictionary *dictReplace = [arrFilter [indexPath.row] mutableCopy];
                 [dictReplace setValue:selectedOption forKey:@"selected"];
                 [arrFilter replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                 
                 [dictFilter setValue:selectedOption forKey:@"filter_distance"];
                 [dictFilter setValue:selectedOption forKey:[arrFilter[indexPath.row] stringValueForJSON:@"title"]];
                 NSLog(@"dict == %@", dictFilter);
             }];
            
            return cell;
            
        }
        else if((indexPath.row % 2 != 0 || indexPath.row == 0 || indexPath.row == 2) && indexPath.row != 3)
        {
            //..... SELECT AVAILABILITY & EMPLOYER & GENDER
            
            static NSString *identifier = @"MIFilterOptionTableViewCell";
            MIFilterOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            [cell.viewContain setConstraintConstant:indexPath.row == 1 ? 0 : 8 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
            
            if (indexPath.row == 2)
            {
                if ([[dictFilter stringValueForJSON:CWeeklyAvailability] isBlankValidationPassed])
                {
                    cell.collOption.userInteractionEnabled = YES;
                }
                else
                {
                    cell.collOption.userInteractionEnabled = NO;
                    [dictFilter removeObjectForKey:[arrFilter[indexPath.row] stringValueForJSON:@"title"]];
                    
                    NSMutableDictionary *dictReplace = [arrFilter [indexPath.row] mutableCopy];
                    [dictReplace setValue:@"" forKey:@"selected"];
                    [arrFilter replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                    
                    NSLog(@"selected data == %@", dictFilter);
                }
            }
            
            
            [cell configureData:arrFilter[indexPath.row] andDict:dictFilter handler:^(NSString *selectedOption)
             {
                 switch (indexPath.row)
                 {
                     case 0: break;
                         
                     case 1: break;
                         
                     case 2: break;
                         
                     case 5:
                     {
                         NSNumber *employerID = [selectedOption isEqualToString:@"Trust"] ? @1 : @2;
                         [dictFilter setValue:employerID forKey:@"filter_employer"];
                         break;
                     }
                         
                     case 7:
                     {
                         NSNumber *genderID = [selectedOption isEqualToString:@"Female"] ? @1 : @2;
                         [dictFilter setValue:genderID forKey:@"filter_gender"];
                         break;
                     }
                 }
                 
                 NSMutableDictionary *dictReplace = [arrFilter [indexPath.row] mutableCopy];
                 [dictReplace setValue:selectedOption forKey:@"selected"];
                 [arrFilter replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                 
                 [dictFilter setValue:selectedOption forKey:[arrFilter[indexPath.row] stringValueForJSON:@"title"]];
                 [tblFilter reloadData];
                 NSLog(@"dict == %@", dictFilter);
             }];
            
            return cell;
        }
        else
        {
            //..... SELECT LOCAL AUHTORITY SPECIALTIES & LANGUAGE
            
            static NSString *identifier = @"MIFilterSelectOptionTableViewCell";
            MIFilterSelectOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            [cell configureData:arrFilter[indexPath.row] handler:^(NSString *selectedOption)
             {
                 __strong NSArray *arrID;
                 
                 switch (indexPath.row)
                 {
                     case 3:
                     {
                         arrID = [_arrFilterLocalAuthority filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"authority_name == %@", selectedOption]];
                         NSNumber *localAuthID = [[arrID firstObject] valueForKey:@"authority_id"];
                         [dictFilter setValue:localAuthID forKey:@"filter_local_authority"];
                         break;
                     }
                         
                     case 6:
                     {
                         arrID = [_arrFilterSpecialties filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"specialties_name == %@", selectedOption]];
                         NSNumber *specialtyID = [[arrID firstObject] valueForKey:@"specialties_id"];
                         [dictFilter setValue:specialtyID forKey:@"filter_specialty"];
                         break;
                     }
                         
                     case 8:
                     {
                         arrID = [_arrFilterLanguage filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"language_name == %@", selectedOption]];
                         NSNumber *languageID = [[arrID firstObject] valueForKey:@"language_id"];
                         [dictFilter setValue:languageID forKey:@"filter_language"];
                         break;
                     }
                 }
                 
                 NSMutableDictionary *dictReplace = [arrFilter [indexPath.row] mutableCopy];
                 [dictReplace setValue:selectedOption forKey:@"selected"];
                 [arrFilter replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                 
                 [dictFilter setValue:selectedOption forKey:[arrFilter[indexPath.row] stringValueForJSON:@"title"]];
                 NSLog(@"dict == %@", dictFilter);
             }];
            
            return cell;
            
        }
    }
    else                                                        //..... ADDRESS BOOK FILTER
    {
        if(indexPath.row % 2 == 0 || indexPath.row == 1)
        {
            //..... SELECT AVAILABILITY & EMPLOYER & GENDER
            
            static NSString *identifier = @"MIFilterOptionTableViewCell";
            MIFilterOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            [cell.viewContain setConstraintConstant:indexPath.row == 1 ? 0 : 8 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
            
            if (indexPath.row == 2)
            {
                if ([[dictFilter stringValueForJSON:CWeeklyAvailability] isBlankValidationPassed])
                {
                    cell.collOption.userInteractionEnabled = YES;
                }
                else
                {
                    cell.collOption.userInteractionEnabled = NO;
                    [dictFilter removeObjectForKey:[arrFilter[indexPath.row] stringValueForJSON:@"title"]];
                    
                    NSMutableDictionary *dictReplace = [arrFilter [indexPath.row] mutableCopy];
                    [dictReplace setValue:@"" forKey:@"selected"];
                    [arrFilter replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                    
                    NSLog(@"selected data == %@", dictFilter);
                }
            }
            
            [cell configureData:arrFilter[indexPath.row] andDict:dictFilter handler:^(NSString *selectedOption)
             {
                 switch (indexPath.row)
                 {
                     case 0: break;
                         
                     case 1: break;
                         
                     case 2: break;
                         
                     case 4:
                     {
                         NSNumber *employerID = [selectedOption isEqualToString:@"Trust"] ? @1 : @2;
                         [dictFilter setValue:employerID forKey:@"filter_employer"];
                         break;
                     }
                    
                     case 6:
                     {
                         NSNumber *genderID = [selectedOption isEqualToString:@"Female"] ? @1 : @2;
                         [dictFilter setValue:genderID forKey:@"filter_gender"];
                         break;
                     }
                 }
                 
                 NSMutableDictionary *dictReplace = [arrFilter [indexPath.row] mutableCopy];
                 [dictReplace setValue:selectedOption forKey:@"selected"];
                 [arrFilter replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                 
                 [dictFilter setValue:selectedOption forKey:[arrFilter[indexPath.row] stringValueForJSON:@"title"]];
                 [tblFilter reloadData];
                 NSLog(@"dict == %@", dictFilter);
             }];
            
            return cell;
        }
        else
        {
            //..... SELECT LOCAL AUHTORITY & SPECIALTIES & LANGUAGE
            
            static NSString *identifier = @"MIFilterSelectOptionTableViewCell";
            MIFilterSelectOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
            [cell configureData:arrFilter[indexPath.row] handler:^(NSString *selectedOption)
             {
                 __strong NSArray *arrID;
                 
                 switch (indexPath.row)
                 {
                     case 3:
                     {
                         arrID = [_arrFilterLocalAuthority filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"authority_name == %@", selectedOption]];
                         NSNumber *localAuthID = [[arrID firstObject] valueForKey:@"authority_id"];
                         [dictFilter setValue:localAuthID forKey:@"filter_local_authority"];
                         break;
                     }
                         
                     case 5:
                     {
                         arrID = [_arrFilterSpecialties filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"specialties_name == %@", selectedOption]];
                         NSNumber *specialtyID = [[arrID firstObject] valueForKey:@"specialties_id"];
                         [dictFilter setValue:specialtyID forKey:@"filter_specialty"];
                         break;
                     }
                         
                     case 7:
                     {
                         arrID = [_arrFilterLanguage filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"language_name == %@", selectedOption]];
                         NSNumber *languageID = [[arrID firstObject] valueForKey:@"language_id"];
                         [dictFilter setValue:languageID forKey:@"filter_language"];
                         break;
                     }
                 }
                 
                 NSMutableDictionary *dictReplace = [arrFilter [indexPath.row] mutableCopy];
                 [dictReplace setValue:selectedOption forKey:@"selected"];
                 [arrFilter replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                 
                 [dictFilter setValue:selectedOption forKey:[arrFilter[indexPath.row] stringValueForJSON:@"title"]];
                 NSLog(@"dict == %@", dictFilter);
             }];
            
            return cell;
            
        }
    }
}

@end
