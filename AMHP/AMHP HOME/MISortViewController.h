//
//  MISortViewController.h
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"

typedef enum : NSUInteger
{
    MISortTypeAddressBook,
    MISortTypeLocation,
} MISortType;

@interface MISortViewController : ParentViewController
{
    IBOutlet UITableView *tblSortBy;
}

@property (nonatomic, assign) MISortType MISortType;
@end
