//
//  MISortViewController.m
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MISortViewController.h"
#import "MISortByRadioOptionTableViewCell.h"
#import "MISortByCheckOptionTableViewCell.h"

@interface MISortViewController ()
{
    NSMutableArray *arrSortBy;
    NSMutableDictionary *dictSortBy;
}
@end

@implementation MISortViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Sort By";
    
    dictSortBy = [[NSMutableDictionary alloc] init];
    
    if(self.iObject)
        [dictSortBy addEntriesFromDictionary:self.iObject];
    
    [self configureSortByData];
    
    UIBarButtonItem *reset = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(btnResetClicked:)];
    
    UIBarButtonItem *apply = [[UIBarButtonItem alloc] initWithTitle:@"Apply" style:UIBarButtonItemStylePlain target:self action:@selector(btnApplyClicked:)];
    
    self.navigationItem.leftBarButtonItem = reset;
    self.navigationItem.rightBarButtonItem = apply;
    
    [tblSortBy setEstimatedRowHeight:50];
    [tblSortBy registerNib:[UINib nibWithNibName:@"MISortByRadioOptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISortByRadioOptionTableViewCell"];
    [tblSortBy registerNib:[UINib nibWithNibName:@"MISortByCheckOptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISortByCheckOptionTableViewCell"];

    
}

- (void)configureSortByData
{
    NSArray *arrAvailability = @[@"Available",@"May be available",@"Unavailable",@"Unknown"];
    
    if(self.MISortType == MISortTypeAddressBook)
    {
        arrSortBy = @[@{@"title":@"Availability",
                        @"selected":[[dictSortBy allKeys] containsObject:@"Availability"] ? [dictSortBy numberForJson:@"Availability"] : @"" ,
                        @"options":arrAvailability},
                      @{@"title":@"Employer",
                        @"selected": [[dictSortBy allKeys] containsObject:@"Employer"] ? [dictSortBy numberForJson:@"Employer"] : @"" ,
                        @"options1":@[@"Trust",@"Independent"],
                        @"options2":@[@"Independent",@"Trust"]},
                      @{@"title":@"Name",
                        @"selected": [[dictSortBy allKeys] containsObject:@"Name"] ? [dictSortBy numberForJson:@"Name"] : @"" ,
                        @"options1":@[@"A",@"Z"],
                        @"options2":@[@"Z",@"A"]}].mutableCopy;
        
    }
    else
    {
        arrSortBy = @[@{@"title":@"Availability",
                        @"selected":[[dictSortBy allKeys] containsObject:@"Availability"] ? [dictSortBy numberForJson:@"Availability"] : @"" ,
                        @"options":arrAvailability},
                      @{@"title":@"Employer",
                        @"selected": [[dictSortBy allKeys] containsObject:@"Employer"] ? [dictSortBy numberForJson:@"Employer"] : @"" ,
                        @"options1":@[@"Trust",@"Independent"],
                        @"options2":@[@"Independent",@"Trust"]},
                      @{@"title":@"Distance",
                        @"selected": [[dictSortBy allKeys] containsObject:@"Distance"] ? [dictSortBy numberForJson:@"Distance"] : @"" ,
                        @"options":@[@"Nearest First"]},
                      @{@"title":@"Name",
                        @"selected": [[dictSortBy allKeys] containsObject:@"Name"] ? [dictSortBy numberForJson:@"Name"] : @"" ,
                        @"options1":@[@"A",@"Z"],
                        @"options2":@[@"Z",@"A"]}].mutableCopy;
        
    }
}





# pragma mark
# pragma mark - Action Event

- (void)btnResetClicked:(UIBarButtonItem*)sender
{
    [dictSortBy removeAllObjects];
    [self configureSortByData];
    [tblSortBy reloadData];
    
}

- (void)btnApplyClicked:(UIBarButtonItem*)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
        if (self.block)
            self.block(dictSortBy,nil);
        
    }];
}




# pragma mark
# pragma mark - UITableview Delegate And Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSortBy.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.MISortType)
    {
        case MISortTypeAddressBook: //..... ADDRESSBOOK
        {
            if (indexPath.row == 0)
            {
                static NSString *identifier = @"MISortByCheckOptionTableViewCell";
                MISortByCheckOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                [cell configureData:arrSortBy[indexPath.row]];
                
                [cell.btnCheckBox touchUpInsideClicked:^{
                    
                    NSMutableDictionary *dictReplace = [arrSortBy [indexPath.row] mutableCopy];
                    [dictReplace setValue:[NSNumber numberWithBool:cell.btnCheckBox.selected] forKey:@"selected"];
                    [arrSortBy replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                    
                    [dictSortBy setValue:[NSNumber numberWithBool:cell.btnCheckBox.selected] forKey:@"sort_availability"];
                    
                    [dictSortBy setValue:[NSNumber numberWithBool:cell.btnCheckBox.selected] forKey:[arrSortBy[indexPath.row] stringValueForJSON:@"title"]];
                    
                }];
                
                return cell;
            }
            else
            {
                static NSString *identifier = @"MISortByRadioOptionTableViewCell";
                MISortByRadioOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                
                [cell configureData:arrSortBy[indexPath.row] handler:^(NSNumber *selected)
                {
                    NSMutableDictionary *dictReplace = [arrSortBy [indexPath.row] mutableCopy];
                    [dictReplace setValue:selected forKey:@"selected"];
                    [arrSortBy replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                    
                    [dictSortBy setValue:selected forKey: indexPath.row == 1 ? @"sort_employer" : @"sort_name"];
                    [dictSortBy setValue:selected forKey:[arrSortBy[indexPath.row] stringValueForJSON:@"title"]];
                }];
                
                return cell;
            }

            break;
        }

        case MISortTypeLocation: //..... MAP
        {
            if (indexPath.row %2 == 0)
            {
                static NSString *identifier = @"MISortByCheckOptionTableViewCell";
                MISortByCheckOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                [cell configureData:arrSortBy[indexPath.row]];
                
                [cell.btnCheckBox touchUpInsideClicked:^{
                    
                    NSMutableDictionary *dictReplace = [arrSortBy [indexPath.row] mutableCopy];
                    [dictReplace setValue:[NSNumber numberWithBool:cell.btnCheckBox.selected] forKey:@"selected"];
                    [arrSortBy replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                    
                    [dictSortBy setValue:[NSNumber numberWithBool:cell.btnCheckBox.selected] forKey: indexPath.row == 0 ? @"sort_availability" : @"sort_distance"];
                    
                    [dictSortBy setValue:[NSNumber numberWithBool:cell.btnCheckBox.selected] forKey:[arrSortBy[indexPath.row] stringValueForJSON:@"title"]];
                    
                }];
                
                return cell;
            }
            else
            {
                static NSString *identifier = @"MISortByRadioOptionTableViewCell";
                MISortByRadioOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                
                [cell configureData:arrSortBy[indexPath.row] handler:^(NSNumber *selected)
                {
                    NSMutableDictionary *dictReplace = [arrSortBy [indexPath.row] mutableCopy];
                    [dictReplace setValue:selected forKey:@"selected"];
                    [arrSortBy replaceObjectAtIndex:indexPath.row withObject:dictReplace];
                    
                    [dictSortBy setValue:selected forKey: indexPath.row == 1 ? @"sort_employer" : @"sort_name"];
                    [dictSortBy setValue:selected forKey:[arrSortBy[indexPath.row] stringValueForJSON:@"title"]];
                }];
                
                return cell;
            }

            break;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
