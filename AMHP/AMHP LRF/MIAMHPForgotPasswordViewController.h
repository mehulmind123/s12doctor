//
//  MIForgotPasswordViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/18/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAMHPForgotPasswordViewController : ParentViewController
{    
    IBOutlet MIFloatingTextField *txtEmailAddress;
}


- (IBAction)btnSubmitClicked:(id)sender;

@end
