//
//  MILoginViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/18/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAMHPLoginViewController : ParentViewController
{
    IBOutlet MIFloatingTextField *txtEmailAddress;
    IBOutlet MIFloatingTextField *txtPassword;
}


- (IBAction)btnLoginClicked:(id)sender;
- (IBAction)btnForgotPasswordClicked:(id)sender;

@end
