//
//  MILoginViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/18/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPLoginViewController.h"
#import "MIAMHPHomeViewController.h"
#import "MIAMHPForgotPasswordViewController.h"
#import "MIAMHPCMSViewController.h"
#import "MIAMHPChangePasswordViewController.h"
#import "MITutorialViewController.h"
#import "MIMainViewController.h"

@interface MIAMHPLoginViewController ()

@end

@implementation MIAMHPLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self initialize];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Login";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    
    if (IS_IPHONE_SIMULATOR)
    {
        txtEmailAddress.text = @"sejal.mindinventory@gmail.com";
        txtPassword.text = @"mind@123";
    }
}




# pragma mark
# pragma mark - General Method

- (void)backClicked
{
    MIMainViewController *mainVC = [[MIMainViewController alloc] initWithNibName:@"MIMainViewController" bundle:nil];
    [appDelegate.window setRootViewController:[[UINavigationController alloc] initWithRootViewController:mainVC]];
}

- (IBAction)btnLoginClicked:(id)sender
{
    if (![txtEmailAddress.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    
    else if (![txtEmailAddress.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidEmail onView:self];
    
    else if (![txtPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankPassword onView:self];
    
    else
    {
        //..... Logout Event
//        [appDelegate trackTheEventWithName:CEventLogin];
        
        [[APIRequest request] loginWithEmail:txtEmailAddress.text andPassword:txtPassword.text completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
             {
                 //..... GET LOCATION
                 [appDelegate getCurrentLocation];
                 
                 [[APIRequest request] saveLoginUserToLocal:responseObject];
                 
                 if (appDelegate.loginUserAMHP.accept_terms_condition == 0)
                 {
                     MIAMHPCMSViewController *cmsVC = [[MIAMHPCMSViewController alloc] initWithNibName:@"MIAMHPCMSViewController" bundle:nil];
                     cmsVC.cmsType = AMHPTermsAndCondition;
                     [self.navigationController pushViewController:cmsVC animated:YES];
                 }
                 else if (appDelegate.loginUserAMHP.change_password == 0)
                 {
                     MIAMHPChangePasswordViewController *changePwdVC = [[MIAMHPChangePasswordViewController alloc] initWithNibName:@"MIAMHPChangePasswordViewController" bundle:nil];
                     changePwdVC.isFromTermsCondition = YES;
                     [self.navigationController pushViewController:changePwdVC animated:YES];
                 }
                 else
                 {
                     MIAMHPHomeViewController *homeVC = [[MIAMHPHomeViewController alloc] initWithNibName:@"MIAMHPHomeViewController" bundle:nil];
                     [appDelegate openMenuViewcontroller:homeVC animated:YES];
                 }
             }
             else if (!error)
                 [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
         }];
    }
}

- (IBAction)btnForgotPasswordClicked:(id)sender
{
    MIAMHPForgotPasswordViewController *forgotPwdVC = [[MIAMHPForgotPasswordViewController alloc] initWithNibName:@"MIAMHPForgotPasswordViewController" bundle:nil];
    appDelegate.isFromS12 = @2;
    [self.navigationController pushViewController:forgotPwdVC animated:YES];
}

@end
