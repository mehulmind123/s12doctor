//
//  MICMSViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    AMHPAboutUs,
    AMHPTermsAndCondition,
    AMHPPrivacyPolicy
    
} AMHPCMSType;


@interface MIAMHPCMSViewController : ParentViewController
{
    IBOutlet UIWebView *webViewCMS;
    IBOutlet UILabel *lblAcceptTAndC;
    IBOutlet MIGenericButton *btnAcceptAndProceed;
}


@property (nonatomic, assign) AMHPCMSType cmsType;
@property (nonatomic, assign) BOOL isFromSideMenu;


- (IBAction)btnAcceptAndProceedClicked:(id)sender;

@end
