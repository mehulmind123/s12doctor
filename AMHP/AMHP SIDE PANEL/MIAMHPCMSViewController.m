//
//  MICMSViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPCMSViewController.h"
#import "MIAMHPChangePasswordViewController.h"

@interface MIAMHPCMSViewController ()

@end

@implementation MIAMHPCMSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    if (_cmsType == AMHPAboutUs)
        self.title = @"About Us";
    else if (_cmsType == AMHPTermsAndCondition)
        self.title = @"Terms & Conditions";
    else
        self.title = @"Privacy Policy";
    
    if (![AFNetworkReachabilityManager sharedManager].reachable)
        [appDelegate toastAlertWithMessage:CMessageOffline showDone:NO];
    
    if (_isFromSideMenu)
    {
        [btnAcceptAndProceed setConstraintConstant:0 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        [webViewCMS setConstraintConstant:0 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        [lblAcceptTAndC hideByHeight:YES];
        [btnAcceptAndProceed hideByHeight:YES];
    }
    else
        [self.navigationItem setHidesBackButton:YES];
    
    
    [self setDataFromLocal];
    
    
    //..... LOAD DATA FROM SERVER
    
//    [self startLoadingAnimationInView:self.view];
    
    [[APIRequest request] cmsWithType:_cmsType == AMHPAboutUs ? 1 : _cmsType == AMHPTermsAndCondition ? 2 : 3 completed:^(id responseObject, NSError *error)
     {
//         [self stopLoadingAnimationInView:self.view];
         
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             NSDictionary *dictData = [responseObject valueForKey:CJsonData];
             
             NSString *strContent = [NSString stringWithFormat:@"<html><head><style>*{font-family:AvenirLTStd-Roman;}</style></head><body>%@</body></html>",_cmsType == AMHPAboutUs ? [dictData stringValueForJSON:@"about_us_content"] : _cmsType == AMHPTermsAndCondition ? [dictData stringValueForJSON:@"terms_and_conditions_content"] : [dictData stringValueForJSON:@"privacy_content"]];
             
             [webViewCMS loadHTMLString:strContent baseURL:nil];
             
             [self saveCoreDataValue:dictData];
         }
     }];
}

- (void)setDataFromLocal
{
    TBLCms *objCMS = [TBLCms findOrCreate:nil];
    
    NSString *strContent = [NSString stringWithFormat:@"<html><head><style>*{font-family:AvenirLTStd-Roman;}</style></head><body>%@</body></html>",_cmsType == AMHPAboutUs ? objCMS.about_us : _cmsType == AMHPTermsAndCondition ? objCMS.terms_condition : objCMS.privacy_policy];
    
    [webViewCMS loadHTMLString:strContent baseURL:nil];
}

- (void)saveCoreDataValue:(NSDictionary *)dictCMS
{
    TBLCms *objCMS = [TBLCms findOrCreate:nil];
    
    objCMS.about_us = [dictCMS stringValueForJSON:@"about_us_content"];
    objCMS.terms_condition = [dictCMS stringValueForJSON:@"terms_and_conditions_content"];
    objCMS.privacy_policy = [dictCMS stringValueForJSON:@"privacy_content"];
    
    objCMS.loginUserAMHP = appDelegate.loginUserAMHP;
    
    [[[Store sharedInstance] mainManagedObjectContext] save];
}






# pragma mark
# pragma mark - Action Events

- (IBAction)btnAcceptAndProceedClicked:(id)sender
{
    [[APIRequest request] acceptTermsAndCondition:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             appDelegate.loginUserAMHP.accept_terms_condition = 1;
             [[[Store sharedInstance] mainManagedObjectContext] save];
             
             MIAMHPChangePasswordViewController *changePwdVC = [[MIAMHPChangePasswordViewController alloc] initWithNibName:@"MIAMHPChangePasswordViewController" bundle:nil];
             changePwdVC.isFromTermsCondition = YES;
             [self.navigationController pushViewController:changePwdVC animated:YES];
         }
     }];
}






# pragma mark
# pragma mark - Webview Delegate

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked)
    {
        [[UIApplication sharedApplication] openURL:[inRequest URL] options:@{} completionHandler:nil];
        return NO;
    }
    
    return YES;
}

@end
