//
//  MIChangePasswordViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPChangePasswordViewController.h"
#import "MIAMHPHomeViewController.h"

@interface MIAMHPChangePasswordViewController ()

@end

@implementation MIAMHPChangePasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}






# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Change Password";
    
    if (_isFromTermsCondition)
        [self.navigationItem setHidesBackButton:YES];
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnSubmitClicked:(id)sender
{
    if (![txtOldPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankOldPassword onView:self];
    
    else if (![txtNewPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankNewPassword onView:self];
    
    else if (![txtNewPassword.text isValidPassword:txtNewPassword.text] || txtNewPassword.text.length < 8)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidPassword onView:self];
    
    else if (![txtConfirmPasssword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankConfirmPassword onView:self];
    
    else if (![txtNewPassword.text isEqualToString:txtConfirmPasssword.text])
        [CustomAlertView iOSAlert:@"" withMessage:CMessagePasswordNotMatch onView:self];
    
    else if ([txtOldPassword.text isEqualToString:txtNewPassword.text])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageOldNewPasswordNotSame onView:self];
    
    else
    {
        [appDelegate resignKeyboard];
        
        [[APIRequest request] changePasswordWithOldPassword:txtOldPassword.text andNewPassword:txtNewPassword.text andLoginType:appDelegate.loginUserAMHP.login_type completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
             {
                 appDelegate.loginUserAMHP.change_password = 1;
                 [[[Store sharedInstance] mainManagedObjectContext] save];
                 
                 [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessagePasswordSuccessfullyChanged buttonTitle:@"OK" handler:^(UIAlertAction *action)
                  {
//                      [appDelegate trackTheEventWithName:CEventPasswordChanged];
                      
                      MIAMHPHomeViewController *homeVC = [[MIAMHPHomeViewController alloc] initWithNibName:@"MIAMHPHomeViewController" bundle:nil];
                      [appDelegate openMenuViewcontroller:homeVC animated:YES];
                  } inView:self];
             }
             else if (!error)
                 [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
         }];
    }
}

@end
