//
//  MIEditProfileViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/21/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selectedValue)(NSString *data);

@interface MIAMHPEditProfileViewController : ParentViewController
{
    IBOutlet MIFloatingTextField *txtName;
    IBOutlet MIFloatingTextField *txtMobileNumber;
    IBOutlet MIFloatingTextField *txtEmailAddress;
    IBOutlet MIFloatingTextView *txtVLocalAuth;
    
}


@property (nonatomic, copy) selectedValue selectedValue;

- (IBAction)btnSubmitClicked:(id)sender;
- (IBAction)textFieldTextDidChange:(id)sender;

@end
