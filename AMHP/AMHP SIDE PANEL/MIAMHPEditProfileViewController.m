//
//  MIEditProfileViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/21/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPEditProfileViewController.h"
#import "MIAMHPHomeViewController.h"

@interface MIAMHPEditProfileViewController ()

@end

@implementation MIAMHPEditProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Methods

- (void)initialize
{
    self.title = @"Edit Profile";
    
    //..... LOAD DATA FROM LOCAL
    [self setEditProfileDataFromLocal];
    [self getUserDetailFromServer];
}





# pragma mark
# pragma mark - API Methods

- (void)setEditProfileDataFromLocal
{
    txtName.text = appDelegate.loginUserAMHP.name;
    txtMobileNumber.text = appDelegate.loginUserAMHP.mobile_number;
    txtEmailAddress.text = appDelegate.loginUserAMHP.email;
    if ([[appDelegate.loginUserAMHP.local_authority valueForKeyPath:@"authority_name"] count] > 0)
    {
        txtVLocalAuth.text = [[appDelegate.loginUserAMHP.local_authority valueForKeyPath:@"authority_name"] componentsJoinedByString:CComponentJoinedString];
    }
}

- (void)getUserDetailFromServer
{
    [[APIRequest request] userDetails:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             [[APIRequest request] saveLoginUserToLocal:responseObject];
             [self setEditProfileDataFromLocal];
         }
     }];
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnSubmitClicked:(id)sender
{
    if (![txtName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankName onView:self];
    
    else if ([txtMobileNumber.text isBlankValidationPassed] && txtMobileNumber.text.length != 11)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidMobileNumber onView:self];
    
    else
    {
        [appDelegate resignKeyboard];
        
        NSString *strLocalAuthorityID;
        if ([[appDelegate.loginUserAMHP.local_authority valueForKeyPath:@"authority_id"] count])
        {
            strLocalAuthorityID = [[appDelegate.loginUserAMHP.local_authority valueForKeyPath:@"authority_id"] componentsJoinedByString:CComponentJoinedString];
        }
        
        
        //..... EDIT PROFILE API
        
        [[APIRequest request] editProfileWithName:txtName.text andMobileNumber:txtMobileNumber.text andEmail:txtEmailAddress.text andLocalAuthority:strLocalAuthorityID ? strLocalAuthorityID : @""  completed:^(id responseObject, NSError *error)
        {
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [[APIRequest request] saveLoginUserToLocal:responseObject];
                
                [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self];
                
                MIAMHPHomeViewController *homeVC = [[MIAMHPHomeViewController alloc] initWithNibName:@"MIAMHPHomeViewController" bundle:nil];
                [appDelegate openMenuViewcontroller:homeVC animated:YES];
            }
        }];
    }
}





# pragma mark
# pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return (textField.text.length - range.length) < 11;
}

- (IBAction)textFieldTextDidChange:(UITextField *)sender
{
    if ([sender isEqual:txtName])
    {
        if (sender.text.length > CCharacterLimit256)
        {
            sender.text = [sender.text substringToIndex:CCharacterLimit256];
            return;
        }
        
        sender.text = sender.text;
    }
}

@end
