//
//  MIUsefulInfoViewController.h
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"
#import <WebKit/WebKit.h>

@interface MIAMHPUsefulInfoViewController : ParentViewController<WKUIDelegate, WKNavigationDelegate>
{
    IBOutlet UITableView *tblInfo;
    IBOutlet UILabel *lblNoData;
    WKWebView *webView;
}
@end
