//
//  MIUsefulInfoViewController.m
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPUsefulInfoViewController.h"
#import "MIAMHPUsefulInfoTableViewCell.h"


@interface MIAMHPUsefulInfoViewController ()
{
    int iOffset;
    NSMutableArray *arrUsefulInfo;
    UIRefreshControl *refreshControl;
}
@end

@implementation MIAMHPUsefulInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark
#pragma mark - General Method

- (void)initialize
{
    self.title = @"Useful Information";
    
//    [appDelegate trackTheEventWithName:CEventUsefulInformationClicked];
    
    NSString *strAuthorizationToken = [CUserDefaults valueForKey:UserDefaultLoginToken];
    
    NSString *strReplaceBearer = [[NSString stringWithFormat:@"%@useful-info/%@",BASEURLAMHP, strAuthorizationToken] stringByReplacingOccurrencesOfString:@"Bearer " withString:@""];
    
    NSMutableURLRequest *request;
    
    if ([AFNetworkReachabilityManager sharedManager].isReachable)
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strReplaceBearer] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    else
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strReplaceBearer] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:60.0];
        
        [appDelegate toastAlertWithMessage:CMessageOffline showDone:NO];
    }

    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    
    webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, CScreenHeight - 64) configuration:    theConfiguration];
    webView.navigationDelegate = self;
    [webView loadRequest:request];
    [self.view addSubview:webView];
    [self startLoadingAnimationInView:self.view];
    
    
    //..... REGISTER CELL
    
//    [tblInfo registerNib:[UINib nibWithNibName:@"MIAMHPUsefulInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAMHPUsefulInfoTableViewCell"];
//    [tblInfo setContentInset:UIEdgeInsetsMake(11, 0, 0, 0)];
    
    
    //..... UIREFRESH CONTROL
    
//    refreshControl = [[UIRefreshControl alloc] init];
//    refreshControl.tintColor = ColorBlue_1371b9;
//    [refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
//    [tblInfo addSubview:refreshControl];
    
    
    //..... API CALL
    
//    arrUsefulInfo = [NSMutableArray new];
//    [self loadData];
}





# pragma mark
# pragma mark - API Function

//- (void)loadData
//{
//    iOffset = 0;
//    [self getUserInformationListFromServer:YES];
//}

- (void)getUserInformationListFromServer
{
//    if (iOffset == 0 && isLoading)
//        [self startLoadingAnimationInView:self.view];
    
    [[APIRequest request] amhpUsefulInformation:^(id responseObject, NSError *error)
    {
//        [self stopLoadingAnimationInView:self.view];
        
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            if (iOffset == 0)
                [arrUsefulInfo removeAllObjects];
            
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            if (arrData.count > 0)
            {
                [arrUsefulInfo addObjectsFromArray:arrData];
                iOffset = [[responseObject valueForKey:CJsonMeta] intForKey:CJsonNewOffset];
                [tblInfo reloadData];
            }
        }
        
        
//        if (arrUsefulInfo.count == 0)
//        {
//            [lblNoData setHidden:NO];
//            [tblInfo setHidden:YES];
//        }
        
    }];
}





# pragma mark
# pragma mark - UITableView Delegate and Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrUsefulInfo.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MIAMHPUsefulInfoTableViewCell";
    MIAMHPUsefulInfoTableViewCell *cell = [tblInfo dequeueReusableCellWithIdentifier:identifier];
    
    NSDictionary *dict = arrUsefulInfo[indexPath.row];
    [cell.lblAuthorityName setText:[dict stringValueForJSON:@"local_authority"]];
    [cell.lblInfo setAttributedText:[cell setAttributeString:[dict stringValueForJSON:@"useful_info"]]];
    
    
    //..... LOAD MORE DATA
    
//    if (indexPath.row == (arrUsefulInfo.count - 1))
//        [self getUserInformationListFromServer:NO];
    
    return cell;
}





# pragma mark
# pragma mark - WKWebView Delegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    [self stopLoadingAnimationInView:self.view];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    [self stopLoadingAnimationInView:self.view];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [self stopLoadingAnimationInView:self.view];
}

@end
