//
//  MIViewClaimFormViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIAMHPCommonPopup.h"

@interface MIAMHPViewClaimFormViewController : ParentViewController
{
    IBOutlet UIView *viewApprovalStatus;
    IBOutlet UIView *viewLocalAuthority;
    IBOutlet UIView *viewAMHPName;
    IBOutlet UIView *views12DoctorName;
    IBOutlet UIView *viewDoctorAddress;
    IBOutlet UIView *viewAssessmentLocationType;
    IBOutlet UIView *viewAssessmentLocationTypeDesc;
//    IBOutlet UIView *viewAssessmentLocation;
    IBOutlet UIView *viewAssessmentPostcode;
    IBOutlet UIView *viewAssessmentDateAndTime;
    IBOutlet UIView *viewPatientNHSNumber;
    IBOutlet UIView *viewPatientPostcode;
    IBOutlet UIView *viewAdditionalInfo;
    IBOutlet UIView *viewSectionImplemented;
    IBOutlet UIView *viewSectionImplementedDesc;
    IBOutlet UIView *viewCarMake;
    IBOutlet UIView *viewCarModel;
    IBOutlet UIView *viewCarRegistrationPlate;
    IBOutlet UIView *viewEngineSize;
//    IBOutlet UIView *viewMilesTraveled;
 
//    IBOutlet UIView *viewMileage;
    IBOutlet UIView *viewFromPostCode;
    IBOutlet UIView *viewToPostCode;
    IBOutlet UIView *viewNumberOfMiles;
    
    IBOutlet UILabel *lblStatus;
    
    IBOutlet UILabel *lblLocalAuthority;
    IBOutlet UILabel *lblAMHPName;
    IBOutlet UILabel *lblDoctorName;
    IBOutlet UILabel *lblLocationType;
    IBOutlet UILabel *lblLocationTypeDesc;
//    IBOutlet UILabel *lblLocation;
    IBOutlet UILabel *lblAssessmentPostCode;
    IBOutlet UILabel *lblAssessmentDateAndTime;
    IBOutlet UILabel *lblPatientNHSNumber;
    IBOutlet UILabel *lblPatientPostCode;
    IBOutlet UILabel *lblSectionImplemented;
    IBOutlet UILabel *lblSectionImplementedDesc;
    
    IBOutlet UILabel *lblCarDetails;
    
    IBOutlet UILabel *lblCarMake;
    IBOutlet UILabel *lblCarModel;
    IBOutlet UILabel *lblCarRegistrationPlate;
    IBOutlet UILabel *lblEngineSize;
//    IBOutlet UILabel *lblMilesTraveled;
    
    IBOutlet UILabel *lblDoctorAddress;
    IBOutlet UILabel *lblAdditionalInfo;
    IBOutlet UILabel *lblMileage;
    IBOutlet UILabel *lblFromPostCode;
    IBOutlet UILabel *lblToPostCode;
    IBOutlet UILabel *lblNumberOfMiles;
    
    MIAMHPCommonPopup *commonPopup;
    
}

@property (nonatomic, strong) NSDictionary *dictClaimForm;

@end
