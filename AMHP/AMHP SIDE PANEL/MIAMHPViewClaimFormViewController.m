//
//  MIViewClaimFormViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPViewClaimFormViewController.h"

@interface MIAMHPViewClaimFormViewController ()
{
    NSMutableArray *arrCCG;
    NSDictionary *dictClaimFormDetail;
}
@end

@implementation MIAMHPViewClaimFormViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"View Claim Form";
 
    viewApprovalStatus.layer.cornerRadius = viewLocalAuthority.layer.cornerRadius = viewAMHPName.layer.cornerRadius = views12DoctorName.layer.cornerRadius = viewAssessmentLocationType.layer.cornerRadius = viewAssessmentPostcode.layer.cornerRadius = viewAssessmentDateAndTime.layer.cornerRadius = viewPatientNHSNumber.layer.cornerRadius = viewPatientPostcode.layer.cornerRadius = viewSectionImplemented.layer.cornerRadius = viewCarMake.layer.cornerRadius = viewCarModel.layer.cornerRadius = viewCarRegistrationPlate.layer.cornerRadius = viewEngineSize.layer.cornerRadius = 5;

    //..... API CALL
    [self getClaimFormDetailFromServer];
}






# pragma mark
# pragma mark - API Methods

- (void)getClaimFormDetailFromServer
{
    if (_dictClaimForm)
    {
        [self startLoadingAnimationInView:self.view];
        
        [[APIRequest request] viewClaimFormWithFormID:[_dictClaimForm stringValueForJSON:@"claim_id"] andLoginType:appDelegate.loginUserAMHP.login_type completed:^(id responseObject, NSError *error)
        {
            [self stopLoadingAnimationInView:self.view];
            
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                //..... BAR BUTTONS
                
                UIBarButtonItem *btnReject =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"close"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnRejectClicked)];
                
                UIBarButtonItem *btnApprove =  [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"right"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnAcceptClicked)];
                
                self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:btnReject,btnApprove,nil];
                
                dictClaimFormDetail = [responseObject valueForKey:CJsonData];
                [self setClaimFormData];
                [self getCCGListFromServer];
                
            }
        }];
    }
}

- (void)setClaimFormData
{
    if (dictClaimFormDetail)
    {
        lblLocalAuthority.text          = [[dictClaimFormDetail stringValueForJSON:@"local_authority"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"local_authority"] : CNotAvailable;
        lblAMHPName.text                = [[dictClaimFormDetail stringValueForJSON:@"amhp_name"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"amhp_name"] : CNotAvailable;
        lblDoctorName.text              = [[dictClaimFormDetail stringValueForJSON:@"doctor_name"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"doctor_name"] : CNotAvailable;
        lblDoctorAddress.text              = [[dictClaimFormDetail stringValueForJSON:@"doctor_address"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"doctor_address"] : CNotAvailable;
        lblLocationType.text            = [[dictClaimFormDetail stringValueForJSON:@"assessment_location_type_text"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"assessment_location_type_text"] : CNotAvailable;

        lblAssessmentPostCode.text      = [[dictClaimFormDetail stringValueForJSON:@"assessment_postcode"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"assessment_postcode"] : CNotAvailable;
        
        NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:[dictClaimFormDetail stringValueForJSON:@"assessment_date_time"] withFormat:CDateFormate] withFormat:CDateFormate];
        NSDate *systemDate = [NSDate dateFromString:strClaimFormDate withFormat:CDateFormate];
        lblAssessmentDateAndTime.text = [NSDate stringFromDate:systemDate withFormat:CDateFormateDisplay];
        
        lblPatientNHSNumber.text        = [[dictClaimFormDetail stringValueForJSON:@"patient_nhs_number"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"patient_nhs_number"] : CNotAvailable;
        lblPatientPostCode.text         = [[dictClaimFormDetail stringValueForJSON:@"patient_postcode"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"patient_postcode"] : CNotAvailable;
        lblAdditionalInfo.text              = [[dictClaimFormDetail stringValueForJSON:@"additional_information"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"additional_information"] : CNotAvailable;
        lblSectionImplemented.text      = [[dictClaimFormDetail stringValueForJSON:@"section_implemented_text"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"section_implemented_text"] : CNotAvailable;
        lblCarMake.text                 = [[dictClaimFormDetail stringValueForJSON:@"car_make"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"car_make"] : CNotAvailable;
        lblCarModel.text                = [[dictClaimFormDetail stringValueForJSON:@"car_model"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"car_model"] : CNotAvailable;
        lblCarRegistrationPlate.text    = [[dictClaimFormDetail stringValueForJSON:@"car_registration_plate"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"car_registration_plate"] : CNotAvailable;
        lblEngineSize.text              = [[dictClaimFormDetail stringValueForJSON:@"engine_size"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"engine_size"] : CNotAvailable;
        
        
        lblFromPostCode.text    = [[dictClaimFormDetail stringValueForJSON:@"from_postcode"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"from_postcode"] : CNotAvailable;
        lblToPostCode.text              = [[dictClaimFormDetail stringValueForJSON:@"to_postcode"] isBlankValidationPassed] ? [dictClaimFormDetail stringValueForJSON:@"to_postcode"] : CNotAvailable;
        lblNumberOfMiles.text           = [[dictClaimFormDetail stringValueForJSON:@"miles_traveled"] isBlankValidationPassed] ? [NSString stringWithFormat:@"%@ mi", [dictClaimFormDetail stringValueForJSON:@"miles_traveled"]] : CNotAvailable;

        
        if ([[dictClaimFormDetail stringValueForJSON:@"assessment_location_type_description"] isBlankValidationPassed])
        {
            [viewAssessmentLocationTypeDesc hideByHeight:NO];
            lblLocationTypeDesc.text = [dictClaimFormDetail stringValueForJSON:@"assessment_location_type_description"];
        }
        else
        {
            [viewAssessmentLocationTypeDesc hideByHeight:YES];
            [viewAssessmentPostcode setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        if ([[dictClaimFormDetail stringValueForJSON:@"section_type_description"] isBlankValidationPassed])
        {
            [viewSectionImplementedDesc hideByHeight:NO];
            lblSectionImplementedDesc.text = [dictClaimFormDetail stringValueForJSON:@"section_type_description"];
        }
        else
        {
            [viewSectionImplementedDesc hideByHeight:YES];
            [lblMileage setConstraintConstant:27 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
    }
}

- (void)getCCGListFromServer
{
    if (dictClaimFormDetail)
    {
        [[APIRequest request] ccgListWithLocalAuthorityID:[dictClaimFormDetail stringValueForJSON:@"local_authority_id"] completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
             {
                 NSArray *arrCCGList = [responseObject valueForKey:CJsonData];
                 
                 if (arrCCGList.count > 0)
                 {
                     arrCCG = [NSMutableArray new];
                     [arrCCG addObjectsFromArray:arrCCGList];
                 }
             }
         }];
    }
}






# pragma mark
# pragma mark - Action Events

- (void)btnAcceptClicked
{
    [self openAcceptOrRejectionBox:YES];
}

- (void)btnRejectClicked
{
    [self openAcceptOrRejectionBox:NO];
}






# pragma mark -
# pragma mark - Accept Reject Popup

-(void)openAcceptOrRejectionBox:(BOOL)isAccept
{
    commonPopup = [MIAMHPCommonPopup customCommonPopup];
    [commonPopup.lblAddNewLanguage hideByHeight:NO];
    commonPopup.txtCommonField.placeholder = isAccept?@"Select CCG":@"Provide Reason";
    commonPopup.lblAddNewLanguage.text = isAccept?@"Select CCG to submit to":@"Rejection Reason";
    commonPopup.txtCommonField.secureTextEntry = NO;
    [commonPopup.btnSubmit setBackgroundColor:ColorGreen_47C0B6];

    [commonPopup.btnSubmit setTitle:@"Confirm" forState:UIControlStateNormal];
    
    __block NSString *ccgID;
    
    if (isAccept)
    {
        [commonPopup.txtCommonField  setRightImage:[UIImage imageNamed:@"dropdown"] withSize:CGSizeMake(25, 7)];
        [commonPopup.txtCommonField setPickerData:[arrCCG valueForKeyPath:@"organisation_name"] update:^(NSString *text, NSInteger row, NSInteger component)
         {
             ccgID = [arrCCG[row] stringValueForJSON:@"organisation_id"];
         }];
    }
    else
        commonPopup.txtCommonField.text = @"";
    
    [commonPopup.btnSubmit touchUpInsideClicked:^{
        
        if (![commonPopup.txtCommonField.text isBlankValidationPassed])
        {
            [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:isAccept?CMessageSelectCCG:CMessageProvideReason buttonTitle:@"OK" handler:nil inView:self];
        }
        else
        {
            //..... ACCEPT / REJECT CLAIM FORM API
            
            [appDelegate resignKeyboard];
            
            [[APIRequest request] acceptRejectClaimFormWithFormID:[_dictClaimForm stringValueForJSON:@"claim_id"] andStatus:isAccept ? 1 : 2 andCCGID:ccgID ? ccgID : @"" andRejectionReason:isAccept ? @"" : commonPopup.txtCommonField.text completed:^(id responseObject, NSError *error)
             {
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] buttonTitle:@"OK" handler:^(UIAlertAction *action)
                     {
                         [self dismissPopUp:commonPopup];
                         [self.navigationController popViewControllerAnimated:NO];
                     }
                     inView:self];
                 }
             }];
        }
        
    }];
    
    [self presentPopUp:commonPopup from:PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

@end
