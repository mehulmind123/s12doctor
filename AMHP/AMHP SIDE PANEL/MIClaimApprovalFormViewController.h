//
//  MIClaimApprovalFormViewController.h
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"
#import "MIAMHPCommonPopup.h"

@interface MIClaimApprovalFormViewController : ParentViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tblClaimForm;
    IBOutlet UILabel *lblNoData;
    
    MIAMHPCommonPopup * commonPopup;
}
@end
