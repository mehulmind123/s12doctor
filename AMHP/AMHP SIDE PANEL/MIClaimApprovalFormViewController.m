//
//  MIClaimApprovalFormViewController.m
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIClaimApprovalFormViewController.h"
#import "MIClaimApprovalFormTableViewCell.h"
#import "MIAMHPViewClaimFormViewController.h"


@interface MIClaimApprovalFormViewController ()
{
    NSMutableArray *arrClaimApprovalFormList;
    int iOffset;
    UIRefreshControl *refreshControl;
}
@end

@implementation MIClaimApprovalFormViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}
    
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadClaimFormData];
}
    
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"Claim Forms for Review";
    
    
    //..... REGISTER CELL
    
    [tblClaimForm registerNib:[UINib nibWithNibName:@"MIClaimApprovalFormTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIClaimApprovalFormTableViewCell"];
    [tblClaimForm setContentInset:UIEdgeInsetsMake(11, 0, 0, 0)];
    
    
    //..... UIREFRESH CONTROL
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = ColorGreen_47C0B6;
    [refreshControl addTarget:self action:@selector(loadClaimFormData) forControlEvents:UIControlEventValueChanged];
    [tblClaimForm addSubview:refreshControl];
    
    
    //..... API CALL
    
    arrClaimApprovalFormList = [NSMutableArray new];
}





# pragma mark
# pragma mark - UITableView Delegate And Datasource
    
- (void)loadClaimFormData
{
    iOffset = 0;
    [self getClaimFormApprovalListFromServer:YES];
}

- (void)getClaimFormApprovalListFromServer:(BOOL)isLoading
{
    if (iOffset == 0 && isLoading)
        [self startLoadingAnimationInView:self.view];
    
    [[APIRequest request] claimFormApprovalListWithOffset:iOffset completed:^(id responseObject, NSError *error)
    {
        [refreshControl endRefreshing];
        [self stopLoadingAnimationInView:self.view];
        
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            if (iOffset == 0)
                [arrClaimApprovalFormList removeAllObjects];
            
            NSArray *arrResponseData = [responseObject valueForKey:CJsonData];
            
            if (arrResponseData.count > 0)
            {
                [arrClaimApprovalFormList addObjectsFromArray:arrResponseData];
                iOffset = [[responseObject valueForKey:CJsonMeta] intForKey:CJsonNewOffset];
                [tblClaimForm reloadData];
            }
        }
        else if (!error)
            [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
        
        
        if (arrClaimApprovalFormList.count == 0)
        {
            [lblNoData setHidden:NO];
            [tblClaimForm setHidden:YES];
        }
        
    }];
}





# pragma mark -
# pragma mark - UITableView Delegate And Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrClaimApprovalFormList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIClaimApprovalFormTableViewCell *cell = [tblClaimForm dequeueReusableCellWithIdentifier:@"MIClaimApprovalFormTableViewCell"];
    
    NSDictionary *dict = arrClaimApprovalFormList[indexPath.row];
    
    [cell.lblName setText:[dict stringValueForJSON:@"doctor_name"]];
    [cell.lblAssessmentLocationType setText:[dict stringValueForJSON:@"assessment_location_type"]];
    
    NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:[dict stringValueForJSON:@"assessment_date_time"] withFormat:CDateFormate] withFormat:CDateFormate];
    NSDate *fromDate = [NSDate dateFromString:strClaimFormDate withFormat:CDateFormate];
    cell.lblDateTime.text = [NSDate stringFromDate:fromDate withFormat:CDateFormateDisplay];
    
    
    //..... LOAD MORE DATA
    
    if (indexPath.row == (arrClaimApprovalFormList.count - 1))
        [self getClaimFormApprovalListFromServer:NO];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIAMHPViewClaimFormViewController *viewClaimFormVC = [[MIAMHPViewClaimFormViewController alloc] initWithNibName:@"MIAMHPViewClaimFormViewController" bundle:nil];
    viewClaimFormVC.dictClaimForm = arrClaimApprovalFormList[indexPath.row];
    [self.navigationController pushViewController:viewClaimFormVC animated:YES];
}

@end
