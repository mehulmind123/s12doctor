//
//  MIMyFavouritesViewController.h
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIMyFavouritesViewController : ParentViewController
{
    IBOutlet UITableView *tblVMyFavourite;
    IBOutlet UILabel *lblNoData;
}
@end
