//
//  MIMyFavouritesViewController.m
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIMyFavouritesViewController.h"
#import "MIDoctorDetailViewController.h"
#import "MIAddressBookTableViewCell.h"

@interface MIMyFavouritesViewController ()
{
    NSMutableArray *arrMyFavourite;
    int iOffset;
    NSURLSessionDataTask *sessionDataTask;
    UIRefreshControl *refreshControl;
}
@end

@implementation MIMyFavouritesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"My Favourites";
    

    //..... REGISTER CELL
    
    [tblVMyFavourite registerNib:[UINib nibWithNibName:@"MIAddressBookTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAddressBookTableViewCell"];
    [tblVMyFavourite setContentInset:UIEdgeInsetsMake(13, 0, 0, 0)];
    [tblVMyFavourite setEstimatedRowHeight:20];
    
    //..... UIREFRESH CONTROL
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = ColorGreen_47C0B6;
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblVMyFavourite addSubview:refreshControl];
    
    
    //..... API CALL
    
    arrMyFavourite = [NSMutableArray new];
    [self pullToRefresh];
}





# pragma mark
# pragma mark - API Function

- (void)pullToRefresh
{
    [self getFavouriteListFromServer];
}

- (void)getFavouriteListFromServer
{
    int offset = iOffset;
    if (refreshControl.isRefreshing) offset = 0;
    if (arrMyFavourite.count == 0) [self startLoadingAnimationInView:self.view];
    
    [[APIRequest request] myFavouriteListWithOffset:offset completed:^(id responseObject, NSError *error)
    {
        [refreshControl endRefreshing];
        [self stopLoadingAnimationInView:self.view];
        
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
//            [appDelegate trackTheEventWithName:CEventDoctorUnfavouriteClicked properties:@{@"number_of_results":[NSNumber numberWithInteger:10]}];
            
            if (offset == 0) [arrMyFavourite removeAllObjects];
            
            NSArray *arrResponseData = [responseObject valueForKey:CJsonData];
            
            if (arrResponseData.count > 0)
            {
                [arrMyFavourite addObjectsFromArray:arrResponseData];
                iOffset = [[responseObject valueForKey:CJsonMeta] intForKey:CJsonNewOffset];
                [tblVMyFavourite reloadData];
            }
        }
        
        if (arrMyFavourite.count == 0)
        {
            [lblNoData setHidden:NO];
            [tblVMyFavourite reloadData];
            [tblVMyFavourite setHidden:YES];
        }
        else
        {
            [lblNoData setHidden:YES];
            [tblVMyFavourite setHidden:NO];
        }
        
    }];
}







# pragma mark
# pragma mark - UITable view delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrMyFavourite.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MIAddressBookTableViewCell";
    MIAddressBookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dictMyFavourite = arrMyFavourite[indexPath.row];
    [cell.imgVFavourite setHidden:YES];
    
    cell.lblTitle.text = [dictMyFavourite stringValueForJSON:@"doctor_name"];
    [cell.btnFavourite setSelected:[dictMyFavourite booleanForKey:@"is_favourite"]];
    [cell.btnFavourite setImage:[UIImage imageNamed:@"unselect_star"] forState:UIControlStateNormal];
    [cell.btnFavourite setImage:[UIImage imageNamed:@"selected_star"] forState:UIControlStateSelected];
    
    [cell configureStatus:[dictMyFavourite integerForKey:@"availability_status"]];
    
    [cell.lblStatusUpdatedTime setText:([dictMyFavourite intForKey:@"availability_status"] == 4)?@"":([[dictMyFavourite stringValueForJSON:@"updated_status_timestamp"] isBlankValidationPassed] ? [NSDate timeAgoFromTimestamp:[dictMyFavourite stringValueForJSON:@"updated_status_timestamp"]] : CNotAvailable)];
    
    
    [cell.vDistance setHidden:YES];
    [cell.btnFavourite touchUpInsideClicked:^{
        
        //..... UNFAVOURITE DOCTOR
        
        if (sessionDataTask &&  sessionDataTask.state == NSURLSessionTaskStateRunning)
            [sessionDataTask cancel];
        
        int status = [[dictMyFavourite numberForJson:@"is_favourite"] isEqual:@1] ? 0 : 1;
        
        cell.btnFavourite.selected = !cell.btnFavourite.selected;
        
        NSMutableDictionary *dictReplace = [dictMyFavourite mutableCopy];
        
        if ([[dictReplace numberForJson:@"is_favourite"] isEqual:@1])
        {
            //..... Unfavourite
            
            [dictReplace setValue:@0 forKey:@"is_favourite"];
            [arrMyFavourite replaceObjectAtIndex:indexPath.row withObject:dictReplace];
        }
        else
        {
            //..... Favourite
            
            [dictReplace setValue:@1 forKey:@"is_favourite"];
            [arrMyFavourite replaceObjectAtIndex:indexPath.row withObject:dictReplace];
        }
        
        [cell.btnFavourite setSelected:[dictMyFavourite booleanForKey:@"is_favourite"]];
        [tblVMyFavourite reloadData];
        
        sessionDataTask =  [[APIRequest request] favouriteUnFavouriteDoctorWithStatus:status andDoctorID:[dictMyFavourite stringValueForJSON:@"doctor_id"] completed:nil];
        
    }];
    
    
    //..... LOAD MORE DATA
    
    if (indexPath.row == (arrMyFavourite.count - 1))
        [self getFavouriteListFromServer];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([AFNetworkReachabilityManager sharedManager].reachable)
    {
        MIDoctorDetailViewController *doctorDetails = [[MIDoctorDetailViewController alloc] initWithNibName:@"MIDoctorDetailViewController" bundle:nil];
        doctorDetails.dictDoctor = arrMyFavourite [indexPath.row];
        [self.navigationController pushViewController:doctorDetails animated:YES];
    }
}

@end
