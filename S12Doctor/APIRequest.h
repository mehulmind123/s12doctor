//
//  APIRequest.h
//  EdSmart
//
//  Created by S12 Solutions on 08/03/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//





/*===========================================================
 ===========================================================*/

//********** Server

//#define BASEURL       @"http://192.168.1.30:8000/api/"
//#define BASEURL       @"https://www.itrainacademy.in/doctorApp/api/"


#define BASEURL         @"https://s12solutions.com/application/api/"              //..... LIVE URL

//..... Don't forget to change this URL when update BASEURL just replace all string before "api/"
#define BASEURLAMHP     @"https://s12solutions.com/application/"





//********** Request, Response Constant

#define CVersion    @"1"
#define COffset     @0
#define CLimit      @20

#define CJsonStatus             @"status"
#define CJsonMeta               @"meta"
#define CJsonMessage            @"message"
#define CJsonToken              @"token"
#define CJsonTitle              @"title"
#define CJsonData               @"data"
#define CJsonResponse           @"response"
#define CJsonNewTimestamp       @"new-timestamp"
#define CJsonNewOffset          @"new_offset"

#define CStatusZero             0
#define CStatusOne              1
#define CStatusFive             5
#define CStatusNine             9
#define CStatusTen              10
#define CStatus200              200
#define CStatus500              500
#define CStatus555              555




//********** API Tag


typedef void(^SocialUserInfoBlock)(id userInfo, NSError *error);



/*===========================================================
 ===========================================================*/



#import <Foundation/Foundation.h>


@interface APIRequest : NSObject

+ (id)request;

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler;

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task;

- (BOOL)isJSONDataValidWithResponse:(id)response;

- (BOOL)isJSONStatusValidWithResponse:(id)response;



# pragma mark
# pragma mark - API TAG

//=========================== S12 ===========================


//.....LRF

#define CAPITagLogin                                    @"login"
#define CAPITagForgotPassword                           @"forgot-password"
#define CAPITagChangePassword                           @"change-password"
#define CAPITagAcceptTermsCondition                     @"s12-accept-term-condition"
#define CAPITagVerifyPassword                           @"verify-password"
#define CAPITagUserLatLong                              @"user-lat-lng"
#define CAPITagUserDetails                              @"user-details"


//.....CMS

#define CAPITagCMS                                      @"cms"


//.....HOME

#define CAPITagChangeAvailabilityStatus                 @"change-availability-status"
#define CAPITagGetWeeklyAvailability                    @"get-weekly-availability"
#define CAPITagChangeWeeklyAvailability                 @"change-weekly-availability"


//.....CLAIM FORM

#define CAPITagLocalAuthorityList                       @"local-authority-list"
#define CAPITagAMHPList                                 @"amhp-list"
#define CAPITagAssessmentLocationTypeList               @"assessment-location-type-list"
#define CAPITagSectionImplementedList                   @"section-implement-list"
#define CAPITagCreateClaimForm                          @"create-claim-form"
#define CAPITagSaveClaimForm                            @"save-claim-form"
#define CAPITagDeleteDraft                              @"delete-claim-form"



//.....SIDE MENU

#define CAPITagUsefulInformation                        @"s12-useful-information"
#define CAPITagRegionList                               @"region-list"
#define CAPITagEditProfile                              @"s12-edit-profile"

#define CAPITagEmployerList                             @"organisation-list"
#define CAPITagLanguageList                             @"language-list"
#define CAPITagSpecialtiesList                          @"specialties-list"
#define CAPITagAddNewLanguage                           @"add-language"
#define CAPITagSettings                                 @"setting"
#define CAPITagDraftList                                @"draft-list"
#define CAPITagCompletedClaimForms                      @"completed-claim-forms"
#define CAPITagViewClaimForm                            @"view-claim-form"



//=========================== AMHP ===========================

//..... AMHP HOME

#define CAPITagAMHPHome                                 @"amhp-home"
#define CAPITagFavouriteUnfavouriteDoctor               @"favourite-unfavourite-doctor"
#define CAPITagContactDoctor                            @"contact-doctor"


//..... AMHP SIDE MENU

#define CAPITagAMHPUsefulInformation                    @"amhp-useful-information"
#define CAPITagMyFavourite                              @"my-favourite"
#define CAPITagDoctorDetail                             @"doctor-detail"
#define CAPITagClaimFormForApproval                     @"claim-form-for-approval"
#define CAPITagAMHPViewClaimForm                        @"view-claim-form"
#define CAPITagCCGList                                  @"ccg-list"
#define CAPITagAcceptRejectClaimForm                    @"accept-reject-claim-form"
#define CAPITagAMHPEditProfile                          @"amhp-edit-profile"







//..... API PARAMETERS

#define CParameterLoginType                             @"login_type"





# pragma mark
# pragma mark - Location

- (NSURLSessionDataTask*)LoadGooglePlacesList:(NSString *)strSearchText completed:(void (^)(id responseObject,NSError *error))completion;





# pragma mark
# pragma mark - LRF

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password completed:(void (^)(id responseObject, NSError *error))completion;

- (void)forgotPasswordWithEmail:(NSString *)email completed:(void (^)(id responseObject, NSError *error))completion;

- (void)changePasswordWithOldPassword:(NSString *)oldPassword andNewPassword:(NSString *)newPassword andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)acceptTermsAndCondition:(void (^)(id responseObject, NSError *error))completion;

- (void)verifyPassword:(NSString *)Password andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)userLatLong:(void (^)(id responseObject, NSError *error))completion;

- (void)userDetails:(void (^)(id responseObject, NSError *error))completion;






# pragma mark
# pragma mark - CMS

- (void)cmsWithType:(int)cmsType completed:(void (^)(id responseObject, NSError *error))completion;






# pragma mark
# pragma mark - Home

- (void)changeAvailabilityStatusWithStatus:(int)availabilityStatus andAvailabilityMessage:(NSString *)availabilityMessage andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)getWeeklyAvailability:(void (^)(id responseObject, NSError *error))completion;

- (void)changeWeeklyAvailability:(NSDictionary *)dictParam completed:(void (^)(id responseObject, NSError *error))completion;





# pragma mark
# pragma mark - CLAIM FORM

- (void)localAuthorityListWithLoginType:(int)loginType andType:(NSString *)type andRegionID:(NSString *)regionID completed:(void (^)(id responseObject, NSError *error))completion;

- (void)amhpListWithLoginType:(int)loginType andLocalAuthorityID:(NSString *)localAuthorityID completed:(void (^)(id responseObject, NSError *error))completion;

- (void)assessmentLocationTypeListWithLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)sectionImplementedListWithLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask *)createClaimFormWithLocalAuthority:(NSString *)localAuthority andAMHP:(NSString *)amhp andDoctorName:(NSString *)doctorName andDoctorAddress:(NSString *)doctorAddress andAssessmentLocationType:(NSString *)assessmentLocationType andAssessmentPostcode:(NSString *)assessmentPostcode andAssessmentDateTime:(NSString *)assessmentDateTime andPatientNHSNumber:(NSString *)patientNHSNumber andPatientPostcode:(NSString *)patientPostcode andAdditionalInfo:(NSString *)additionalInfo andSectionImplemented:(NSString *)sectionImplemented andCarMake:(NSString *)carMake andCarModel:(NSString *)carModel andCarRegistrationPlate:(NSString *)carRegistrationPlate andEngineSize:(NSString *)engineSize andNotes:(NSString *)notes andDisclaimer:(NSString *)disclaimer andLocalAuthorityID:(NSString *)LocalAuthorityID andAMHPID:(NSString *)amhpID andLoginType:(int)loginType andAssessmentLocatinTypeDescription:(NSString *)assessmentLocationTypeDescription andSelectSectionDescription:(NSString *)sectionDescription andClaimFormID:(NSString *)claimFormID andFromPostCode:(NSString *)fromPostCode andToPostCode:(NSString *)toPostCode andNumberOfMiles:(NSString *)numberOfMiles andFormType:(int)formType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)deleteDraftWithClaimID:(NSString *)claimID completed:(void (^)(id responseObject, NSError *error))completion;



# pragma mark
# pragma mark - SIDE MENU

- (void)usefulInformationWithOffset:(int)offset andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)getRegionList:(void (^)(id responseObject, NSError *error))completion;

- (void)editProfileWithName:(NSString *)name andGMCReferenceNumber:(NSString *)gmcReferenceNumber andEmployer:(NSString *)employer andLocalAuthority:(NSString *)localAuthority andGeneralAvailability:(NSString *)generalAvailability andMobileNumber:(NSString *)mobileNumber andLandlineNumber:(NSString *)landlineNumber andOfficeBasePostcode:(NSString *)officeBasePostcode andOfficeBaseTeam:(NSString *)officeBaseTeam andLanguageSpoken:(NSString *)languageSpoken andSpecialties:(NSString *)specialties andDefaultClaimFormAddress:(NSString *)defaultClaimFormAddress andCarMake:(NSString *)carMake andCarModel:(NSString *)carModel andCarRegistrationPlate:(NSString *)carRegistrationPlate andEngineSize:(NSString *)engineSize andLoginType:(int)loginType andLanguageName:(NSString *)languageName andRegionID:(NSString *)regionID completed:(void (^)(id responseObject, NSError *error))completion;

- (void)employerList:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)languageList:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)specialtiesList:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)addNewLanguageWithLanguageName:(NSString *)languageName andLoginType:(int)loginType completion:(void (^)(id responseObject, NSError *error))completion;

- (void)settingsWithNotification:(int)notificationReminder andLoginType:(int)loginType completion:(void (^)(id responseObject, NSError *error))completion;

- (void)draftList:(int)offset andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask *)completedClaimFormsWithFormType:(int)formType Offset:(int)offset andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)viewClaimFormWithFormID:(NSString *)formID andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion;







# pragma mark
# pragma mark - ****************************** AMHP ******************************



# pragma mark
# pragma mark - AMHP HOME

- (NSURLSessionDataTask *)amhpHomeWithType:(int)type andOffset:(int)offset andSearchTxt:(NSString *)searchTxt andFilterAvailability:(NSString *)filterAvailability andFilterLocalAuthority:(int)filterLocalAuthority andFilterDistance:(int)filterDistance andFilterEmployer:(int)filterEmployer andFilterSpecialty:(int)filterSpecialty andFilterGender:(int)filterGender andFilterLanguage:(int)filterLanguage andSortbyAvailability:(int)sortbyAvailability andSortbyEmployer:(int)sortbyEmployer andSortbyDistance:(int)sortbyDistance andSortbyName:(int)sortbyName andLatitude:(double)latitude andlongitude:(double)longitude andWeekDays:(NSString *)weekDays andTimeSlot:(NSString *)timeSlot completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask *)favouriteUnFavouriteDoctorWithStatus:(int)favouriteStatus andDoctorID:(NSString *)doctorID completed:(void (^)(id responseObject, NSError *error))completion;

- (void)contactDoctorWithDoctorID:(NSString *)doctorID andContactType:(int)contactType completed:(void (^)(id responseObject, NSError *error))completion;





# pragma mark
# pragma mark - AMHP SIDE MENU

- (void)amhpUsefulInformation:(void (^)(id responseObject, NSError *error))completion;

- (void)myFavouriteListWithOffset:(int)offset completed:(void (^)(id responseObject, NSError *error))completion;

- (void)doctorDetailWithDoctorID:(NSString *)doctorID completed:(void (^)(id responseObject, NSError *error))completion;

- (void)claimFormApprovalListWithOffset:(int)offset completed:(void (^)(id responseObject, NSError *error))completion;

- (void)ccgListWithLocalAuthorityID:(NSString *)localAuthorityID completed:(void (^)(id responseObject, NSError *error))completion;

- (void)acceptRejectClaimFormWithFormID:(NSString *)formID andStatus:(int)status andCCGID:(NSString *)ccgID andRejectionReason:(NSString *)rejectionReason completed:(void (^)(id responseObject, NSError *error))completion;

- (void)editProfileWithName:(NSString *)name andMobileNumber:(NSString *)mobileNumber andEmail:(NSString *)email andLocalAuthority:(NSString *)localAuthority completed:(void (^)(id responseObject, NSError *error))completion;







# pragma mark
# pragma mark - Save To Coredata

- (void)saveLoginUserToLocal:(NSDictionary *)dictData;

- (void)saveS12UsefulInfo:(NSArray *)arrUsefulInfo;

- (void)saveAddressBookData:(NSArray *)arrAddressBook andType:(BOOL)isAddress;

- (void)saveLocationSearchData:(NSArray *)arrLocationSearch;


@end
