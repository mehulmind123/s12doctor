//
//  APIRequest.m
//  EdSmart
//
//  Created by S12 Solutions on 08/03/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "APIRequest.h"
#import "MIAFNetworking.h"

static APIRequest *request = nil;

@interface APIRequest()
{
    SocialUserInfoBlock uInfoBlock;
    
    UIViewController *currentVC;
    
    BOOL isAPIErrorAlertDisplaying;
}
@end


@implementation APIRequest

+ (id)request
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        request = [[APIRequest alloc] init];
        [[MIAFNetworking sharedInstance] setBaseURL:BASEURL];
        
        NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:[[[[MIAFNetworking sharedInstance] sessionManager] responseSerializer] acceptableStatusCodes]];
        
        [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:@"application/x.api.v2+json" forHTTPHeaderField:@"Accept"];
        
        [indexSet addIndex:400];
        [indexSet addIndex:401];
        [indexSet addIndex:555];
        [indexSet addIndex:556];
        [indexSet addIndex:550];
        
        [[[MIAFNetworking sharedInstance] sessionManager] responseSerializer].acceptableStatusCodes = indexSet;
    });
    
    if ([CUserDefaults valueForKey:UserDefaultLoginToken])
        [[[MIAFNetworking sharedInstance] sessionManager].requestSerializer setValue:[CUserDefaults valueForKey:UserDefaultLoginToken] forHTTPHeaderField:@"Authorization"];
    else
        [[[MIAFNetworking sharedInstance] sessionManager].requestSerializer setValue:@"" forHTTPHeaderField:@"Authorization"];
    
    return request;
}





# pragma mark
# pragma mark - Check API Response

- (void)checkResponseStatus:(id)responseObject
{
    switch ([responseObject intForKey:CJsonStatus])
    {
        case CStatusTen:
        {
            //...Inactive User
            if ([UIApplication userId])
            {
                [appDelegate logoutUser];
                [CustomAlertView iOSAlert:@"Inactive User" withMessage:nil onView:[UIApplication topMostController]];
            }
            break;
        }
        case CStatusNine:
        {
            //...Under Maintenance
        }
        default:
            break;
    }
}

- (void)checkLoginUserStatus:(NSString *)message
{
    if ([UIApplication userId])
    {
        [appDelegate logoutUser];
        [CustomAlertView iOSAlert:@"" withMessage:message onView:[UIApplication topMostController]];
    }
}

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler
{
    [[MILoader sharedInstance] stopAnimation];
    
    NSLog(@"%@ API Error == %@", api, error);
    
    if (error.code != -999 && !isAPIErrorAlertDisplaying)
    {
        isAPIErrorAlertDisplaying = YES;
        
        [[UIApplication topMostController] alertWithAPIErrorTitle:@"ERROR!" message:@"An error has occured. Please check your network connection or try again." handler:^(NSInteger index, NSString *btnTitle)
        {
            isAPIErrorAlertDisplaying = NO;
            if (handler) handler(index == 0);
        }];
    }
}

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task
{
    return ((NSHTTPURLResponse *)task.response).statusCode == 200 ? nil : [NSError errorWithDomain:@"" code:((NSHTTPURLResponse *)task.response).statusCode userInfo:nil];
}

- (BOOL)isJSONDataValidWithResponse:(id)response
{
    id data = [response valueForKey:CJsonData];
    
    if (!data) {
        return NO;
    }
    
    if ([data isEqual:[NSNull null]]) {
        return NO;
    }
    
    if ([data isKindOfClass:[NSString class]]) {
        if (((NSString *)data).length == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSArray class]]) {
        if (((NSArray *)data).count == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        if (((NSDictionary *)data).count == 0)
            return NO;
    }
    
    return [self isJSONStatusValidWithResponse:response];
}

- (BOOL)isJSONStatusValidWithResponse:(id)response
{
    if (!response)
        return NO;
    
    if ([response intForKey:@"status_code"] == CStatus555)
    {
        [self checkLoginUserStatus:[response stringValueForJSON:CJsonMessage]];
        return NO;
    }
    
    return [[response valueForKey:CJsonMeta] intForKey:CJsonStatus] == CStatus200;
}





# pragma mark
# pragma mark - Location

- (NSURLSessionDataTask*)LoadGooglePlacesList:(NSString *)strSearchText completed:(void (^)(id responseObject,NSError *error))completion
{
    NSString *URL = @"https://maps.googleapis.com/maps/api/";
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager] initWithBaseURL:[NSURL URLWithString:URL]];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    return [manager GET:@"place/autocomplete/json?"
                    parameters:@{
                                @"input": strSearchText,
                                @"sensor": @"",
                                @"key":GooglePlaceKey
                                }
            success:^(NSURLSessionDataTask *task, id responseObject)
            {
                if (completion)
                    completion(responseObject, nil);
            }
            failure:^(NSURLSessionDataTask *task, NSError *error)
            {   
                if (completion)
                    completion(nil, error);
            }];
}




# pragma mark
# pragma mark - LRF

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"login_type":[appDelegate.isFromS12 isEqual:@1] ? @1 : @2,
                                @"email":email,
                                @"password":password
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagLogin parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagLogin andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self loginWithEmail:email andPassword:password completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)forgotPasswordWithEmail:(NSString *)email completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"email":email,
                                @"login_type":[appDelegate.isFromS12 isEqual:@1] ? @1 : @2};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagForgotPassword parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagForgotPassword andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self forgotPasswordWithEmail:email completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)changePasswordWithOldPassword:(NSString *)oldPassword andNewPassword:(NSString *)newPassword andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"old_password":oldPassword,
                                @"new_password":newPassword,
                                @"login_type":[NSNumber numberWithInt:loginType]
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagChangePassword parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagChangePassword andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self changePasswordWithOldPassword:oldPassword andNewPassword:newPassword andLoginType:loginType completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)acceptTermsAndCondition:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"login_type":[appDelegate.isFromS12 isEqual:@1] ? @1 : @2};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagAcceptTermsCondition parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagAcceptTermsCondition andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self acceptTermsAndCondition:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)verifyPassword:(NSString *)Password andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"password":Password,
                                @"login_type":[NSNumber numberWithInt:loginType]
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagVerifyPassword parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagVerifyPassword andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self verifyPassword:Password andLoginType:loginType completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)userLatLong:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"latitude":[NSNumber numberWithDouble:appDelegate.current_lat],
                                @"longitude":[NSNumber numberWithDouble:appDelegate.current_long],
                                @"login_type":[appDelegate.isFromS12 isEqual:@1] ? @1 : @2
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagUserLatLong parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"location updated successfully");
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)userDetails:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[appDelegate.isFromS12 isEqual:@1] ? [NSNumber numberWithInt:appDelegate.loginUser.login_type] : [NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagUserDetails parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}






# pragma mark
# pragma mark - CMS

- (void)cmsWithType:(int)cmsType completed:(void (^)(id responseObject, NSError *error))completion
{
//    NSDictionary *dictParam = @{@"cms_type":[NSNumber numberWithInt:cmsType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagCMS parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}





# pragma mark
# pragma mark - S12 HOME

- (void)changeAvailabilityStatusWithStatus:(int)availabilityStatus andAvailabilityMessage:(NSString *)availabilityMessage andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"availability_status":[NSNumber numberWithInt:availabilityStatus],
                                @"availability_message":availabilityMessage ? availabilityMessage : @"",
                                @"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagChangeAvailabilityStatus parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)getWeeklyAvailability:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[NSNumber numberWithInt:appDelegate.loginUser.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagGetWeeklyAvailability parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)changeWeeklyAvailability:(NSDictionary *)dictParam completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagChangeWeeklyAvailability parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}







# pragma mark
# pragma mark - S12 CLAIM FORM

- (void)localAuthorityListWithLoginType:(int)loginType andType:(NSString *)type andRegionID:(NSString *)regionID completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam;
    
    if ([type isEqualToString:CLAAll])
    {
        dictParam = @{@"login_type":[NSNumber numberWithInt:loginType],
                      @"region_id":regionID ? regionID : @"",
                      @"type":type};
    }
    else
    {
        dictParam = @{@"login_type":[NSNumber numberWithInt:loginType],
                      @"type":type};
    }
    
    [[MIAFNetworking sharedInstance] POST:CAPITagLocalAuthorityList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)amhpListWithLoginType:(int)loginType andLocalAuthorityID:(NSString *)localAuthorityID completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[NSNumber numberWithInt:loginType],
                                @"local_authority_id":localAuthorityID};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagAMHPList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)assessmentLocationTypeListWithLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagAssessmentLocationTypeList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)sectionImplementedListWithLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagSectionImplementedList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (NSURLSessionDataTask *)createClaimFormWithLocalAuthority:(NSString *)localAuthority andAMHP:(NSString *)amhp andDoctorName:(NSString *)doctorName andDoctorAddress:(NSString *)doctorAddress andAssessmentLocationType:(NSString *)assessmentLocationType andAssessmentPostcode:(NSString *)assessmentPostcode andAssessmentDateTime:(NSString *)assessmentDateTime andPatientNHSNumber:(NSString *)patientNHSNumber andPatientPostcode:(NSString *)patientPostcode andAdditionalInfo:(NSString *)additionalInfo andSectionImplemented:(NSString *)sectionImplemented andCarMake:(NSString *)carMake andCarModel:(NSString *)carModel andCarRegistrationPlate:(NSString *)carRegistrationPlate andEngineSize:(NSString *)engineSize andNotes:(NSString *)notes andDisclaimer:(NSString *)disclaimer andLocalAuthorityID:(NSString *)LocalAuthorityID andAMHPID:(NSString *)amhpID andLoginType:(int)loginType andAssessmentLocatinTypeDescription:(NSString *)assessmentLocationTypeDescription andSelectSectionDescription:(NSString *)sectionDescription andClaimFormID:(NSString *)claimFormID andFromPostCode:(NSString *)fromPostCode andToPostCode:(NSString *)toPostCode andNumberOfMiles:(NSString *)numberOfMiles andFormType:(int)formType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"local_authority_name":localAuthority?:@"",
                                @"amhp_name":amhp?:@"",
                                @"doctor_name":doctorName?:@"",
                                @"doctor_address":doctorAddress?:@"",
                                @"assessment_location_type":assessmentLocationType?:@"",
                                @"assessment_postcode":assessmentPostcode?:@"",
                                @"assessment_date_time":assessmentDateTime?:@"",
                                @"patient_nhs_number":patientNHSNumber?:@"",
                                @"patient_postcode":patientPostcode?:@"",
                                @"additional_information":additionalInfo,
                                @"section_implemented":sectionImplemented?:@"",
                                @"car_make":carMake?:@"",
                                @"car_model":carModel?:@"",
                                @"car_registration_plate":carRegistrationPlate?:@"",
                                @"engine_size":engineSize?:@"",
                                @"miles_traveled":numberOfMiles?:@"",
                                @"notes":notes?:@"",
                                @"disclaimer":disclaimer?:@"",
                                @"authority_id":LocalAuthorityID?:@"",
                                @"amhp_id":amhpID?:@"",
                                @"login_type":[NSNumber numberWithInt:loginType],
                                @"assessment_location_type_description":assessmentLocationTypeDescription?:@"",
                                @"section_type_description":sectionDescription?:@"",
                                @"claim_id":claimFormID?:@"",
                                @"from_postcode":fromPostCode?:@"",
                                @"to_postcode":toPostCode?:@"",
                                @"claim_type":[NSNumber numberWithInt:formType]
                                };
    
    NSLog(@"%@", dictParam);
    
    return [[MIAFNetworking sharedInstance] POST:CAPITagCreateClaimForm parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (formType == 2)
         {
             [self failureWithAPI:CAPITagCreateClaimForm andError:error andHandler:^(BOOL retry)
              {
                  if (retry) [self createClaimFormWithLocalAuthority:localAuthority andAMHP:amhp andDoctorName:doctorName andDoctorAddress:doctorAddress andAssessmentLocationType:assessmentLocationType andAssessmentPostcode:assessmentPostcode andAssessmentDateTime:assessmentDateTime andPatientNHSNumber:patientNHSNumber andPatientPostcode:patientPostcode andAdditionalInfo:additionalInfo andSectionImplemented:sectionImplemented andCarMake:carMake andCarModel:carModel andCarRegistrationPlate:carRegistrationPlate andEngineSize:engineSize andNotes:notes andDisclaimer:disclaimer andLocalAuthorityID:LocalAuthorityID andAMHPID:amhpID andLoginType:loginType andAssessmentLocatinTypeDescription:assessmentLocationTypeDescription andSelectSectionDescription:sectionDescription andClaimFormID:claimFormID andFromPostCode:fromPostCode andToPostCode:toPostCode andNumberOfMiles:numberOfMiles andFormType:formType completed:completion];
              }];
         }
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)saveClaimFormWithLocalAuthority:(NSString *)localAuthority andAMHP:(NSString *)amhp andDoctorName:(NSString *)doctorName andAssessmentLocationType:(NSString *)assessmentLocationType andAssessmentLocation:(NSString *)assessmentLocation andAssessmentPostcode:(NSString *)assessmentPostcode andAssessmentDateTime:(NSString *)assessmentDateTime andPatientNHSNumber:(NSString *)patientNHSNumber andPatientPostcode:(NSString *)patientPostcode andSectionImplemented:(NSString *)sectionImplemented andCarMake:(NSString *)carMake andCarModel:(NSString *)carModel andCarRegistrationPlate:(NSString *)carRegistrationPlate andEngineSize:(NSString *)engineSize andMilesTraveled:(NSString *)milesTraveled andNotes:(NSString *)notes andDisclaimer:(NSString *)disclaimer andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"local_authority":localAuthority,
                                @"AMHP":amhp,
                                @"doctor_name":doctorName,
                                @"assessment_location_type":assessmentLocationType,
                                @"assessment_location":assessmentLocation,
                                @"assessment_postcode":assessmentPostcode,
                                @"assessment_date_time":assessmentDateTime,
                                @"patient_nhs_number":patientNHSNumber,
                                @"patient_postcode":patientPostcode,
                                @"section_implemented":sectionImplemented,
                                @"car_make":carMake,
                                @"car_model":carModel,
                                @"car_registration_plate":carRegistrationPlate,
                                @"engine_size":engineSize,
                                @"miles_traveled":milesTraveled,
                                @"notes":notes,
                                @"disclaimer":disclaimer,
                                @"login_type":[NSNumber numberWithInt:loginType]
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagSaveClaimForm parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagSaveClaimForm andError:error andHandler:^(BOOL retry)
          {
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)deleteDraftWithClaimID:(NSString *)claimID completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"claim_id":claimID,
                                @"login_type":[NSNumber numberWithInteger:appDelegate.loginUser.login_type]};
    
    [[MILoader sharedInstance] startAnimation];
    [[MIAFNetworking sharedInstance] POST:CAPITagDeleteDraft parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagDeleteDraft andError:error andHandler:^(BOOL retry) {
             if (retry) [self deleteDraftWithClaimID:claimID completed:completion];
         }];
         
         if (completion)
             completion(nil, error);
     }];
}






# pragma mark
# pragma mark - S12 SIDE MENU

- (void)usefulInformationWithOffset:(int)offset andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"offset":[NSNumber numberWithInt:offset],
                                @"limit":CLimit,
                                @"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagUsefulInformation parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)getRegionList:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUser.login_type]
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagRegionList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
//         [self failureWithAPI:CAPITagEditProfile andError:error andHandler:^(BOOL retry)
//          {
//              if (retry) [self getRegionList:completion];
//          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)editProfileWithName:(NSString *)name andGMCReferenceNumber:(NSString *)gmcReferenceNumber andEmployer:(NSString *)employer andLocalAuthority:(NSString *)localAuthority andGeneralAvailability:(NSString *)generalAvailability andMobileNumber:(NSString *)mobileNumber andLandlineNumber:(NSString *)landlineNumber andOfficeBasePostcode:(NSString *)officeBasePostcode andOfficeBaseTeam:(NSString *)officeBaseTeam andLanguageSpoken:(NSString *)languageSpoken andSpecialties:(NSString *)specialties andDefaultClaimFormAddress:(NSString *)defaultClaimFormAddress andCarMake:(NSString *)carMake andCarModel:(NSString *)carModel andCarRegistrationPlate:(NSString *)carRegistrationPlate andEngineSize:(NSString *)engineSize andLoginType:(int)loginType andLanguageName:(NSString *)languageName andRegionID:(NSString *)regionID completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"name":name,
                                @"gmc_referance_number":gmcReferenceNumber,
                                @"employer":employer,
                                @"local_authorities":localAuthority,
                                @"general_availability":generalAvailability,
                                @"mobile_number":mobileNumber,
                                @"landline_number":landlineNumber,
                                @"office_base_postcode":officeBasePostcode,
                                @"office_base_team":officeBaseTeam,
                                @"language_spoken":languageSpoken,
                                @"specialties":specialties,
                                @"default_claim_form_address":defaultClaimFormAddress,
                                @"car_make":carMake,
                                @"car_model":carModel,
                                @"car_registration_plate":carRegistrationPlate,
                                @"engine_size":engineSize,
                                @"login_type":[NSNumber numberWithInt:loginType],
                                @"language_name":languageName,
                                @"region_id":regionID
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagEditProfile parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagEditProfile andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self editProfileWithName:name andGMCReferenceNumber:gmcReferenceNumber andEmployer:employer andLocalAuthority:localAuthority andGeneralAvailability:generalAvailability andMobileNumber:mobileNumber andLandlineNumber:landlineNumber andOfficeBasePostcode:officeBasePostcode andOfficeBaseTeam:officeBaseTeam andLanguageSpoken:languageSpoken andSpecialties:specialties andDefaultClaimFormAddress:defaultClaimFormAddress andCarMake:carMake andCarModel:carModel andCarRegistrationPlate:carRegistrationPlate andEngineSize:engineSize andLoginType:loginType andLanguageName:languageName andRegionID:regionID completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)employerList:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagEmployerList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagEmployerList andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self employerList:loginType completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)languageList:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagLanguageList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)specialtiesList:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagSpecialtiesList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)addNewLanguageWithLanguageName:(NSString *)languageName andLoginType:(int)loginType completion:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"language_name":languageName,
                                @"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagAddNewLanguage parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)settingsWithNotification:(int)notificationReminder andLoginType:(int)loginType completion:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{
                                @"notification_reminder":[NSNumber numberWithInt:notificationReminder],
                                @"login_type":[NSNumber numberWithInt:loginType]
                                };
    
    [[MILoader sharedInstance] startAnimation];
    
    [[MIAFNetworking sharedInstance] POST:CAPITagSettings parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagSettings andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self settingsWithNotification:notificationReminder andLoginType:loginType completion:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)draftList:(int)offset andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"offset":[NSNumber numberWithInt:offset],
                                @"limit":CLimit,
                                @"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagDraftList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagDraftList andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self draftList:offset andLoginType:loginType completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (NSURLSessionDataTask *)completedClaimFormsWithFormType:(int)formType Offset:(int)offset andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"form_type":[NSNumber numberWithInt:formType],
                                @"offset":[NSNumber numberWithInt:offset],
                                @"limit":CLimit,
                                @"login_type":[NSNumber numberWithInt:loginType]};
    
    return [[MIAFNetworking sharedInstance] POST:CAPITagCompletedClaimForms parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)viewClaimFormWithFormID:(NSString *)formID andLoginType:(int)loginType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"claim_id":formID,
                                CParameterLoginType:[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagViewClaimForm parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagAcceptRejectClaimForm andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self viewClaimFormWithFormID:formID andLoginType:loginType completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}





# pragma mark
# pragma mark - ****************************** AMHP ******************************



# pragma mark
# pragma mark - AMHP HOME

- (NSURLSessionDataTask *)amhpHomeWithType:(int)type andOffset:(int)offset andSearchTxt:(NSString *)searchTxt andFilterAvailability:(NSString *)filterAvailability andFilterLocalAuthority:(int)filterLocalAuthority andFilterDistance:(int)filterDistance andFilterEmployer:(int)filterEmployer andFilterSpecialty:(int)filterSpecialty andFilterGender:(int)filterGender andFilterLanguage:(int)filterLanguage andSortbyAvailability:(int)sortbyAvailability andSortbyEmployer:(int)sortbyEmployer andSortbyDistance:(int)sortbyDistance andSortbyName:(int)sortbyName andLatitude:(double)latitude andlongitude:(double)longitude andWeekDays:(NSString *)weekDays andTimeSlot:(NSString *)timeSlot completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"type":[NSNumber numberWithInt:type],
                                @"offset":[NSNumber numberWithInt:offset],
                                @"limit":CLimit,
                                @"search_text":searchTxt,
                                @"filter_availability":filterAvailability,
                                @"filter_local_authority":[NSNumber numberWithInt:filterLocalAuthority],
                                @"filter_distance":[NSNumber numberWithInt:filterDistance],
                                @"filter_employer":[NSNumber numberWithInt:filterEmployer],
                                @"filter_specialty":[NSNumber numberWithInt:filterSpecialty],
                                @"filter_gender":[NSNumber numberWithInt:filterGender],
                                @"filter_language":[NSNumber numberWithInt:filterLanguage],
                                @"sortby_availability":[NSNumber numberWithInt:sortbyAvailability],
                                @"sortby_employer":[NSNumber numberWithInt:sortbyEmployer],
                                @"sortby_distance":[NSNumber numberWithInt:sortbyDistance],
                                @"sortby_name":[NSNumber numberWithInt:sortbyName],
                                @"latitude":[NSNumber numberWithDouble:latitude],
                                @"longitude":[NSNumber numberWithDouble:longitude],
                                @"week_day":weekDays,
                                @"time_slot":timeSlot,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]
                                };
    
    return [[MIAFNetworking sharedInstance] POST:CAPITagAMHPHome parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
//         [self failureWithAPI:CAPITagAMHPHome andError:error andHandler:^(BOOL retry)
//          {
//              if (retry) [self amhpHomeWithType:type andOffset:offset andSearchTxt:searchTxt andFilterAvailability:filterAvailability andFilterLocalAuthority:filterLocalAuthority andFilterDistance:filterDistance andFilterEmployer:filterEmployer andFilterSpecialty:filterSpecialty andFilterGender:filterGender andFilterLanguage:filterLanguage andSortbyAvailability:sortbyAvailability andSortbyEmployer:sortbyEmployer andSortbyDistance:sortbyDistance andSortbyName:sortbyName andLatitude:latitude andlongitude:longitude completed:completion];
//          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (NSURLSessionDataTask *)favouriteUnFavouriteDoctorWithStatus:(int)favouriteStatus andDoctorID:(NSString *)doctorID completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"favourite_status":[NSNumber numberWithInt:favouriteStatus],
                                @"doctor_id":doctorID,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    return [[MIAFNetworking sharedInstance] POST:CAPITagFavouriteUnfavouriteDoctor parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)contactDoctorWithDoctorID:(NSString *)doctorID andContactType:(int)contactType completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"doctor_id":doctorID,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type],
                                @"contact_type":[NSNumber numberWithInt:contactType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagContactDoctor parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
            {
                if (completion)
                    completion(responseObject, nil);
                
            } failure:^(NSURLSessionDataTask *task, NSError *error)
            {
                if (completion)
                    completion(nil, error);
            }];
}






# pragma mark
# pragma mark - AMHP SIDE MENU

- (void)amhpUsefulInformation:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagAMHPUsefulInformation parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)myFavouriteListWithOffset:(int)offset completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"offset":[NSNumber numberWithInt:offset],
                                @"limit":CLimit,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagMyFavourite parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)doctorDetailWithDoctorID:(NSString *)doctorID completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"doctor_id":doctorID,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagDoctorDetail parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagDoctorDetail andError:error andHandler:^(BOOL retry)
         {
             if (retry) [self doctorDetailWithDoctorID:doctorID completed:completion];
         }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)claimFormApprovalListWithOffset:(int)offset completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"offset":[NSNumber numberWithInt:offset],
                                @"limit":CLimit,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagClaimFormForApproval parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)ccgListWithLocalAuthorityID:(NSString *)localAuthorityID completed:(void (^)(id responseObject, NSError *error))completion
{    
    NSDictionary *dictParam = @{@"local_authority_id":localAuthorityID,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagCCGList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)acceptRejectClaimFormWithFormID:(NSString *)formID andStatus:(int)status andCCGID:(NSString *)ccgID andRejectionReason:(NSString *)rejectionReason completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"form_id":formID,
                                @"status":[NSNumber numberWithInt:status],
                                @"ccg_id":ccgID,
                                @"rejection_reason":rejectionReason,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagAcceptRejectClaimForm parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagAcceptRejectClaimForm andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self acceptRejectClaimFormWithFormID:formID andStatus:status andCCGID:ccgID andRejectionReason:rejectionReason completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)editProfileWithName:(NSString *)name andMobileNumber:(NSString *)mobileNumber andEmail:(NSString *)email andLocalAuthority:(NSString *)localAuthority completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{@"name":name,
                                @"mobile_number":mobileNumber,
                                @"email":email,
                                @"local_authority":localAuthority,
                                CParameterLoginType:[NSNumber numberWithInt:appDelegate.loginUserAMHP.login_type]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagAMHPEditProfile parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagAMHPEditProfile andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self editProfileWithName:name andMobileNumber:mobileNumber andEmail:email andLocalAuthority:localAuthority completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}







# pragma mark
# pragma mark - Save To Coredata
     
- (void)saveLoginUserToLocal:(NSDictionary *)dictData
{
    if ([appDelegate.isFromS12 isEqualToNumber:@1])
    {
        appDelegate.loginUser = [self userWithDictionary:[dictData valueForKey:CJsonData]];
        appDelegate.loginUser.draft_count = [[dictData valueForKey:CJsonMeta] intForKey:@"draft_count"];
    }
    else
        appDelegate.loginUserAMHP = [self amhpUserWithDictionary:[dictData valueForKey:CJsonData]];
   
    
    if (appDelegate.loginUser || appDelegate.loginUserAMHP)
    {
        if ([[dictData valueForKey:CJsonMeta] valueForKey:CJsonToken])
        {
            [CUserDefaults setObject:[[dictData valueForKey:CJsonMeta] stringValueForJSON:CJsonToken] forKey:UserDefaultLoginToken];
            [CUserDefaults synchronize];
        }
        
        [UIApplication setUserId:[[dictData valueForKey:CJsonData] stringValueForJSON:@"user_id"]];
    }
}

- (TBLUser *)userWithDictionary:(NSDictionary *)dictUserData
{
    TBLUser *user;
    
    if (dictUserData)
    {
        user = [TBLUser findOrCreate:@{@"userID":[dictUserData stringValueForJSON:@"user_id"]} context:[[Store sharedInstance] mainManagedObjectContext]];
        
        user.login_type             = 1;
        user.userID                 = [dictUserData stringValueForJSON:@"user_id"];
        user.email                  = [dictUserData stringValueForJSON:@"email"];
        user.availability_status    = [dictUserData intForKey:@"availability_status"];
        user.availability_message   = [dictUserData stringValueForJSON:@"availability_message"];
        user.general_availability   = [dictUserData stringValueForJSON:@"general_availability"];
        user.gmc_referance_number   = [dictUserData stringValueForJSON:@"gmc_referance_number"];
        user.name                   = [dictUserData stringValueForJSON:@"name"];
        user.gender                 = [dictUserData intForKey:@"gender"];
        user.mobile_number          = [dictUserData stringValueForJSON:@"mobile_number"];
        user.landline_number        = [dictUserData stringValueForJSON:@"landline_number"];
        user.office_base_postcode   = [dictUserData stringValueForJSON:@"office_base_postcode"];
        user.office_base_team       = [dictUserData stringValueForJSON:@"office_base_team"];
        
        user.employer               = [dictUserData valueForKey:@"employer"];
        user.local_authorities      = [dictUserData valueForKey:@"local_authority"];
        user.language_spoken        = [dictUserData valueForKey:@"language_spoken"];
        user.specialties            = [dictUserData valueForKey:@"specialties"];
        
        user.default_claim_address  = [dictUserData stringValueForJSON:@"default_claim_address"];
        
        user.employer_id            = [dictUserData intForKey:@"employer_id"];
        user.car_model              = [dictUserData stringValueForJSON:@"car_model"];
        user.car_make               = [dictUserData stringValueForJSON:@"car_make"];
        user.car_registration_plate = [dictUserData stringValueForJSON:@"car_registration_plate"];
        user.engine_size            = [dictUserData stringValueForJSON:@"engine_size"];
        user.gps_enable             = [dictUserData booleanForKey:@"gps_enable"];
        user.notification_reminder  = [dictUserData booleanForKey:@"notification_reminder"];
        user.accept_terms_condition = [dictUserData booleanForKey:@"accept_terms_condition"];
        user.change_password        = [dictUserData booleanForKey:@"change_password"];
        
        user.region_id                 = [dictUserData intForKey:@"region_id"];
        user.region_name                = [dictUserData stringValueForJSON:@"region_name"];
        
        NSLog(@"login user == %@", user);
        
        [[[Store sharedInstance] mainManagedObjectContext] save];
    }
    
    return user;
}

- (TBLAMHPUser *)amhpUserWithDictionary:(NSDictionary *)dictUserData
{
    TBLAMHPUser *amhpUser;
    
    if (dictUserData)
    {
        amhpUser = [TBLAMHPUser findOrCreate:@{@"userID":[dictUserData stringValueForJSON:@"user_id"]} context:[[Store sharedInstance] mainManagedObjectContext]];
        
        amhpUser.login_type             = 2;
        amhpUser.userID                 = [dictUserData stringValueForJSON:@"user_id"];
        amhpUser.email                  = [dictUserData stringValueForJSON:@"email"];
        amhpUser.name                   = [dictUserData stringValueForJSON:@"name"];
        amhpUser.mobile_number          = [dictUserData stringValueForJSON:@"mobile_number"];
        
        if ([[dictUserData valueForKey:@"local_authority"] count] > 0)
            amhpUser.local_authority    = [dictUserData valueForKey:@"local_authority"];
        
        amhpUser.change_password        = [dictUserData booleanForKey:@"change_password"];
        amhpUser.accept_terms_condition = [dictUserData booleanForKey:@"accept_terms_condition"];
        
        NSLog(@"login user == %@", amhpUser);
        
        [[[Store sharedInstance] mainManagedObjectContext] save];
    }
    
    return amhpUser;
}

- (void)saveS12UsefulInfo:(NSArray *)arrUsefulInfo
{
    for (int i = 0; i < arrUsefulInfo.count; i++)
    {
        NSDictionary *dictusefulinfo = [arrUsefulInfo objectAtIndex:i];
        
        TBLS12UsefulInfo *s12UsefulInfo = [TBLS12UsefulInfo findOrCreate:@{@"local_authority_id":[NSNumber numberWithInt:[dictusefulinfo intForKey:@"local_authority_id"]]}];
        
        s12UsefulInfo.local_authority_id  = [dictusefulinfo intForKey:@"local_authority_id"];
        
        s12UsefulInfo.amhps             = [dictusefulinfo valueForKey:@"amhps"];
        s12UsefulInfo.ccgs              = [dictusefulinfo valueForKey:@"ccgs"];
        
        s12UsefulInfo.address           = [dictusefulinfo stringValueForJSON:@"address"];
        s12UsefulInfo.contact_number    = [dictusefulinfo stringValueForJSON:@"contact_number"];
        s12UsefulInfo.local_authority   = [dictusefulinfo stringValueForJSON:@"local_authority"];
    }
    
    [[[Store sharedInstance] mainManagedObjectContext] save];
    
    NSLog(@"%@", [TBLS12UsefulInfo fetchAllObjects]);
}




- (void)saveAddressBookData:(NSArray *)arrAddressBook andType:(BOOL)isAddress
{
    if (arrAddressBook.count > 0)
    {
        for (int i = 0; i < arrAddressBook.count; i++)
        {
            NSDictionary *dictAddressBook = [arrAddressBook objectAtIndex:i];
            
            TBLAddressBook *addressBook = [TBLAddressBook findOrCreate:@{@"doctor_id":[dictAddressBook valueForKey:@"doctor_id"]} context:[[Store sharedInstance] mainManagedObjectContext]];
            
            addressBook.doctor_id                   = [dictAddressBook stringValueForJSON:@"doctor_id"];
            addressBook.availability_status         = [dictAddressBook intForKey:@"availability_status"];
            addressBook.distance                    = [dictAddressBook stringValueForJSON:@"distance"];
            addressBook.favourite_status            = [dictAddressBook intForKey:@"favourite_status"];
            addressBook.latitude                    = [dictAddressBook doubleForKey:@"latitude"];
            addressBook.longitude                   = [dictAddressBook doubleForKey:@"longitude"];
            addressBook.availability_message        = [dictAddressBook stringValueForJSON:@"availability_message"];
            addressBook.doctor_name                 = [dictAddressBook stringValueForJSON:@"doctor_name"];
            addressBook.email                       = [dictAddressBook stringValueForJSON:@"email"];
            addressBook.employer_name               = [dictAddressBook stringValueForJSON:@"employer_name"];
            addressBook.landline_number             = [dictAddressBook stringValueForJSON:@"landline_number"];
            addressBook.mobile_number               = [dictAddressBook stringValueForJSON:@"mobile_number"];
            addressBook.updated_status_timestamp    = [dictAddressBook stringValueForJSON:@"updated_status_timestamp"];
            
            if (isAddress)
                addressBook.isAddressBook = YES;
            else
                addressBook.isLocationSearch = YES;
        }
        
        [[[Store sharedInstance] mainManagedObjectContext] save];
    }
}

- (void)saveLocationSearchData:(NSArray *)arrLocationSearch
{
    if (arrLocationSearch.count > 0)
    {
        for (int i = 0; i < arrLocationSearch.count; i++)
        {
            NSDictionary *dictLocationSearch = [arrLocationSearch objectAtIndex:i];
            
            TBLLocationSearch *locationSearch = [TBLLocationSearch findOrCreate:@{@"doctor_id":[dictLocationSearch valueForKey:@"doctor_id"]} context:[[Store sharedInstance] mainManagedObjectContext]];
            
            locationSearch.doctor_id                   = [dictLocationSearch intForKey:@"doctor_id"];
            locationSearch.availability_status         = [dictLocationSearch intForKey:@"availability_status"];
            locationSearch.distance                    = [dictLocationSearch intForKey:@"distance"];
            locationSearch.favourite_status            = [dictLocationSearch intForKey:@"favourite_status"];
            locationSearch.latitude                    = [dictLocationSearch doubleForKey:@"latitude"];
            locationSearch.longitude                   = [dictLocationSearch doubleForKey:@"longitude"];
            locationSearch.availability_message        = [dictLocationSearch stringValueForJSON:@"availability_message"];
            locationSearch.doctor_name                 = [dictLocationSearch stringValueForJSON:@"doctor_name"];
            locationSearch.email                       = [dictLocationSearch stringValueForJSON:@"email"];
            locationSearch.employer_name               = [dictLocationSearch stringValueForJSON:@"employer_name"];
            locationSearch.landline_number             = [dictLocationSearch stringValueForJSON:@"landline_number"];
            locationSearch.mobile_number               = [dictLocationSearch stringValueForJSON:@"mobile_number"];
            locationSearch.updated_status_timestamp    = [dictLocationSearch stringValueForJSON:@"updated_status_timestamp"];
        }
        
        [[[Store sharedInstance] mainManagedObjectContext] save];
    }
}
     


@end
