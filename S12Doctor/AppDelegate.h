//
//  AppDelegate.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/18/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

#import "TWTSideMenuViewController.h"
#import "BasicAppDelegate.h"

#import "TBLUser+CoreDataClass.h"
#import "TBLAMHPUser+CoreDataClass.h"


@interface AppDelegate : BasicAppDelegate <UIApplicationDelegate, TWTSideMenuViewControllerDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (nonatomic, strong) TWTSideMenuViewController *sideMenuViewController;

@property (nonatomic, strong) TBLUser *loginUser;

@property (nonatomic, strong) TBLAMHPUser *loginUserAMHP;

@property (nonatomic, strong) NSDateFormatter *dateFormate;

@property (nonatomic, strong) NSNumber *isFromS12;

@property (nonatomic,strong) CLLocationManager *locationManager;

@property (strong, nonatomic) NSDate *lastTimestamp;

@property (nonatomic, assign) double current_lat;
@property (nonatomic, assign) double current_long;
@property (nonatomic, strong) NSURLSessionDataTask *syncDraftAPI;





- (void)saveContext;


-(void)openMenuViewcontroller:(UIViewController *)viewController animated:(BOOL)animated;

- (void)resignKeyboard;
- (void)logoutUser;
- (void)callMobileNo:(NSString *)strMobileNo;
- (void)openURL:(NSString *)url;
- (NSString *)timestampToDateWithTimestamp:(double)timestamp;
- (void)openSettings;
- (void)checkLocationServiceWhenUserDenyService;
- (void)contactDoctor:(NSString *)doctorID andCotactType:(int)cotactType;

- (void)displayToast:(NSString *)message;
- (void)toastAlertWithMessage:(NSString *)message showDone:(BOOL)showDone;



- (void)getCurrentLocation;
-(CLLocationCoordinate2D) getLatLongFromAddressString:(NSString*)addressStr;


- (void)syncSavedDraftWithServer;


- (void)trackTheEventWithName:(NSString *)name;
- (void)trackTheEventWithName:(NSString *)name properties:(NSDictionary *)properties;

@end

