//
//  AppDelegate.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/18/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "AppDelegate.h"
#import "MIMainViewController.h"
#import "MIHomeViewController.h"
#import "MIAMHPHomeViewController.h"
#import "MILeftPanelViewController.h"
#import "NoInternetView.h"
#import "MIAFNetworking.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()
{
    MILeftPanelViewController *leftPanelVC;
    NSInteger indexForSyncToServer;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = ColorBlue_1371b9;
    [self.window makeKeyAndVisible];
    
    //...... MIXPANEL .....
//    [self mixpanel];
    
    
    [[MIAFNetworking sharedInstance] setDisableTracing:NO];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [self internetGoesToOfflineOnline]; //..... Display No internet view
    
    [Fabric with:@[[Crashlytics class]]];
    
    _dateFormate = [NSDateFormatter new];
    
    [self initializeWithLaunchOptions:launchOptions];
    
    if ([UIApplication userId])
    {
        if ([[TBLAMHPUser fetchAllObjects] count] > 0)
        {
            _loginUserAMHP = [TBLAMHPUser findOrCreate:@{@"userID":[UIApplication userId]} context:[[Store sharedInstance] mainManagedObjectContext]];
            
            if (_loginUserAMHP)
            {
                if (_loginUserAMHP.accept_terms_condition == 1 && _loginUserAMHP.change_password == 1)
                {
                    MIAMHPHomeViewController *amhpHomeVC = [[MIAMHPHomeViewController alloc] initWithNibName:@"MIAMHPHomeViewController" bundle:nil];
                    appDelegate.isFromS12 = @2;
                    [self openMenuViewcontroller:amhpHomeVC animated:YES];
                }
                else
                {
                    [self openMainViewController];
                }
            }
        }
        else
            
        {
            _loginUser = [TBLUser findOrCreate:@{@"userID":[UIApplication userId]} context:[[Store sharedInstance] mainManagedObjectContext]];
            
            if (_loginUser.accept_terms_condition == 1 && _loginUser.change_password == 1)
            {
                MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                appDelegate.isFromS12 = appDelegate.loginUser.login_type == 1 ? @1 : @2;
                [self openMenuViewcontroller:homeVC animated:YES];
            }
            else
            {
                [self openMainViewController];
            }
        }
    }
    else
    {
        [self openMainViewController];
    }
    
    return YES;
}

- (void)openMainViewController
{
    MIMainViewController *mainVC = [[MIMainViewController alloc] initWithNibName:@"MIMainViewController" bundle:nil];
    [self.window setRootViewController:[[UINavigationController alloc] initWithRootViewController:mainVC]];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    UIApplication *app = [UIApplication sharedApplication];
//
//    __block UIBackgroundTaskIdentifier bgTask;
//    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
//
//        NSLog(@"location updated in background");
//
//        [self latLongAPI];
//        [app endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//
//    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if ([UIApplication userId])
        [self getCurrentLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}




#pragma mark -
#pragma mark - MixPanel

- (void)mixpanel
{
    [Mixpanel sharedInstanceWithToken:@"eb397cfd67162d164331d5d0224da9e4"];
}

- (void)trackTheEventWithName:(NSString *)name
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:name];
}

- (void)trackTheEventWithName:(NSString *)name properties:(NSDictionary *)properties
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:name properties:properties];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"S12Doctor"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}





# pragma mark
# pragma mark - APP Launch Events

- (void)initializeWithLaunchOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:CFontAvenirLTStd55Roman(16), NSForegroundColorAttributeName:ColorBlack}];
    [[UINavigationBar appearance] setBarTintColor:ColorBlack];
    [[UINavigationBar appearance] setTintColor:[appDelegate.isFromS12 isEqual:@1] ? ColorBlue_1371b9 : ColorGreen_47C0B6];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    
    //....
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncSavedDraftWithServer) name:AFNetworkingReachabilityDidChangeNotification object:nil];
}





# pragma mark
# pragma mark - SidePanelViewController

- (void)setWindowRootViewController:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed
{
    [UIView transitionWithView:self.window
                      duration:animated?0.5:0.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         self.window.rootViewController = vc;
         [UIView setAnimationsEnabled:oldState];
         
         
     } completion:^(BOOL finished)
     {
         if (completed)
             completed(finished);
     }];
}

-(void)openMenuViewcontroller:(UIViewController *)viewController animated:(BOOL)animated
{
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarMenuClicked:)];
    
    if (!_sideMenuViewController)
    {
        if (!leftPanelVC)
            leftPanelVC = [[MILeftPanelViewController alloc] initWithNibName:@"MILeftPanelViewController" bundle:nil];
        
        _sideMenuViewController = [[TWTSideMenuViewController alloc] initWithMenuViewController:leftPanelVC mainViewController:navigationController];
        
        _sideMenuViewController.shadowColor = [UIColor blackColor];
        _sideMenuViewController.edgeOffset = (UIOffset) { .horizontal = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? Is_iPhone_4 || Is_iPhone_5 ? 0.0f : 26.0f : 0.0f };
        _sideMenuViewController.zoomScale = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? Is_iPhone_5 ? 0.6050f : 0.7050f : 0.85f;
        _sideMenuViewController.delegate = self;
        
        [self setWindowRootViewController:_sideMenuViewController animated:animated completion:nil];
    }
    else
    {
        [_sideMenuViewController setMainViewController:navigationController animated:animated closeMenu:YES];
    }
}

-(void)leftBarMenuClicked:(UIBarButtonItem *)sender
{
    [self resignKeyboard];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}





# pragma mark
# pragma mark - General Method

- (void)resignKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)logoutUser
{
    [UIApplication removeUserId];
    [TBLDraft deleteAllObjects];
    [TBLUser deleteAllObjects];
    [TBLAMHPUser deleteAllObjects];
    [TBLAddressBook deleteAllObjects];
    
    [CUserDefaults removeObjectForKey:UserDefaultLoginToken];
    [CUserDefaults removeObjectForKey:UserDefaultAddressBook];
    [CUserDefaults removeObjectForKey:UserDefaultLocationSearch];
    [CUserDefaults synchronize];
    
    MIMainViewController *mainVC = [[MIMainViewController alloc] initWithNibName:@"MIMainViewController" bundle:nil];
    [self setWindowRootViewController:[[UINavigationController alloc] initWithRootViewController:mainVC] animated:YES completion:^(BOOL finished)
     {
         _sideMenuViewController = nil;
         leftPanelVC = nil;
     }];
}

- (void)callMobileNo:(NSString *)strMobileNo
{
    if (IosVersion >= 10.0)
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",strMobileNo]]])
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", strMobileNo]] options:@{} completionHandler:nil];
        else
        {
            [CustomAlertView iOSAlert:@"" withMessage:CMessageCallNotSupport onView:[UIApplication topMostController]];
        }
    }
    else
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",strMobileNo]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", strMobileNo]]];
        }
        else
            [CustomAlertView iOSAlert:@"" withMessage:CMessageCallNotSupport onView:[UIApplication topMostController]];
    }
}

- (void)openURL:(NSString *)url
{
    NSURL *urlWebsite;
    
    if ([url.lowercaseString hasPrefix:@"http://"] || [url.lowercaseString hasPrefix:@"https://"])
        urlWebsite = [NSURL URLWithString:url];
    else
        urlWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",url]];
    
    if ([[UIApplication sharedApplication] canOpenURL: urlWebsite])
    {
        if (IosVersion >= 10)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", urlWebsite]] options:@{} completionHandler:nil];
        else
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", urlWebsite]]];
    }
}

- (NSString *)timestampToDateWithTimestamp:(double)timestamp
{
    NSTimeInterval _interval = timestamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    [_dateFormate setDateFormat:@"dd/MM/yyyy hh:mm a"];
    [_dateFormate setTimeZone:[NSTimeZone systemTimeZone]];
    
    return [_dateFormate stringFromDate:date];
}

- (void)openSettings
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)contactDoctor:(NSString *)doctorID andCotactType:(int)cotactType
{
    [[APIRequest request] contactDoctorWithDoctorID:doctorID andContactType:cotactType completed:nil];
}






# pragma mark
# pragma mark - Toast Alert

- (void)displayToast:(NSString *)message
{
    if (![AFNetworkReachabilityManager sharedManager].reachable)
    {
        [self toastAlertWithMessage:message showDone:NO];
    }
}

- (void)toastAlertWithMessage:(NSString *)message showDone:(BOOL)showDone
{
    MIToastAlert *toastAlert = [[MIToastAlert alloc] initWithMessage:message];
    [toastAlert.imgVDone hideByWidth:!showDone];
    
    CViewSetY(toastAlert, CViewHeight([UIApplication topMostController].view) - CViewHeight(toastAlert));
    
    [[UIApplication topMostController].view addSubview:toastAlert];
    [self performSelector:@selector(removeToastAlert:) withObject:toastAlert afterDelay:2];
}

- (void)removeToastAlert:(MIToastAlert *)toastAlert
{
    [UIView animateWithDuration:3 animations:^{
        [toastAlert setAlpha:0];
    } completion:^(BOOL finished)
     {
         [toastAlert removeFromSuperview];
     }];
}






# pragma mark
# pragma mark - Sync Draft

- (void)syncSavedDraftWithServer
{
    if ([AFNetworkReachabilityManager sharedManager].isReachable)
    {
        void(^syncDraftToServer)(TBLDraft *) = ^(TBLDraft *draft)
        {
            _syncDraftAPI = [[APIRequest request] createClaimFormWithLocalAuthority:draft.local_authority
                                                            andAMHP:draft.amhp
                                                      andDoctorName:draft.doctor_name
                                                   andDoctorAddress:draft.doctor_address
                                          andAssessmentLocationType:draft.assessment_location_type
                                              andAssessmentPostcode:draft.assessment_postcode
                                              andAssessmentDateTime:draft.assessment_date_time
                                                andPatientNHSNumber:draft.patient_nhs_number
                                                 andPatientPostcode:draft.patient_postcode
                                                  andAdditionalInfo:draft.additional_information
                                              andSectionImplemented:draft.section_implemented
                                                         andCarMake:draft.car_make
                                                        andCarModel:draft.car_model
                                            andCarRegistrationPlate:draft.car_registration_plate
                                                      andEngineSize:draft.engine_size
                                                           andNotes:draft.notes
                                                      andDisclaimer:draft.disclaimer
                                                andLocalAuthorityID:draft.local_authority_id
                                                          andAMHPID:draft.amhp_id
                                                       andLoginType:appDelegate.loginUser.login_type
                                andAssessmentLocatinTypeDescription:draft.specify_assessment_locattion
                                        andSelectSectionDescription:draft.specify_section
                                                     andClaimFormID:draft.claim_id
                                                    andFromPostCode:draft.from_postcode
                                                      andToPostCode:draft.to_postcode
                                                   andNumberOfMiles:draft.miles_traveled
                                                        andFormType:1
                                                          completed:^(id responseObject, NSError *error)
             {
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     appDelegate.loginUser.draft_count = [[responseObject valueForKey:CJsonMeta] intForKey:@"draft_count"];
                     draft.iSynced = 1;
                     [[Store sharedInstance].mainManagedObjectContext save];
                     
                     [self syncSavedDraftWithServer];
                 }
             }];
        };
        
        
        NSArray *unsyncedDraft = [TBLDraft fetch:[NSPredicate predicateWithFormat:@"iSynced == 0"] sortedBy:nil];
        if (unsyncedDraft.count > 0)
            syncDraftToServer([unsyncedDraft firstObject]);
        else
            NSLog(@"All local draft synced with server....");
    }
}

#pragma mark - Internet Offline/Online

-(void)internetGoesToOfflineOnline
{
    // If Internet Connection appear to offline.......
    __block NoInternetView *objNoInternet = [NoInternetView customInternetPopup];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.google.com"]];
    NSOperationQueue *operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         switch (status)
         {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi:
             {
                 NSLog(@"NETWORK REACHABLE");
                 
                 [operationQueue setSuspended:NO];
                 
                 objNoInternet.viewInternet.backgroundColor = CRGB(46, 204, 113);
                 objNoInternet.lblNoInternet.text = @"Internet detected";
                 
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [objNoInternet removeFromSuperview];
                 });
             }
                 break;
             case AFNetworkReachabilityStatusNotReachable:
             default:
             {
                 NSLog(@"NETWORK UNREACHABLE");
                 [operationQueue setSuspended:YES];
                 [objNoInternet removeFromSuperview];
                 [appDelegate.window addSubview:objNoInternet];
                 objNoInternet.viewInternet.backgroundColor = [UIColor redColor];
                 objNoInternet.lblNoInternet.text = @"No internet connection";
                 
                 break;
             }
         }
     }];
    [manager.reachabilityManager startMonitoring];
}









# pragma mark
# pragma mark - LocationManager Delegate

- (void)getCurrentLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];
    
    //..... IOS 8
    if (IS_Ios8)
    {
        //[_locationManager requestWhenInUseAuthorization];
        [_locationManager requestAlwaysAuthorization];
    }
    
    //..... IOS 9
    if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)])
        [self.locationManager setAllowsBackgroundLocationUpdates:YES];
    
    NSLog(@"start updating");
    
    _locationManager.distanceFilter = 100;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSString *errorString;
    
    switch([error code])
    {
        case kCLErrorDenied:
        {
            // ..... DON'T ALLOW CLICK .....
            
            self.current_lat = 0;
            self.current_long = 0;
            
            [self latLongAPI];
            
            [CUserDefaults removeObjectForKey:UserDefaultCurrentLatitude];
            [CUserDefaults removeObjectForKey:UserDefaultCurrentLongitude];
            [CUserDefaults synchronize];
            
            break;
        }
        case kCLErrorLocationUnknown:
            errorString = @"Location data unavailable";
            break;
        default:
            errorString = @"An unknown error has occurred";
            break;
    }
    NSLog(@"Location Error :: %@",errorString);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (appDelegate.loginUser.login_type == 1)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:UserDefaultLocationChange object:nil];
        
        if ([[CUserDefaults valueForKey:UserDefaultFirstTimeLocation] isEqualToString:@"1"])
            return;
        else
        {
            [CustomAlertView iOSAlert:@"" withMessage:CMessageFirstTimeLocation onView:[UIApplication topMostController]];
            
            if (status == kCLAuthorizationStatusNotDetermined || status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusAuthorizedAlways)
                [CUserDefaults setObject:@"1" forKey:UserDefaultFirstTimeLocation];
            
            [CUserDefaults synchronize];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"Location updated method called in appdelegate method");
    NSLog(@"Current_lat : %f",  [CUserDefaults doubleForKey:UserDefaultCurrentLatitude]);
    NSLog(@"Current_long : %f",  [CUserDefaults doubleForKey:UserDefaultCurrentLongitude]);
    
    CLLocation *location = [locations lastObject];
    
    _current_lat = location.coordinate.latitude;
    _current_long = location.coordinate.longitude;
    
    [CUserDefaults setDouble:_current_lat forKey:UserDefaultCurrentLatitude];
    [CUserDefaults setDouble:_current_long forKey:UserDefaultCurrentLongitude];
    [CUserDefaults synchronize];
    
//    [manager stopUpdatingLocation];
    
    CLGeocoder *ceo = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude]; //insert your new coordinates
    
    //..... S12 doctor API Call
    [self latLongAPI];
    
    [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
     {
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         if (placemark)
         {
             NSLog(@"placemark %@",placemark);
             //String to hold address
             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:CComponentJoinedString];
             
             NSLog(@"addressDictionary %@", placemark.addressDictionary);
             
             //Print the location to console
             NSLog(@"I am currently at %@",locatedAt);
         }
         else
         {
             NSLog(@"Could not locate");
         }
     }];
}

- (void)checkLocationServiceWhenUserDenyService
{
    if (appDelegate.loginUser.login_type == 1)
    {
        if ((![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]  == kCLAuthorizationStatusDenied))
        {
            if (![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
            {
                [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageGPSDisable firstButton:CChangeNow firstHandler:^(UIAlertAction *action)
                 {
                     [appDelegate openSettings];
                 }
                 secondButton:@"No" secondHandler:^(UIAlertAction *action)
                 {
                     appDelegate.current_lat = appDelegate.current_long = 0;
                     [self latLongAPI];
                     
                 } inView:[UIApplication topMostController]];
            }
        }
        else
        {
            [[UIApplication topMostController] dismissViewControllerAnimated:NO completion:nil];
            [self getCurrentLocation];
        }
    }
}

-(CLLocationCoordinate2D) getLatLongFromAddressString:(NSString*)addressStr
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result)
    {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil])
        {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    NSLog(@"passed string : %@",addressStr);
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
}

- (void)latLongAPI
{
    if ([UIApplication userId])
    {
        if (appDelegate.loginUser.login_type == 1)
        {
            NSLog(@"Updating location...");
            [[APIRequest request] userLatLong:nil];
        }
    }
}

@end
