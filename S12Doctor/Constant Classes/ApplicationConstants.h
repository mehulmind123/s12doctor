//
//  ApplicationConstants.h
//  EdSmart
//
//  Created by S12 Solutions on 08/03/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#ifndef ApplicationConstants_h
#define ApplicationConstants_h

//#define NSLog(...)

#define GooglePlaceKey              @"AIzaSyBauqxotjsQ9yUWG5eDSklr7ubzmGfkehM"





/*======== WHITE SPACE CHAR SET =========*/
#define CWhitespaceCharSet [NSCharacterSet whitespaceAndNewlineCharacterSet]


/*======== FONT =========*/
#define CFontHelveticaRegular(fontSize) [UIFont fontWithName:@"HelveticaNeue" size:fontSize]
#define CFontHelveticaBold(fontSize)    [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize]
#define CFontHelveticaMedium(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize]
#define CFontHelveticaLight(fontSize)   [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize]
#define CFontHelveticaItalic(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Italic" size:fontSize]

#define CFontAvenirLTStd55Roman(fontSize)    [UIFont fontWithName:@"AvenirLTStd-Roman" size:fontSize]
#define CFontAvenirLTStd65Meduim(fontSize)     [UIFont fontWithName:@"AvenirLTStd-Medium" size:fontSize]
#define CFontAvenirLTStd85Heavy(fontSize)       [UIFont fontWithName:@"AvenirLTStd-Heavy" size:fontSize]
#define CFontAvenirLTStd95Balck(fontSize)   [UIFont fontWithName:@"AvenirLTStd-Black" size:fontSize]



/*======== COLOR =========*/

#define ColorLoader                     CRGB(49, 110, 189)
#define ColorBlack                      CRGB(0, 0, 0)
#define ColorWhite                      CRGB(255, 255, 255)
#define ColorBlue_1371b9                CRGB(19, 113, 185)
#define ColorGreen_47C0B6               CRGB(71, 192, 180)
#define ColorBlack_4f5d60               CRGB(79, 93, 96)
#define ColorDarkGray_ededf1            CRGB(165, 167, 185)
#define ColorGray_ededf1                CRGB(237, 237, 241)
#define ColorMediumGray                 CRGB(239, 244, 250)
#define ColorAvaiable_00d800            CRGB(0, 216, 0)
#define ColorMayBeAvaialble_ffba00      CRGB(255, 186, 0)
#define ColorUnavailable_ffba00         CRGB(255, 72, 88)
#define ColorUnknown_00d800             CRGB(151, 153, 175)




/*======== Character Limit Constant =========*/

#define CCharacterLimit8                8
#define CCharacterLimit9                9
#define CCharacterLimit11               11
#define CCharacterLimit256              256
#define CCharacterLimit500              500
#define CCharacterLimit5000             5000


#define COtherTypeSelect                @"Other (Please specify)"




//..... DEFAULT UK LAT LONG

#define CDefaultLatitude              53.8609437
#define CDefaultLongitude             -3.4693975



#define CNotAvailable               @"N/A"


#define CLAS12                      @"s12"
#define CLAAll                      @"all"












/*======== USER DEFAULT =========*/

#define UserDefaultLoginToken                   @"LoginToken"
#define UserDefaultDeviceToken                  @"DeviceToken"
#define UserDefaultGotIT                        @"gotIT"

#define UserDefaultCurrentLatitude              @"currentLatitude"
#define UserDefaultCurrentLongitude             @"currentLongitude"


#define UserDefaultUpdateStatus                 @"updatedStatus"

#define UserDefaultFirstTimeLocation            @"FirstTimeLocation"
#define UserDefaultLocationChange               @"locationChange"



//.....Tutorial

#define UserDefaultTutorialS12                  @"S12Tutorial"
#define UserDefaultTutorialAMHP                 @"AMHPTutorial"


//.....AMHP HOME

#define UserDefaultAddressBook                  @"addressBookData"
#define UserDefaultLocationSearch               @"locationSearch"
#define UserDefaultMapSearch                    @"mapSearch"







/*======== NOTIFICATION CONSTANTS =========*/

#define NotificationImageUploaded               @"NotificationImageUploaded"
#define NotificationArrive                      @"NotificationArrive"
#define NotificationHomeDataUpdate              @"homeDataUpdate"
#define NotificationAddressUpdate               @"addressupdate"





/*======== AMHP HOME =========*/

#define CChooseOption               @"Choose Option"
#define CMarkAsFavourite            @"Mark as Favourite"
#define CMarkAsUnFavourite          @"Mark as Unfavourite"
#define CCallMobile                 @"Call Mobile"
#define CCallLandline               @"Call Landline"
#define CCallDoctor                 @"Call doctor"
#define CSMSDoctor                  @"SMS doctor"
#define CEmailDoctor                @"Email doctor"
#define CEmailUs                    @"Email Us"
#define CEmailS12                   @"Email S12"
#define CUserGuide                  @"User Guide"

#define CWeeklyAvailability         @"Weekly Availability"
#define CTimeSlot                   @"Time Slot"




#define CContactEmail                1
#define CContactMobile               2
#define CContactLandline             3
#define CContactSms                  4



#define CWeeklyAvailabilityID        1













/*======== OTHER =========*/
#define CComponentJoinedString                  @", "
#define CGoToSettings                           @"Go to settings"
#define CDateFormate                            @"yyyy-MM-dd HH:mm:ss"
#define CDateFormateDisplay                     @"yyyy-MM-dd HH:mm"








#define CHelpAndSupportEmail                    @"support@s12solutions.com"





/*======== EVENT NAME =========*/

#define CEventLogin                     @"login"
#define CEventLogout                    @"logout"
#define CEventAppOnline                 @"App online"
#define CEventAppOffline                @"App offline"



#define CEventDoctorDetailsLoaded                @"Doctor Details Loaded"
#define CEventDoctorCallButtonClicked            @"Doctor Call Button Clicked"
#define CEventDoctorSMSButtonClicked             @"Doctor SMS Button Clicked"
#define CEventDoctorEmailButtonClicked           @"Doctor Email Button Clicked"
#define CEventMultiTextCreated                   @"Multi text created"
#define CEventLocationMarkerClicked              @"Location marker clicked"
#define CEventUsefulInformationClicked           @"Useful information clicked"
#define CEventDoctorFavouriteClicked             @"Doctor favourite Clicked"
#define CEventDoctorUnfavouriteClicked           @"Doctor unfavourite Clicked"
#define CEventMyFavouritesClicked                @"My Favourites Clicked"
#define CEventPasswordChanged                    @"Password changed"
#define CEventHelp&SupportClicked                @"Help & Support clicked"















#endif /* ApplicationConstants_h */
