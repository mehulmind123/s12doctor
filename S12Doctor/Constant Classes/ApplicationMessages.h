//
//  ApplicationMessages.h
//  EdSmart
//
//  Created by S12 Solutions on 08/03/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#ifndef ApplicationMessages_h
#define ApplicationMessages_h


/*======== Loader Messages =========*/
#define CMessageCongratulation              @"Congratulations!"
#define CMessageSorry                       @"Sorry!"
#define CMessageLoading                     @"Loading..."
#define CMessageSearching                   @"Searching..."
#define CMessageVerifying                   @"Verifying..."
#define CMessageWait                        @"Please Wait..."
#define CMessageUpdating                    @"Updating..."
#define CMessageAuthenticating              @"Authenticating..."
#define CMessageErrorInternetNotAvailable   @"Internet Connection Not Available!\n Please Try Again Later."



/*======== Validation Messages =========*/


//.... Tutorial .....

#define CMessageTutorial                   @"Hello and welcome to the S12 Solutions digital platform"


//.....AMHP

#define CMessageTutorial1Desc                  @"We'd like to thank you very much for supporting this pilot and embracing the innovation that we hope will improve MHA assessment set-up for AMHPs, doctors and service users. You can access a User Guide via the Menu which explains how to use the platform but if you have any issues, questions or feedback, please email support@s12solutions.com"

#define CMessageAMHPTutorial2               @"Use Address Book to search for doctor's names\n\n Click on a search result to view a doctor's full profile\n\n Click the burger icon for quick contact options"

#define CMessageAMHPTutorial3               @"Text multiple doctors at once\n\n Click the speech bubble symbol to select doctors from a list\n\n Click Done, and your device's messaging service will open a message to selected doctors"

#define CMessageAMHPTutorial4               @"Search for doctors by Location\n\n Click on a pin to see a doctor's basic profile; click on the basic profile to view a doctor's full profile and contact them\n\n Tip: List View allows you to view results in list format"

#define CMessageAMHPTutorial5               @"Exporting from website function"
#define CMessageTutorial5Desc               @"◆ When logged in to S12 Solutions’ website you can view, copy and paste, or export a list of doctors you have tried to make contact with when setting-up the assessment, for service users records\n\n ◆ When logged in to S12 Solutions’ website, you can also export the availability of the doctor address book or location search for your records e.g. if there are no doctors available out of hours, this can be exported to provide rationale for no assessment"


//.....S12

#define CMessageS12Tutorial1Desc               @"We'd like to thank you very much for supporting this pilot and embracing the innovation that we hope will improve MHA assessment set-up for doctors, AMHPs and service users. You can access a User Guide via the Menu which explains how to use the platform but if you have any issues, questions or feedback, please email support@s12solutions.com"

#define CMessageS12Tutorial2               @"Setting up your S12 App\n\n Specify your current availability for s.12 work\n\n Update your general availability calendar so that your profile will be returned when AMHPs search for doctors by time and day\n\n You can update this as often as you'd like"

#define CMessageS12Tutorial3               @"Setting up your Profile\n\n Tip: Please complete your profile in full when you first login to help you and all other users gain maximum benefit from the system"

#define CMessageS12Tutorial4               @"Tip: Please update your availability and contact details whenever they change to help make sure that AMHPs contact you about appropriate, convenient work\n\n The app will send a reminder every 24 hours if you have not updated your status\n\n This function can be enabled or disabled in Menu > Settings > Notification Reminder"

#define CMessageS12Tutorial5               @"S12 Solutions uses GPS to find your location and facilitate effective assessment coordination\n\n If you’d prefer not to disclose your current location, please add the postcode that best describes where you are based to your profile in the Base Postcode field"






//..... LRF .....

#define CMessageBlankEmail                  @"Email Address cannot be blank."
#define CMessageBlankPassword               @"Password cannot be blank."
#define CMessageInvalidEmail                @"Please enter a valid Email Address."


//..... HOME .....

#define CMessagePlaceHolderAvailabilty      @"Please write further detail of availability"
#define CMessageBlankAvailabiltyMesssage    @"Please write further details of availability."
#define CMessageChangeStatus                @"You have not updated your availability for 24 hours. Please update your status."
#define CMessageSync                        @"The update will sync when regained internet connection."
#define CMessageGPSDisable                  @"S12 has detected that GPS is enabled within the app, but that it doesn't have appropriate permissions to access your device's GPS. Would you like to change your device's settings now?"

#define CMessageFirstTimeLocation           @"S12 uses your location to allow AMHPs to find you when setting up MHA assessments. Go to the Settings screen within S12 to view the current GPS status for your device. If you'd prefer not to disclose your location, please make sure that your base post code is added to your profile."

#define CMessageLocationSetting             @"S12 uses your location to allow AMHPs to find you when setting up MHA assessments. You can check and amend your device's Location Services here: Settings > Privacy Settings > Location Services. If you'd prefer not to disclose your location, please make sure that your base post code is added to your profile."




//..... CHANGE PASSWORD .....

#define CMessageBlankOldPassword            @"Old Password cannot be blank."
#define CMessageBlankNewPassword            @"New Password cannot be blank."
#define CMessageInvalidPassword             @"Passwords should be at least 8 characters long and use three of four of the following four types of characters: uppercase, lowercase, numbers and special characters such as !@#$%^&*(){}[]."
#define CMessageBlankConfirmPassword        @"Please confirm your password."
#define CMessagePasswordNotMatch            @"The passwords don't match."
#define CMessageOldNewPasswordNotSame       @"Old Password and New Password can't be same."
#define CMessagePasswordSuccessfullyChanged @"Password changed successfully."



//..... EDIT PROFILE .....

#define CMessageBlankName                   @"Name cannot be blank."
#define CMessageBlankEmployer               @"Please select employer."
#define CMessageBlankLocalAuthority         @"Please select local authority."
#define CMessageBlankGender                 @"Please select gender."
#define CMessageInvalidMobileNumber         @"Please enter valid 11 digit mobile number."
#define CMessageInvalidLandlineNumber       @"Please enter valid 11 digit landline number."
#define CMessageInvalidOfficePostcode       @"Base postcode must be alphanumeric and between 5 and 9 characters only."
#define CMessageProfileSuccessfullyUpdated  @"Profile updated successfully."
#define CMessageOfficeBasePostcodeInfo      @"This postcode is used to give AMHPs your approximate location when your GPS is unavailable."
#define CMessageBlankLanguageName           @"Please enter language name."
#define CMessageNoLocalAuthoritySelected    @"Please select any local authorities."
#define CMessageNoLanguageSelected          @"Please select any language."
#define CMessageNoSpecialtiesSelected       @"Please select any specialties."






//..... CREATE CLAIM FORM .....

#define CMessageBlankAuthority              @"Please select local authority."
#define CMessageBlankAMHP                   @"Please select an AMHP."
#define CMessageBlankUserName               @"Please provide your name."
#define CMessageBlankDoctorAddress          @"Please provide your address."
#define CMessageBlankAssessmentLocationType @"Please select assessment location type."
#define CMessageBlankAssessmentLocationTypeDesc @"Please provide assessment location type description."
#define CMessageBlankAssessmentLocationAddress     @"Please provide assessment location address."
#define CMessageBlankAssessmentPostcode     @"Please provide assessment location postcode."
#define CMessageInvalidAssessmentPostcode   @"Assessment location postcode must be alphanumeric and between 5 and 9 characters only."
#define CMessageBlankDateTime               @"Please select assessment date and time."
#define CMessageBlankPatientNHSNumber       @"Please provide patient NHS number."
#define CMessageInvalidPatientNHSNumber     @"Please enter patient’s 10 digit NHS number."
#define CMessageBlankPatientPostcode        @"Please provide patient postcode."
#define CMessageInvalidPatientPostcode      @"Patient postcode must be alphanumeric and between 5 and 9 characters only."
#define CMessageBlankSectionImplemented     @"Please select section implemented."
#define CMessageBlankSectionImplementedDesc @"Please provide other outcome."
#define CMessageNotAccept                   @"You cannot submit claim request without accepting disclaimer."
#define CMessageClaimFormSubmitted          @"Claim form submitted successfully."
#define CMessageSavedClaimForm              @"Partially filled claim form saved in draft list."
#define CMessageSavedClaimFormBack          @"Do you want to save the partially filled claim form as a draft?"
#define CMessageDisclaimer                  @"I confirm that the information I have supplied above is true and accurate to the best of my knowledge. I understand if any details supplied by me are later proved to be knowingly false I shall be liable for prosecution."
#define CMessageInvalidFromPostcode         @"From postcode must be alphanumeric and between 5 and 9 characters only."
#define CMessageInvalidToPostcode           @"To postcode must be alphanumeric and between 5 and 9 characters only."
#define CMessageAdditionalInfo              @"Please state reason why there is no NHS number or patient postcode."






//..... OTHER
#define CMessageUserNotLogin                @"You have not logged in."
#define CMessageLogout                      @"Are you sure you want to logout?"
#define CMessageNoSyncLogout                @"Draft claim forms will be lost if you log out now because they have not finished syncing with S12’s server. Do you still wish to log out?"
#define CMessageCallNotSupport              @"Your device does not support calling feature."
#define CMessageMailNotSupport              @"Your device doesn't support email feature."
#define CMessageSMSNotSupport               @"Your device not supprot SMS feature."
#define CMessageLocationOFF                 @"Please turn on location settings to explore store close to your location."
#define CMessageDraftDelete                 @"Are you sure you want to delete this draft?"
#define CMessageProvideReason               @"Please enter a Reason."
#define CMessageSelectCCG                   @"Please select a CCG."
#define CMessageAcceptClaimForm             @"Are you sure you want to accept this claim form?"
#define CMessageRejectClaimForm             @"Are you sure you want to reject this claim form?"
#define CMessageUnableSaveDraft              @"You can only save 10 draft claim forms while you’re not connected to the internet so we’re unable to save this form. Please connect to a network if you’d like to save more drafts."
#define CMessageOffline                     @"This information can only be viewed offline if it has been loaded prior to losing internet connection."






#define CMessageBlankContactNumber          @"Sorry, no contact numbers are available for this doctor."
#define CMessageBlankMobileNumber           @"Sorry, there is no mobile phone number available for this doctor."
#define CMessageBlankDoctorEmail            @"Sorry, there is no email available for this doctor."

#define CMessageSearchPlaceholder           @"Type required location here"
#define CMessageAddressBookPlaceholder      @"Type doctor's name here"
#define CMessageNoDoctorForSort             @"There is no any doctors available for sort."


#define CLocationServiceMap                 @"Please enable your location or search for a location before applying filters for map."
#define CLocationServiceList                @"Please enable your location or search for a location before applying filters for the list."
#define COpenAppSetting                     @"Open app settings to enable location"
#define CChangeNow                          @"Change now"
#define CCancel                             @"Cancel"


#define CSelectTimeSlot                     @"Please select a time slot in which you would like to search for the doctors on the selected day of the week."






//******** AMHP ********
#define CDoctorSmsSubject                   @"AMHP request for MHA ax"












#endif /* ApplicationMessages_h */
