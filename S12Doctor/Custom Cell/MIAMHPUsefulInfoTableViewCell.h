//
//  MIUsefulInfoTableViewCell.h
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAMHPUsefulInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAuthorityName;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UIView *vContent;
@property (weak, nonatomic) IBOutlet UIView *vMain;

- (NSAttributedString *)setAttributeString:(NSString *)string;

@end
