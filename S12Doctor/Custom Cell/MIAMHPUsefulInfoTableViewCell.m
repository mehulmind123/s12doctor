//
//  MIUsefulInfoTableViewCell.m
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPUsefulInfoTableViewCell.h"

@implementation MIAMHPUsefulInfoTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    //...UIView
    [_vMain.layer setCornerRadius:3];
    [_vMain.layer setShadowColor:ColorBlack.CGColor];
    [_vMain.layer setShadowOffset:CGSizeZero];
    [_vMain.layer setShadowRadius:2];
    [_vMain.layer setShadowOpacity:0.2];
    [_vMain.layer setMasksToBounds:NO];
    
    [_vContent.layer setCornerRadius:3];
    [_vContent.layer setShadowColor:ColorBlack.CGColor];
    [_vContent.layer setMasksToBounds:YES];
}

- (NSAttributedString *)setAttributeString:(NSString *)string
{
    NSMutableAttributedString* attrString = [[NSMutableAttributedString  alloc] initWithString:string];
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    [style setLineSpacing:5];
    [attrString addAttribute:NSParagraphStyleAttributeName
                       value:style
                       range:NSMakeRange(0, string.length)];
    return attrString;
}

@end
