//
//  MIAddressBookTableViewCell.h
//  AMHP
//
//  Created by S12 Solutions on 7/25/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAddressBookTableViewCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIView *vShadow;
@property (nonatomic, weak)IBOutlet UIView *vContent;
@property (nonatomic, weak)IBOutlet UIView *vDistance;

@property (nonatomic, weak)IBOutlet UIImageView *imgVIcon;
@property (nonatomic, weak)IBOutlet UIImageView *imgVFavourite;

@property (nonatomic, weak)IBOutlet UILabel *lblTitle;
@property (nonatomic, weak)IBOutlet UILabel *lblSubTitle;
@property (nonatomic, weak)IBOutlet UILabel *lblDistance;
@property (nonatomic, weak)IBOutlet UILabel *lblStatusUpdatedTime;

@property (nonatomic, weak)IBOutlet UIButton *btnFavourite;

- (void)configureStatus:(NSInteger)status;

@end
