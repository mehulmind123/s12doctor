//
//  MIBubbleViewLayout.h
//  Mospur
//
//  Created by S12 Solutions on 22/02/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MIBubbleViewLayoutDelegate <NSObject>

- (CGSize)collectionView:(UICollectionView *)collectionView itemSizeAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface MIBubbleViewLayout : UICollectionViewFlowLayout

@property (nonatomic, readonly) id <MIBubbleViewLayoutDelegate> delegate;

- (id)initWithDelegate:(id <MIBubbleViewLayoutDelegate>)delegate;

@end
