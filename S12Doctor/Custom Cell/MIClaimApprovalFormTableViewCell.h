//
//  MIClaimApprovalFormTableViewCell.h
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIClaimApprovalFormTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lblAssessmentLocationType;
@property (weak, nonatomic) IBOutlet UIButton *btnViewForm;
@property (weak, nonatomic) IBOutlet UIView *vContent;

@end
