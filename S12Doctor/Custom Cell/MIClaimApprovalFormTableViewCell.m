//
//  MIClaimApprovalFormTableViewCell.m
//  AMHP
//
//  Created by S12 Solutions on 25/07/2017.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIClaimApprovalFormTableViewCell.h"

@implementation MIClaimApprovalFormTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    //...UIButton
    [_btnViewForm.layer setCornerRadius:CViewHeight(_btnViewForm)/2];
    [_btnViewForm.layer setBorderWidth:1.0];
    [_btnViewForm.layer setBorderColor:ColorGreen_47C0B6.CGColor];
    
    
    //...UIView
    [_vContent.layer setCornerRadius:2];
    [_vContent.layer setShadowColor:ColorBlack.CGColor];
    [_vContent.layer setShadowOffset:CGSizeZero];
    [_vContent.layer setShadowRadius:2];
    [_vContent.layer setShadowOpacity:0.2];
    [_vContent.layer setMasksToBounds:NO];
}


@end
