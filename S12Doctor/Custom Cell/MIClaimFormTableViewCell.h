//
//  MIClaimFormTableViewCell.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIClaimFormTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIView *viewLocation;
@property (weak, nonatomic) IBOutlet UIView *viewDateAndTime;
@property (weak, nonatomic) IBOutlet UIView *viewAMHPName;
@property (weak, nonatomic) IBOutlet UIView *viewRejectionReason;


@property (weak, nonatomic) IBOutlet UILabel *lblLocationType;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAMHPName;
@property (weak, nonatomic) IBOutlet UILabel *lblRejectionReason;


@property (weak, nonatomic) IBOutlet UIButton *btnRetry;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnView;


@end
