//
//  MIClaimFormTableViewCell.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIClaimFormTableViewCell.h"

@implementation MIClaimFormTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _viewMain.layer.cornerRadius = 5;
    _viewMain.layer.cornerRadius = 6;
    _viewMain.layer.shadowColor = ColorBlack.CGColor;
    _viewMain.layer.shadowOffset = CGSizeZero;
    _viewMain.layer.shadowRadius = 4;
    _viewMain.layer.shadowOpacity = 0.2;
    _viewMain.layer.masksToBounds = NO;
    
    _btnLeft.layer.cornerRadius = CViewHeight(_btnLeft) / 2;
    _btnLeft.layer.masksToBounds = YES;
    
    _btnView.layer.cornerRadius = CViewHeight(_btnView) / 2;
    _btnView.layer.masksToBounds = YES;
 
    _btnRetry.layer.borderWidth = 1;
    _btnRetry.layer.borderColor = ColorBlue_1371b9.CGColor;
    _btnRetry.layer.cornerRadius = CViewHeight(_btnRetry) / 2;
    _btnRetry.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
