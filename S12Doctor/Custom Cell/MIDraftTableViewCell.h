//
//  MIDraftTableViewCell.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIDraftTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewDraft;
@property (weak, nonatomic) IBOutlet UILabel *lblDraftTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDraftCreatedDate;


@end
