//
//  MIDraftTableViewCell.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIDraftTableViewCell.h"

@implementation MIDraftTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _viewDraft.layer.cornerRadius = 6;
    _viewDraft.layer.shadowColor = ColorBlack.CGColor;
    _viewDraft.layer.shadowOffset = CGSizeZero;
    _viewDraft.layer.shadowRadius = 4;
    _viewDraft.layer.shadowOpacity = 0.2;
    _viewDraft.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
