//
//  MIInterestedCatCollectionViewCell.h
//  Zubba
//
//  Created by S12 Solutions on 24/04/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIFilterOptionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *viewMain;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
