//
//  MIInterestedCatCollectionViewCell.m
//  Zubba
//
//  Created by S12 Solutions on 24/04/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIFilterOptionCollectionViewCell.h"

@implementation MIFilterOptionCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setSelected:NO];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.viewMain.layer setCornerRadius:CViewHeight(self.viewMain) / 2];
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (selected)
    {
        [self.viewMain setBackgroundColor:ColorGreen_47C0B6];
        [self.viewMain.layer setBorderWidth:0];
        [self.lblTitle setTextColor:ColorWhite];
    }
    else
    {
        [self.viewMain setBackgroundColor:ColorWhite];
        [self.viewMain.layer setBorderWidth:1];
        [self.viewMain.layer setBorderColor:CRGB(216, 218, 228).CGColor];
        [self.lblTitle setTextColor:ColorDarkGray_ededf1];
    }
}

@end
