//
//  MIFilterOptionTableViewCell.h
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CellSelectedEventBlock)(NSString*);

@interface MIFilterOptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContain;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *collOption;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnHeightColl;

- (void)configureData:(NSDictionary*)dict andDict:(NSDictionary *)dictMain handler:(CellSelectedEventBlock)handler;
@end
