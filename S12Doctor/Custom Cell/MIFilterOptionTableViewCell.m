//
//  MIFilterOptionTableViewCell.m
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIFilterOptionTableViewCell.h"
#import "MIFilterOptionCollectionViewCell.h"


@interface MIFilterOptionTableViewCell()<MIBubbleViewLayoutDelegate>

@property (strong, nonatomic) NSDictionary *dictOptions;
@property (strong, nonatomic) NSDictionary *dictSelected;
@property (strong, nonatomic) NSArray *arrOptions;
@property (strong, nonatomic) NSMutableArray *arrSelectedOptions;
@property (nonatomic, copy) CellSelectedEventBlock CellSelectedEventBlock;

@end

@implementation MIFilterOptionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    //...UICollectionView
    MIBubbleViewLayout *layout = [[MIBubbleViewLayout alloc] initWithDelegate:self];
    [layout setMinimumLineSpacing:6.0f];
    [layout setMinimumInteritemSpacing:0.0f];
    
    [self.collOption setCollectionViewLayout:layout];
    [self.collOption setAllowsMultipleSelection:NO];
    [self.collOption  registerNib:[UINib nibWithNibName:@"MIFilterOptionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIFilterOptionCollectionViewCell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)configureData:(NSDictionary*)dict andDict:(NSDictionary *)dictMain handler:(CellSelectedEventBlock)handler
{
    [self.collOption setAllowsMultipleSelection:[[_dictOptions stringValueForJSON:@"title"] isEqualToString:@"Availability"] || [[_dictOptions stringValueForJSON:@"title"] isEqualToString:CWeeklyAvailability] || [[_dictOptions stringValueForJSON:@"title"] isEqualToString:CTimeSlot]];
    _dictOptions = dict;
    _dictSelected = dictMain;
    
    if ([[dict stringValueForJSON:@"selected"] isBlankValidationPassed])
        self.arrSelectedOptions = [[NSMutableArray alloc] initWithArray:[[_dictOptions stringValueForJSON:@"selected"] componentsSeparatedByString:CComponentJoinedString]];
    else
        self.arrSelectedOptions = [[NSMutableArray alloc] init];
    
    self.CellSelectedEventBlock = handler;
    self.lblTitle.text = [_dictOptions stringValueForJSON:@"title"];
    self.arrOptions = [_dictOptions valueForKey:@"options"];
    [self.collOption reloadData];
    [self.collOption performBatchUpdates:nil completion:^(BOOL finished)
    {
        [self.collOption layoutIfNeeded];
    }];
}


#pragma mark
#pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrOptions.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView itemSizeAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = _arrOptions[indexPath.row];
    CGSize size = [title sizeWithAttributes:@{NSFontAttributeName:CFontAvenirLTStd65Meduim(11)}];
    size.height = 32;
    
    size.width += [self.lblTitle.text isEqualToString:@"Availabilty"] || [self.lblTitle.text isEqualToString:CWeeklyAvailability] || [self.lblTitle.text isEqualToString:CTimeSlot] ? 26 : 44;
    
    if (size.width > CViewWidth(collectionView))
        size.width = CViewWidth(collectionView);
    
    return size;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *indentifier = @"MIFilterOptionCollectionViewCell";
    MIFilterOptionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
    
    NSString *title = _arrOptions[indexPath.row];
    cell.lblTitle.text = title;
    
    if ([[_dictOptions stringValueForJSON:@"title"] isEqualToString:CTimeSlot])
    {
        if ([[_dictSelected stringValueForJSON:CWeeklyAvailability] isBlankValidationPassed])
            cell.viewMain.backgroundColor = [UIColor whiteColor];
        else
            cell.viewMain.backgroundColor = CRGB(230, 230, 230);
    }

    if ([self.arrSelectedOptions containsObject:title])
    {
        [cell setSelected:YES];
        [_collOption selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.allowsMultipleSelection)
    {
        if (![self.arrSelectedOptions containsObject:_arrOptions[indexPath.row]])
            [self.arrSelectedOptions addObject:_arrOptions[indexPath.row]];
    
        if (self.CellSelectedEventBlock)
            self.CellSelectedEventBlock([self.arrSelectedOptions componentsJoinedByString:CComponentJoinedString]);
    }
    
    else if (self.CellSelectedEventBlock)
        self.CellSelectedEventBlock(_arrOptions[indexPath.row]);
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.allowsMultipleSelection)
    {
        if ([self.arrSelectedOptions containsObject:_arrOptions[indexPath.row]])
            [self.arrSelectedOptions removeObject:_arrOptions[indexPath.row]];
        
        if (self.CellSelectedEventBlock)
            self.CellSelectedEventBlock([self.arrSelectedOptions componentsJoinedByString:CComponentJoinedString]);
    }
}

@end
