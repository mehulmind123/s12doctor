//
//  MIFilterSelectOptionTableViewCell.h
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIFloatingTextField.h"

typedef void(^CellSelectedEventBlock)(NSString*);

@interface MIFilterSelectOptionTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet MIFloatingTextField *txtOption;

- (void)configureData:(NSDictionary*)dict handler:(CellSelectedEventBlock)handler;

@end
