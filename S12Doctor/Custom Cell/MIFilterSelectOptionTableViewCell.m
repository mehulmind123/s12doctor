//
//  MIFilterSelectOptionTableViewCell.m
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIFilterSelectOptionTableViewCell.h"

@interface MIFilterSelectOptionTableViewCell()

@property (nonatomic, copy) CellSelectedEventBlock CellSelectedEventBlock;

@end


@implementation MIFilterSelectOptionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [_txtOption setRightImage:[UIImage imageNamed:@"dropdown"] withSize:CGSizeMake(20, 7)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)configureData:(NSDictionary*)dict handler:(CellSelectedEventBlock)handler
{
    self.CellSelectedEventBlock = handler;
    
    [_txtOption setText:[dict stringValueForJSON:@"selected"]];
    [_txtOption setPlaceholder:[dict stringValueForJSON:@"title"]];
    
    [_txtOption setPickerData:[dict valueForKey:@"options"] update:^(NSString *text, NSInteger row, NSInteger component)
    {
        if(self.CellSelectedEventBlock)
            self.CellSelectedEventBlock([dict valueForKey:@"options"][row]);
    }];
}

@end
