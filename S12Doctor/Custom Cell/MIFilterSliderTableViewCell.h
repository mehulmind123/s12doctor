//
//  MIFilterSliderTableViewCell.h
//  AMHP
//
//  Created by S12 Solutions on 7/27/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CellSelectedEventBlock)(NSString *selectedValue);

@interface MIFilterSliderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedDistance;
@property (weak, nonatomic) IBOutlet UISlider *slider;

- (void)configureData:(NSDictionary*)dict handler:(CellSelectedEventBlock)handler;

@end
