//
//  MIFilterSliderTableViewCell.m
//  AMHP
//
//  Created by S12 Solutions on 7/27/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIFilterSliderTableViewCell.h"

@interface MIFilterSliderTableViewCell()

@property (nonatomic, copy) CellSelectedEventBlock CellSelectedEventBlock;

@end

@implementation MIFilterSliderTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)configureData:(NSDictionary*)dict handler:(CellSelectedEventBlock)handler
{
    self.CellSelectedEventBlock = handler;
    
    self.lblSelectedDistance.text = [NSString stringWithFormat:@"%@ mi",[dict stringValueForJSON:@"selected"]];
    [self.slider setValue:[dict floatForKey:@"selected"]];
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
    NSString *sliderValue = [NSString stringWithFormat:@"%d",(int)sender.value];
    
    if(self.CellSelectedEventBlock)
        self.CellSelectedEventBlock(sliderValue);
    
    self.lblSelectedDistance.text = [NSString stringWithFormat:@"%@ mi",sliderValue];    
}

@end
