//
//  MILeftPanelTableViewCell.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MILeftPanelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVLeftPanelItem;
@property (weak, nonatomic) IBOutlet UILabel *lblLeftPanelItem;
@property (weak, nonatomic) IBOutlet UILabel *lblDraftClaimForm;
@end
