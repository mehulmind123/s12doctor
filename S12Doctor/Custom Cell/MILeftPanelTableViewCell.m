//
//  MILeftPanelTableViewCell.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MILeftPanelTableViewCell.h"

@implementation MILeftPanelTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _lblDraftClaimForm.layer.cornerRadius = CViewHeight(_lblDraftClaimForm) / 2;
    _lblDraftClaimForm.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
