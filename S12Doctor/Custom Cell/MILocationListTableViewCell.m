//
//  MILocationListTableViewCell.m
//  S12Doctor
//
//  Created by S12 Solutions on 15/09/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MILocationListTableViewCell.h"

@implementation MILocationListTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.vShadow.layer.cornerRadius =
    self.vContent.layer.cornerRadius = 5;
    
    self.vShadow.layer.masksToBounds =
    self.vContent.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.vDistance.layer.cornerRadius = CViewHeight(self.vDistance)/2;
    self.vDistance.layer.masksToBounds = YES;
}

- (void)configureStatus:(NSInteger)status
{
    switch (status)
    {
        case 1: // Available
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_available_selected"];
            self.lblSubTitle.text = @"Available";
            self.lblSubTitle.textColor = ColorAvaiable_00d800;
        }
        break;
            
        case 2: // May be available
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_may_be_available_selected"];
            self.lblSubTitle.text = @"May be available";
            self.lblSubTitle.textColor = ColorMayBeAvaialble_ffba00;
        }
        break;
            
        case 3: // Unavailable
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_unavailable_selected"];
            self.lblSubTitle.text = @"Unavailable";
            self.lblSubTitle.textColor = ColorUnavailable_ffba00;
        }
        break;
            
        case 4: // Unknown
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_unknown_selected"];
            self.lblSubTitle.text = @"Unknown";
            self.lblSubTitle.textColor = ColorUnknown_00d800;
        }
        break;
    }
}

@end
