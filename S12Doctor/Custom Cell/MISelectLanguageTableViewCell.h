//
//  MISelectLanguageTableViewCell.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISelectLanguageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblLanguageName;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;

@end
