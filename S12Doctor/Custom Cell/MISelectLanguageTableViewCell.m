//
//  MISelectLanguageTableViewCell.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MISelectLanguageTableViewCell.h"

@implementation MISelectLanguageTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.btnSelect.selected = selected;
}

@end
