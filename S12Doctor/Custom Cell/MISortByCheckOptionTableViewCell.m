//
//  MISortByCheckOptionTableViewCell.m
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MISortByCheckOptionTableViewCell.h"

@implementation MISortByCheckOptionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)configureData:(NSDictionary*)dict
{
    self.lblTitle.text = [dict stringValueForJSON:@"title"];
    self.btnCheckBox.selected = [dict booleanForKey:@"selected"];
    
    _lblOption.attributedText = [self setAttributedString:[dict valueForKey:@"options"]];
}

- (NSMutableAttributedString*)setAttributedString:(NSArray*)array
{
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc]init];
    
    if(array.count <= 1)
    {
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",array[0]]];
        [attributedTitle appendAttributedString:title];
    }
    else
    {
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@"arrow"];
   
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];

        for (int i = 0; i < array.count; i++)
        {
            NSString *name =  array[i];
            NSAttributedString *title = [[NSAttributedString alloc] initWithString:name];
            [attributedTitle appendAttributedString:title];
            
            if (array.count - 1 != i)
            {
                [attributedTitle appendAttributedString:attachmentString];
                [attributedTitle appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
            }
        }
        
    }
    
    return attributedTitle;
}

- (IBAction)btnCheckBoxClicked:(UIButton*)sender
{
    sender.selected = !sender.selected;
}

@end
