//
//  MISortByRadioOptionTableViewCell.h
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CellSelectedEventBlock)(NSNumber *selected);

@interface MISortByRadioOptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOption1;
@property (weak, nonatomic) IBOutlet UILabel *lblOption2;

@property (weak, nonatomic) IBOutlet UIButton *btnOption1;
@property (weak, nonatomic) IBOutlet UIButton *btnOption2;


- (void)configureData:(NSDictionary*)dict handler:(CellSelectedEventBlock)handler;

@end
