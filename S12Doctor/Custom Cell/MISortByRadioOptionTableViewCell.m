//
//  MISortByRadioOptionTableViewCell.m
//  AMHP
//
//  Created by S12 Solutions on 7/26/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MISortByRadioOptionTableViewCell.h"

@interface MISortByRadioOptionTableViewCell()

@property (nonatomic, copy) CellSelectedEventBlock CellSelectedEventBlock;

@end

@implementation MISortByRadioOptionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)configureData:(NSDictionary*)dict handler:(CellSelectedEventBlock)handler
{
    self.CellSelectedEventBlock = handler;
    self.lblTitle.text = [dict stringValueForJSON:@"title"];
    
    if ([[dict stringValueForJSON:@"selected"] isBlankValidationPassed])
    {
        _btnOption1.selected = [[dict numberForJson:@"selected"] isEqual:@1];
        _btnOption2.selected = [[dict numberForJson:@"selected"] isEqual:@2];
    }
    else
    {
        _btnOption1.selected =
        _btnOption2.selected = NO;
    }
    
    
    _lblOption1.attributedText = [self setAttributedString:[dict valueForKey:@"options1"]];
    _lblOption2.attributedText = [self setAttributedString:[dict valueForKey:@"options2"]];

}

- (NSMutableAttributedString *)setAttributedString:(NSArray*)array
{
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc]init];
    
    if(array.count <= 1)
    {
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",array[0]]];
        
        [attributedTitle appendAttributedString:title];
        
    }
    else
    {
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@"arrow"];
        
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        
        for (int i = 0; i < array.count; i++)
        {
            NSString *name =  array[i];
            NSAttributedString *title = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",name]];
            
            [attributedTitle appendAttributedString:title];
            
            if(array.count - 1 != i)
            {
                [attributedTitle appendAttributedString:attachmentString];
                [attributedTitle appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
            }
        }
    }
    
    return attributedTitle;
}

-(IBAction)btnRadioClicked:(UIButton*)sender
{
    if (sender.selected) return;
    
    _btnOption1.selected =
    _btnOption2.selected = NO;
    
    sender.selected = YES;
    
    if (self.CellSelectedEventBlock)
        self.CellSelectedEventBlock([NSNumber numberWithInteger:sender.tag]);
}
@end
