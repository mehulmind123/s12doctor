//
//  MITutorialCollectionViewCell.h
//  S12Doctor
//
//  Created by S12 Solutions on 12/1/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITutorialCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgVTutorial;
@property (strong, nonatomic) IBOutlet UILabel *lblTutorial;

@property (strong, nonatomic) IBOutlet UIView *viewTutorialImg;
@property (strong, nonatomic) IBOutlet UIView *viewTutorialText;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;

@end
