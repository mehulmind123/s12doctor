//
//  MITutorialCollectionViewCell.m
//  S12Doctor
//
//  Created by S12 Solutions on 12/1/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MITutorialCollectionViewCell.h"

@implementation MITutorialCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (IS_IPAD)
        _lblTutorial.font = _lblDescription.font = CFontAvenirLTStd85Heavy(12);
}

@end
