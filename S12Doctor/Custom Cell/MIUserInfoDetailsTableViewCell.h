//
//  MIUserInfoDetailsTableViewCell.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIUserInfoDetailsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *vwShadow;
@property (nonatomic, weak) IBOutlet UIView *vwContent;
@property (nonatomic, weak) IBOutlet UIView *vwTopSeprator;
@property (nonatomic, weak) IBOutlet UIView *vwBottomSeprator;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblAddress;
@property (nonatomic, weak) IBOutlet UILabel *lblContactNo;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;

@end
