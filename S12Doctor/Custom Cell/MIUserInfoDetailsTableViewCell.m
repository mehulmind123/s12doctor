//
//  MIUserInfoDetailsTableViewCell.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIUserInfoDetailsTableViewCell.h"

@implementation MIUserInfoDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.vwShadow.layer.cornerRadius = 5;
    self.vwShadow.layer.shadowColor = ColorBlack.CGColor;
    self.vwShadow.layer.shadowOffset = CGSizeZero;
    self.vwShadow.layer.shadowRadius = 4;
    self.vwShadow.layer.shadowOpacity = 0.2;
    self.vwShadow.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
