//
//  CustomAnnotationView.h
//  PuncturePoint
//
//  Created by S12 Solutions on 6/18/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomAnnotationView : MKAnnotationView
{
    IBOutlet UIView *innerView;
    IBOutlet UIView *shadowView;
}

@property (strong, nonatomic) IBOutlet UIButton *btnSelf;

@property (nonatomic, weak)IBOutlet UIImageView *imgVIcon;
@property (nonatomic, weak)IBOutlet UILabel *lblTitle;
@property (nonatomic, weak)IBOutlet UILabel *lblStatusUpdateTime;
@property (nonatomic, weak)IBOutlet UILabel *lblStatus;
@property (nonatomic, weak)IBOutlet UILabel *lblStatusDescription;
@property (nonatomic, weak)IBOutlet UILabel *lblEmployer;
@property (nonatomic, weak)IBOutlet UILabel *lblEmployerType;
@property (nonatomic, weak)IBOutlet UIButton *btnFavourite;


- (void)configureStatus:(NSInteger)status;

@end
