//
//  CustomAnnotationView.m
//  PuncturePoint
//
//  Created by S12 Solutions on 6/18/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "CustomAnnotationView.h"

#define  Arrow_height 15

@implementation CustomAnnotationView


- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
//    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    self = [[[NSBundle mainBundle] loadNibNamed:@"CustomAnnotationView" owner:nil options:nil] lastObject];
    
    if (self)
    {
        self.canShowCallout = NO;
        shadowView.layer.cornerRadius =
        innerView.layer.cornerRadius = 5.0;
        shadowView.clipsToBounds = true;
        innerView.clipsToBounds = true;
    }
    
    return self;
}

- (void)configureStatus:(NSInteger)status
{
    
    switch (status)
    {
        case 3: // Unavailable
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_unavailable_selected"];
            self.lblStatus.text = @"Unavailable:";
            self.lblStatus.textColor = ColorUnavailable_ffba00;
            break;
        }
        case 2: // May be available
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_may_be_available_selected"];
            self.lblStatus.text = @"May be available:";
            self.lblStatus.textColor = ColorMayBeAvaialble_ffba00;
            
            break;
        }
        case 1: // Available
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_available_selected"];
            self.lblStatus.text = @"Available:";
            self.lblStatus.textColor = ColorAvaiable_00d800;
            
            break;
        }
        case 4: // Unknown
        {
            self.imgVIcon.image = [UIImage imageNamed:@"amhp_unknown_selected"];
            self.lblStatus.text = @"Unknown:";
            self.lblStatus.textColor = ColorUnknown_00d800;
            
            break;
        }
    }
}

@end
