//
//  CustomMapAnnotation.h
//  PuncturePoint
//
//  Created by S12 Solutions on 6/18/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

@interface CustomMapAnnotation : NSObject <MKAnnotation>
{
	CLLocationDegrees _latitude;
	CLLocationDegrees _longitude;
    
    
}

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;
@property (nonatomic, copy) NSIndexPath *indexPath;

- (id)initWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude;

@end
