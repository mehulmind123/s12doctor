//
//  CustomMapAnnotation.m
//  PuncturePoint
//
//  Created by S12 Solutions on 6/18/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "CustomMapAnnotation.h"

@interface CustomMapAnnotation()


@end

@implementation CustomMapAnnotation

@synthesize latitude = _latitude;
@synthesize longitude = _longitude;

- (id)initWithLatitude:(CLLocationDegrees)latitude
		  andLongitude:(CLLocationDegrees)longitude {
	if (self = [super init]) {
		self.latitude = latitude;
		self.longitude = longitude;
	}
	return self;
}

- (CLLocationCoordinate2D)coordinate {
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = self.latitude;
	coordinate.longitude = self.longitude;
	return coordinate;
}

@end
