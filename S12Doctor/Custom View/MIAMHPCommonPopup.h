//
//  MICommonPopup.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIAMHPCommonPopup : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblAddNewLanguage;
@property (weak, nonatomic) IBOutlet UITextField *txtCommonField;
@property (weak, nonatomic) IBOutlet MIGenericButton *btnSubmit;


+(id)customCommonPopup;

@end
