//
//  MICommonPopup.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIAMHPCommonPopup.h"

@implementation MIAMHPCommonPopup

+(id)customCommonPopup
{
    MIAMHPCommonPopup  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIAMHPCommonPopup" owner:nil options:nil] lastObject];
    
    CGFloat width = (CScreenWidth * 311) / 375;
    
    customView.frame = CGRectMake(0, Is_iPhone_4 || Is_iPhone_5 ? 125 : 0, width , (width * 196) / 311);
    
    customView.layer.cornerRadius = 6;
    
    customView.txtCommonField.autocorrectionType = UITextAutocorrectionTypeYes;
    [customView.txtCommonField addLeftPaddingWithWidth:15];
    customView.txtCommonField.layer.borderWidth = 1;
    customView.txtCommonField.layer.borderColor = CRGB(242, 242, 242).CGColor;
    customView.txtCommonField.layer.cornerRadius = 2;
    
    return customView;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if ([_txtCommonField isFirstResponder])
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
        }];
    }
    return [super canPerformAction:action withSender:sender];
}

@end
