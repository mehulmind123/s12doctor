//
//  MICommonPopup.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MICommonPopup.h"

@implementation MICommonPopup

+(id)customCommonPopup
{
    MICommonPopup  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MICommonPopup" owner:nil options:nil] lastObject];
    
    CGFloat width = (CScreenWidth * 311) / 375;
    
    customView.frame = CGRectMake(0, Is_iPhone_4 || Is_iPhone_5 ? 125 : 0, width , (width * 196) / 311);
    
    customView.layer.cornerRadius = 6;
    
    if (IS_IPHONE_SIMULATOR)
        customView.txtCommonField.text = @"mind@123";
    
    customView.txtCommonField.autocorrectionType = UITextAutocorrectionTypeYes;
    [customView.txtCommonField addLeftPaddingWithWidth:15];
    customView.txtCommonField.layer.borderWidth = 1;
    customView.txtCommonField.layer.borderColor = CRGB(242, 242, 242).CGColor;
    customView.txtCommonField.layer.cornerRadius = 2;
    
    return customView;
}





# pragma mark
# pragma mark - UITextField Delegate

- (IBAction)textFieldTextDidChange:(UITextField *)sender
{
    if (sender.text.length > CCharacterLimit256)
    {
        sender.text = [sender.text substringToIndex:CCharacterLimit256];
        return;
    }
    else
        sender.text = sender.text;
}

@end
