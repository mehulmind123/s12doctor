//
//  MIUpdateStatusPopup.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIUpdateStatusPopup : UIView<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (strong, nonatomic) IBOutlet UITextView *txtAvailabilityStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnAvailable;
@property (weak, nonatomic) IBOutlet UIButton *btnMaybeAvailable;
@property (weak, nonatomic) IBOutlet UIButton *btnUnAvailable;
@property (weak, nonatomic) IBOutlet MIGenericButton *btnSave;


+(id)customUpdateStatusPopup;

@end
