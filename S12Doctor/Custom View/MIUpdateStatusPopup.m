//
//  MIUpdateStatusPopup.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIUpdateStatusPopup.h"

@implementation MIUpdateStatusPopup

- (void)awakeFromNib
{
    [super awakeFromNib];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:nil];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_txtAvailabilityStatus setContentOffset:CGPointZero animated:NO];
    [_txtAvailabilityStatus layoutIfNeeded];
}

+(id)customUpdateStatusPopup
{
    MIUpdateStatusPopup  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIUpdateStatusPopup" owner:nil options:nil] lastObject];
    
    customView.frame = CGRectMake(0, Is_iPhone_5 || Is_iPhone_4 ? 65 : 0, Is_iPhone_5 || Is_iPhone_4 ? 290 : 311 , 279);
    
    customView.viewMain.layer.cornerRadius = 6;
    
    [customView.txtAvailabilityStatus setPlaceholder:CMessagePlaceHolderAvailabilty];
    customView.txtAvailabilityStatus.layer.borderWidth = 1;
    customView.txtAvailabilityStatus.layer.borderColor = CRGB(236, 236, 247).CGColor;
    customView.txtAvailabilityStatus.layer.cornerRadius = 4;
    
    return customView;
}





# pragma mark
# pragma mark - UITextview Delegate

- (void)textDidChange
{
    if (_txtAvailabilityStatus.text.length > CCharacterLimit256)
    {
        _txtAvailabilityStatus.text = [_txtAvailabilityStatus.text substringToIndex:CCharacterLimit256];
        return;
    }
}

@end
