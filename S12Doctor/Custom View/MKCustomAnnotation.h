//
//  MKCustomAnnotation.h
//  AMHP
//
//  Created by S12 Solutions on 7/25/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MKCustomAnnotation : MKPointAnnotation

@property (nonatomic, copy) NSIndexPath *indexPath;
@property (nonatomic, assign) BOOL isSelected;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
