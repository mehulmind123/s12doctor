//
//  MKCustomAnnotation.m
//  AMHP
//
//  Created by S12 Solutions on 7/25/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MKCustomAnnotation.h"

@implementation MKCustomAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    self = [super init];
    
    if (self)
    {
        self.coordinate = coordinate;
    }
    
    return self;
}

@end
