//
//  NoInternetView.h
//  Patch
//
//  Created by S12 Solutions on 11/17/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoInternetView : UIView


@property(nonatomic,strong) IBOutlet UILabel *lblNoInternet;
@property(nonatomic,strong) IBOutlet UIView *viewInternet;

+(id)customInternetPopup;

@end
