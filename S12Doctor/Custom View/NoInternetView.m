//
//  NoInternetView.m
//  Patch
//
//  Created by S12 Solutions on 11/17/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "NoInternetView.h"

@implementation NoInternetView

- (void)awakeFromNib
{
    [super awakeFromNib];
}

+(id)customInternetPopup
{
    NoInternetView  *customView = [[[NSBundle mainBundle] loadNibNamed:@"NoInternetView" owner:nil options:nil] lastObject];
    
    customView.frame = CGRectMake(0, 0, CScreenWidth, 30);
    
    return customView;
}

@end
