//
//  MIGenericButton.m
//  EdSmart
//
//  Created by S12 Solutions on 15/07/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIGenericButton.h"

@implementation MIGenericButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    NSString *fontName = self.titleLabel.font.fontName;
    CGFloat fontSize = self.titleLabel.font.pointSize;
    
    if(Is_iPhone_4 || Is_iPhone_5)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 1)];
    else if(Is_iPhone_6_PLUS)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 1)];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setShadow];
}


- (void)setShadow
{
    self.layer.cornerRadius = CViewHeight(self) / 2;
    self.layer.shadowColor = self.tag == 500 ? ColorGreen_47C0B6.CGColor : ColorBlue_1371b9.CGColor;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowRadius = 4;
    self.layer.shadowOpacity = 0.8;
    self.layer.masksToBounds = NO;
}

@end
