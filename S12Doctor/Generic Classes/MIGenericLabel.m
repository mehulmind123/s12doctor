//
//  MIGenericLabel.m
//  EdSmart
//
//  Created by S12 Solutions on 15/07/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIGenericLabel.h"

@implementation MIGenericLabel

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    if(Is_iPhone_4 || Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 1)];
    else if(Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 1)];
}

@end
