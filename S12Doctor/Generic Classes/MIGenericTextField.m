//
//  MIGenericTextField.m
//  EdSmart
//
//  Created by S12 Solutions on 15/07/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIGenericTextField.h"

@implementation MIGenericTextField

-(void)awakeFromNib
{
    [super awakeFromNib];
    
//    if(Is_iPhone_4 || Is_iPhone_5)
//        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 2)];
//    else if(Is_iPhone_6_PLUS)
//        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 2)];
    
    [self initialize];
}

-(void)initialize
{
    if (self.tag == 100)
    {
        self.textColor = ColorWhite;
        self.layer.cornerRadius = 4;
        self.layer.borderWidth = 1;
        self.layer.borderColor = ColorWhite.CGColor;
        [self addLeftPaddingWithWidth:10];
    }
}

@end
