//
//  MIGenericView.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIGenericView.h"

@implementation MIGenericView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.cornerRadius = 5;
    self.layer.shadowColor = ColorBlack.CGColor;
    
    if (self.tag == 500)
    {
        self.layer.shadowOffset = CGSizeMake(1,1);
        self.layer.shadowRadius = 0.5;
        self.layer.shadowOpacity = 0.2;
    }
    else
    {
        self.layer.shadowOffset = CGSizeZero;
        self.layer.shadowRadius = 6;
        self.layer.shadowOpacity = 0.1;
    }
}

@end
