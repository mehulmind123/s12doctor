//
//  BasicAppDelegate.h
//  TestSuperAppDelegate
//
//  Created by S12 Solutions on 10/11/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Master.h"

@interface BasicAppDelegate : UIResponder


- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions NS_AVAILABLE_IOS(6_0);


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

@end
