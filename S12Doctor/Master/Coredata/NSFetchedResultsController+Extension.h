//
//  NSFetchedResultsController+Extension.h
//  Master
//
//  Created by S12 Solutions on 25/06/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSFetchedResultsController (Extension)

-(NSUInteger)objectsCount;
-(NSUInteger)objectsCountInSection:(NSUInteger)section;

@end
