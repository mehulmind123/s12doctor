//
//  NSManagedObjectContext+Helper.h
//  MI API Example
//
//  Created by S12 Solutions on 9/19/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "Master.h"

@interface NSManagedObjectContext (Helper)

-(void)save;
-(void)saveInBackground;

-(void)performSafeBlock:(MIVoidBlock)block;

-(void)saveWithHandler:(void (^)(BOOL successs, NSError *error))completion;
@end
