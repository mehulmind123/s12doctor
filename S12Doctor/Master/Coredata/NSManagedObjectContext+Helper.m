//
//  NSManagedObjectContext+Helper.m
//  MI API Example
//
//  Created by S12 Solutions on 9/19/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "NSManagedObjectContext+Helper.h"

#import "NSObject+NewProperty.h"


@implementation NSManagedObjectContext (Helper)

-(void)save
{
    [self saveInBackground];    
}

-(void)saveInBackground
{
    if ([self hasChanges])
    {
        [self performSafeBlock:^{
            __block NSError *error;
            if (![self save:&error])
                NSLog(@"Error while saving for context %@ == %@ == %@", self, error, [error localizedDescription]);
            else if ([self parentContext])
                [[self parentContext] saveInBackground];
        }];
    }
}

- (void)saveWithHandler:(void (^)(BOOL success, NSError *))completion
{
    if ([self hasChanges])
    {
        [self performSafeBlock:^{
            __block NSError *error;
            
            BOOL isSave = [self save:&error];
         
            
            if (isSave && completion)
            {
                NSLog(@"Saved successfully ");
                completion(YES, nil);
            }
            
            if (!isSave)
                NSLog(@"Error while saving for context %@ == %@ == %@", self, error, [error localizedDescription]);
            else if ([self parentContext])
                [[self parentContext] saveWithHandler:completion];
        }];
    }
}

-(void)performSafeBlock:(MIVoidBlock)block
{
    NSThread *thread = [self objectForKey:@"thread"];
    
    if (self.concurrencyType == NSMainQueueConcurrencyType && [NSThread isMainThread])
        block();
    else if (thread == [NSThread currentThread])
        block();
    else
    {
        [self performBlock:^{
            
            if (!thread)
                [self setObject:[NSThread currentThread] forKey:@"thread"];
            
            block();
            
        }];
    }
}



@end
