//
//  CustomAlertView.h
//  EdSmart
//
//  Created by S12 Solutions on 15/03/2016.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView

@property (strong, nonatomic) IBOutlet UILabel *lblAlert;
@property (strong, nonatomic)  NSTimer *timerAlert;

+ (instancetype)sharedInstance;

+ (void)iOSAlert:(NSString *)title withMessage:(NSString *)message onView:(UIViewController *)vc;
+ (void)showStatusBarAlert:(NSString *)message;
+ (void)dismissStatusbarAlert;
@end
