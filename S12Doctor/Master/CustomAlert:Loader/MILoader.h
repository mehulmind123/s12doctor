//
//  MILoader.h
//  Example
//
//  Created by S12 Solutions on 19/03/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MILoader : UIView

@property (nonatomic, strong) IBOutlet UIButton     *btnBack;
@property (nonatomic, strong) IBOutlet UIView       *containView;
@property (nonatomic, strong) IBOutlet UIView       *loaderView;
@property (nonatomic, strong) IBOutlet UILabel      *lblText;

+ (instancetype)sharedInstance;

- (void)startAnimation;
- (void)stopAnimation;
- (BOOL)isAnimating;


@end
