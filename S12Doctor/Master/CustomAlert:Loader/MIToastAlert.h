//
//  MIToastAlert.h
//  Engage
//
//  Created by S12 Solutions on 07/01/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIToastAlert : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgVDone;

- (id)initWithMessage:(NSString *)message;

@end
