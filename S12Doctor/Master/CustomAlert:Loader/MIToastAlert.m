//
//  MIToastAlert.m
//  Engage
//
//  Created by S12 Solutions on 07/01/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIToastAlert.h"

@interface MIToastAlert ()
{
    IBOutlet UILabel *lblMessage;
}
@end

@implementation MIToastAlert

- (id)initWithMessage:(NSString *)message
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"MIToastAlert" owner:nil options:nil] lastObject];
    
    [lblMessage setText:message];
    [lblMessage layoutIfNeeded];
    
    self.backgroundColor = appDelegate.loginUser.login_type == 1 ? ColorBlue_1371b9 : ColorGreen_47C0B6;
    
    CGFloat vHeight = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CViewSetSize(self, CScreenWidth, vHeight);
    return self;
}

@end
