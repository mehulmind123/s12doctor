//
//  DelegateObserver.h
//  EdSmart
//
//  Created by S12 Solutions on 02/01/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface DelegateObserver : NSObject <UIPickerViewDelegate, UIPickerViewDataSource,AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate, UITextViewDelegate>

+ (DelegateObserver *)sharedInstance;

-(NSMutableDictionary *)content;

@end
