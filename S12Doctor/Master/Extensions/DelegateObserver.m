//
//  DelegateObserver.m
//  EdSmart
//
//  Created by S12 Solutions on 02/01/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "DelegateObserver.h"

#import "Master.h"
#import "NSObject+NewProperty.h"

#import "JTSImageInfo.h"
#import "JTSImageViewController.h"
#import "JTSSimpleImageDownloader.h"
#import "JTSAnimatedGIFUtility.h"
#import "UIImage+JTSImageEffects.h"
#import "UIApplication+JTSImageViewController.h"

@interface NSObject ()

@end



@interface DelegateObserver ()
{
    NSMutableDictionary *_content;
}
@end

@implementation DelegateObserver

+ (DelegateObserver *)sharedInstance
{
    static DelegateObserver *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[DelegateObserver alloc] init];
    });
    
    return _sharedInstance;
}

-(NSMutableDictionary *)content
{
    if (!_content)
        _content = [[NSMutableDictionary alloc] init];
    
    return _content;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return [textField textFieldShouldBeginEditing:textField];
}


#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView textViewDidBeginEditing:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView textViewDidEndEditing:textView];
}

- (void)textViewDidChange:(UITextView *)textView
{
    [textView textViewDidChange:textView];
}

#pragma mark - UIPickerView

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return [[UITextField activePickerField] numberOfComponentsInPickerView:pickerView];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[UITextField activePickerField] pickerView:pickerView numberOfRowsInComponent:component];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[UITextField activePickerField] pickerView:pickerView titleForRow:row forComponent:component];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [[UITextField activePickerField] pickerView:pickerView didSelectRow:row inComponent:component];
}


#pragma mark - UIImageView


- (void)imageTapped:(UITapGestureRecognizer *)sender {
    
    UIImageView *imageView = (UIImageView *) sender.view;
    
    // Create image info
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.image = imageView.image;
    imageInfo.referenceRect = imageView.frame;
    imageInfo.referenceView = imageView.superview;
    imageInfo.referenceContentMode = imageView.contentMode;
    imageInfo.referenceCornerRadius = imageView.layer.cornerRadius;
    
    // Setup view controller
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_None];
    
    // Present the view controller.
    [imageViewer showFromViewController:[imageView viewController] transition:JTSImageViewControllerTransition_FromOriginalPosition];
}





@end
