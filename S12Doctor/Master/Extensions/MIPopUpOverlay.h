//
//  MIPopUpOverlay.h
//  Engage
//
//  Created by S12 Solutions on 18/11/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXBlurView.h"

typedef enum : NSUInteger
{
    PresentTypeBottom,
    PresentTypeCenter,
    PresentTypeRight,
    PresentTypeCustom
} PresentType;

@interface MIPopUpOverlay : UIView

@property (nonatomic, assign) PresentType presentType;

@property (assign, nonatomic) BOOL shouldCloseOnClickOutside;

@property (weak, nonatomic) IBOutlet FXBlurView *blurView;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;

+(id)popUpOverlay;

@end
