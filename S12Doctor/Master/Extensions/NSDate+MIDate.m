//
//  NSDate+MIDate.m
//  EdSmart
//
//  Created by S12 Solutions on 7/16/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "NSDate+MIDate.h"

@implementation NSDate (MIDate)

#ifndef NSDateTimeAgoLocalizedStrings
#define NSDateTimeAgoLocalizedStrings(key) \
[NSString stringWithFormat:@"%@", key]
#endif

+ (NSDateFormatter *)dateFormatter
{
    static dispatch_once_t onceToken;
    static NSDateFormatter *dateFormatter;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    });
    
    return dateFormatter;
}

+ (NSCalendar *)calendar
{
    static dispatch_once_t onceToken;
    static NSCalendar *calendar;
    dispatch_once(&onceToken, ^{
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        calendar.timeZone = [NSTimeZone localTimeZone];
        calendar.locale = [NSLocale currentLocale];
    });
    
    return calendar;
}

+ (NSInteger)getCurrentHour
{
    NSTimeZone *defaultTimezone = [NSTimeZone defaultTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:[NSDate date]];
    NSInteger hour = [components hour];
    [NSTimeZone setDefaultTimeZone:defaultTimezone];
    return hour;
}

+ (NSString *)currentDateStringWithServerFormat
{
    NSDate *date = [NSDate date];
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    inputFormatter.dateFormat = @"EEE, d LLL yyyy HH:mm:ss zzzz";
    inputFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSString *dateString = [inputFormatter stringFromDate:date];
    return dateString;
}

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    NSDate *date = [[NSDate dateFormatter] dateFromString:string];
    return date;
}

+ (NSString *)convertStringFromDate:(NSString *)string withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSDate *date = [[NSDate dateFormatter] dateFromString:string];
    
    [[NSDate dateFormatter] setDateFormat:format];
    
    return [[NSDate dateFormatter] stringFromDate:date];
}

+ (NSString *)stringFromDateForGMT:(NSDate *)date withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    [[NSDate dateFormatter] setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    NSString *string = [[NSDate dateFormatter] stringFromDate:date];
    return string;
}

+ (NSDate *)dateFromStringGMT:(NSString *)datestring withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    [[NSDate dateFormatter] setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];  // Convert into GMT formate
    return [[NSDate dateFormatter] dateFromString:datestring];
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    [[NSDate dateFormatter] setTimeZone:[NSTimeZone systemTimeZone]];
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier: [[NSLocale currentLocale] objectForKey:@"kCFLocaleLanguageCodeKey"]];
    [[NSDate dateFormatter] setLocale:locale];
    NSString *string = [[NSDate dateFormatter] stringFromDate:date];
    return string;
}

+ (NSDate *)firstDayOfMonth:(NSDate *)date
{
    NSDateComponents *components = [[NSDate calendar] components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date];
    [components setDay:1];
    
    return [[NSDate calendar] dateFromComponents:components];
}

+ (NSDate *)lastDayOfMonth:(NSDate *)date
{
    NSDateComponents *components = [[NSDate calendar] components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    if (month == 12)
    {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else
        [components setMonth:month+1];
    
    [components setDay:1];
    
    return [[NSDate calendar] dateFromComponents:components];
}

- (NSString *)timeAgo
{
    NSDate *now = [NSDate date];
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    
    int minutes;
    
    if (deltaMinutes <= (24 * 60))
    {
        minutes = (int)floor(deltaMinutes/60);
        
        if (minutes <= 4)
            return @"Breaking Post";
        else
            return @"Posted Today";
    }
    //    if(deltaSeconds < 5)
    //    {
    //        return NSDateTimeAgoLocalizedStrings(@"Just now");
    //    }
    //    else if(deltaSeconds < 60)
    //    {
    //        return [self stringFromFormat:@"%%d %@seconds ago" withValue:deltaSeconds];
    //    }
    //    else if(deltaSeconds < 120)
    //    {
    //        return NSDateTimeAgoLocalizedStrings(@"A minute ago");
    //    }
    //    else if (deltaMinutes < 60)
    //    {
    //        return [self stringFromFormat:@"%%d %@minutes ago" withValue:deltaMinutes];
    //    }
    //    else if (deltaMinutes < 120)
    //    {
    //        return NSDateTimeAgoLocalizedStrings(@"An hour ago");
    //    }
    //    else if (deltaMinutes < (24 * 60))
    //    {
    //        minutes = (int)floor(deltaMinutes/60);
    //        return [self stringFromFormat:@"%%d %@hours ago" withValue:minutes];
    //    }
    else if (deltaMinutes < (24 * 60 * 2))
    {
        return NSDateTimeAgoLocalizedStrings(@"Yesterday");
    }
    else if (deltaMinutes < (24 * 60 * 7))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24));
        return [self stringFromFormat:@"%%d %@days ago" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 14))
    {
        return NSDateTimeAgoLocalizedStrings(@"Last week");
    }
    else if (deltaMinutes < (24 * 60 * 31))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 7));
        return [self stringFromFormat:@"%%d %@weeks ago" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 61))
    {
        return NSDateTimeAgoLocalizedStrings(@"Last month");
    }
    else if (deltaMinutes < (24 * 60 * 365.25))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 30));
        return [self stringFromFormat:@"%%d %@months ago" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 731))
    {
        return NSDateTimeAgoLocalizedStrings(@"Last year");
    }
    
    minutes = (int)floor(deltaMinutes/(60 * 24 * 365));
    return [self stringFromFormat:@"%%d %@years ago" withValue:minutes];
}


// Similar to timeAgo, but only returns "

- (NSString *)displayTimeofStatusChange:(NSString *)createdDate
{
    NSDate *fromDate = [NSDate dateFromString:createdDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate * now = [NSDate date];
    NSDateComponents *components = [calendar components:
                                    NSCalendarUnitYear|
                                    NSCalendarUnitMonth|
                                    NSCalendarUnitWeekOfMonth|
                                    NSCalendarUnitDay|
                                    NSCalendarUnitHour|
                                    NSCalendarUnitMinute|
                                    NSCalendarUnitSecond
                                               fromDate:fromDate
                                                 toDate:now
                                                options:0];
    
    if (components.year >= 1)
    {
        if (components.year == 1)
            return NSDateTimeAgoLocalizedStrings(@"1 year ago");
        
        return [self stringFromFormat:@"%%d %@years ago" withValue:components.year];
    }
    else if (components.month >= 1)
    {
        if (components.month == 1)
            return NSDateTimeAgoLocalizedStrings(@"1 month ago");
        
        return [self stringFromFormat:@"%%d %@months ago" withValue:components.month];
    }
    else if (components.weekOfMonth >= 1)
    {
        if (components.weekOfMonth == 1)
            return NSDateTimeAgoLocalizedStrings(@"1 week ago");
        
        return [self stringFromFormat:@"%%d %@weeks ago" withValue:components.weekOfMonth];
    }
    else if (components.day >= 1)    // up to 6 days ago
    {
        if (components.day == 1)
            return NSDateTimeAgoLocalizedStrings(@"Yesterday");

        return [self stringFromFormat:@"%%d %@days ago" withValue:components.day];
    }
    else if (components.hour >= 1)   // up to 23 hours ago
    {
        if (components.hour == 1)
            return NSDateTimeAgoLocalizedStrings(@"An hour ago");
        
        return [self stringFromFormat:@"%%d %@hours ago" withValue:components.hour];
    }
    else if (components.minute >= 1) // up to 59 minutes ago
    {
        if (components.minute == 1)
            return NSDateTimeAgoLocalizedStrings(@"A minute ago");
        
        return [self stringFromFormat:@"%%d %@minutes ago" withValue:components.minute];
    }
    else
        return NSDateTimeAgoLocalizedStrings(@"Just now");
}



+ (NSString *)setTime:(NSString *)createdDate
{
    NSDate *fromDate = [NSDate dateFromString:createdDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:fromDate toDate:[NSDate date] options:0];
    
    NSInteger years  = [dateComponents year];
    NSInteger months  = [dateComponents month];
    NSInteger week    = [dateComponents weekOfYear];
    NSInteger days     = [dateComponents day];
    NSInteger hours    = [dateComponents hour];
    NSInteger minutes  = [dateComponents minute];
    
    NSString *duration;
    
    if (years > 0)
    {
        (years == 1)?
        (duration = [NSString stringWithFormat:@"updated %ld year ago",(long)years]):
        (duration = [NSString stringWithFormat:@"updated %ld years ago",(long)years]);
    }
    else if (months > 0)
    {
        (months == 1)?
        (duration = [NSString stringWithFormat:@"updated %ld month ago",(long)months]):
        (duration = [NSString stringWithFormat:@"updated %ld months ago",(long)months]);
    }
    else if (week > 0)
    {
        (week == 1)?
        (duration = [NSString stringWithFormat:@"updated %ld month ago",(long)months]):
        (duration = [NSString stringWithFormat:@"updated %ld months ago",(long)months]);
    }
    else if (days > 0)
    {
        (days == 1)?
        (duration = [NSString stringWithFormat:@"updated %ld day ago",(long)days]):
        (duration = [NSString stringWithFormat:@"updated %ld days ago",(long)days]);
    }
    else if (hours > 0)
    {
        (hours == 1)?
        (duration = [NSString stringWithFormat:@"updated %ld hour ago",(long)hours]):
        (duration = [NSString stringWithFormat:@"updated %ld hours ago",(long)hours]);
    }
    else if (minutes > 0)
    {
        (minutes < 1) ?(duration = @"Just Now"):
        (duration = [NSString stringWithFormat:@"updated %ld minutes ago",(long)minutes]);
    }
    else
    {
        duration = @"Just Now";
    }
    
    return duration;
}

+ (NSString *)timeAgoFromTimestamp:(NSString *)timestamp
{
    NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:timestamp withFormat:@"yyyy-MM-dd HH:mm:ss"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *systemDate = [NSDate dateFromString:strClaimFormDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *fromDate = [NSDate dateFromString:strClaimFormDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:fromDate toDate:[NSDate date] options:0];
    
    NSInteger years     = [dateComponents year];
    NSInteger month     = [dateComponents month];
    NSInteger weeks     = [dateComponents weekOfYear];
    NSInteger days      = [dateComponents day];
    NSInteger hours     = [dateComponents hour];
    NSInteger minutes   = [dateComponents minute];
    
    NSString *duration;
    
    if (years > 0)
        duration = [NSString stringWithFormat:@"updated %ld %@ ago", (long)years, (years > 1)?@"years":@"year"];
    
    if (month > 0)
        duration = [NSString stringWithFormat:@"updated %ld %@ ago", (long)month, (month > 1)?@"months":@"month"];
    
    else if (weeks > 0)
        duration = [NSString stringWithFormat:@"updated %ld %@ ago", (long)weeks, (weeks > 1)?@"weeks":@"week"];
    
    else if (days > 0)
        duration = [NSString stringWithFormat:@"updated %ld %@ ago", (long)days, (days > 1)?@"days":@"day"];
    
    else if (hours > 0)
        duration = [NSString stringWithFormat:@"updated %ld %@ ago", (long)hours, (hours > 1)?@"hours":@"hour"];
    
    else if (minutes > 1)
        duration = [NSString stringWithFormat:@"updated %ld %@ ago", (long)minutes, (minutes > 1)?@"minutes":@"minute"];
    
    else
        duration = @"updated Just now";
    
    return [NSString stringWithFormat:@"%@", duration];
}



- (NSString *)dateTimeUntilNow
{
    NSDate * now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitHour
                                               fromDate:self
                                                 toDate:now
                                                options:0];
    
    if (components.hour >= 6) // if more than 6 hours ago, change precision
    {
        NSInteger startDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
                                                 inUnit:NSCalendarUnitEra
                                                forDate:self];
        NSInteger endDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
                                               inUnit:NSCalendarUnitEra
                                              forDate:now];
        
        NSInteger diffDays = endDay - startDay;
        if (diffDays == 0) // today!
        {
            NSDateComponents * startHourComponent = [calendar components:NSCalendarUnitHour fromDate:self];
            NSDateComponents * endHourComponent = [calendar components:NSCalendarUnitHour fromDate:self];
            if (startHourComponent.hour < 12 &&
                endHourComponent.hour > 12)
            {
                return NSDateTimeAgoLocalizedStrings(@"This morning");
            }
            else if (startHourComponent.hour >= 12 &&
                     startHourComponent.hour < 18 &&
                     endHourComponent.hour >= 18)
            {
                return NSDateTimeAgoLocalizedStrings(@"This afternoon");
            }
            return NSDateTimeAgoLocalizedStrings(@"Today");
        }
        else if (diffDays == 1)
        {
            return NSDateTimeAgoLocalizedStrings(@"Yesterday");
        }
        else
        {
            NSInteger startWeek = [calendar ordinalityOfUnit:NSCalendarUnitWeekOfMonth
                                                      inUnit:NSCalendarUnitEra
                                                     forDate:self];
            NSInteger endWeek = [calendar ordinalityOfUnit:NSCalendarUnitWeekOfMonth
                                                    inUnit:NSCalendarUnitEra
                                                   forDate:now];
            NSInteger diffWeeks = endWeek - startWeek;
            if (diffWeeks == 0)
            {
                return NSDateTimeAgoLocalizedStrings(@"This week");
            }
            else if (diffWeeks == 1)
            {
                return NSDateTimeAgoLocalizedStrings(@"Last week");
            }
            else
            {
                NSInteger startMonth = [calendar ordinalityOfUnit:NSCalendarUnitMonth
                                                           inUnit:NSCalendarUnitEra
                                                          forDate:self];
                NSInteger endMonth = [calendar ordinalityOfUnit:NSCalendarUnitMonth
                                                         inUnit:NSCalendarUnitEra
                                                        forDate:now];
                NSInteger diffMonths = endMonth - startMonth;
                if (diffMonths == 0)
                {
                    return NSDateTimeAgoLocalizedStrings(@"This month");
                }
                else if (diffMonths == 1)
                {
                    return NSDateTimeAgoLocalizedStrings(@"Last month");
                }
                else
                {
                    NSInteger startYear = [calendar ordinalityOfUnit:NSCalendarUnitYear
                                                              inUnit:NSCalendarUnitEra
                                                             forDate:self];
                    NSInteger endYear = [calendar ordinalityOfUnit:NSCalendarUnitYear
                                                            inUnit:NSCalendarUnitEra
                                                           forDate:now];
                    NSInteger diffYears = endYear - startYear;
                    if (diffYears == 0)
                    {
                        return NSDateTimeAgoLocalizedStrings(@"This year");
                    }
                    else if (diffYears == 1)
                    {
                        return NSDateTimeAgoLocalizedStrings(@"Last year");
                    }
                }
            }
        }
    }
    
    // anything else uses "time ago" precision
//    return [self displayTimeofDataPost];
    return nil;
}



- (NSString *) stringFromFormat:(NSString *)format withValue:(NSInteger)value
{
    NSString * localeFormat = [NSString stringWithFormat:format, [self getLocaleFormatUnderscoresWithValue:value]];
    return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(localeFormat), value];
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

- (NSString *)getLocaleFormatUnderscoresWithValue:(double)value
{
    NSString *localeCode = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    // Russian (ru)
    if([localeCode isEqual:@"ru"]) {
        int XY = (int)floor(value) % 100;
        int Y = (int)floor(value) % 10;
        
        if(Y == 0 || Y > 4 || (XY > 10 && XY < 15)) return @"";
        if(Y > 1 && Y < 5 && (XY < 10 || XY > 20))  return @"_";
        if(Y == 1 && XY != 11)                      return @"__";
    }
    
    // Add more languages here, which are have specific translation rules...
    
    return @"";
}

#pragma clang diagnostic pop

@end
