//
//  NSDictionary+MIExtension.h
//  MI API Example
//
//  Created by S12 Solutions on 8/6/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MIExtension)

-(id)valueForJSON:(NSString *)key;
-(NSString *)stringValueForJSON:(NSString *)key;

-(NSNumber *)numberForJson:(NSString *)key;



-(NSNumber *)numberForInt:(NSString *)key;
-(NSNumber *)numberForDouble:(NSString *)key;
-(NSNumber *)numberForFloat:(NSString *)key;

@end
