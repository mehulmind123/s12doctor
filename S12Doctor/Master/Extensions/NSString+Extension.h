//
//  NSString+Extension.h
//  Master
//
//  Created by S12 Solutions on 12/12/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UIApplication+Extension.h"

@interface NSString (Extension)

-(void)setApplicationLifecycleComplete;
-(void)setInstallationLifecycleComplete;



-(void)performBlockForApplicaitonLifecycle:(Block)block;
-(void)performBlockForInstallationLifecycle:(Block)block;

-(void)performBlockForApplicaitonLifecycleWithoutAutoComplete:(Block)block;
-(void)performBlockForInstallationLifecycleWithoutAutoComplete:(Block)block;

@end
