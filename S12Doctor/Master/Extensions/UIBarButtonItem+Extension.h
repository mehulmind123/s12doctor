//
//  UIBarButtonItem+Extension.h
//  Master
//
//  Created by S12 Solutions on 16/04/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIButton+EventHandler.h"

@interface UIBarButtonItem (Extension)

-(void)clicked:(TouchUpInsideHandler)handler;

@end
