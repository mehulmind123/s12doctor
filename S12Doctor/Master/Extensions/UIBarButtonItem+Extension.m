//
//  UIBarButtonItem+Extension.m
//  Master
//
//  Created by S12 Solutions on 16/04/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "UIBarButtonItem+Extension.h"

#import "NSObject+NewProperty.h"

static NSString *const BARBUTTONITEMCALLBACKHANDLER = @"barbuttonitemHandler";

@implementation UIBarButtonItem (Extension)

-(void)clicked:(TouchUpInsideHandler)handler
{
    [self setObject:handler forKey:BARBUTTONITEMCALLBACKHANDLER];
    [self setTarget:self];
    [self setAction:@selector(touchUpInsideClickedEventFired:)];
}

-(void)touchUpInsideClickedEventFired:(UIBarButtonItem *)sender
{
    TouchUpInsideHandler handler = [self objectForKey:BARBUTTONITEMCALLBACKHANDLER];

    if (handler)
        handler();
}


@end
