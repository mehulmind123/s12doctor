//
//  UIImage+Extensiom.h
//  Master
//
//  Created by S12 Solutions on 28/02/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

+(UIImage *)leftImage;
+(UIImage *)rightImage;
+(UIImage *)backImage;

+(UIImage *)safariGlymphImage;
+(UIImage *)preferencesImage:(NSString *)image;

@end
