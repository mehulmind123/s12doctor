//
//  UILabel+Extension.h
//  Master
//
//  Created by S12 Solutions on 12/06/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

- (void)detectHashTagClickHandler:(void(^)(NSString *hashTag))block;;

- (void)detectLinkClickHandler:(void(^)(NSString *stringURL))block;

@end
