//
//  UILabel+Extension.m
//  Master
//
//  Created by S12 Solutions on 12/06/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "UILabel+Extension.h"

#import "NSObject+NewProperty.h"

static NSString *const TTTATTRIBUTEDLABELCLICKHANDLER = @"blockKey";

@interface NSObject ()

-(void)setDelegate:(id)delegate;

-(void)addLinkWithTextCheckingResult:(NSTextCheckingResult*)match attributes:(NSDictionary*)attributes;

@end


static NSRegularExpression *hashtagExpression = nil;


@implementation UILabel (Extension)

- (void)detectHashTagClickHandler:(void(^)(NSString *hashTag))block;
{
    if (![self isKindOfClass:NSClassFromString(@"TTTAttributedLabel")])
        NSAssert(nil, @"This will support only  TTTAttributedLabel.");
    
    
    NSString *string = self.text;
    NSArray *matches = [[NSRegularExpression regularExpressionWithPattern:@"(?:^|\\s)(#\\w+|@\\w+)" options:NO error:nil] matchesInString:string options:0 range:NSMakeRange(0, string.length)];
    
    
    for (NSTextCheckingResult *match in matches)
        [self addLinkWithTextCheckingResult:match attributes:nil];
    
    self.userInteractionEnabled = YES;
    [self setDelegate:self];
    
    [self setObject:block forKey:TTTATTRIBUTEDLABELCLICKHANDLER];
}

- (void)detectLinkClickHandler:(void(^)(NSString *stringURL))block
{
    if (![self isKindOfClass:NSClassFromString(@"TTTAttributedLabel")])
        NSAssert(nil, @"This will support only  TTTAttributedLabel.");
    
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
    
    [detector enumerateMatchesInString:self.text options:kNilOptions range:NSMakeRange(0, self.text.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         NSLog(@"Match: %@", result);
         
         if (result)
         {
             [self addLinkWithTextCheckingResult:result
              attributes:@{(id)kCTForegroundColorAttributeName:CRGB(0, 0, 0),
                           (id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInt:kCTUnderlineStyleNone],
                           (id)kCTFontAttributeName:self.font}];
         }
     }];
    
    self.userInteractionEnabled = YES;
    [self setDelegate:self];
    
    [self setObject:block forKey:TTTATTRIBUTEDLABELCLICKHANDLER];
}




#pragma mark - 
#pragma mark - TTTAttributedLabel Delegate

- (void)attributedLabel:(UILabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result
{
    void(^handler)(NSString *string) = [self objectForKey:TTTATTRIBUTEDLABELCLICKHANDLER];
    
    if (handler)
        handler(result.URL.absoluteString);
}


@end
