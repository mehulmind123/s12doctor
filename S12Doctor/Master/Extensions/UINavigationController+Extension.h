//
//  UINavigationController+Extension.h
//  MI API Example
//
//  Created by S12 Solutions on 25/11/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Extension)


+(UINavigationController *)navigationController;
+(UINavigationController *)navigationControllerWithRootViewController:(UIViewController *)rootViewController;


-(BOOL)popToViewControllerOfClass:(Class)class animated:(BOOL)animated;

@end


// To-Do - If removing backbar button text is not working or is applyed to common classes (PhotoLibrary, MessageComposer) than add in UIViewcontroller
