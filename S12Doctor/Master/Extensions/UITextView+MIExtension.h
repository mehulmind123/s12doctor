//
//  UITextView+MIExtension.h
//  MI API Examples
//
//  Created by S12 Solutions on 7/30/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (MIExtension)
- (void)setContainerView:(UIView *)view;
- (void)setPlaceholder:(NSString *)placeholder;
- (void)setPlaceholderColor:(UIColor *)placeholderColor;



- (void)textViewDidBeginEditing:(UITextView *)textView;
- (void)textViewDidEndEditing:(UITextView *)textView;
- (void)textViewDidChange:(UITextView *)textView;


@end


/*      /// # MI-UITextView+MIExtension
 
 [txtView setPlaceholder:@"Placeholder..."];
 [txtView setPlaceholderColor:[UIColor redColor]];

*/

// To-Do Check Logic for other uitexview delegates
// To-Do Add other delegate methods
