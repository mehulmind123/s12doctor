//
//  UIViewController+BlockHandler.h
//  MI API Example
//
//  Created by S12 Solutions on 20/11/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Master.h"

@interface UIViewController (BlockHandler)

-(void)setBlock:(MIIdResultBlock)block;

-(MIIdResultBlock)block;

@end
