//
//  UIViewController+BlockHandler.m
//  MI API Example
//
//  Created by S12 Solutions on 20/11/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "UIViewController+BlockHandler.h"

#import "Master.h"
#import "NSObject+NewProperty.h"


static NSString *const BlockHandler = @"BlockHandler";

@implementation UIViewController (BlockHandler)

-(void)setBlock:(MIIdResultBlock)block
{
    [self setObject:block forKey:BlockHandler];
}

-(MIIdResultBlock)block
{
    return [self objectForKey:BlockHandler];
}

@end
