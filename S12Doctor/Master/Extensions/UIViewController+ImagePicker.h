//
//  UIViewController+ImagePicker.h
//  Engage
//
//  Created by S12 Solutions on 03/12/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>


#define ChoosePhotoOption   @"Select Source Type"
#define PhotoLibraryOption  @"Choose From Phone"
#define PhotoCameraOption   @"Take A Photo"


typedef void (^CompletionHandler)(UIImage *image, NSDictionary *info);

@interface UIViewController (ImagePicker) <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (void)presentImagePickerSource:(CompletionHandler)handler;
- (void)presentPhotoLibray:(CompletionHandler)handler;
- (void)presentPhotoCamera:(CompletionHandler)handler;

@end
