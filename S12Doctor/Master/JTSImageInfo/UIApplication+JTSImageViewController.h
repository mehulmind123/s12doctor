//
//  UIApplication+JTSImageViewController.h
//  Riposte
//
//  Created by S12 Solutions on 4/3/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

@import UIKit;

@interface UIApplication (JTSImageViewController)

- (BOOL)jts_usesViewControllerBasedStatusBarAppearance;

@end
