//
//  UIImageView+FullScreenImage.h
//  Master
//
//  Created by S12 Solutions on 11/03/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (FullScreenImage)

-(void)enableFullScreenImage;

@end
