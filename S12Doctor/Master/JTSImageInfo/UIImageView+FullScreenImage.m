//
//  UIImageView+FullScreenImage.m
//  Master
//
//  Created by S12 Solutions on 11/03/15.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "UIImageView+FullScreenImage.h"

#import "Master.h"

#import "DelegateObserver.h"

@implementation UIImageView (FullScreenImage)

-(void)enableFullScreenImage
{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] init];
    [tapRecognizer addTarget:[DelegateObserver sharedInstance] action:NSSelectorFromString(@"imageTapped:")];
    [self addGestureRecognizer:tapRecognizer];
    self.userInteractionEnabled = YES;
   // self.layer.cornerRadius = self.bounds.size.width/2.0f;
}




@end
