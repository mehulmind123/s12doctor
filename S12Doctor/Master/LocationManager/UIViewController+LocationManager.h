//
//  UIViewController+LocationManager.h
//  MI API Example
//
//  Created by S12 Solutions on 11/18/14.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LocationManager.h"

@interface UIViewController (LocationManager)

-(void)fetchLocationUpdate:(LocatioUpdateHandler)handler;

-(void)getCurrentLocationOnly:(LocatioUpdateHandler)handler;

@end
