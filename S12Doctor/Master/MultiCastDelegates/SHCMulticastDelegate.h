//
//  MulticastDelegate.h
//  ClearStyle
//
//  Created by S12 Solutions on 13/11/2012.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

// handles messages sent to delegates, multicasting these messages to multiple observers
@interface SHCMulticastDelegate : NSObject

// Adds the given delegate implementation to the list of observers
- (void)addDelegate:(id)delegate;
- (void)removeDelegate:(id)delegate;


-(NSArray *)delegatesObjects;

@end
