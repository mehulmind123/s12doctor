//
//  MICreateClaimFormViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/21/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"
#import "MIFloatingTextView.h"


typedef enum
{
    ClaimFormDraft,
    ClaimFormCreate,
    ClaimFormRetry
    
}  claimFormType;


@interface MICreateClaimFormViewController : ParentViewController
{
    IBOutlet MIFloatingTextField *txtSelectLocalAuthority;
    IBOutlet MIFloatingTextField *txtSelectAMHP;
    IBOutlet MIFloatingTextField *txtDoctarName;
//    IBOutlet MIFloatingTextField *txtDoctorAddress;
    IBOutlet MIFloatingTextView *txtVDoctorAddress;
    IBOutlet MIFloatingTextField *txtSelectAssessmentLocationType;
    IBOutlet MIFloatingTextField *txtSelectAssessmentLocationDescription;
    IBOutlet MIFloatingTextField *txtAssessmentLocation;
    IBOutlet MIFloatingTextField *txtAssessmentPostCode;
    IBOutlet MIFloatingTextField *txtAssessmentDateAndTime;
    IBOutlet MIFloatingTextField *txtPatientNHSNumber;
    IBOutlet MIFloatingTextField *txtPatientPostCode;
    IBOutlet MIFloatingTextField *txtAdditionalInformation;
    IBOutlet MIFloatingTextField *txtSelectSectionImplemented;
    IBOutlet MIFloatingTextField *txtSelectSectionDescription;
    
    IBOutlet MIFloatingTextField *txtMileage;
    IBOutlet MIFloatingTextField *txtFromPostCode;
    IBOutlet MIFloatingTextField *txtToPostCode;
    IBOutlet MIFloatingTextField *txtNumberOfMiles;
    
    IBOutlet MIFloatingTextField *txtCarMake;
    IBOutlet MIFloatingTextField *txtCarModel;
    IBOutlet MIFloatingTextField *txtCarRegistrationPlate;
    IBOutlet MIFloatingTextField *txtEngineSize;
    IBOutlet MIFloatingTextView *txtViewNote;
    
    IBOutlet UILabel *lblDisclaimer;
    
    IBOutlet UIButton *btnSave;
    IBOutlet UIButton *btnAgree;
    
    IBOutlet UIView *viewMileage;
    IBOutlet UIView *viewCarDetails;
    IBOutlet UIView *viewSave;
    IBOutlet UIView *viewRightBar;
}


@property (nonatomic, assign) claimFormType claimFormType;

- (IBAction)textFieldTextDidChange:(id)sender;
- (IBAction)btnAdditionalInfoClicked:(id)sender;

@end
