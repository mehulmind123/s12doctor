//
//  MICreateClaimFormViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/21/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MICreateClaimFormViewController.h"
#import "MIHomeViewController.h"


#import "MICommonPopup.h"

@interface MICreateClaimFormViewController ()
{
    MICommonPopup *commonPopup;
    
    NSArray *arrLocalAuthority;
    NSMutableArray *arrAMHP;
    
    NSString *strLocalAuthorityID;
    NSString *strAMHPID;
    NSString *strAssessmentLocationTypeID;
    NSString *strSectionImplementedID;
    NSString *strSelectedDate;
}
@end

@implementation MICreateClaimFormViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Create Claim Form";
    
    [self setTxtFieldImages];
    [self manageDescriptionField];
    
    viewSave.layer.cornerRadius = CViewHeight(viewSave) / 2;
    viewSave.layer.masksToBounds = YES;
    viewSave.layer.borderWidth = 1;
    viewSave.layer.borderColor = ColorBlue_1371b9.CGColor;
    
    //..... BAR BUTTON
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(backbtnClicked)];
    
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithCustomView:viewRightBar];
    self.navigationItem.rightBarButtonItem = save;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextViewTextDidChangeNotification object:nil];
    
    
    switch (_claimFormType)
    {
        case ClaimFormDraft:        //..... DRAFT SCREEN
        {
            [self setDataFromLocal];
            break;
        }
            
        case ClaimFormCreate:       //..... CREATE CLAIM
        {
            txtDoctarName.text = appDelegate.loginUser.name;
            txtVDoctorAddress.text = appDelegate.loginUser.default_claim_address;
            txtCarMake.text = appDelegate.loginUser.car_make;
            txtCarModel.text = appDelegate.loginUser.car_model;
            txtEngineSize.text = appDelegate.loginUser.engine_size;
            txtCarRegistrationPlate.text = appDelegate.loginUser.car_registration_plate;
            break;
        }
            
        case ClaimFormRetry:        //..... RETRY
        {
            btnAgree.selected = YES;
            [self setDataFromCompletedClaimForm];
            break;
        }
    }
    
    if (![txtSelectLocalAuthority.text isBlankValidationPassed])
        [self disableUserInteractionOfAMHP];
    
    //..... API CALL
    
    [self loadLocalAuthorityListFromServer];
    [self loadAssessmentLocationTypeListFromServer];
    [self loadSectionImplementedListFromServer];
    if (strLocalAuthorityID)
        [self loadAMHPListFromServer];
}

- (void)setTxtFieldImages
{
    CGSize size = CGSizeMake(14, 7);
    
    [txtSelectLocalAuthority setRightImage:[UIImage imageNamed:@"dropdown"] withSize:size];
    
    [txtSelectAMHP setRightImage:[UIImage imageNamed:@"dropdown"] withSize:size];
    
    [txtSelectAssessmentLocationType setRightImage:[UIImage imageNamed:@"dropdown"] withSize:size];
    
    [txtAssessmentDateAndTime setRightImage:[UIImage imageNamed:@"date_time_assesment"] withSize:CGSizeMake(20, 20)];
    
    [txtSelectSectionImplemented setRightImage:[UIImage imageNamed:@"dropdown"] withSize:size];
    
    [txtAssessmentDateAndTime setDatePickerWithDateFormat:CDateFormateDisplay defaultDate:nil];
    
    lblDisclaimer.text = CMessageDisclaimer;
}

- (void)manageDescriptionField
{
    [txtSelectAssessmentLocationDescription hideByHeight:YES];
    [txtSelectSectionDescription hideByHeight:YES];
    [txtAssessmentPostCode setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    [viewMileage setConstraintConstant:9 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    txtViewNote.autocorrectionType = UITextAutocorrectionTypeYes;
}

- (void)setDataFromLocal
{
    if (self.iObject)
    {
        TBLDraft *_draft = self.iObject;
        txtDoctarName.text = _draft.doctor_name;
        txtVDoctorAddress.text = _draft.doctor_address;
        txtSelectAssessmentLocationType.text = _draft.assessment_location_type;
        txtAssessmentPostCode.text = _draft.assessment_postcode;
        
        NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:_draft.assessment_date_time withFormat:CDateFormate] withFormat:CDateFormate];
        NSDate *fromDate = [NSDate dateFromString:strClaimFormDate withFormat:CDateFormate]; // GMT time date
        txtAssessmentDateAndTime.text = [NSDate stringFromDate:fromDate withFormat:CDateFormateDisplay];
        [txtAssessmentDateAndTime setDatePickerWithDateFormat:CDateFormateDisplay defaultDate:fromDate];
        
        if ([_draft.specify_assessment_locattion isBlankValidationPassed])
        {
            [txtSelectAssessmentLocationDescription hideByHeight:NO];
            [txtSelectAssessmentLocationDescription setText:_draft.specify_assessment_locattion];
            [txtAssessmentPostCode setConstraintConstant:17 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            [txtSelectAssessmentLocationDescription hideByHeight:YES];
            [txtAssessmentPostCode setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        //..... SECTION IMPLEMENTED DESCRIPTION
        
        if ([_draft.specify_section isBlankValidationPassed])
        {
            [txtSelectSectionDescription hideByHeight:NO];
            [txtSelectSectionDescription setText:_draft.specify_section];
            [viewMileage setConstraintConstant:30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            [txtSelectSectionDescription hideByHeight:YES];
            [viewMileage setConstraintConstant:9 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }

        txtPatientNHSNumber.text = _draft.patient_nhs_number;
        txtPatientPostCode.text = _draft.patient_postcode;
        txtAdditionalInformation.text = _draft.additional_information;
        txtSelectSectionImplemented.text = _draft.section_implemented;
        txtCarMake.text = _draft.car_make;
        txtCarModel.text = _draft.car_model;
        txtCarRegistrationPlate.text = _draft.car_registration_plate;
        txtEngineSize.text = _draft.engine_size;
        txtViewNote.text = _draft.notes;
        strLocalAuthorityID = _draft.local_authority_id;
        strAMHPID = _draft.amhp_id;
        strAssessmentLocationTypeID = _draft.assessment_locaiton_id;
        strSectionImplementedID = _draft.section_implemented_id;
        btnAgree.selected = _draft.agreed;
        
        txtFromPostCode.text = _draft.from_postcode;
        txtToPostCode.text = _draft.to_postcode;
        txtNumberOfMiles.text = _draft.miles_traveled;
    }
}

- (void)setDataFromCompletedClaimForm
{
    if (self.iObject)
    {
        NSDictionary *_dictRejectedFormData = self.iObject;
        txtSelectLocalAuthority.text = [_dictRejectedFormData stringValueForJSON:@"local_authority"];
        strLocalAuthorityID = [_dictRejectedFormData stringValueForJSON:@"local_authority_id"];
        
        txtSelectAMHP.text = [_dictRejectedFormData stringValueForJSON:@"amhp_name"];
        strAMHPID = [_dictRejectedFormData stringValueForJSON:@"amhp_id"];
        
        txtDoctarName.text = [_dictRejectedFormData stringValueForJSON:@"doctor_name"];
        
        txtVDoctorAddress.text = [_dictRejectedFormData stringValueForJSON:@"doctor_address"];
        
        txtSelectAssessmentLocationType.text = [_dictRejectedFormData stringValueForJSON:@"assessment_location_type_text"];
        strAssessmentLocationTypeID = [_dictRejectedFormData stringValueForJSON:@"assessment_location_type"];
        
        txtAssessmentPostCode.text = [_dictRejectedFormData stringValueForJSON:@"assessment_postcode"];
        
        NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:[_dictRejectedFormData stringValueForJSON:@"assessment_date_time"] withFormat:CDateFormate] withFormat:CDateFormate];
        NSDate *systemDate = [NSDate dateFromString:strClaimFormDate withFormat:CDateFormate];
        txtAssessmentDateAndTime.text = [NSDate stringFromDate:systemDate withFormat:CDateFormateDisplay];
        
        [txtAssessmentDateAndTime setDatePickerWithDateFormat:CDateFormateDisplay defaultDate:systemDate];
        
        txtPatientNHSNumber.text = [_dictRejectedFormData stringValueForJSON:@"patient_nhs_number"];
        txtPatientPostCode.text = [_dictRejectedFormData stringValueForJSON:@"patient_postcode"];
        txtAdditionalInformation.text = [_dictRejectedFormData stringValueForJSON:@"additional_information"];
        
        txtSelectSectionImplemented.text = [_dictRejectedFormData stringValueForJSON:@"section_implemented_text"];
        strSectionImplementedID = [_dictRejectedFormData stringValueForJSON:@"section_implemented"];
        
        txtCarMake.text = [_dictRejectedFormData stringValueForJSON:@"car_make"];
        txtCarModel.text = [_dictRejectedFormData stringValueForJSON:@"car_model"];
        txtCarRegistrationPlate.text = [_dictRejectedFormData stringValueForJSON:@"car_registration_plate"];
        txtEngineSize.text = [_dictRejectedFormData stringValueForJSON:@"engine_size"];
        txtViewNote.text = [_dictRejectedFormData stringValueForJSON:@"notes"];
        
        
        txtMileage.text = [_dictRejectedFormData stringValueForJSON:@"mileage"];
        txtFromPostCode.text = [_dictRejectedFormData stringValueForJSON:@"from_postcode"];
        txtToPostCode.text = [_dictRejectedFormData stringValueForJSON:@"to_postcode"];
        txtNumberOfMiles.text = [_dictRejectedFormData stringValueForJSON:@"miles_traveled"];
        
        //..... ASSESSMENT LOCATION TYPE DESCRIPTION
        
        if ([[_dictRejectedFormData stringValueForJSON:@"assessment_location_type_description"] isBlankValidationPassed])
        {
            [txtSelectAssessmentLocationDescription hideByHeight:NO];
            [txtSelectAssessmentLocationDescription setText:[_dictRejectedFormData stringValueForJSON:@"assessment_location_type_description"]];
            [txtAssessmentPostCode setConstraintConstant:17 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            [txtSelectAssessmentLocationDescription hideByHeight:YES];
            [txtAssessmentPostCode setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        //..... SECTION IMPLEMENTED DESCRIPTION
        
        if ([[_dictRejectedFormData stringValueForJSON:@"section_type_description"] isBlankValidationPassed])
        {
            [txtSelectSectionDescription hideByHeight:NO];
            [txtSelectSectionDescription setText:[_dictRejectedFormData stringValueForJSON:@"section_type_description"]];
            [viewMileage setConstraintConstant:30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            [txtSelectSectionDescription hideByHeight:YES];
            [viewMileage setConstraintConstant:9 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
    }
}

- (void)saveDraftToLocal:(BOOL)isSync
{
    TBLDraft *_draft;
    if ([self.iObject isKindOfClass:[TBLDraft class]])
        _draft = self.iObject;
    
    if (!_draft)
    {
        NSArray *arrTotal = [TBLDraft fetch:[NSPredicate predicateWithFormat:@"iSynced == 0"] sortedBy:nil];
        
        if (arrTotal.count >= 10)
        {
            [CustomAlertView iOSAlert:@"" withMessage:CMessageUnableSaveDraft onView:self];
            
            switch (_claimFormType)
            {
                case ClaimFormDraft:
                {
                    [self.navigationController popViewControllerAnimated:YES];
                    break;
                }
                case ClaimFormCreate:
                case ClaimFormRetry:
                {
                    [CustomAlertView iOSAlert:@"" withMessage:CMessageSavedClaimForm onView:self];
                    MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:homeVC animated:YES];
                    
                    break;
                }
            }
            
            return;
        }
        else
        {
            _draft = [TBLDraft findOrCreate:@{@"timestamp":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]]} context:[[Store sharedInstance] mainManagedObjectContext]];
            _draft.draftID = [NSNumber numberWithInteger:arrTotal.count];
        }
        
    }
    
    _draft.timestamp = [[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] doubleValue];
    _draft.local_authority = txtSelectLocalAuthority.text;
    _draft.amhp = txtSelectAMHP.text;
    _draft.doctor_name = txtDoctarName.text;
    _draft.doctor_address = txtVDoctorAddress.text;
    _draft.assessment_location_type = txtSelectAssessmentLocationType.text;
    _draft.specify_assessment_locattion = txtSelectAssessmentLocationDescription.text;
    _draft.assessment_location_address = txtAssessmentLocation.text;
    _draft.assessment_postcode = txtAssessmentPostCode.text;
    _draft.assessment_date_time = strSelectedDate;
    _draft.patient_nhs_number = txtPatientNHSNumber.text;
    _draft.patient_postcode = txtPatientPostCode.text;
    _draft.additional_information = txtAdditionalInformation.text;
    _draft.section_implemented = txtSelectSectionImplemented.text;
    _draft.specify_section = txtSelectSectionDescription.text;
    _draft.car_make = txtCarMake.text;
    _draft.car_model = txtCarModel.text;
    _draft.car_registration_plate = txtCarRegistrationPlate.text;
    _draft.engine_size = txtEngineSize.text;
    //            objDraft.miles_traveled = txtMilesTraveled.text;
    _draft.notes = txtViewNote.text;
    _draft.disclaimer = lblDisclaimer.text;
    _draft.local_authority_id = strLocalAuthorityID;
    _draft.amhp_id = strAMHPID;
    _draft.assessment_locaiton_id = strAssessmentLocationTypeID;
    _draft.section_implemented_id = strSectionImplementedID;
    _draft.agreed = btnAgree.selected ? YES : NO;
    
    _draft.from_postcode = txtFromPostCode.text;
    _draft.to_postcode = txtToPostCode.text;
    _draft.miles_traveled = txtNumberOfMiles.text;
    _draft.iSynced = isSync;
    
    _draft.login_user = appDelegate.loginUser;
    
    [[[Store sharedInstance] mainManagedObjectContext] save];
}






# pragma mark
# pragma mark - API Function

- (void)loadLocalAuthorityListFromServer
{
    //...... AS PER LOCAL AUTHORITY SELECTION AMHP LIST WILL COME FROM SERVER
    
    txtSelectLocalAuthority.userInteractionEnabled = NO;
    txtSelectLocalAuthority.alpha = 1;
    
    [[APIRequest request] localAuthorityListWithLoginType:appDelegate.loginUser.login_type andType:CLAAll andRegionID:@"" completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             arrLocalAuthority = [responseObject valueForKey:CJsonData];
             
             NSArray *arrLocalAuthorityData = [responseObject valueForKey:CJsonData];
             
             if (arrLocalAuthorityData.count > 0)
             {
                 txtSelectLocalAuthority.userInteractionEnabled = YES;
                 txtSelectLocalAuthority.alpha = 1;
                 
                 NSArray *arrLocalAuth = [arrLocalAuthorityData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"authority_id == %d", [self.iObject isKindOfClass:[TBLDraft class]] ? [((TBLDraft *)self.iObject).local_authority_id intValue] : [self.iObject intForKey:@"local_authority_id"]]];
                 
                 if (arrLocalAuth.count > 0)
                 {
                     NSDictionary *dictLocalAuth = [arrLocalAuth firstObject];
                     
                     txtSelectLocalAuthority.text = [dictLocalAuth stringValueForJSON:@"authority_name"];
                     strLocalAuthorityID = [dictLocalAuth stringValueForJSON:@"authority_id"];
                 }
                 else
                 {
                     txtSelectLocalAuthority.text = strLocalAuthorityID = @"";
                 }
                 
                 [txtSelectLocalAuthority setPickerData:[arrLocalAuthorityData valueForKeyPath:@"authority_name"] update:^(NSString *text, NSInteger row, NSInteger component)
                  {
                      strAMHPID = @"";
                      txtSelectAMHP.text = @"";
                      
                      strLocalAuthorityID = [[arrLocalAuthorityData objectAtIndex:row] stringValueForJSON:@"authority_id"];
                      
                      [[APIRequest request] amhpListWithLoginType:appDelegate.loginUser.login_type andLocalAuthorityID:strLocalAuthorityID completed:^(id responseObject, NSError *error)
                       {
                           if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                           {
                               arrAMHP = [NSMutableArray new];
                               [arrAMHP addObjectsFromArray:[responseObject valueForKey:CJsonData]];
                               
                               if (arrAMHP.count > 0)
                               {
                                   txtSelectAMHP.userInteractionEnabled = YES;
                                   txtSelectAMHP.alpha = 1;
                                   
                                   [txtSelectAMHP setPickerData:[arrAMHP valueForKeyPath:@"amhp_name"] update:^(NSString *text, NSInteger row, NSInteger component)
                                    {
                                        strAMHPID = [[arrAMHP objectAtIndex:row] stringValueForJSON:@"amhp_id"];
                                    }];
                               }
                               else
                                   [self disableUserInteractionOfAMHP];
                           }
                           else
                               [self disableUserInteractionOfAMHP];
                       }];
                  }];
             }
             else
             {
                 txtSelectLocalAuthority.userInteractionEnabled = NO;
                 txtSelectLocalAuthority.alpha = 0.5;
                 
                 [self disableUserInteractionOfAMHP];
             }
         }
     }];
}

- (void)loadAssessmentLocationTypeListFromServer
{
    //..... LOAD ASSESSMENT LOCATION TYPE LIST FROM SERVER
    
    txtSelectAssessmentLocationType.userInteractionEnabled = NO;
    
    [[APIRequest request] assessmentLocationTypeListWithLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             NSArray *arrAssessmentLocationType = [responseObject valueForKey:CJsonData];
             
             if (arrAssessmentLocationType.count > 0)
             {
                 txtSelectAssessmentLocationType.userInteractionEnabled = YES;
                 
                 [txtSelectAssessmentLocationType setPickerData:[arrAssessmentLocationType valueForKeyPath:@"assessment_location_type_name"] update:^(NSString *text, NSInteger row, NSInteger component)
                  {
                      
                      strAssessmentLocationTypeID = [[arrAssessmentLocationType objectAtIndex:row] stringValueForJSON:@"assessment_location_type_id"];
                      
                      
                      if ([text isEqualToString:COtherTypeSelect])
                      {
                        
                          [txtSelectAssessmentLocationDescription hideByHeight:NO];
                          [txtAssessmentPostCode setConstraintConstant:17 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
                      }
                      else
                      {
                          [txtSelectAssessmentLocationDescription hideByHeight:YES];
                          [txtAssessmentPostCode setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
                      }
                      
                  }];
             }
         }
     }];
}

- (void)loadSectionImplementedListFromServer
{
    //..... LOAD SECTION IMPLEMENTED LIST FROM SERVER
    
    txtSelectSectionImplemented.userInteractionEnabled = NO;
    
    [[APIRequest request] sectionImplementedListWithLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             NSArray *arrSectionImplement = [responseObject valueForKey:CJsonData];
             
             if (arrSectionImplement.count > 0)
             {
                 txtSelectSectionImplemented.userInteractionEnabled = YES;
                 
                 [txtSelectSectionImplemented setPickerData:[arrSectionImplement valueForKeyPath:@"section_implement_name"] update:^(NSString *text, NSInteger row, NSInteger component)
                  {
                      strSectionImplementedID = [[arrSectionImplement objectAtIndex:row] stringValueForJSON:@"section_implement_id"];
                      
                      if ([text isEqualToString:COtherTypeSelect])
                      {
                          [txtSelectSectionDescription hideByHeight:NO];
                          [viewMileage setConstraintConstant:30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
                      }
                      else
                      {
                          [txtSelectSectionDescription hideByHeight:YES];
                          [viewMileage setConstraintConstant:9 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
                      }
                      
                  }];
             }
         }
     }];
}

- (void)loadAMHPListFromServer
{
    [self disableUserInteractionOfAMHP];
    
    //..... LOAD AMHP LIST FROM SERVER
    
    [[APIRequest request] amhpListWithLoginType:appDelegate.loginUser.login_type andLocalAuthorityID:strLocalAuthorityID completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             arrAMHP = [NSMutableArray new];
             [arrAMHP addObjectsFromArray:[responseObject valueForKey:CJsonData]];
             
             NSArray *arrLocalAuth = [arrAMHP filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"amhp_id == %d", [self.iObject isKindOfClass:[TBLDraft class]] ? [((TBLDraft *)self.iObject).amhp_id intValue] : [self.iObject intForKey:@"amhp_id"]]];
             
             if (arrLocalAuth.count > 0)
             {
                 NSDictionary *dictLocalAuth = [arrLocalAuth firstObject];
                 
                 txtSelectAMHP.text = [dictLocalAuth stringValueForJSON:@"amhp_name"];
                 strAMHPID = [dictLocalAuth stringValueForJSON:@"amhp_id"];
             }
             else
             {
                 txtSelectAMHP.text = strAMHPID = @"";
             }
             
             if (arrAMHP.count > 0)
             {
                 txtSelectAMHP.userInteractionEnabled = YES;
                 txtSelectAMHP.alpha = 1;
                 
                 [txtSelectAMHP setPickerData:[arrAMHP valueForKeyPath:@"amhp_name"] update:^(NSString *text, NSInteger row, NSInteger component)
                  {
                      strAMHPID = [[arrAMHP objectAtIndex:row] stringValueForJSON:@"amhp_id"];
                  }];
             }
             else
                 [self disableUserInteractionOfAMHP];
         }
         else
             [self disableUserInteractionOfAMHP];
     }];
}

- (void)disableUserInteractionOfAMHP
{
    txtSelectAMHP.userInteractionEnabled = NO;
    txtSelectAMHP.alpha = 0.5;
}

- (void)submitClaimFormWithType:(int)claimType
{
    NSString *trimmedAssessmentPostCode = [txtAssessmentPostCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    txtAssessmentPostCode.text = trimmedAssessmentPostCode;
    
    NSString *trimmedPatientPostCode = [txtPatientPostCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    txtPatientPostCode.text = trimmedPatientPostCode;
    
    //..... CONVERT SELECTED DATE INTO GMT FORMAT
    
    NSDate *dateSelected = [NSDate dateFromString:txtAssessmentDateAndTime.text withFormat:CDateFormateDisplay];
    strSelectedDate = [NSDate stringFromDateForGMT:dateSelected withFormat:CDateFormate];
    
    NSString *strClaimFormID = [self.iObject isKindOfClass:[NSDictionary class]] ? [self.iObject stringValueForJSON:@"claim_id"] : ((TBLDraft *)self.iObject).claim_id ? ((TBLDraft *)self.iObject).claim_id : @"";
    
    [[MILoader sharedInstance] startAnimation];
    
    [[APIRequest request] createClaimFormWithLocalAuthority:txtSelectLocalAuthority.text andAMHP:txtSelectAMHP.text andDoctorName:txtDoctarName.text andDoctorAddress:txtVDoctorAddress.text andAssessmentLocationType:strAssessmentLocationTypeID ? strAssessmentLocationTypeID : @"" andAssessmentPostcode:txtAssessmentPostCode.text andAssessmentDateTime:strSelectedDate ? strSelectedDate : @"" andPatientNHSNumber:txtPatientNHSNumber.text andPatientPostcode:txtPatientPostCode.text andAdditionalInfo:txtAdditionalInformation.text andSectionImplemented:strSectionImplementedID ? strSectionImplementedID : @"" andCarMake:txtCarMake.text andCarModel:txtCarModel.text andCarRegistrationPlate:txtCarRegistrationPlate.text andEngineSize:txtEngineSize.text andNotes:txtViewNote.text andDisclaimer:lblDisclaimer.text andLocalAuthorityID:strLocalAuthorityID ? strLocalAuthorityID : @"" andAMHPID:strAMHPID ? strAMHPID : @"" andLoginType:appDelegate.loginUser.login_type andAssessmentLocatinTypeDescription:txtSelectAssessmentLocationDescription.text andSelectSectionDescription:txtSelectSectionDescription.text andClaimFormID:strClaimFormID andFromPostCode:txtFromPostCode.text andToPostCode:txtToPostCode.text andNumberOfMiles:txtNumberOfMiles.text andFormType:claimType completed:^(id responseObject, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             [self saveDraftToLocal:YES];
             
             switch (_claimFormType)
             {
                 case ClaimFormDraft:
                 {
                     if (claimType == 2)
                     {
                         if (self.iObject && [self.iObject isKindOfClass:[TBLDraft class]])
                             [((TBLDraft *)self.iObject) deleteObject];
                     }
                     
                     [self.navigationController popViewControllerAnimated:YES];
                     
                     break;
                 }
                 case ClaimFormCreate:
                 case ClaimFormRetry:
                 {
                     if (claimType == 1)
                     {
                         [CustomAlertView iOSAlert:@"" withMessage:CMessageSavedClaimForm onView:[UIApplication topMostController]];
                     }
                     else
                     {
                         if (self.iObject && [self.iObject isKindOfClass:[TBLDraft class]])
                             [((TBLDraft *)self.iObject) deleteObject];
                         
                         [CustomAlertView iOSAlert:@"" withMessage:CMessageClaimFormSubmitted onView:[UIApplication topMostController]];
                     }
                     
                     MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                     [appDelegate openMenuViewcontroller:homeVC animated:YES];
                     
                     break;
                 }
             }
             
             appDelegate.loginUser.draft_count = [[responseObject valueForKey:CJsonMeta] intForKey:@"draft_count"];
             [[[Store sharedInstance] mainManagedObjectContext] save];
         }
         else if (claimType == 1)
         {
             [self saveDraftToLocal:NO];
             
             switch (_claimFormType)
             {
                 case ClaimFormDraft:
                 {
                     [self.navigationController popViewControllerAnimated:YES];
                     break;
                 }
                 case ClaimFormCreate:
                 case ClaimFormRetry:
                 {
                     [CustomAlertView iOSAlert:@"" withMessage:CMessageSavedClaimForm onView:self];
                     MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                     [appDelegate openMenuViewcontroller:homeVC animated:YES];
                     
                     break;
                 }
             }
         }
         else if (!error)
         {
             [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
         }
     }];
}






# pragma mark
# pragma mark - Action Event

- (void)backbtnClicked
{
    [appDelegate resignKeyboard];
    
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageSavedClaimFormBack firstButton:@"No" firstHandler:^(UIAlertAction *action)
     {
         [self.navigationController popViewControllerAnimated:YES];
         
     } secondButton:@"Yes" secondHandler:^(UIAlertAction *action)
     {
         [self barBtnSaveClicked:btnSave];
         
     } inView:self];
}

- (IBAction)btnAdditionalInfoClicked:(id)sender
{
    [CustomAlertView iOSAlert:@"" withMessage:CMessageAdditionalInfo onView:self];
}

- (IBAction)btnAgreeCliecked:(UIButton*)sender
{
    sender.selected = !sender.selected;
}

- (IBAction)barBtnSaveClicked:(UIButton*)sender
{
    [appDelegate resignKeyboard];
    
    if ([AFNetworkReachabilityManager sharedManager].reachable)
        [self submitClaimFormWithType:1];
    else
    {
        NSDate *dateSelected = [NSDate dateFromString:txtAssessmentDateAndTime.text withFormat:CDateFormateDisplay];
        strSelectedDate = [NSDate stringFromDateForGMT:dateSelected withFormat:CDateFormate];
        
        [self saveDraftToLocal:NO];
        
        switch (_claimFormType)
        {
            case ClaimFormDraft:
            {
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            case ClaimFormCreate:
            case ClaimFormRetry:
            {
                [CustomAlertView iOSAlert:@"" withMessage:CMessageSavedClaimForm onView:self];
                MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                [appDelegate openMenuViewcontroller:homeVC animated:YES];
                
                break;
            }
        }
    }
}





- (IBAction)btnSubmitClicked:(UIButton*)sender
{
    if (![txtSelectLocalAuthority.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankAuthority onView:self];
    
    else if (![txtSelectAMHP.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankAMHP onView:self];
    
    else if (![txtDoctarName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankUserName onView:self];
    
    else if (![txtVDoctorAddress.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankDoctorAddress onView:self];
    
    else if (![txtSelectAssessmentLocationType.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankAssessmentLocationType onView:self];
    
    else if ([txtSelectAssessmentLocationType.text isEqualToString:COtherTypeSelect] && ![txtSelectAssessmentLocationDescription.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankAssessmentLocationTypeDesc onView:self];
    
    else if (![txtAssessmentPostCode.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankAssessmentPostcode onView:self];
    
    else if ((txtAssessmentPostCode.text.length < 5 || txtAssessmentPostCode.text.length > 9))
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidAssessmentPostcode onView:self];
    
    else if (![txtAssessmentDateAndTime.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankDateTime onView:self];
    
    else if ([txtPatientNHSNumber.text isBlankValidationPassed] && txtPatientNHSNumber.text.length != 12)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidPatientNHSNumber onView:self];
    
    else if ([txtPatientPostCode.text isBlankValidationPassed] && (txtPatientPostCode.text.length < 5 || txtPatientPostCode.text.length > 9))
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidPatientPostcode onView:self];
    
    else if (![txtSelectSectionImplemented.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankSectionImplemented onView:self];
    
    else if ([txtSelectSectionImplemented.text isEqualToString:COtherTypeSelect] && ![txtSelectSectionDescription.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankSectionImplementedDesc onView:self];
    
    else if ([txtFromPostCode.text isBlankValidationPassed] && (txtFromPostCode.text.length < 5 || txtFromPostCode.text.length > 9))
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidFromPostcode onView:self];
    
    else if ([txtToPostCode.text isBlankValidationPassed] && (txtToPostCode.text.length < 5 || txtToPostCode.text.length > 9))
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidToPostcode onView:self];
    
    else if (!btnAgree.selected)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageNotAccept onView:self];
    
    else
    {
        commonPopup = [MICommonPopup customCommonPopup];
        [commonPopup.lblAddNewLanguage hideByHeight:YES];
        commonPopup.txtCommonField.placeholder = @"Enter your password";
        
        [commonPopup.btnSubmit touchUpInsideClicked:^{
            
            [appDelegate resignKeyboard];
            
            if (![commonPopup.txtCommonField.text isBlankValidationPassed])
            {
                [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageBlankPassword buttonTitle:@"OK" handler:nil inView:self];
            }
            else
            {
                [[APIRequest request] verifyPassword:commonPopup.txtCommonField.text andLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
                 {
                     if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                     {
                         [self dismissPopUp:commonPopup];
                         [self submitClaimFormWithType:2];
                     }
                     else if (!error)
                     {
                         [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:[responseObject stringValueForJSON:CJsonMessage] buttonTitle:@"OK" handler:nil inView:self];
                     }
                 }];
            }
            
        }];
        
        [self presentPopUp:commonPopup from:Is_iPhone_4 || Is_iPhone_5 ? PresentTypeCustom : PresentTypeCenter shouldCloseOnTouchOutside:YES];
    }
}






# pragma mark
# pragma mark - UITextField And UITextView Delegate

- (void)textDidChange:(UITextView *)textView
{
    if (textView == txtVDoctorAddress)
    {
        if (txtVDoctorAddress.text.length > CCharacterLimit5000)
        {
            txtVDoctorAddress.text = [txtVDoctorAddress.text substringToIndex:CCharacterLimit5000];
            return;
        }
    }
    else
    {
        if (txtViewNote.text.length > CCharacterLimit5000)
        {
            txtViewNote.text = [txtViewNote.text substringToIndex:CCharacterLimit5000];
            return;
        }
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (([txtSelectLocalAuthority isFirstResponder] || [txtSelectAMHP isFirstResponder] || [txtSelectAssessmentLocationType isFirstResponder] || [txtAssessmentDateAndTime isFirstResponder] || [txtSelectSectionImplemented isFirstResponder]))
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
        }];
    }
    return [super canPerformAction:action withSender:sender];
}

- (IBAction)textFieldTextDidChange:(UITextField *)sender
{
    if ([sender isEqual:txtDoctarName] || [sender isEqual:txtSelectAssessmentLocationDescription] || [sender isEqual:txtSelectSectionDescription] || [sender isEqual:txtCarMake] || [sender isEqual:txtCarModel])
    {
        if (sender.text.length > CCharacterLimit256)
        {
            sender.text = [sender.text substringToIndex:CCharacterLimit256];
            return;
        }
        else
            sender.text = sender.text;
    }
    else if ([sender isEqual:txtCarRegistrationPlate])
    {
        if (sender.text.length > CCharacterLimit8)
        {
            sender.text = [sender.text substringToIndex:CCharacterLimit8];
            return;
        }
        else
            sender.text = sender.text;
    }
    else if ([sender isEqual:txtEngineSize])
    {
        if (sender.text.length > 5)
        {
            sender.text = [sender.text substringToIndex:5];
            return;
        }
        else
            sender.text = sender.text;
    }
    else if ([sender isEqual:txtNumberOfMiles])
    {
        if (sender.text.length > 4)
        {
            sender.text = [sender.text substringToIndex:4];
            return;
        }
        else
            sender.text = sender.text;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:txtPatientNHSNumber])
    {
        // All digits entered
        if (range.location == 12)
            return NO;
        
        // Reject appending non-digit characters
        if (range.length == 0 && ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]])
            return NO;
        
        // Auto-add hyphen before appending 4rd or 7th digit
        if (range.length == 0 && (range.location == 3 || range.location == 7))
        {
            textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
            return NO;
        }
        
        // Delete hyphen when deleting its trailing digit
        if (range.length == 1 && (range.location == 4 || range.location == 8))
        {
            range.location--;
            range.length = 2;
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else if ([textField isEqual:txtPatientPostCode] || [textField isEqual:txtAssessmentPostCode] || [textField isEqual:txtFromPostCode] || [textField isEqual:txtToPostCode])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@" 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        return YES;
    }
    
    return YES;
}

@end
