//
//  MIViewClaimFormViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    MIClaimFormTypeApproved,
    MIClaimFormTypeReject,
    MIClaimFormTypePending
} MIClaimFormType;


@interface MIViewClaimFormViewController : ParentViewController
{
    IBOutlet NSLayoutConstraint *cnLocalAuthorityViewTop;
    IBOutlet MIGenericView *viewApprovalStatus;
    IBOutlet UIView *viewCCG;
    IBOutlet UIView *viewRejectionReason;
    IBOutlet UIView *viewLocalAuthority;
    IBOutlet UIView *viewAMHPName;
    IBOutlet UIView *views12DoctorName;
    IBOutlet UIView *viewDoctorAddress;
    IBOutlet UIView *viewAssessmentLocationType;
    IBOutlet UIView *viewAssessmentLocationTypeDescription;
//    IBOutlet UIView *viewAssessmentLocation;
    IBOutlet UIView *viewAssessmentPostcode;
    IBOutlet UIView *viewAssessmentDateAndTime;
    IBOutlet UIView *viewPatientNHSNumber;
    IBOutlet UIView *viewPatientPostcode;
    IBOutlet UIView *viewAdditionalInformation;
    IBOutlet UIView *viewSectionImplemented;
    IBOutlet UIView *viewSectionImplementedDescription;
    
    IBOutlet UIView *viewFromPostCode;
    IBOutlet UIView *viewToPostCode;
    IBOutlet UIView *viewNumberOfMiles;
    IBOutlet UIView *viewCarMake;
    IBOutlet UIView *viewCarModel;
    IBOutlet UIView *viewCarRegistrationPlate;
    IBOutlet UIView *viewEngineSize;
//    IBOutlet UIView *viewMilesTraveled;
    IBOutlet UIView *viewNotes;
    
    IBOutlet UILabel *lblStatus;
    IBOutlet UILabel *lblCCGName;
    IBOutlet UILabel *lblRejectionReason;
    IBOutlet UILabel *lblLocalAuthority;
    IBOutlet UILabel *lblAMHPName;
    IBOutlet UILabel *lblDoctorName;
    IBOutlet UILabel *lblLocationType;
    IBOutlet UILabel *lblLocationTypeDescription;
//    IBOutlet UILabel *lblLocation;
    IBOutlet UILabel *lblAssessmentPostCode;
    IBOutlet UILabel *lblAssessmentDateAndTime;
    IBOutlet UILabel *lblPatientNHSNumber;
    IBOutlet UILabel *lblPatientPostCode;
    
    IBOutlet UILabel *lblSectionImplemented;
    IBOutlet UILabel *lblSectionImplementedDescription;
    
    IBOutlet UILabel *lblMileage;
    IBOutlet UILabel *lblCarMake;
    IBOutlet UILabel *lblCarModel;
    IBOutlet UILabel *lblCarRegistrationPlate;
    IBOutlet UILabel *lblEngineSize;
//    IBOutlet UILabel *lblMilesTraveled;
    
    IBOutlet UILabel *lblDoctorAddress;
    IBOutlet UILabel *lblAdditionalInfo;
//    IBOutlet UILabel *lblMileage;
    IBOutlet UILabel *lblFromPostCode;
    IBOutlet UILabel *lblToPostCode;
    IBOutlet UILabel *lblNumberOfMiles;
    
    IBOutlet UILabel *lblNotes;
    IBOutlet UILabel *lblCarDetails;
}


@property (nonatomic, assign) MIClaimFormType MIClaimFormType;
@property (nonatomic, strong) NSDictionary *dictClaimForm;

@end
