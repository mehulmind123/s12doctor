//
//  MIViewClaimFormViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIViewClaimFormViewController.h"

@interface MIViewClaimFormViewController ()

@end

@implementation MIViewClaimFormViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    [self.navigationController.navigationBar setShadowImage:nil];
    
    self.title = @"View Claim Form";
    
    viewFromPostCode.layer.cornerRadius = viewToPostCode.layer.cornerRadius = viewNumberOfMiles.layer.cornerRadius = viewCarMake.layer.cornerRadius = viewCarModel.layer.cornerRadius = viewCarRegistrationPlate.layer.cornerRadius = viewEngineSize.layer.cornerRadius = viewNotes.layer.cornerRadius = 5;
    
    [viewCCG hideByHeight:YES];
    [viewRejectionReason hideByHeight:YES];
    
    switch (_MIClaimFormType)
    {
        case MIClaimFormTypeApproved:   //..... APPROVED
        {
            [viewCCG hideByHeight:NO];
            [viewLocalAuthority setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
            break;
        }
           
        case MIClaimFormTypeReject:     //..... REJECTED
        {
            lblStatus.text = @"Rejected";
            [viewRejectionReason hideByHeight:NO];
            lblStatus.textColor = ColorUnavailable_ffba00;
            [viewRejectionReason setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
            break;
        }
            
        case MIClaimFormTypePending:    //..... PENDING
        {
            lblStatus.text = @"Pending";
            lblStatus.textColor = ColorMayBeAvaialble_ffba00;
            [viewLocalAuthority setConstraintConstant:-11 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
            break;
        }
    }
    
    //..... Set Data
    [self setData];
    [self getClaimFormDetailFromServer];
}




# pragma mark
# pragma mark - Set Data

- (void)setData
{
    if (_dictClaimForm)
    {
        lblLocalAuthority.text          = [[_dictClaimForm stringValueForJSON:@"local_authority"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"local_authority"] : CNotAvailable;
        lblCCGName.text                 = [[_dictClaimForm stringValueForJSON:@"ccg_name"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"ccg_name"] : CNotAvailable;
        lblRejectionReason.text         = [[_dictClaimForm stringValueForJSON:@"rejection_reason"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"rejection_reason"] : CNotAvailable;
        lblAMHPName.text                = [[_dictClaimForm stringValueForJSON:@"amhp_name"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"amhp_name"] : CNotAvailable;
        lblDoctorName.text              = [[_dictClaimForm stringValueForJSON:@"doctor_name"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"doctor_name"] : CNotAvailable;
        lblLocationType.text            = [[_dictClaimForm stringValueForJSON:@"assessment_location_type_text"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"assessment_location_type_text"] : CNotAvailable;
        
        lblAssessmentPostCode.text      = [[_dictClaimForm stringValueForJSON:@"assessment_postcode"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"assessment_postcode"] : CNotAvailable;
        
        NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:[_dictClaimForm stringValueForJSON:@"assessment_date_time"] withFormat:CDateFormate] withFormat:CDateFormate];
        NSDate *fromDate = [NSDate dateFromString:strClaimFormDate withFormat:CDateFormate]; // GMT time date
        lblAssessmentDateAndTime.text = [NSDate stringFromDate:fromDate withFormat:CDateFormateDisplay];
        
        lblPatientNHSNumber.text        = [[_dictClaimForm stringValueForJSON:@"patient_nhs_number"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"patient_nhs_number"] : CNotAvailable;
        lblPatientPostCode.text         = [[_dictClaimForm stringValueForJSON:@"patient_postcode"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"patient_postcode"] : CNotAvailable;
        lblSectionImplemented.text      = [[_dictClaimForm stringValueForJSON:@"section_implemented_text"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"section_implemented_text"] : CNotAvailable;
        lblCarMake.text                 = [[_dictClaimForm stringValueForJSON:@"car_make"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"car_make"] : CNotAvailable;
        lblCarModel.text                = [[_dictClaimForm stringValueForJSON:@"car_model"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"car_model"] : CNotAvailable;
        lblCarRegistrationPlate.text    = [[_dictClaimForm stringValueForJSON:@"car_registration_plate"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"car_registration_plate"] : CNotAvailable;
        lblEngineSize.text              = [[_dictClaimForm stringValueForJSON:@"engine_size"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"engine_size"] : CNotAvailable;
        
        lblNotes.text                   = [[_dictClaimForm stringValueForJSON:@"notes"] isBlankValidationPassed] ? [_dictClaimForm stringValueForJSON:@"notes"] : CNotAvailable;
        
        if ([[_dictClaimForm stringValueForJSON:@"assessment_location_type_description"] isBlankValidationPassed])
        {
            [viewAssessmentLocationTypeDescription hideByHeight:NO];
            lblLocationTypeDescription.text = [_dictClaimForm stringValueForJSON:@"assessment_location_type_description"];
        }
        else
        {
            [viewAssessmentLocationTypeDescription hideByHeight:YES];
            [viewAssessmentPostcode setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        if ([[_dictClaimForm stringValueForJSON:@"section_type_description"] isBlankValidationPassed])
        {
            [viewSectionImplementedDescription hideByHeight:NO];
            lblSectionImplementedDescription.text = [_dictClaimForm stringValueForJSON:@"section_type_description"];
        }
        else
        {
            [viewSectionImplementedDescription hideByHeight:YES];
            [lblMileage setConstraintConstant:27 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }   
    }
}

# pragma mark
# pragma mark - API Methods

- (void)getClaimFormDetailFromServer
{
    if (_dictClaimForm)
    {
        [self startLoadingAnimationInView:self.view];
        
        [[APIRequest request] viewClaimFormWithFormID:[_dictClaimForm stringValueForJSON:@"claim_id"] andLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
        {
            [self stopLoadingAnimationInView:self.view];
            
            if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
            {
                NSDictionary *dictResponse = [responseObject valueForKey:CJsonData];
                
                lblLocalAuthority.text          = [[dictResponse stringValueForJSON:@"local_authority"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"local_authority"] : CNotAvailable;
                lblCCGName.text                 = [[dictResponse stringValueForJSON:@"ccg_name"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"ccg_name"] : CNotAvailable;
                lblRejectionReason.text         = [[dictResponse stringValueForJSON:@"rejection_reason"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"rejection_reason"] : CNotAvailable;
                lblAMHPName.text                = [[dictResponse stringValueForJSON:@"amhp_name"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"amhp_name"] : CNotAvailable;
                lblDoctorName.text              = [[dictResponse stringValueForJSON:@"doctor_name"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"doctor_name"] : CNotAvailable;
                lblLocationType.text            = [[dictResponse stringValueForJSON:@"assessment_location_type_text"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"assessment_location_type_text"] : CNotAvailable;

                lblAssessmentPostCode.text      = [[dictResponse stringValueForJSON:@"assessment_postcode"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"assessment_postcode"] : CNotAvailable;
                
                lblDoctorAddress.text              = [[dictResponse stringValueForJSON:@"doctor_address"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"doctor_address"] : CNotAvailable;
                
                lblAdditionalInfo.text              = [[dictResponse stringValueForJSON:@"additional_information"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"additional_information"] : CNotAvailable;
                
                NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:[dictResponse stringValueForJSON:@"assessment_date_time"] withFormat:CDateFormate] withFormat:CDateFormate];
                NSDate *fromDate = [NSDate dateFromString:strClaimFormDate withFormat:CDateFormate]; // GMT time date
                lblAssessmentDateAndTime.text = [NSDate stringFromDate:fromDate withFormat:CDateFormateDisplay];
                
                lblPatientNHSNumber.text        = [[dictResponse stringValueForJSON:@"patient_nhs_number"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"patient_nhs_number"] : CNotAvailable;
                lblPatientPostCode.text         = [[dictResponse stringValueForJSON:@"patient_postcode"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"patient_postcode"] : CNotAvailable;
                lblSectionImplemented.text      = [[dictResponse stringValueForJSON:@"section_implemented_text"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"section_implemented_text"] : CNotAvailable;
                lblCarMake.text                 = [[dictResponse stringValueForJSON:@"car_make"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"car_make"] : CNotAvailable;
                lblCarModel.text                = [[dictResponse stringValueForJSON:@"car_model"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"car_model"] : CNotAvailable;
                lblCarRegistrationPlate.text    = [[dictResponse stringValueForJSON:@"car_registration_plate"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"car_registration_plate"] : CNotAvailable;
                lblEngineSize.text              = [[dictResponse stringValueForJSON:@"engine_size"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"engine_size"] : CNotAvailable;
                
                lblNotes.text                   = [[dictResponse stringValueForJSON:@"notes"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"notes"] : CNotAvailable;
                

                lblFromPostCode.text    = [[dictResponse stringValueForJSON:@"from_postcode"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"from_postcode"] : CNotAvailable;
                lblToPostCode.text              = [[dictResponse stringValueForJSON:@"to_postcode"] isBlankValidationPassed] ? [dictResponse stringValueForJSON:@"to_postcode"] : CNotAvailable;
                lblNumberOfMiles.text           = [[dictResponse stringValueForJSON:@"miles_traveled"] isBlankValidationPassed] ? [NSString stringWithFormat:@"%@ mi", [dictResponse stringValueForJSON:@"miles_traveled"]] : CNotAvailable;
                
                
                
                if ([[dictResponse stringValueForJSON:@"assessment_location_type_description"] isBlankValidationPassed])
                {
                    [viewAssessmentLocationTypeDescription hideByHeight:NO];
                    lblLocationTypeDescription.text = [dictResponse stringValueForJSON:@"assessment_location_type_description"];
                }
                else
                {
                    [viewAssessmentLocationTypeDescription hideByHeight:YES];
                    [viewAssessmentPostcode setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
                }
                
                if ([[dictResponse stringValueForJSON:@"section_type_description"] isBlankValidationPassed])
                {
                    [viewSectionImplementedDescription hideByHeight:NO];
                    lblSectionImplementedDescription.text = [dictResponse stringValueForJSON:@"section_type_description"];
                }
                else
                {
                    [viewSectionImplementedDescription hideByHeight:YES];
                    [lblMileage setConstraintConstant:27 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
                }
            }
        }];
    }
}

@end
