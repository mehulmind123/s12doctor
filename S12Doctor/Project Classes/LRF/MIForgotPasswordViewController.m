//
//  MIForgotPasswordViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/18/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIForgotPasswordViewController.h"
#import "MISelectLanguageViewController.h"

@interface MIForgotPasswordViewController ()

@end

@implementation MIForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Forgot password";
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnSubmitClicked:(id)sender
{
    if (![txtEmailAddress.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    
    else if (![txtEmailAddress.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidEmail onView:self];
    
    else
    {
        [appDelegate resignKeyboard];
        
        [[APIRequest request] forgotPasswordWithEmail:txtEmailAddress.text completed:^(id responseObject, NSError *error)
        {
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] buttonTitle:@"OK" handler:^(UIAlertAction *action)
                {
                    [self.navigationController popViewControllerAnimated:YES];
                } inView:self];
            }
            else if (!error)
                [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
        }];
    }
}

@end
