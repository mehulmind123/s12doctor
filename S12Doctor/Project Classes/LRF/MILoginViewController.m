//
//  MILoginViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/18/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MILoginViewController.h"
#import "MIForgotPasswordViewController.h"
#import "MICMSViewController.h"
#import "MIHomeViewController.h"
#import "MIChangePasswordViewController.h"
#import "MIMainViewController.h"

@interface MILoginViewController ()

@end

@implementation MILoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self initialize];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Login";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    
    if (IS_IPHONE_SIMULATOR)
    {
        txtEmailAddress.text = @"sejal.mindinventory@gmail.com";
        txtPassword.text = @"mind@123";
    }
}






# pragma mark
# pragma mark - General Method

- (void)backClicked
{
    MIMainViewController *mainVC = [[MIMainViewController alloc] initWithNibName:@"MIMainViewController" bundle:nil];
    [appDelegate.window setRootViewController:[[UINavigationController alloc] initWithRootViewController:mainVC]];
}

- (IBAction)btnLoginClicked:(id)sender
{
    if (![txtEmailAddress.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    
    else if (![txtEmailAddress.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidEmail onView:self];
    
    else if (![txtPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankPassword onView:self];
    
    else
    {
        [appDelegate resignKeyboard];
        
        //..... Login Event
//        [appDelegate trackTheEventWithName:CEventLogin];
        
        [[APIRequest request] loginWithEmail:txtEmailAddress.text andPassword:txtPassword.text completed:^(id responseObject, NSError *error)
        {
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                //..... GET LOCATION
                [appDelegate getCurrentLocation];
                
                [[APIRequest request] saveLoginUserToLocal:responseObject];
                
                if (appDelegate.loginUser.accept_terms_condition == 0)
                {
                    MICMSViewController *cmsVC = [[MICMSViewController alloc] initWithNibName:@"MICMSViewController" bundle:nil];
                    cmsVC.cmsType = TermsAndCondition;
                    [self.navigationController pushViewController:cmsVC animated:YES];
                }
                else if (appDelegate.loginUser.change_password == 0)
                {
                    //..... WHEN NOT CHANGE PASSWORD
                    
                    MIChangePasswordViewController *changePwdVC = [[MIChangePasswordViewController alloc] initWithNibName:@"MIChangePasswordViewController" bundle:nil];
                    changePwdVC.isFromTermsCondition = YES;
                    [self.navigationController pushViewController:changePwdVC animated:YES];
                }
                else
                {
                    MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:homeVC animated:YES];
                }   
            }
            else if (!error)
                [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
        }];
    }
}

- (IBAction)btnForgotPasswordClicked:(id)sender
{
    MIForgotPasswordViewController *forgotPwdVC = [[MIForgotPasswordViewController alloc] initWithNibName:@"MIForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:forgotPwdVC animated:YES];
}

@end
