//
//  MIMainViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 8/9/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIMainViewController : ParentViewController
{
    IBOutlet UIImageView *imgBG;
    IBOutlet UIImageView *imgAMHP;
    IBOutlet UIImageView *imgS12;
    
    IBOutlet UILabel *lblAMHPTitle;
    IBOutlet UILabel *lblS12Title;
    IBOutlet NSLayoutConstraint *imgAMHPAspectRatioConstraint;
    IBOutlet NSLayoutConstraint *imgS12AspectRatioConstraint;
}


- (IBAction)btnS12Clicked:(id)sender;
- (IBAction)btnAMHPClicked:(id)sender;

@end
