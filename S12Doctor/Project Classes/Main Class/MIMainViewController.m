//
//  MIMainViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 8/9/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIMainViewController.h"
#import "MILoginViewController.h"
#import "MIAMHPLoginViewController.h"
#import "MICMSViewController.h"
#import "MIChangePasswordViewController.h"
#import "MIHomeViewController.h"
#import "MITutorialViewController.h"

@interface MIMainViewController ()

@end

@implementation MIMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [imgBG setImage:[UIImage imageNamed:IS_IPAD ? @"landing_4.png" : Is_iPhone_5 ? @"landing_5.png" : Is_iPhone_6_PLUS ? @"landing_6+.png" : Is_iPhone_X ? @"landing_x.png" : @"landing_6.png"]];
    
    if (IS_IPAD)
    {
        [lblAMHPTitle setFont:CFontAvenirLTStd95Balck(18)];
        [lblS12Title setFont:CFontAvenirLTStd95Balck(18)];
        imgAMHPAspectRatioConstraint.constant = 50;
        imgS12AspectRatioConstraint.constant = 50;
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - Action Events

- (IBAction)btnS12Clicked:(id)sender
{
    appDelegate.isFromS12 = @1;
    
    if ([CUserDefaults objectForKey:UserDefaultTutorialS12])
    {
        MILoginViewController *s12LoginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
        [self.navigationController pushViewController:s12LoginVC animated:YES];
    }
    else
    {
        [self redirectToTutorialScreen];
    }
}

- (IBAction)btnAMHPClicked:(id)sender
{
    appDelegate.isFromS12 = @2;
    
    if ([CUserDefaults objectForKey:UserDefaultTutorialAMHP])
    {
        MIAMHPLoginViewController *amhpLoginVC = [[MIAMHPLoginViewController alloc] initWithNibName:@"MIAMHPLoginViewController" bundle:nil];
        [self.navigationController pushViewController:amhpLoginVC animated:YES];
    }
    else
    {
        [self redirectToTutorialScreen];
    }
}

- (void)redirectToTutorialScreen
{
    MITutorialViewController *tutorialVC = [[MITutorialViewController alloc] initWithNibName:@"MITutorialViewController" bundle:nil];
    [self.navigationController pushViewController:tutorialVC animated:YES];
}

@end
