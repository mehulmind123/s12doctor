//
//  ParentViewController.h
//  EdSmart
//
//  Created by S12 Solutions on 09/03/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentViewController : UIViewController

@property (nonatomic, assign) BOOL isNavigateFromSidePanel;

@property (nonatomic, assign) BOOL showTabBar;

@property (nonatomic, assign) BOOL isPresentedVC;

@property (nonatomic, strong) id iObject;

- (void)leftBurgerMenuClicked;



- (void)startLoadingAnimationInView:(UIView *)view;

- (void)stopLoadingAnimationInView:(UIView *)view;

@end
