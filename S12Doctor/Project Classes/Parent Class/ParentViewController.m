//
//  ParentViewController.m
//  EdSmart
//
//  Created by S12 Solutions on 09/03/16.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"

@interface ParentViewController ()

@end



@implementation ParentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back"]];
    [self.navigationController.navigationBar setBackIndicatorImage:[UIImage imageNamed:@"back"]];
    [self.navigationController.navigationBar setBarTintColor:ColorWhite];
    [self.navigationController.navigationBar setTintColor:[appDelegate.isFromS12 isEqual:@1] ? ColorBlue_1371b9 : ColorGreen_47C0B6];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate resignKeyboard];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
}

#pragma mark -
#pragma mark - Action Event

- (void)leftBurgerMenuClicked
{
    [appDelegate resignKeyboard];
}







# pragma mark -
# pragma mark - Helper Method

- (void)startLoadingAnimationInView:(UIView *)view
{
    UIView *animationView = (UIView *)[view viewWithTag:1000];

    if (!animationView)
    {
        animationView = [[UIView alloc] initWithFrame:CGRectZero];
        animationView.translatesAutoresizingMaskIntoConstraints = NO;
        animationView.backgroundColor = [UIColor whiteColor];
        animationView.tag = 1000;

        //....
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [activityIndicator setColor:[appDelegate.isFromS12 isEqual:@1] ? ColorBlue_1371b9 : ColorGreen_47C0B6];
        [activityIndicator setCenter:animationView.center];
        [activityIndicator startAnimating];

            [animationView addSubview:activityIndicator];
        [animationView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [animationView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];


        //....
        [view addSubview:animationView];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
    }
}

- (void)stopLoadingAnimationInView:(UIView *)view
{
    UIView *animationView = (UIView *)[view viewWithTag:1000];
    if (animationView)
        [animationView removeFromSuperview];
}


@end
