//
//  MIChangePasswordViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIChangePasswordViewController : ParentViewController
{
    IBOutlet MIFloatingTextField *txtOldPassword;
    IBOutlet MIFloatingTextField *txtNewPassword;
    IBOutlet MIFloatingTextField *txtConfirmPasssword;
}


@property (nonatomic, assign) BOOL isFromTermsCondition;

- (IBAction)btnSubmitClicked:(id)sender;

@end
