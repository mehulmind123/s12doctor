//
//  MIClaimFormsViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICommonPopup.h"

@interface MIClaimFormsViewController : ParentViewController
{
    IBOutlet UIView *viewSelectedTab;
    
    IBOutlet UIButton *btnApproved;
    IBOutlet UIButton *btnRejected;
    IBOutlet UIButton *btnPending;
    
    IBOutlet UITableView *tblVClaimForm;
    
    IBOutlet UILabel *lblNoData;
    
    MICommonPopup *commonPopup;
}


- (IBAction)btnTabClicked:(id)sender;

@end
