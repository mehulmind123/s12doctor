//
//  MIClaimFormsViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIClaimFormsViewController.h"
#import "MIViewClaimFormViewController.h"
#import "MICreateClaimFormViewController.h"

#import "MIClaimFormTableViewCell.h"

@interface MIClaimFormsViewController ()
{
    NSMutableArray *arrClaimForm;
    
    NSArray *arrApprove;
    NSArray *arrReject;
    NSArray *arrPending;
    
    int iOffset;
    
    NSURLSessionDataTask *sessionTask;
    UIRefreshControl *refreshControl;
}
@end

@implementation MIClaimFormsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    if (btnRejected.selected)
        [self btnTabClicked: btnRejected];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Claim Forms";
    
    
    //.....REGISTER CELL
    
    [tblVClaimForm registerNib:[UINib nibWithNibName:@"MIClaimFormTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIClaimFormTableViewHideCell"];
    [tblVClaimForm registerNib:[UINib nibWithNibName:@"MIClaimFormTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIClaimFormTableViewShowCell"];
    
    [tblVClaimForm setContentInset:UIEdgeInsetsMake(10, 0, 10, 0)];
    [tblVClaimForm setRowHeight:UITableViewAutomaticDimension];
    [tblVClaimForm setEstimatedRowHeight:1000];
    
    
    //..... UIREFRESH CONTROL
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = ColorBlue_1371b9;
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblVClaimForm addSubview:refreshControl];
    
    arrClaimForm = [NSMutableArray new];
    [self btnTabClicked: btnRejected.selected ? btnRejected : btnPending.selected ? btnPending : btnApproved];
}







# pragma mark
# pragma mark - API Function

- (void)pullToRefresh
{
    [self loadClaimFormData:btnApproved.selected ? 3 : btnRejected.selected ? 4 : 2];
}

- (void)loadClaimFormData:(int)type
{
    iOffset = 0;
    [self loadClaimFormListFromServerWithType:type andShowLoadingAnimation:YES];
}

- (void)cancelRequest
{
    if (sessionTask && sessionTask.state == NSURLSessionTaskStateRunning)
        [sessionTask cancel];
}

- (void)loadClaimFormListFromServerWithType:(int)type andShowLoadingAnimation:(BOOL)showLoadingAnimation
{
    [self cancelRequest];
    
    if (iOffset == 0 && showLoadingAnimation)
        [self startLoadingAnimationInView:tblVClaimForm];
    
    sessionTask = [[APIRequest request] completedClaimFormsWithFormType:type Offset:iOffset andLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
    {
        [refreshControl endRefreshing];
        [self stopLoadingAnimationInView:tblVClaimForm];
        
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            appDelegate.loginUser.draft_count = [[responseObject valueForKey:CJsonMeta] intForKey:@"draft_count"];
            [[[Store sharedInstance] mainManagedObjectContext] save];
            
            if (iOffset == 0) [arrClaimForm removeAllObjects];
            
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            if (arrData.count > 0)
            {
                [arrClaimForm addObjectsFromArray:[arrData sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"claim_id" ascending:NO]]]];
                iOffset = [[responseObject valueForKey:CJsonMeta] intForKey:CJsonNewOffset];
                [tblVClaimForm reloadData];
            }
        }
        
        
        //..... Display No Data
        
        if (arrClaimForm.count == 0)
        {
            lblNoData.hidden = NO;
            tblVClaimForm.hidden = YES;
        }
        else
        {
            lblNoData.hidden = YES;
            tblVClaimForm.hidden = NO;
        }
        
    }];
}






# pragma mark
# pragma mark - Action Events

- (IBAction)btnTabClicked:(UIButton *)sender
{
//    if (sender.selected) return;

    btnApproved.selected = btnRejected.selected = btnPending.selected = NO;
    
    sender.selected = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [viewSelectedTab setConstraintConstant:CViewX(sender) toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
        [viewSelectedTab.superview layoutIfNeeded];
        
    }];
    
    [arrClaimForm removeAllObjects];
    [tblVClaimForm reloadData];
    [self loadClaimFormData:btnApproved.selected ? 3 : btnRejected.selected ? 4 : 2];
}






# pragma mark -
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrClaimForm.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnRejected.selected)
    {
        MIClaimFormTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIClaimFormTableViewShowCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //..... SET DATA
        
        NSDictionary *dictRejectedForm = [arrClaimForm objectAtIndex:indexPath.row];
        
        cell.lblLocationType.text = [[dictRejectedForm stringValueForJSON:@"assessment_location_type_text"] isBlankValidationPassed] ? [dictRejectedForm stringValueForJSON:@"assessment_location_type_text"] : CNotAvailable;
        cell.lblAMHPName.text = [[dictRejectedForm stringValueForJSON:@"amhp_name_for_display"] isBlankValidationPassed] ? [dictRejectedForm stringValueForJSON:@"amhp_name_for_display"] : CNotAvailable;
        
        if ([[dictRejectedForm stringValueForJSON:@"rejection_reason"] isBlankValidationPassed])
        {
            NSString *strRejectionReason = [dictRejectedForm stringValueForJSON:@"rejection_reason"];
            
            NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",strRejectionReason]];
            
            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
            [style setLineSpacing:4];
            [style setParagraphSpacingBefore:4];
            [attrString addAttribute:NSParagraphStyleAttributeName
                               value:style
                               range:NSMakeRange(0, [strRejectionReason length])];
            
            cell.lblRejectionReason.attributedText = attrString;
        }
        else
            cell.lblRejectionReason.text = CNotAvailable;
        
        NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:[dictRejectedForm stringValueForJSON:@"assessment_date_time"] withFormat:@"yyyy-MM-dd HH:mm:ss"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *systemDate = [NSDate dateFromString:strClaimFormDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
        cell.lblDate.text = [NSDate stringFromDate:systemDate withFormat:@"yyyy-MM-dd HH:mm"];
        
        
        //..... CLICKED EVENTS
        
        [cell.btnRetry touchUpInsideClicked:^{
            
            MICreateClaimFormViewController *createClaimFormVC = [[MICreateClaimFormViewController alloc] initWithNibName:@"MICreateClaimFormViewController" bundle:nil];
            createClaimFormVC.iObject = dictRejectedForm;
            createClaimFormVC.claimFormType = ClaimFormRetry;
            [self.navigationController pushViewController:createClaimFormVC animated:YES];
            
        }];
        
        
        [cell.btnView touchUpInsideClicked:^{
            
            MIViewClaimFormViewController *viewClaimFormVC = [[MIViewClaimFormViewController alloc] initWithNibName:@"MIViewClaimFormViewController" bundle:nil];
            viewClaimFormVC.MIClaimFormType = MIClaimFormTypeReject;
            viewClaimFormVC.dictClaimForm = dictRejectedForm;
            [self.navigationController pushViewController:viewClaimFormVC animated:YES];
            
        }];

        
        
        //..... Load More
        
        if (indexPath.row == (arrClaimForm.count - 1))
            [self loadClaimFormListFromServerWithType:4 andShowLoadingAnimation:NO];
        
        return cell;
    }
    
    //..... APPROVED || PENDING
    
    MIClaimFormTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIClaimFormTableViewHideCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [cell.viewRejectionReason hideByHeight:YES];
    
    [cell.btnLeft setTitle:btnApproved.selected ? @"Approved" : @"Pending" forState:UIControlStateNormal];
    [cell.btnLeft setBackgroundColor:btnApproved.selected ? ColorAvaiable_00d800 : ColorMayBeAvaialble_ffba00];
    
    
    //..... SET DATA
    
    NSDictionary *dictApprovePendingForm = [arrClaimForm objectAtIndex:indexPath.row];
    
    cell.lblLocationType.text = [[dictApprovePendingForm stringValueForJSON:@"assessment_location_type_text"] isBlankValidationPassed] ? [dictApprovePendingForm stringValueForJSON:@"assessment_location_type_text"] : CNotAvailable;
    cell.lblAMHPName.text = [[dictApprovePendingForm stringValueForJSON:@"amhp_name_for_display"] isBlankValidationPassed] ? [dictApprovePendingForm stringValueForJSON:@"amhp_name_for_display"] : CNotAvailable;
    
    NSString *strClaimFormDate = [NSDate stringFromDate:[NSDate dateFromStringGMT:[dictApprovePendingForm stringValueForJSON:@"assessment_date_time"] withFormat:@"yyyy-MM-dd HH:mm:ss"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *systemDate = [NSDate dateFromString:strClaimFormDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
    cell.lblDate.text = [NSDate stringFromDate:systemDate withFormat:@"yyyy-MM-dd HH:mm"];
    
    
    //..... CLICKED EVENTS
    
    [cell.btnView touchUpInsideClicked:^{
        
        MIViewClaimFormViewController *viewClaimFormVC = [[MIViewClaimFormViewController alloc] initWithNibName:@"MIViewClaimFormViewController" bundle:nil];
        viewClaimFormVC.MIClaimFormType = btnApproved.selected ? MIClaimFormTypeApproved : MIClaimFormTypePending;
        viewClaimFormVC.dictClaimForm = dictApprovePendingForm;
        [self.navigationController pushViewController:viewClaimFormVC animated:YES];
        
    }];
    
    
    
    //..... Load More
    
    if (indexPath.row == (arrClaimForm.count - 1))
        [self loadClaimFormListFromServerWithType:btnApproved.selected ? 3 : 2 andShowLoadingAnimation:NO];
    
    return cell;
}

@end
