//
//  MIDraftViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICommonPopup.h"

@interface MIDraftViewController : ParentViewController
{
    IBOutlet UITableView *tblVDraft;
    IBOutlet UIView *viewNoDraft;
    MICommonPopup *commonPopup;
}
@end
