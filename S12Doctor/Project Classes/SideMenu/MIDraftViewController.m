
//
//  MIDraftViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIDraftViewController.h"
#import "MICreateClaimFormViewController.h"

#import "MIDraftTableViewCell.h"

@interface MIDraftViewController ()
{
    NSMutableArray *arrDraft;
    NSURLSessionDataTask *sessionTask;
    BOOL needToRefreshOnCurrentViewAppear;
}
@end

@implementation MIDraftViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
    needToRefreshOnCurrentViewAppear = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (needToRefreshOnCurrentViewAppear)
    {
        if ([AFNetworkReachabilityManager sharedManager].isReachable)
        {
            //....Stop syncing from appDelegate if already running...
            if (appDelegate.syncDraftAPI && appDelegate.syncDraftAPI.state == NSURLSessionTaskStateRunning)
                [appDelegate.syncDraftAPI cancel];
            
            //....first sync with server and load latest from server...
            [self syncSavedDraftWithServer];
        }
        else
            [self loadDraftsFromLocal];
    }
    else
    {
        needToRefreshOnCurrentViewAppear = YES;
        
        //...just reload for sorting latest draft on top...
        [self loadDraftsFromLocal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Drafts";
    
    arrDraft = [NSMutableArray new];
    
    //.....Register Cell
    
    [tblVDraft registerNib:[UINib nibWithNibName:@"MIDraftTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIDraftTableViewCell"];
    [tblVDraft setContentInset:UIEdgeInsetsMake(10, 0, 10, 0)];
}

- (void)syncSavedDraftWithServer
{
    void(^syncDraftToServer)(TBLDraft *) = ^(TBLDraft *draft)
    {
        [[APIRequest request] createClaimFormWithLocalAuthority:draft.local_authority
                                                        andAMHP:draft.amhp
                                                  andDoctorName:draft.doctor_name
                                               andDoctorAddress:draft.doctor_address
                                      andAssessmentLocationType:draft.assessment_location_type
                                          andAssessmentPostcode:draft.assessment_postcode
                                          andAssessmentDateTime:draft.assessment_date_time
                                            andPatientNHSNumber:draft.patient_nhs_number
                                             andPatientPostcode:draft.patient_postcode
                                              andAdditionalInfo:draft.additional_information
                                          andSectionImplemented:draft.section_implemented
                                                     andCarMake:draft.car_make
                                                    andCarModel:draft.car_model
                                        andCarRegistrationPlate:draft.car_registration_plate
                                                  andEngineSize:draft.engine_size
                                                       andNotes:draft.notes
                                                  andDisclaimer:draft.disclaimer
                                            andLocalAuthorityID:draft.local_authority_id
                                                      andAMHPID:draft.amhp_id
                                                   andLoginType:appDelegate.loginUser.login_type
                            andAssessmentLocatinTypeDescription:draft.specify_assessment_locattion
                                    andSelectSectionDescription:draft.specify_section
                                                 andClaimFormID:draft.claim_id
                                                andFromPostCode:draft.from_postcode
                                                  andToPostCode:draft.to_postcode
                                               andNumberOfMiles:draft.miles_traveled
                                                    andFormType:1
                                                      completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
             {
                 draft.iSynced = 1;
                 [[Store sharedInstance].mainManagedObjectContext save];
                 
                 [self syncSavedDraftWithServer];
             }
             else
             {
                 [[MILoader sharedInstance] stopAnimation];
                 [self loadDraftsFromServerWithLoadingAnimation:YES];
             }
         }];
    };
    
    
    NSArray *unsyncedDraft = [TBLDraft fetch:[NSPredicate predicateWithFormat:@"iSynced == 0"] sortedBy:nil];
    if (unsyncedDraft.count > 0)
    {
        [[MILoader sharedInstance] startAnimation];
        syncDraftToServer([unsyncedDraft firstObject]);
    }
    else
    {
        NSLog(@"All local unsynced draft synced with server....");
        [[MILoader sharedInstance] stopAnimation];
        [self loadDraftsFromServerWithLoadingAnimation:YES];
    }
}



# pragma mark
# pragma mark - API Events

- (void)loadDraftsFromLocal
{
    [arrDraft removeAllObjects];
    arrDraft = [[NSMutableArray alloc] initWithArray:[TBLDraft fetch:nil orderBy:@"timestamp" ascending:NO]];
    [tblVDraft reloadData];
    [self displayNoData];
}

- (void)loadDraftsFromServerWithLoadingAnimation:(BOOL)showLoadingAnimation
{
    if (sessionTask && sessionTask.state == NSURLSessionTaskStateRunning)
        [sessionTask cancel];
    
    int iOffset = 0;
    if (showLoadingAnimation)
    {
        [arrDraft removeAllObjects];
        [tblVDraft reloadData];
        [viewNoDraft setHidden:YES];
        [self startLoadingAnimationInView:tblVDraft];
    }
    else
        iOffset = (int)([TBLDraft fetch:[NSPredicate predicateWithFormat:@"iSynced == 1"] sortedBy:nil].count);
    
    sessionTask = [[APIRequest request] completedClaimFormsWithFormType:1 Offset:iOffset andLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
    {
        [self stopLoadingAnimationInView:tblVDraft];
        
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            if (iOffset == 0)
                [TBLDraft deleteObjects:[NSPredicate predicateWithFormat:@"iSynced == 1"]];
            
            appDelegate.loginUser.draft_count = [[responseObject valueForKey:CJsonMeta] intForKey:@"draft_count"];
            
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            for (NSDictionary *dictDraft in arrData)
            {
                TBLDraft *draft = [TBLDraft findOrCreate:@{@"claim_id":[dictDraft stringValueForJSON:@"claim_id"]}];
                draft.claim_id = [dictDraft stringValueForJSON:@"claim_id"];
                draft.amhp_id = [dictDraft stringValueForJSON:@"amhp_id"];
                draft.local_authority_id = [dictDraft stringValueForJSON:@"local_authority_id"];
                
                draft.additional_information = [dictDraft stringValueForJSON:@"additional_information"];
                draft.amhp = [dictDraft stringValueForJSON:@"amhp_name"];
                draft.assessment_date_time = [dictDraft stringValueForJSON:@"assessment_date_time"];
                draft.assessment_location_type = [dictDraft stringValueForJSON:@"assessment_location_type_text"];
                draft.specify_assessment_locattion = [dictDraft stringValueForJSON:@"assessment_location_type_description"];
                draft.assessment_postcode = [dictDraft stringValueForJSON:@"assessment_postcode"];
                draft.car_make = [dictDraft stringValueForJSON:@"car_make"];
                draft.car_model = [dictDraft stringValueForJSON:@"car_model"];
                draft.car_registration_plate = [dictDraft stringValueForJSON:@"car_registration_plate"];
                draft.timestamp = [dictDraft doubleForKey:@"claim_creation_timestamp"];
                draft.disclaimer = [dictDraft stringValueForJSON:@"disclaimer"];
                draft.doctor_name = [dictDraft stringValueForJSON:@"doctor_name"];
                draft.doctor_address = [dictDraft stringValueForJSON:@"doctor_address"];
                draft.engine_size = [dictDraft stringValueForJSON:@"engine_size"];
                draft.from_postcode = [dictDraft stringValueForJSON:@"from_postcode"];
                draft.local_authority = [dictDraft stringValueForJSON:@"local_authority"];
                draft.miles_traveled = [dictDraft stringValueForJSON:@"miles_traveled"];
                draft.patient_nhs_number = [dictDraft stringValueForJSON:@"patient_nhs_number"];
                draft.patient_postcode = [dictDraft stringValueForJSON:@"patient_postcode"];
                draft.section_implemented = [dictDraft stringValueForJSON:@"section_implemented_text"];
                draft.specify_section = [dictDraft stringValueForJSON:@"section_type_description"];
                draft.notes = [dictDraft stringValueForJSON:@"notes"];
                draft.to_postcode = [dictDraft stringValueForJSON:@"to_postcode"];
                draft.assessment_locaiton_id = [dictDraft stringValueForJSON:@"assessment_location_type"];
                draft.section_implemented_id = [dictDraft stringValueForJSON:@"section_implemented"];
                draft.iSynced = YES;
            }
            [[Store sharedInstance].mainManagedObjectContext save];
            [self loadDraftsFromLocal];
        }
        
        [self displayNoData];
        
    }];
}

- (void)displayNoData
{
    if (arrDraft.count == 0)        //..... Display No Data
    {
        viewNoDraft.hidden = NO;
        tblVDraft.hidden = YES;
    }
    else
    {
        viewNoDraft.hidden = YES;
        tblVDraft.hidden = NO;
    }
}




# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDraft.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        //..... DELETE DRAFT OBJECT
        
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageDraftDelete firstButton:@"No" firstHandler:nil secondButton:@"Yes" secondHandler:^(UIAlertAction *action)
         {
             TBLDraft *draft = [arrDraft objectAtIndex:indexPath.row];
             
             if (draft.iSynced)
             {
                 [[APIRequest request] deleteDraftWithClaimID:draft.claim_id completed:^(id responseObject, NSError *error)
                  {
                      if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                      {
                          appDelegate.loginUser.draft_count = [[responseObject valueForKey:CJsonMeta] intForKey:@"draft_count"];
                          [[[Store sharedInstance] mainManagedObjectContext] save];
                          
                          [arrDraft removeObject:draft];
                          [draft deleteObject];
                          [tblVDraft reloadData];
                          viewNoDraft.hidden = arrDraft.count != 0;
                      }
                  }];
             }
             else
             {
                 appDelegate.loginUser.draft_count = appDelegate.loginUser.draft_count - 1;
                 [[[Store sharedInstance] mainManagedObjectContext] save];
                 
                 [arrDraft removeObject:draft];
                 [draft deleteObject];
                 [tblVDraft reloadData];
                 viewNoDraft.hidden = arrDraft.count != 0;
             }
             
         } inView:self];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIDraftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIDraftTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    TBLDraft *draft = [arrDraft objectAtIndex:indexPath.row];
    
    if (draft.amhp.isBlankValidationPassed)
        cell.lblDraftTitle.text = [NSString stringWithFormat:@"AMHP: %@", draft.amhp];
    else if (draft.assessment_postcode.isBlankValidationPassed)
        cell.lblDraftTitle.text = [NSString stringWithFormat:@"Postcode: %@", draft.assessment_postcode];
    else
        cell.lblDraftTitle.text = @"Draft";
    
    cell.lblDraftCreatedDate.text = [NSString stringWithFormat:@"%@", [appDelegate timestampToDateWithTimestamp:draft.timestamp]];
    
    if (indexPath.row == (arrDraft.count - 1))
        [self loadDraftsFromServerWithLoadingAnimation:NO];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    needToRefreshOnCurrentViewAppear = NO;
    MICreateClaimFormViewController *createClaimFormVC = [[MICreateClaimFormViewController alloc] initWithNibName:@"MICreateClaimFormViewController" bundle:nil];
    createClaimFormVC.iObject = [arrDraft objectAtIndex:indexPath.row];
    createClaimFormVC.claimFormType = ClaimFormDraft;
    [self.navigationController pushViewController:createClaimFormVC animated:YES];
}

@end
