//
//  MIEditProfileViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/21/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIHomeViewController.h"

typedef void(^selectedValue)(NSString *data);

@interface MIEditProfileViewController : ParentViewController
{
    IBOutlet MIFloatingTextField *txtName;
    IBOutlet MIFloatingTextField *txtGMCReferanceNumber;
    IBOutlet UITextField *txtEmployer;
    IBOutlet MIFloatingTextView *txtVEmployer;
    IBOutlet MIFloatingTextField *txtRegion;
    IBOutlet MIFloatingTextView *txtVGeneralAvailibility;
    IBOutlet UITextField *txtEmail;
    IBOutlet MIFloatingTextField *txtMobileNumber;
    IBOutlet MIFloatingTextField *txtLandlineNumber;
    IBOutlet MIFloatingTextField *txtOfficeBasePostcode;
    IBOutlet MIFloatingTextField *txtOfficeBaseTeam;
//    IBOutlet UITextField *txtDefaultClaimFormAddress;
    IBOutlet UITextField *txtCarMake;
    IBOutlet UITextField *txtCarModel;
    IBOutlet UITextField *txtCarRegistrationPlate;
    IBOutlet UITextField *txtEngineSize;
    IBOutlet MIFloatingTextView *txtVLocalAuth;
    IBOutlet MIFloatingTextView *txtVLanguage;
    IBOutlet MIFloatingTextView *txtVSpecialities;
    IBOutlet MIFloatingTextView *txtVDefaultClaimFormAddress;
    
}


@property (nonatomic , copy) selectedValue selectedValue;
@property (nonatomic , assign) BOOL isFromHomeScreen;

- (IBAction)btnSubmitClicked:(id)sender;
- (IBAction)btnEmployerClicked:(id)sender;
- (IBAction)btnLocalAuthorityClicked:(id)sender;
- (IBAction)btnLanguageClicked:(id)sender;
- (IBAction)btnSpecialitiesClicked:(id)sender;
- (IBAction)btnOfficeBasePostCodeInfoClicked:(id)sender;

- (IBAction)textFieldTextDidChange:(id)sender;

@end
