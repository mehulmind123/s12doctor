 //
//  MIEditProfileViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/21/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIEditProfileViewController.h"
#import "MISelectLanguageViewController.h"

@interface MIEditProfileViewController ()
{
    NSString *strEmployerID;
    NSString *strRegionID;
    NSString *strLocalAuthorityID;
    NSString *strLanguageID;
    NSString *strSpecialtyID;
    
    NSArray *arrEmployer;
    NSArray *arrRegionList;
    NSArray *arrAddedLanguage;
}
@end

@implementation MIEditProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Edit Profile";
    
    CGFloat size = 10;
    [txtEmployer setRightImage:[UIImage imageNamed:@"dropdown"] withSize:CGSizeMake(size, size)];
    
    txtVGeneralAvailibility.autocorrectionType = UITextAutocorrectionTypeYes;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidChange:) name:UITextViewTextDidChangeNotification object:nil];

    
    //..... Load Employer Data
    [self loadEmployerListFromServer];
    [self loadRegionListFromServer];
    
    //..... Set Data From Local
    [self setDataFromLocal];
}





# pragma mark
# pragma mark - API Function

- (void)setDataFromLocal
{
    txtName.text = appDelegate.loginUser.name;
    txtGMCReferanceNumber.text = appDelegate.loginUser.gmc_referance_number;
    
    //..... Employer
    
    NSLog(@"%@", appDelegate.loginUser.employer);
    
    txtVEmployer.text = [[appDelegate.loginUser.employer valueForKeyPath:@"organisation_name"] componentsJoinedByString:CComponentJoinedString];
    strEmployerID = [[appDelegate.loginUser.employer valueForKeyPath:@"organisation_id"] componentsJoinedByString:CComponentJoinedString];
    
    //..... Region
    
    txtRegion.text = appDelegate.loginUser.region_name;
    strRegionID = [NSString stringWithFormat:@"%lld", appDelegate.loginUser.region_id];
 
    //..... Local Authority
    
    txtVLocalAuth.text = [[appDelegate.loginUser.local_authorities valueForKeyPath:@"authority_name"] componentsJoinedByString:CComponentJoinedString];
    strLocalAuthorityID = [[appDelegate.loginUser.local_authorities valueForKeyPath:@"authority_id"] componentsJoinedByString:CComponentJoinedString];
    
    txtVGeneralAvailibility.text = appDelegate.loginUser.general_availability;
    txtEmail.text = appDelegate.loginUser.email;
    txtMobileNumber.text = appDelegate.loginUser.mobile_number;
    txtLandlineNumber.text = appDelegate.loginUser.landline_number;
    txtOfficeBasePostcode.text = appDelegate.loginUser.office_base_postcode;
    txtOfficeBaseTeam.text = appDelegate.loginUser.office_base_team;
    
    //..... Language
    
    txtVLanguage.text = [[appDelegate.loginUser.language_spoken valueForKeyPath:@"language_name"] componentsJoinedByString:CComponentJoinedString];
    strLanguageID = [[appDelegate.loginUser.language_spoken valueForKeyPath:@"language_id"] componentsJoinedByString:CComponentJoinedString];
    
    //..... Specialty
    
    txtVSpecialities.text = [[appDelegate.loginUser.specialties valueForKeyPath:@"specialties_name"] componentsJoinedByString:CComponentJoinedString];
    strSpecialtyID = [[appDelegate.loginUser.specialties valueForKeyPath:@"specialties_id"] componentsJoinedByString:CComponentJoinedString];
    
    txtVDefaultClaimFormAddress.text = appDelegate.loginUser.default_claim_address;
    txtCarMake.text = appDelegate.loginUser.car_make;
    txtCarModel.text = appDelegate.loginUser.car_model;
    txtCarRegistrationPlate.text = appDelegate.loginUser.car_registration_plate;
    txtEngineSize.text = appDelegate.loginUser.engine_size;
}

- (void)getUserDetailFromServer
{
    [[APIRequest request] userDetails:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            [[APIRequest request] saveLoginUserToLocal:responseObject];
            [self setDataFromLocal];
        }
    }];
}

- (void)loadEmployerListFromServer
{
    [[APIRequest request] employerList:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            arrEmployer = [responseObject valueForKey:CJsonData];
            
            [self getUserDetailFromServer];
            
            if (arrEmployer.count > 0)
            {
                [txtEmployer setPickerData:[arrEmployer valueForKeyPath:@"organisation_name"] update:^(NSString *text, NSInteger row, NSInteger component)
                 {
                     strEmployerID = [[arrEmployer objectAtIndex:row] stringValueForJSON:@"organisation_id"];
                 }];
            }
        }
        
        if (_isFromHomeScreen)
            [txtVGeneralAvailibility becomeFirstResponder];
    }];
}

- (void)loadRegionListFromServer
{
    [self setRegionUserInteraction];
    
    [[APIRequest request] getRegionList:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             arrRegionList = [responseObject valueForKey:CJsonData];
             
             if (arrRegionList.count > 0)
             {
//                 txtRegion.userInteractionEnabled = YES;
                 
                 [txtRegion setPickerData:[arrRegionList valueForKeyPath:@"region_name"] update:^(NSString *text, NSInteger row, NSInteger component)
                  {
                      strRegionID = [[arrRegionList objectAtIndex:row] stringValueForJSON:@"region_id"];
                      appDelegate.loginUser.local_authorities = nil;
                      txtVLocalAuth.text = strLocalAuthorityID = @"";
                  }];
             }
//             else
//             {
//                 [self setRegionUserInteraction];
//             }
         }
//         else
//         {
//             [self setRegionUserInteraction];
//         }
         
     }];
}

- (void)setRegionUserInteraction
{
    txtRegion.userInteractionEnabled = NO;
}







# pragma mark
# pragma mark - Action Events

- (IBAction)btnEmployerClicked:(id)sender
{
    MISelectLanguageViewController *selectVC = [[MISelectLanguageViewController alloc] initWithNibName:@"MISelectLanguageViewController" bundle:nil];
    selectVC.selectionType = employer;
    selectVC.arrSelectedValue = [appDelegate.loginUser.employer mutableCopy];
    [selectVC setBlock:^(NSArray *arrSelectedEmployer, NSError *error)
     {
         if (arrSelectedEmployer.count > 0)
         {
             appDelegate.loginUser.employer = arrSelectedEmployer;
             txtVEmployer.text = [[arrSelectedEmployer valueForKey:@"organisation_name"] componentsJoinedByString:CComponentJoinedString];
             strEmployerID = [[arrSelectedEmployer valueForKey:@"organisation_id"] componentsJoinedByString:CComponentJoinedString];
         }
         else
         {
             appDelegate.loginUser.employer = nil;
             txtVEmployer.text = strEmployerID = @"";
         }
     }];
    
    [self.navigationController pushViewController:selectVC animated:YES];
}

- (IBAction)btnLocalAuthorityClicked:(id)sender
{
    MISelectLanguageViewController *selectVC = [[MISelectLanguageViewController alloc] initWithNibName:@"MISelectLanguageViewController" bundle:nil];
    selectVC.regionID = strRegionID;
    selectVC.selectionType = localAuthority;
    selectVC.arrSelectedValue = [appDelegate.loginUser.local_authorities mutableCopy];
    [selectVC setBlock:^(NSArray *arrSelectedAuthorities, NSError *error)
    {
        if (arrSelectedAuthorities.count > 0)
        {
            appDelegate.loginUser.local_authorities = arrSelectedAuthorities;
            txtVLocalAuth.text = [[arrSelectedAuthorities valueForKey:@"authority_name"] componentsJoinedByString:CComponentJoinedString];
            strLocalAuthorityID = [[arrSelectedAuthorities valueForKey:@"authority_id"] componentsJoinedByString:CComponentJoinedString];
        }
        else
        {
            appDelegate.loginUser.local_authorities = nil;
            txtVLocalAuth.text = strLocalAuthorityID = @"";
        }
    }];
    
    [self.navigationController pushViewController:selectVC animated:YES];
}

- (IBAction)btnLanguageClicked:(id)sender
{
    MISelectLanguageViewController *selectVC = [[MISelectLanguageViewController alloc] initWithNibName:@"MISelectLanguageViewController" bundle:nil];
    selectVC.selectionType = languageSelection;
    selectVC.arrSelectedValue = [appDelegate.loginUser.language_spoken mutableCopy];
    selectVC.arrNewLanguage = arrAddedLanguage;
    
    [selectVC setSelectLanguageBlock:^(NSArray *arrSelectedLanguage, NSArray *arrNewLanguage)
     {
         if (arrSelectedLanguage.count > 0)
         {  
             arrAddedLanguage = arrNewLanguage;
             appDelegate.loginUser.language_spoken = arrSelectedLanguage;
             txtVLanguage.text = [[arrSelectedLanguage valueForKey:@"language_name"] componentsJoinedByString:CComponentJoinedString];
             strLanguageID = [[arrSelectedLanguage valueForKey:@"language_id"] componentsJoinedByString:CComponentJoinedString];
         }
         else
         {
             arrAddedLanguage = nil;
             appDelegate.loginUser.language_spoken = nil;
             txtVLanguage.text = strLanguageID = @"";
         }
     }];
    
    [self.navigationController pushViewController:selectVC animated:YES];
}

- (IBAction)btnSpecialitiesClicked:(id)sender
{
    MISelectLanguageViewController *selectVC = [[MISelectLanguageViewController alloc] initWithNibName:@"MISelectLanguageViewController" bundle:nil];
    selectVC.selectionType = specialities;
    selectVC.arrSelectedValue = [appDelegate.loginUser.specialties mutableCopy];
    
    [selectVC setBlock:^(NSArray *arrSelectedSpecialties, NSError *error)
     {
         if (arrSelectedSpecialties.count > 0)
         {
             appDelegate.loginUser.specialties = arrSelectedSpecialties;
             txtVSpecialities.text = [[arrSelectedSpecialties valueForKey:@"specialties_name"] componentsJoinedByString:CComponentJoinedString];
             strSpecialtyID = [[arrSelectedSpecialties valueForKey:@"specialties_id"] componentsJoinedByString:CComponentJoinedString];
         }
         else
         {
             appDelegate.loginUser.specialties = nil;
             txtVSpecialities.text = strSpecialtyID = @"";
         }
     }];
    
    [self.navigationController pushViewController:selectVC animated:YES];
}

- (IBAction)btnOfficeBasePostCodeInfoClicked:(id)sender
{
    [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageOfficeBasePostcodeInfo buttonTitle:@"OK" handler:nil inView:self];
}

- (IBAction)btnSubmitClicked:(id)sender
{
    NSString *trimmedPostCode = [txtOfficeBasePostcode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    txtOfficeBasePostcode.text = trimmedPostCode;
    
    if (![txtName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankName onView:self];
    
    else if (![txtVEmployer.text isBlankValidationPassed] || ![strEmployerID isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmployer onView:self];
    
    else if ([txtMobileNumber.text isBlankValidationPassed] && txtMobileNumber.text.length != 11)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidMobileNumber onView:self];
    
    else if ([txtLandlineNumber.text isBlankValidationPassed] && txtLandlineNumber.text.length != 11)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidLandlineNumber onView:self];
    
    else if([txtOfficeBasePostcode.text isBlankValidationPassed] && ((txtOfficeBasePostcode.text.length < 5 || txtOfficeBasePostcode.text.length > CCharacterLimit9)))
    {
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidOfficePostcode onView:self];
    }
    
    else
    {
        [appDelegate resignKeyboard];

        [[APIRequest request] editProfileWithName:txtName.text andGMCReferenceNumber:txtGMCReferanceNumber.text andEmployer:strEmployerID ? strEmployerID : @""  andLocalAuthority:strLocalAuthorityID ? strLocalAuthorityID : @"" andGeneralAvailability:txtVGeneralAvailibility.text andMobileNumber:txtMobileNumber.text andLandlineNumber:txtLandlineNumber.text andOfficeBasePostcode:txtOfficeBasePostcode.text andOfficeBaseTeam:txtOfficeBaseTeam.text andLanguageSpoken:strLanguageID ? strLanguageID : @"" andSpecialties:strSpecialtyID ? strSpecialtyID : @"" andDefaultClaimFormAddress:txtVDefaultClaimFormAddress.text andCarMake:txtCarMake.text andCarModel:txtCarModel.text andCarRegistrationPlate:txtCarRegistrationPlate.text andEngineSize:txtEngineSize.text andLoginType:appDelegate.loginUser.login_type andLanguageName:txtVLanguage.text andRegionID:strRegionID ? strRegionID : @"" completed:^(id responseObject, NSError *error)
        {
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [[APIRequest request] saveLoginUserToLocal:responseObject];
                
                [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self];
                
                MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                [appDelegate openMenuViewcontroller:homeVC animated:YES];
            }
            else if (!error)
                [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
        }];
    }
}






# pragma mark
# pragma mark - UITextField And UITextView Text Did Change

- (BOOL)checkPostCode
{
    if ([txtOfficeBasePostcode.text isValidOfficePostCode])
    {
        NSString* newStr = [txtOfficeBasePostcode.text stringByTrimmingCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
        
        if ([newStr length] < [txtOfficeBasePostcode.text length])
            txtOfficeBasePostcode.text = newStr;
        
        if (txtOfficeBasePostcode.text.length > CCharacterLimit9)
        {
            txtOfficeBasePostcode.text = [txtOfficeBasePostcode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            txtOfficeBasePostcode.text = [txtOfficeBasePostcode.text substringToIndex:CCharacterLimit9];
            return NO;
        }
        else
        {
            NSString *newString = [txtOfficeBasePostcode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            txtOfficeBasePostcode.text = newString;
            return YES;
        }
    }
    else
    {
        NSString* newStr = [txtOfficeBasePostcode.text stringByTrimmingCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
        
        if ([newStr length] < [txtOfficeBasePostcode.text length])
            txtOfficeBasePostcode.text = newStr;
        
        if (txtOfficeBasePostcode.text.length > CCharacterLimit9)
        {
            txtOfficeBasePostcode.text = [[txtOfficeBasePostcode.text substringToIndex:CCharacterLimit9] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            return YES;
        }
        else
            return NO;
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView == txtVGeneralAvailibility)
    {
        if (txtVGeneralAvailibility.text.length > CCharacterLimit500)
        {
            txtVGeneralAvailibility.text = [txtVGeneralAvailibility.text substringToIndex:CCharacterLimit500];
            return;
        }
    }
    else
    {
        if (txtVDefaultClaimFormAddress.text.length > CCharacterLimit500)
        {
            txtVDefaultClaimFormAddress.text = [txtVDefaultClaimFormAddress.text substringToIndex:CCharacterLimit500];
            return;
        }
    }
}

- (IBAction)textFieldTextDidChange:(UITextField *)sender
{
    if ([sender isEqual:txtName] || [sender isEqual:txtOfficeBaseTeam] || [sender isEqual:txtCarMake] || [sender isEqual:txtCarModel])
    {
        if (sender.text.length > CCharacterLimit256)
        {
            sender.text = [sender.text substringToIndex:CCharacterLimit256];
            return;
        }
        else
            sender.text = sender.text;
    }
    else if ([sender isEqual:txtCarRegistrationPlate])
    {
        if (sender.text.length > CCharacterLimit8)
        {
            sender.text = [sender.text substringToIndex:CCharacterLimit8];
            return;
        }
        else
            sender.text = sender.text;
    }
    else if ([sender isEqual:txtMobileNumber] || [sender isEqual:txtLandlineNumber])
    {
        if (sender.text.length > CCharacterLimit11)
        {
            sender.text = [sender.text substringToIndex:CCharacterLimit11];
            return;
        }
        else
            sender.text = sender.text;
    }
//    else if ([sender isEqual:txtDefaultClaimFormAddress])
//    {
//        if (sender.text.length > CCharacterLimit500)
//        {
//            sender.text = [sender.text substringToIndex:CCharacterLimit500];
//            return;
//        }
//        else
//            sender.text = sender.text;
//    }
    else if ([sender isEqual:txtEngineSize])
    {
        if (sender.text.length > 5)
        {
            sender.text = [sender.text substringToIndex:5];
            return;
        }
        else
            sender.text = sender.text;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@" 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
    for (int i = 0; i < [string length]; i++)
    {
        unichar c = [string characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            return NO;
        }
    }
    
    return YES;
}

@end
