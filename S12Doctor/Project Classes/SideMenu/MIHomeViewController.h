//
//  MIHomeViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIHomeViewController : ParentViewController
{
    IBOutlet UIImageView *imgVBg;
    
    IBOutlet UIView *viewAvailabilityStatus;
    IBOutlet UIView *viewWeeklyAvailabilityStatus;
    IBOutlet UIImageView *imgVStatus;
    IBOutlet UILabel *lblStatus;
    IBOutlet UIButton *btnEdit;
    
    IBOutlet UIButton *btnAvailable;
    IBOutlet UIButton *btnMayBeAvailable;
    IBOutlet UIButton *btnUnAvailable;
    IBOutlet MIGenericButton *btnCreateClaimForm;
    
    IBOutlet UIView *viewGeneralAvailability;
    IBOutlet UILabel *lblGeneralAvailability;
    IBOutlet NSLayoutConstraint *availableBottomConstraint;
    IBOutlet NSLayoutConstraint *maybeAvailableBottomConstraint;
    IBOutlet NSLayoutConstraint *unavailableBottomConstraint;
    
    
    IBOutlet UIView *viewAvailabilityCalendar;
    IBOutlet UIView *viewWeekDays;
    IBOutlet UIView *viewAM;
    IBOutlet UIView *viewLunch;
    IBOutlet UIView *viewPM;
    IBOutlet UIView *viewEvening;
    IBOutlet UIView *viewNight;
    
    IBOutlet UIButton *btnAmMon;
    IBOutlet UIButton *btnAmTue;
    IBOutlet UIButton *btnAmWed;
    IBOutlet UIButton *btnAmThu;
    IBOutlet UIButton *btnAmFri;
    IBOutlet UIButton *btnAmSat;
    IBOutlet UIButton *btnAmSun;
    
    IBOutlet UIButton *btnLunchMon;
    IBOutlet UIButton *btnLunchTue;
    IBOutlet UIButton *btnLunchWed;
    IBOutlet UIButton *btnLunchThu;
    IBOutlet UIButton *btnLunchFri;
    IBOutlet UIButton *btnLunchSat;
    IBOutlet UIButton *btnLunchSun;
    
    IBOutlet UIButton *btnPmMon;
    IBOutlet UIButton *btnPmTue;
    IBOutlet UIButton *btnPmWed;
    IBOutlet UIButton *btnPmThu;
    IBOutlet UIButton *btnPmFri;
    IBOutlet UIButton *btnPmSat;
    IBOutlet UIButton *btnPmSun;
    
    IBOutlet UIButton *btnEveningMon;
    IBOutlet UIButton *btnEveningTue;
    IBOutlet UIButton *btnEveningWed;
    IBOutlet UIButton *btnEveningThu;
    IBOutlet UIButton *btnEveningFri;
    IBOutlet UIButton *btnEveningSat;
    IBOutlet UIButton *btnEveningSun;
    
    IBOutlet UIButton *btnNightMon;
    IBOutlet UIButton *btnNightTue;
    IBOutlet UIButton *btnNightWed;
    IBOutlet UIButton *btnNightThu;
    IBOutlet UIButton *btnNightFri;
    IBOutlet UIButton *btnNightSat;
    IBOutlet UIButton *btnNightSun;
}


- (IBAction)btnEditClicked:(id)sender;
- (IBAction)btnAvailabilityStatusClicked:(id)sender;
- (IBAction)btnCreateClaimFromClicked:(id)sender;
- (IBAction)btnStausBoxClicked:(id)sender;
- (IBAction)btnGeneralAvailBoxClicked:(id)sender;


@end
