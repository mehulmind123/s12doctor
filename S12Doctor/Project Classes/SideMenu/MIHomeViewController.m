//
//  MIHomeViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIHomeViewController.h"
#import "MICreateClaimFormViewController.h"
#import "MIEditProfileViewController.h"

#import "MIUpdateStatusPopup.h"
#import "MICommonPopup.h"

@interface MIHomeViewController ()
{
    MIUpdateStatusPopup *editStatusPopup;
    MICommonPopup *commonPopup;
    
    //..... When Come First Time Not Display Internet Gained Alert.
    BOOL isFirstTime;
    
    //..... When Change Status From Popup >>> YES.
    BOOL isEditStatus;
}
@end

@implementation MIHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
    
    //....Stop syncing from appDelegate if already running...
    if (appDelegate.syncDraftAPI.state != NSURLSessionTaskStateRunning)
        [appDelegate syncSavedDraftWithServer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //..... SET DATA
    [self setGeneralAvailabilityData];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    btnAvailable.layer.cornerRadius = CViewHeight(btnAvailable) / 2;
    btnMayBeAvailable.layer.cornerRadius = CViewHeight(btnMayBeAvailable) / 2;
    btnUnAvailable.layer.cornerRadius = CViewHeight(btnUnAvailable) / 2;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Section 12 doctor home";
    
    isFirstTime = YES;
    
    //.....Internet Reachability Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReachAbilityChange) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    
    
    [imgVBg setImage:[UIImage imageNamed:Is_iPhone_4 ? @"home_bg_4s.png" : Is_iPhone_5 ? @"home_bg_5s.png" : Is_iPhone_6_PLUS ? @"home_bg_6+.png" : @"home_bg_6s.png"]];
    
    
    //..... VIEW SHADOW
    
    viewAvailabilityStatus.layer.cornerRadius = viewGeneralAvailability.layer.cornerRadius = 4;
    viewAvailabilityStatus.layer.shadowColor =  viewGeneralAvailability.layer.shadowColor = ColorBlack.CGColor;
    viewAvailabilityStatus.layer.shadowOffset = viewGeneralAvailability.layer.shadowOffset = CGSizeZero;
    viewAvailabilityStatus.layer.shadowRadius = viewGeneralAvailability.layer.shadowRadius = 3;
    viewAvailabilityStatus.layer.shadowOpacity =viewGeneralAvailability.layer.shadowOpacity = 0.2;
    viewAvailabilityStatus.layer.masksToBounds = viewGeneralAvailability.layer.masksToBounds = NO;

    viewWeeklyAvailabilityStatus.layer.cornerRadius = 4;
    viewWeeklyAvailabilityStatus.layer.shadowColor = ColorBlack.CGColor;
    viewWeeklyAvailabilityStatus.layer.shadowOffset = CGSizeZero;
    viewWeeklyAvailabilityStatus.layer.shadowRadius = 3;
    viewWeeklyAvailabilityStatus.layer.shadowOpacity = 0.2;
    viewWeeklyAvailabilityStatus.layer.masksToBounds = YES;
    
    btnCreateClaimForm.layer.cornerRadius = CViewHeight(btnCreateClaimForm) / 2;
    btnCreateClaimForm.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    btnCreateClaimForm.layer.shadowOffset = CGSizeZero;
    btnCreateClaimForm.layer.shadowRadius = 10;
    btnCreateClaimForm.layer.shadowOpacity = 0.9;
    btnCreateClaimForm.layer.masksToBounds = NO;
    

    //..... API CALL AND GET AVAILABILITY STATUS FROM SERVER
    
    appDelegate.loginUser.availability_status == 1 ? [self setAvailable:appDelegate.loginUser.availability_message] : appDelegate.loginUser.availability_status == 2 ? [self setMaybeAvailable:appDelegate.loginUser.availability_message] : appDelegate.loginUser.availability_status == 3 ? [self setUnAvaialble:appDelegate.loginUser.availability_message] : btnCreateClaimForm;
    
    [self changeAvailabilityAPI];
    [self getUserDetailsFromServer];
    [self getWeeklyAvailability];
}

- (void)setGeneralAvailabilityData
{
    lblGeneralAvailability.text = [appDelegate.loginUser.general_availability isBlankValidationPassed] ? appDelegate.loginUser.general_availability : CNotAvailable;
}

- (void)getUserDetailsFromServer
{
    [[APIRequest request] userDetails:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            [[APIRequest request] saveLoginUserToLocal:responseObject];
            
            NSString *strAvailabilityMsg = [[responseObject valueForKey:CJsonData] stringValueForJSON:@"availability_message"];
            
            appDelegate.loginUser.availability_status == 1 ? [self setAvailable:strAvailabilityMsg] : appDelegate.loginUser.availability_status == 2 ? [self setMaybeAvailable:strAvailabilityMsg] : appDelegate.loginUser.availability_status == 3 ? [self setUnAvaialble:strAvailabilityMsg] : btnCreateClaimForm;
        }
    }];
}



- (void)handleReachAbilityChange
{
    if ([AFNetworkReachabilityManager sharedManager].reachable)
    {
        NSLog(@"AFNetworkReachabilityManager reachable");
        
        [self changeAvailabilityAPI];
        [self changeWeeklyAvailabilityStatus];
    }
    else
    {
        if (!isFirstTime)
            [appDelegate toastAlertWithMessage:CMessageSync showDone:NO];
    }
}





# pragma mark
# pragma mark - Doctor Availability

- (void)setAvailable:(NSString *)availabilityMsg
{
    [imgVStatus setImage:[UIImage imageNamed:@"available"]];
    [btnAvailable hideByHeight:YES];
    [btnMayBeAvailable hideByHeight:NO];
    [btnUnAvailable hideByHeight:NO];
    availableBottomConstraint.constant = 0;
    maybeAvailableBottomConstraint.constant = 12;
    unavailableBottomConstraint.constant = 54;
    [btnAvailable setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    [btnMayBeAvailable setConstraintConstant:30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    
    if (![availabilityMsg isBlankValidationPassed])
        [self setAvaialbe:CMessagePlaceHolderAvailabilty];
    else
        [self setAvaialbe:availabilityMsg];
}

- (void)setMaybeAvailable:(NSString *)availabilityMsg
{
    [imgVStatus setImage:[UIImage imageNamed:@"maybe_unavailable"]];
    [btnMayBeAvailable hideByHeight:YES];
    [btnAvailable hideByHeight:NO];
    [btnUnAvailable hideByHeight:NO];
    availableBottomConstraint.constant = 0;
    maybeAvailableBottomConstraint.constant = 12;
    unavailableBottomConstraint.constant = 54;
    [btnAvailable setConstraintConstant:30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    [btnUnAvailable setConstraintConstant:12 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    
    if (![availabilityMsg isBlankValidationPassed])
        [self setMaybeAvaialbe:CMessagePlaceHolderAvailabilty];
    else
        [self setMaybeAvaialbe:availabilityMsg];
}

- (void)setUnAvaialble:(NSString *)availabilityMsg
{
    [imgVStatus setImage:[UIImage imageNamed:@"unavailable"]];
    [btnUnAvailable hideByHeight:YES];
    [btnAvailable hideByHeight:NO];
    [btnMayBeAvailable hideByHeight:NO];
    availableBottomConstraint.constant = 12;
    unavailableBottomConstraint.constant = 0;
    maybeAvailableBottomConstraint.constant = 52;
    [btnAvailable setConstraintConstant:32 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    
    if (![availabilityMsg isBlankValidationPassed])
        [self setUnavaialbe:CMessagePlaceHolderAvailabilty];
    else
        [self setUnavaialbe:availabilityMsg];
}

- (void)setAvaialbe:(NSString *)text
{
    NSString *strAvailable = @"Available:";
    
    if ([text isBlankValidationPassed])
        lblStatus.text = [NSString stringWithFormat:@"%@ %@", strAvailable, text];
    else
        lblStatus.text = [NSString stringWithFormat:@"%@", strAvailable];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:lblStatus.text];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:5];
    
    NSRange range = [lblStatus.text rangeOfString:strAvailable options:NSCaseInsensitiveSearch];
    
    if (range.location != NSNotFound)
    {
        [attributedString addAttribute:NSParagraphStyleAttributeName
                                 value:style
                                 range:NSMakeRange(0, lblStatus.text.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:ColorAvaiable_00d800 range:NSMakeRange(range.location, strAvailable.length)];
        [attributedString addAttribute:NSFontAttributeName value:CFontAvenirLTStd65Meduim(16) range:NSMakeRange(range.location, strAvailable.length)];
    }
    
    lblStatus.attributedText = attributedString;
}

- (void)setMaybeAvaialbe:(NSString *)text
{
    NSString *strMayBeAvailable = @"May be available:";
    
    if ([text isBlankValidationPassed])
        lblStatus.text = [NSString stringWithFormat:@"%@ %@", strMayBeAvailable, text];
    else
        lblStatus.text = [NSString stringWithFormat:@"%@", strMayBeAvailable];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:lblStatus.text];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:5];
    
    NSRange range = [lblStatus.text rangeOfString:strMayBeAvailable options:NSCaseInsensitiveSearch];
    
    if (range.location != NSNotFound)
    {
        [attributedString addAttribute:NSParagraphStyleAttributeName
                                 value:style
                                 range:NSMakeRange(0, lblStatus.text.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:ColorMayBeAvaialble_ffba00 range:NSMakeRange(range.location, strMayBeAvailable.length)];
        [attributedString addAttribute:NSFontAttributeName value:CFontAvenirLTStd65Meduim(16) range:NSMakeRange(range.location, strMayBeAvailable.length)];
    }
    
    lblStatus.attributedText = attributedString;
}

- (void)setUnavaialbe:(NSString *)text
{
    NSString *strUnavailable = @"Unavailable:";
    
    if ([text isBlankValidationPassed])
        lblStatus.text = [NSString stringWithFormat:@"%@ %@", strUnavailable, text];
    else
        lblStatus.text = [NSString stringWithFormat:@"%@", strUnavailable];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:lblStatus.text];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:5];
    
    NSRange range = [lblStatus.text rangeOfString:strUnavailable options:NSCaseInsensitiveSearch];
    
    if (range.location != NSNotFound)
    {
        [attributedString addAttribute:NSParagraphStyleAttributeName
                                 value:style
                                 range:NSMakeRange(0, lblStatus.text.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:ColorUnavailable_ffba00 range:NSMakeRange(range.location, strUnavailable.length)];
        [attributedString addAttribute:NSFontAttributeName value:CFontAvenirLTStd65Meduim(16) range:NSMakeRange(range.location, strUnavailable.length)];
    }
    
    lblStatus.attributedText = attributedString;
}

- (void)changeAvailabilityAPI
{
    [[APIRequest request] changeAvailabilityStatusWithStatus:appDelegate.loginUser.availability_status andAvailabilityMessage:appDelegate.loginUser.availability_message  andLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             appDelegate.loginUser.availability_status = [[responseObject valueForKey:CJsonData] intForKey:@"availability_status"];
             appDelegate.loginUser.availability_message = [[responseObject valueForKey:CJsonData] stringValueForJSON:@"availability_message"];
             [[[Store sharedInstance] mainManagedObjectContext] save];
         }
     }];
}






# pragma mark
# pragma mark - Weekly Availability

- (void)changeWeeklyAvailabilityStatus
{
    TBLWeeklyAvailability *weeklyAvailability = [TBLWeeklyAvailability findOrCreate:@{@"weekly_id":[NSNumber numberWithInt:CWeeklyAvailabilityID]} context:[[Store sharedInstance] mainManagedObjectContext]];
    
    if ([AFNetworkReachabilityManager sharedManager].reachable)
    {
        NSMutableDictionary *dictJson = [NSMutableDictionary new];
        
        [dictJson setValue:@1 forKey:@"login_type"];
        [dictJson setValue:weeklyAvailability.weekly_availability_status forKey:@"weekly_availability_status"];
        
        [[APIRequest request] changeWeeklyAvailability:dictJson completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                 [self saveWeeklyAvailabilityToLocal:[[responseObject valueForKey:CJsonData] valueForKey:@"weekly_availability_status"]];
             }
         }];
    }
    else
    {
        [self saveWeeklyAvailabilityToLocal:(NSDictionary *)weeklyAvailability.weekly_availability_status];
    }
    
    
}

- (void)getWeeklyAvailability
{
    if ([AFNetworkReachabilityManager sharedManager].reachable)
    {
        [[APIRequest request] getWeeklyAvailability:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                 NSDictionary *dictWeeklyAvailability = [[responseObject valueForKey:CJsonData] valueForKey:@"weekly_availability_status"];
                 
                 [self saveWeeklyAvailabilityToLocal:dictWeeklyAvailability];
             }
         }];
    }
    else
    {
        TBLWeeklyAvailability *weeklyAvailability = [TBLWeeklyAvailability findOrCreate:@{@"weekly_id":[NSNumber numberWithInt:CWeeklyAvailabilityID]} context:[[Store sharedInstance] mainManagedObjectContext]];
        [self setWeeklyAvailability:(NSDictionary *)weeklyAvailability.weekly_availability_status];
    }
}

- (void)saveWeeklyAvailabilityToLocal:(NSDictionary *)dictWeeklyData
{
    TBLWeeklyAvailability *weeklyAvailability = [TBLWeeklyAvailability findOrCreate:@{@"weekly_id":[NSNumber numberWithInt:CWeeklyAvailabilityID]} context:[[Store sharedInstance] mainManagedObjectContext]];
    weeklyAvailability.weekly_availability_status = dictWeeklyData;
    weeklyAvailability.loginuser = appDelegate.loginUser;
    [[[Store sharedInstance] mainManagedObjectContext] save];
    [self setWeeklyAvailability:dictWeeklyData];
}

- (void)setWeeklyAvailability:(NSDictionary *)dictWeeklyAvailability
{
    NSArray *arrAM = [[dictWeeklyAvailability valueForKey:@"am"] componentsSeparatedByString:@","];
    
    btnAmMon.tag = [[arrAM objectAtIndex:0] intValue];
    btnAmTue.tag = [[arrAM objectAtIndex:1] intValue];
    btnAmWed.tag = [[arrAM objectAtIndex:2] intValue];
    btnAmThu.tag = [[arrAM objectAtIndex:3] intValue];
    btnAmFri.tag = [[arrAM objectAtIndex:4] intValue];
    btnAmSat.tag = [[arrAM objectAtIndex:5] intValue];
    btnAmSun.tag = [[arrAM objectAtIndex:6] intValue];
    
    NSArray *arrLunch = [[dictWeeklyAvailability valueForKey:@"lunch"] componentsSeparatedByString:@","];
    
    btnLunchMon.tag = [[arrLunch objectAtIndex:0] intValue];
    btnLunchTue.tag = [[arrLunch objectAtIndex:1] intValue];
    btnLunchWed.tag = [[arrLunch objectAtIndex:2] intValue];
    btnLunchThu.tag = [[arrLunch objectAtIndex:3] intValue];
    btnLunchFri.tag = [[arrLunch objectAtIndex:4] intValue];
    btnLunchSat.tag = [[arrLunch objectAtIndex:5] intValue];
    btnLunchSun.tag = [[arrLunch objectAtIndex:6] intValue];
    
    NSArray *arrPM = [[dictWeeklyAvailability valueForKey:@"pm"] componentsSeparatedByString:@","];
    
    btnPmMon.tag = [[arrPM objectAtIndex:0] intValue];
    btnPmTue.tag = [[arrPM objectAtIndex:1] intValue];
    btnPmWed.tag = [[arrPM objectAtIndex:2] intValue];
    btnPmThu.tag = [[arrPM objectAtIndex:3] intValue];
    btnPmFri.tag = [[arrPM objectAtIndex:4] intValue];
    btnPmSat.tag = [[arrPM objectAtIndex:5] intValue];
    btnPmSun.tag = [[arrPM objectAtIndex:6] intValue];
    
    NSArray *arrEvening = [[dictWeeklyAvailability valueForKey:@"evening"] componentsSeparatedByString:@","];
    
    btnEveningMon.tag = [[arrEvening objectAtIndex:0] intValue];
    btnEveningTue.tag = [[arrEvening objectAtIndex:1] intValue];
    btnEveningWed.tag = [[arrEvening objectAtIndex:2] intValue];
    btnEveningThu.tag = [[arrEvening objectAtIndex:3] intValue];
    btnEveningFri.tag = [[arrEvening objectAtIndex:4] intValue];
    btnEveningSat.tag = [[arrEvening objectAtIndex:5] intValue];
    btnEveningSun.tag = [[arrEvening objectAtIndex:6] intValue];
    
    NSArray *arrNight = [[dictWeeklyAvailability valueForKey:@"night"] componentsSeparatedByString:@","];
    
    btnNightMon.tag = [[arrNight objectAtIndex:0] intValue];
    btnNightTue.tag = [[arrNight objectAtIndex:1] intValue];
    btnNightWed.tag = [[arrNight objectAtIndex:2] intValue];
    btnNightThu.tag = [[arrNight objectAtIndex:3] intValue];
    btnNightFri.tag = [[arrNight objectAtIndex:4] intValue];
    btnNightSat.tag = [[arrNight objectAtIndex:5] intValue];
    btnNightSun.tag = [[arrNight objectAtIndex:6] intValue];
    
    [btnAmMon setImage:[self setWeeklyAvailabilityStatus:btnAmMon.tag] forState:UIControlStateNormal];
    [btnAmTue setImage:[self setWeeklyAvailabilityStatus:btnAmTue.tag] forState:UIControlStateNormal];
    [btnAmWed setImage:[self setWeeklyAvailabilityStatus:btnAmWed.tag] forState:UIControlStateNormal];
    [btnAmThu setImage:[self setWeeklyAvailabilityStatus:btnAmThu.tag] forState:UIControlStateNormal];
    [btnAmFri setImage:[self setWeeklyAvailabilityStatus:btnAmFri.tag] forState:UIControlStateNormal];
    [btnAmSat setImage:[self setWeeklyAvailabilityStatus:btnAmSat.tag] forState:UIControlStateNormal];
    [btnAmSun setImage:[self setWeeklyAvailabilityStatus:btnAmSun.tag] forState:UIControlStateNormal];
    
    [btnLunchMon setImage:[self setWeeklyAvailabilityStatus:btnLunchMon.tag] forState:UIControlStateNormal];
    [btnLunchTue setImage:[self setWeeklyAvailabilityStatus:btnLunchTue.tag] forState:UIControlStateNormal];
    [btnLunchWed setImage:[self setWeeklyAvailabilityStatus:btnLunchWed.tag] forState:UIControlStateNormal];
    [btnLunchThu setImage:[self setWeeklyAvailabilityStatus:btnLunchThu.tag] forState:UIControlStateNormal];
    [btnLunchFri setImage:[self setWeeklyAvailabilityStatus:btnLunchFri.tag] forState:UIControlStateNormal];
    [btnLunchSat setImage:[self setWeeklyAvailabilityStatus:btnLunchSat.tag] forState:UIControlStateNormal];
    [btnLunchSun setImage:[self setWeeklyAvailabilityStatus:btnLunchSun.tag] forState:UIControlStateNormal];
    
    [btnPmMon setImage:[self setWeeklyAvailabilityStatus:btnPmMon.tag] forState:UIControlStateNormal];
    [btnPmTue setImage:[self setWeeklyAvailabilityStatus:btnPmTue.tag] forState:UIControlStateNormal];
    [btnPmWed setImage:[self setWeeklyAvailabilityStatus:btnPmWed.tag] forState:UIControlStateNormal];
    [btnPmThu setImage:[self setWeeklyAvailabilityStatus:btnPmThu.tag] forState:UIControlStateNormal];
    [btnPmFri setImage:[self setWeeklyAvailabilityStatus:btnPmFri.tag] forState:UIControlStateNormal];
    [btnPmSat setImage:[self setWeeklyAvailabilityStatus:btnPmSat.tag] forState:UIControlStateNormal];
    [btnPmSun setImage:[self setWeeklyAvailabilityStatus:btnPmSun.tag] forState:UIControlStateNormal];
    
    [btnEveningMon setImage:[self setWeeklyAvailabilityStatus:btnEveningMon.tag] forState:UIControlStateNormal];
    [btnEveningTue setImage:[self setWeeklyAvailabilityStatus:btnEveningTue.tag] forState:UIControlStateNormal];
    [btnEveningWed setImage:[self setWeeklyAvailabilityStatus:btnEveningWed.tag] forState:UIControlStateNormal];
    [btnEveningThu setImage:[self setWeeklyAvailabilityStatus:btnEveningThu.tag] forState:UIControlStateNormal];
    [btnEveningFri setImage:[self setWeeklyAvailabilityStatus:btnEveningFri.tag] forState:UIControlStateNormal];
    [btnEveningSat setImage:[self setWeeklyAvailabilityStatus:btnEveningSat.tag] forState:UIControlStateNormal];
    [btnEveningSun setImage:[self setWeeklyAvailabilityStatus:btnEveningSun.tag] forState:UIControlStateNormal];
    
    [btnNightMon setImage:[self setWeeklyAvailabilityStatus:btnNightMon.tag] forState:UIControlStateNormal];
    [btnNightTue setImage:[self setWeeklyAvailabilityStatus:btnNightTue.tag] forState:UIControlStateNormal];
    [btnNightWed setImage:[self setWeeklyAvailabilityStatus:btnNightWed.tag] forState:UIControlStateNormal];
    [btnNightThu setImage:[self setWeeklyAvailabilityStatus:btnNightThu.tag] forState:UIControlStateNormal];
    [btnNightFri setImage:[self setWeeklyAvailabilityStatus:btnNightFri.tag] forState:UIControlStateNormal];
    [btnNightSat setImage:[self setWeeklyAvailabilityStatus:btnNightSat.tag] forState:UIControlStateNormal];
    [btnNightSun setImage:[self setWeeklyAvailabilityStatus:btnNightSun.tag] forState:UIControlStateNormal];
}

- (UIImage *)setWeeklyAvailabilityStatus:(NSInteger)status
{
    UIImage *image;
    
    switch (status)
    {
        case 0:
        {
            image = [UIImage imageNamed:@""];
        }
        break;
            
        case 1:
        {
            image = [UIImage imageNamed:@"weekly_selected"];
        }
        break;
            
        default:
            break;
    }
    
    return image;
}







# pragma mark
# pragma mark - Action Events

- (IBAction)btnEditClicked:(id)sender
{
    editStatusPopup = [MIUpdateStatusPopup customUpdateStatusPopup];
    
    
    //..... CLICK EVENTS
    
    editStatusPopup.txtAvailabilityStatus.text = appDelegate.loginUser.availability_message;
    
    switch (appDelegate.loginUser.availability_status)
    {
        case 1:
            editStatusPopup.btnAvailable.selected = YES;
            break;
            
        case 2:
            editStatusPopup.btnMaybeAvailable.selected = YES;
            break;
            
        case 3:
            editStatusPopup.btnUnAvailable.selected = YES;
            break;
            
        default:
            break;
    }
    
    
    //..... AVAILABLE
    [editStatusPopup.btnAvailable touchUpInsideClicked:^{
        
        editStatusPopup.btnAvailable.selected = editStatusPopup.btnMaybeAvailable.selected = editStatusPopup.btnUnAvailable.selected = NO;
        editStatusPopup.btnAvailable.selected = !editStatusPopup.btnAvailable.selected;
        
    }];
    
    
    //..... MAY BE AVAILABLE
    [editStatusPopup.btnMaybeAvailable touchUpInsideClicked:^{
        
        editStatusPopup.btnAvailable.selected = editStatusPopup.btnMaybeAvailable.selected = editStatusPopup.btnUnAvailable.selected = NO;
        editStatusPopup.btnMaybeAvailable.selected = !editStatusPopup.btnMaybeAvailable.selected;
        
    }];
    
    
    //..... UNAVAILABLE
    [editStatusPopup.btnUnAvailable touchUpInsideClicked:^{
        
        editStatusPopup.btnAvailable.selected = editStatusPopup.btnMaybeAvailable.selected = editStatusPopup.btnUnAvailable.selected = NO;
        editStatusPopup.btnUnAvailable.selected = !editStatusPopup.btnUnAvailable.selected;
        
    }];
    
    //..... SAVE
    [editStatusPopup.btnSave touchUpInsideClicked:^{
        
        [self dismissPopUp:editStatusPopup];
        
        isEditStatus = YES;
        
        NSString *strStatus = [editStatusPopup.txtAvailabilityStatus.text isBlankValidationPassed] ? editStatusPopup.txtAvailabilityStatus.text : CMessagePlaceHolderAvailabilty;
        
        if (editStatusPopup.btnAvailable.selected)  //..... AVAILABLE
        {
            [self btnAvailabilityStatusClicked:btnAvailable];
            [self setAvaialbe:strStatus];
        }
        else if (editStatusPopup.btnMaybeAvailable.selected)    //..... MAY BE AVAILABLE
        {
            [self btnAvailabilityStatusClicked:btnMayBeAvailable];
            [self setMaybeAvaialbe:strStatus];
        }
        else if (editStatusPopup.btnUnAvailable.selected)   //..... UNAVAILABLE
        {
            [self btnAvailabilityStatusClicked:btnUnAvailable];
            [self setUnavaialbe:strStatus];
        }

    }];
    
    [self presentPopUp:editStatusPopup from:PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

- (IBAction)btnAvailabilityStatusClicked:(UIButton *)sender
{
    if (sender.selected) return;
    
    switch (sender.tag)
    {
        case 1:     //..... AVAILABLE
        {
            [self changeAvailabilityStatusAccordingToStatus:1 andIsFromEdit:isEditStatus ? YES : NO];
            break;
        }
            
        case 2:     //..... MAY BE AVAILABLE
        {
            [self changeAvailabilityStatusAccordingToStatus:2 andIsFromEdit:isEditStatus ? YES : NO];
            break;
        }
            
        case 3:     //..... UNAVAILABLE
        {
            [self changeAvailabilityStatusAccordingToStatus:3 andIsFromEdit:isEditStatus ? YES : NO];
            break;
        }
    }
}

- (void)changeAvailabilityStatusAccordingToStatus:(int)status andIsFromEdit:(BOOL)isFromEdit
{
    [self scheduleNotificationForUpdateStatus];
    
    NSString *statusMessage = isFromEdit ? editStatusPopup.txtAvailabilityStatus.text : @"";
    
    switch (status)
    {
        case 1:
        {
            [self setAvailable:statusMessage];
            break;
        }
            
        case 2:
        {
            [self setMaybeAvailable:statusMessage];
            break;
        }
            
        case 3:
        {
            [self setUnAvaialble:statusMessage]; //..... SET BUTTON AS PER STATUS
            break;
        }
    }
    
    [self saveData:status andMessage:statusMessage];
    isFirstTime = NO;
    [self handleReachAbilityChange];
    
    [[APIRequest request] changeAvailabilityStatusWithStatus:status andAvailabilityMessage:appDelegate.loginUser.availability_message  andLoginType:appDelegate.loginUser.login_type completed:nil];
}

- (void)saveData:(int)status andMessage:(NSString *)message
{
    //..... SAVE DATA TO LOCAL
    isEditStatus = NO;
    appDelegate.loginUser.availability_status = status;
    appDelegate.loginUser.availability_message = message;
    [[[Store sharedInstance] mainManagedObjectContext] save];
}

- (IBAction)btnCreateClaimFromClicked:(id)sender
{
    MICreateClaimFormViewController *createClaimFormVC = [[MICreateClaimFormViewController alloc] initWithNibName:@"MICreateClaimFormViewController" bundle:nil];
    createClaimFormVC.claimFormType = ClaimFormCreate;
    [self.navigationController pushViewController:createClaimFormVC animated:YES];
}

- (IBAction)btnStausBoxClicked:(id)sender
{
    [btnEdit sendActionsForControlEvents: UIControlEventTouchUpInside];
}

- (IBAction)btnGeneralAvailBoxClicked:(id)sender
{
    [appDelegate resignKeyboard];
    MIEditProfileViewController *editProfileClicked = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
    editProfileClicked.isFromHomeScreen = YES;
    [self.navigationController pushViewController:editProfileClicked animated:YES];
}

- (IBAction)btnWeeklyAvailabilityClicked:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            [sender setImage:[UIImage imageNamed:@"weekly_selected"] forState:UIControlStateNormal];
            sender.tag = 1;
        }
        break;
            
        case 1:
        {
            [sender setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            sender.tag = 0;
        }
        break;

        default:
            break;
    }
}

- (IBAction)btnSaveWeeklyAvailabilityClicked:(id)sender
{
    NSMutableDictionary *dictJson = [NSMutableDictionary new];
    
    NSString *strAM = [NSString stringWithFormat:@"%ld,%ld,%ld,%ld,%ld,%ld,%ld", (long)btnAmMon.tag, (long)btnAmTue.tag, (long)btnAmWed.tag, (long)btnAmThu.tag, (long)btnAmFri.tag, (long)btnAmSat.tag, (long)btnAmSun.tag];
    
    NSString *strLunch = [NSString stringWithFormat:@"%ld,%ld,%ld,%ld,%ld,%ld,%ld", (long)btnLunchMon.tag, (long)btnLunchTue.tag, (long)btnLunchWed.tag, (long)btnLunchThu.tag, (long)btnLunchFri.tag, (long)btnLunchSat.tag, (long)btnLunchSun.tag];
    
    NSString *strPM = [NSString stringWithFormat:@"%ld,%ld,%ld,%ld,%ld,%ld,%ld", (long)btnPmMon.tag, (long)btnPmTue.tag, (long)btnPmWed.tag, (long)btnPmThu.tag, (long)btnPmFri.tag, (long)btnPmSat.tag, (long)btnPmSun.tag];
    
    NSString *strEvening = [NSString stringWithFormat:@"%ld,%ld,%ld,%ld,%ld,%ld,%ld", (long)btnEveningMon.tag, (long)btnEveningTue.tag, (long)btnEveningWed.tag, (long)btnEveningThu.tag, (long)btnEveningFri.tag, (long)btnEveningSat.tag, (long)btnEveningSun.tag];
    
    NSString *strNight = [NSString stringWithFormat:@"%ld,%ld,%ld,%ld,%ld,%ld,%ld", (long)btnNightMon.tag, (long)btnNightTue.tag, (long)btnNightWed.tag, (long)btnNightThu.tag, (long)btnNightFri.tag, (long)btnNightSat.tag, (long)btnNightSun.tag];
    
    [dictJson setValue:@1 forKey:@"login_type"];
    [dictJson setValue:@{@"am":strAM, @"lunch":strLunch, @"pm":strPM, @"evening":strEvening, @"night":strNight} forKey:@"weekly_availability_status"];
    
    [self saveWeeklyAvailabilityToLocal:[dictJson valueForKey:@"weekly_availability_status"]];
    isFirstTime = NO;
    [self handleReachAbilityChange];
    
    if ([AFNetworkReachabilityManager sharedManager].reachable)
    {
        [[APIRequest request] changeWeeklyAvailability:dictJson completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                [self saveWeeklyAvailabilityToLocal:[[responseObject valueForKey:CJsonData] valueForKey:@"weekly_availability_status"]];
                 [appDelegate toastAlertWithMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] showDone:NO];
             }
         }];
    }
    
    NSLog(@"%@", dictJson);
    NSLog(@"%@", [[dictJson valueForKey:@"weekly_availability_status"] valueForKey:@"am"]);
}






# pragma mark
# pragma mark - Schedule notification

- (void)scheduleNotificationForUpdateStatus
{
    NSTimeZone *timezone = [NSTimeZone timeZoneWithAbbreviation:@"GMT+5:30"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    [dateFormatter setTimeZone:timezone];
    
    NSDate *eveningScheduleDate = [NSDate dateFromString:[NSDate stringFromDate:[[NSDate date] dateByAddingTimeInterval:24 * 60 * 60] withFormat:@"HH:mm:ss"] withFormat:@"HH:mm:ss"];
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *statuslocalNotification = [[UILocalNotification alloc] init];
    statuslocalNotification.fireDate = eveningScheduleDate;
    statuslocalNotification.repeatInterval = NSCalendarUnitSecond;
    statuslocalNotification.soundName = UILocalNotificationDefaultSoundName;
    statuslocalNotification.alertBody = CMessageChangeStatus;
    [[UIApplication sharedApplication] scheduleLocalNotification:statuslocalNotification];
}

@end
