//
//  MILeftPanelViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MILeftPanelViewController : ParentViewController<TWTSideMenuViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet UITableView *tblVLeftPanel;
}
@end
