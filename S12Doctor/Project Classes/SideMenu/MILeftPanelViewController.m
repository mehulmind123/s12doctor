//
//  MILeftPanelViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MILeftPanelViewController.h"
#import "MIHomeViewController.h"
#import "MISettingsViewController.h"
#import "MIChangePasswordViewController.h"
#import "MIDraftViewController.h"
#import "MIClaimFormsViewController.h"
#import "MIUsefulInfoViewController.h"
#import "MIEditProfileViewController.h"
#import "MICMSViewController.h"
#import "MIUserGuideViewController.h"


#import "MIAMHPHomeViewController.h"
#import "MIAMHPUsefulInfoViewController.h"
#import "MIMyFavouritesViewController.h"
#import "MIAMHPChangePasswordViewController.h"
#import "MIClaimApprovalFormViewController.h"
#import "MIAMHPEditProfileViewController.h"
#import "MIAMHPCMSViewController.h"


#import "MICommonPopup.h"
#import "MIAMHPCommonPopup.h"



#import "MILeftPanelTableViewCell.h"

@interface MILeftPanelViewController ()
{
    NSArray *arrLeftPanelData;
    MICommonPopup *commonPopup;
    MIAMHPCommonPopup *amhpCommonPopup;
    
    NSInteger totaldraftCount;
}
@end

@implementation MILeftPanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    [appDelegate.sideMenuViewController setDelegate:self];
    
    appDelegate.window.backgroundColor = self.view.backgroundColor = appDelegate.loginUser.login_type == 1 ? ColorBlue_1371b9 : ColorGreen_47C0B6;
    
    //.....Internet Reachability Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReachAbilityChange) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    
    if (appDelegate.loginUser.login_type == 1)  //..... S12
    {
        arrLeftPanelData = [[NSArray alloc] initWithObjects:
                            @{@"image":@"home", @"title":@"Home"},
                            @{@"image":@"useful_info", @"title":@"Useful Information"},
                            @{@"image":@"edit_profile", @"title":@"Edit Profile"},
                            @{@"image":@"draft_claim_form", @"title":@"Draft Claim Forms"},
                            @{@"image":@"completed_claim_form", @"title":@"Completed Claim Forms"},
                            @{@"image":@"setting", @"title":@"Settings"},
                            @{@"image":@"change_password", @"title":@"Change Password"},
                            @{@"image":@"about_us", @"title":@"About Us"},
                            @{@"image":@"terms_condition", @"title":@"Terms & Conditions"},
                            @{@"image":@"privacy_policy", @"title":@"Privacy Policy"},
                            @{@"image":@"help_support", @"title":@"Help & Support"},
                            @{@"image":@"logout", @"title":@"Logout"},
                            nil];
    }
    else    //..... AMHP
    {
        arrLeftPanelData = [[NSArray alloc] initWithObjects:
                            @{@"image":@"home", @"title":@"Home"},
                            @{@"image":@"useful_info", @"title":@"Useful Information"},
                            @{@"image":@"my_favourite", @"title":@"My Favourites"},
                            @{@"image":@"claim_forms", @"title":@"Claim Forms"},
                            @{@"image":@"change_password", @"title":@"Change Password"},
                            @{@"image":@"edit_profile", @"title":@"Edit Profile"},
                            @{@"image":@"about_us", @"title":@"About Us"},
                            @{@"image":@"terms_condition", @"title":@"Terms & Conditions"},
                            @{@"image":@"privacy_policy", @"title":@"Privacy Policy"},
                            @{@"image":@"help_support", @"title":@"Help & Support"},
                            @{@"image":@"logout", @"title":@"Logout"},
                            nil];
    }
    
    
    //.....REGISTER CELL
    
    [tblVLeftPanel registerNib:[UINib nibWithNibName:@"MILeftPanelTableViewCell" bundle:nil] forCellReuseIdentifier:@"MILeftPanelTableViewCell"];
    [tblVLeftPanel setContentInset:UIEdgeInsetsMake(20, 0, 0, 0)];
}

- (void)handleReachAbilityChange
{
    [tblVLeftPanel reloadData];
}

- (void)openPasswordPopup:(BOOL)isDraftScreen
{
    //..... S12
    
//    MIDraftViewController *draftVC = [[MIDraftViewController alloc] initWithNibName:@"MIDraftViewController" bundle:nil];
//    [appDelegate openMenuViewcontroller:draftVC animated:YES];
//    return;
    
    commonPopup = [MICommonPopup customCommonPopup];
    [commonPopup.lblAddNewLanguage hideByHeight:YES];
    commonPopup.txtCommonField.placeholder = @"Enter your password";
    [commonPopup.btnSubmit setBackgroundColor:appDelegate.loginUser.login_type == 1 ? ColorBlue_1371b9 : ColorGreen_47C0B6];
    
    [commonPopup.btnSubmit touchUpInsideClicked:^{
        
        [appDelegate resignKeyboard];
        
        if (![commonPopup.txtCommonField.text isBlankValidationPassed])
        {
            [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageBlankPassword buttonTitle:@"OK" handler:nil inView:self];
        }
        else
        {
            [[APIRequest request] verifyPassword:commonPopup.txtCommonField.text andLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
             {
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     [self dismissPopUp:commonPopup];
                     
                     if (appDelegate.loginUser.login_type == 1)
                     {
                         if (isDraftScreen)
                         {
                             MIDraftViewController *draftVC = [[MIDraftViewController alloc] initWithNibName:@"MIDraftViewController" bundle:nil];
                             [appDelegate openMenuViewcontroller:draftVC animated:YES];
                         }
                         else
                         {
                             MIClaimFormsViewController *claimFormVC = [[MIClaimFormsViewController alloc] initWithNibName:@"MIClaimFormsViewController" bundle:nil];
                             [appDelegate openMenuViewcontroller:claimFormVC animated:YES];
                         }
                     }
                     else
                     {
                         [self redirectToCliamFormApprovalScreen];
                     }
                 }
                 else if (!error)
                 {
                     [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
                 }
             }];
        }
        
    }];
    
    [[UIApplication topMostController] presentPopUp:commonPopup from:Is_iPhone_4 || Is_iPhone_5 ? PresentTypeCustom : PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

-(void)openPasswordBox
{
    //..... AMHP
    
    amhpCommonPopup = [MIAMHPCommonPopup customCommonPopup];
    [amhpCommonPopup.lblAddNewLanguage hideByHeight:YES];
    amhpCommonPopup.txtCommonField.placeholder = @"Enter your password";
    [amhpCommonPopup.btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
    
    [amhpCommonPopup.btnSubmit touchUpInsideClicked:^{
        
        [appDelegate resignKeyboard];
        
        if (![amhpCommonPopup.txtCommonField.text isBlankValidationPassed])
        {
            [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageBlankPassword buttonTitle:@"OK" handler:nil inView:self];
        }
        else
        {
            [[APIRequest request] verifyPassword:amhpCommonPopup.txtCommonField.text andLoginType:appDelegate.loginUserAMHP.login_type completed:^(id responseObject, NSError *error)
            {
                if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                {
                    [self dismissPopUp:amhpCommonPopup];
                    [self redirectToCliamFormApprovalScreen];
                }
                else if (!error)
                    [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
            }];
        }
        
    }];
    
    [[UIApplication topMostController] presentPopUp:amhpCommonPopup from:Is_iPhone_4 || Is_iPhone_5 ? PresentTypeCustom : PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

- (void)redirectToCliamFormApprovalScreen
{
    MIClaimApprovalFormViewController *claimFormVC = [[MIClaimApprovalFormViewController alloc] initWithNibName:@"MIClaimApprovalFormViewController" bundle:nil];
    [appDelegate openMenuViewcontroller:claimFormVC  animated:YES];
}






# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrLeftPanelData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CScreenWidth * 0.1333;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MILeftPanelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MILeftPanelTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.lblDraftClaimForm hideByWidth:indexPath.row == 3 ? NO : YES];
    
    if (appDelegate.loginUser.login_type == 1)
    {
        switch (indexPath.row)
        {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            {
                cell.lblLeftPanelItem.textColor = ![AFNetworkReachabilityManager sharedManager].reachable ? [UIColor grayColor] : [UIColor whiteColor];
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 2:
            case 3:
            case 4:
            case 5:
            {
                cell.lblLeftPanelItem.textColor = ![AFNetworkReachabilityManager sharedManager].reachable ? [UIColor grayColor] : [UIColor whiteColor];
            }
                break;
                
            default:
                break;
        }
    }
    
    if (indexPath.row == 3)
    {
        if (totaldraftCount > 0)
        {
            cell.lblDraftClaimForm.text = [NSString stringWithFormat:@"%ld", (long)totaldraftCount];
            [cell.lblDraftClaimForm hideByWidth:NO];
        }
        else
            [cell.lblDraftClaimForm hideByWidth:YES];
    }
    
    NSDictionary *dictSidemenu = [arrLeftPanelData objectAtIndex:indexPath.row];
    
    cell.imgVLeftPanelItem.image = [UIImage imageNamed:[dictSidemenu stringValueForJSON:@"image"]];
    cell.lblLeftPanelItem.text = [dictSidemenu stringValueForJSON:@"title"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (appDelegate.loginUser.login_type == 1)                                  //..... S12
    {
        switch (indexPath.row)
        {
            case 0:
            {
                //..... Home
                
                MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                [appDelegate openMenuViewcontroller:homeVC animated:YES];
                
            }
                break;
                
            case 1:
            {
                //..... Userful Information
                
                MIUsefulInfoViewController *userfulInfoVC = [[MIUsefulInfoViewController alloc] initWithNibName:@"MIUsefulInfoViewController" bundle:nil];
                [appDelegate openMenuViewcontroller:userfulInfoVC  animated:YES];
                
            }
                break;
                
            case 2:
            {
                //..... Edit Profile
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:editProfileVC  animated:YES];
                }
            }
                break;
                
            case 3:
            {
                //..... Draft Claim Forms
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    [self openPasswordPopup:YES];
                }
            }
                break;
                
            case 4:
            {
                //..... Completed Claim Forms
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    [self openPasswordPopup:NO];
                }
            }
                break;
                
            case 5:
            {
                //..... Settings
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    MISettingsViewController *settingVC = [[MISettingsViewController alloc] initWithNibName:@"MISettingsViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:settingVC  animated:YES];
                }
            }
                break;
                
            case 6:
            {
                //..... Change Password
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    MIChangePasswordViewController *changePasswordVC = [[MIChangePasswordViewController alloc] initWithNibName:@"MIChangePasswordViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:changePasswordVC  animated:YES];
                }
            }
                break;
                
            case 7:
            case 8:
            case 9:
            {
                //..... About Us  &&  Terms and condition  &&  Privacy Policy
                
                MICMSViewController *cmsVC = [[MICMSViewController alloc] initWithNibName:@"MICMSViewController" bundle:nil];
                cmsVC.cmsType = indexPath.row == 7 ? AboutUs : indexPath.row == 8 ? TermsAndCondition : PrivacyPolicy;
                cmsVC.isFromSideMenu = YES;
                [appDelegate openMenuViewcontroller:cmsVC  animated:YES];
            }
                break;
                
            case 10:
            {
                //..... Help and Support
                
                dispatch_async(GCDMainThread, ^{
                    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleActionSheet title:@"" message:CChooseOption firstButton:CUserGuide firstHandler:^(UIAlertAction *action)
                     {
                         MIUserGuideViewController *userGuideVC = [[MIUserGuideViewController alloc] initWithNibName:@"MIUserGuideViewController" bundle:nil];
                         [appDelegate openMenuViewcontroller:userGuideVC  animated:YES];
                     }
                     secondButton:CEmailS12 secondHandler:^(UIAlertAction *action)
                     {
                         [self helpAndSupportEmail];
                         
                     } inView:self];
                    
                });
                
            }
                break;
                
            case 11:
            {
                //..... Logout
                
                NSString *logoutMessage = CMessageLogout;
                NSArray *unsyncedDraft = [TBLDraft fetch:[NSPredicate predicateWithFormat:@"iSynced == 0"] sortedBy:nil];
                if (unsyncedDraft.count > 0) logoutMessage = CMessageNoSyncLogout;
                
                dispatch_async(GCDMainThread, ^{
                    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:logoutMessage firstButton:@"No" firstHandler:nil secondButton:@"Yes" secondHandler:^(UIAlertAction *action)
                     {
                         //..... Logout Event
//                         [appDelegate trackTheEventWithName:CEventLogout];
                         
                         [appDelegate logoutUser];
                     } inView:self];
                });
                
            }
                break;
                
            default:
                break;
        }
    }
    else                                //..... AMHP
    {
        switch (indexPath.row)
        {
            case 0:
            {
                //..... Home
                
                MIAMHPHomeViewController *homeVC = [[MIAMHPHomeViewController alloc] initWithNibName:@"MIAMHPHomeViewController" bundle:nil];
                [appDelegate openMenuViewcontroller:homeVC animated:YES];
            }
                break;
                
            case 1:
            {
                //..... Userful Information
                
                MIAMHPUsefulInfoViewController *userfulInfoVC = [[MIAMHPUsefulInfoViewController alloc] initWithNibName:@"MIAMHPUsefulInfoViewController" bundle:nil];
                [appDelegate openMenuViewcontroller:userfulInfoVC  animated:YES];
                
            }
                break;
                
            case 2:
            {
                //..... My Favourites
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    MIMyFavouritesViewController *myFavouritesVC = [[MIMyFavouritesViewController alloc] initWithNibName:@"MIMyFavouritesViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:myFavouritesVC  animated:YES];
                }
            }
                break;
                
            case 3:
            {
                //..... Claim Form
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    [self openPasswordBox];
                }
            }
                break;
                
            case 4:
            {
                //..... Change Password
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    MIAMHPChangePasswordViewController *changePasswordVC = [[MIAMHPChangePasswordViewController alloc] initWithNibName:@"MIAMHPChangePasswordViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:changePasswordVC  animated:YES];
                }
            }
                break;
                
            case 5:
            {
                //..... Edit Profile
                
                if ([AFNetworkReachabilityManager sharedManager].reachable)
                {
                    MIAMHPEditProfileViewController *editProfileVC = [[MIAMHPEditProfileViewController alloc] initWithNibName:@"MIAMHPEditProfileViewController" bundle:nil];
                    [appDelegate openMenuViewcontroller:editProfileVC  animated:YES];
                }
            }
                break;
                
            case 6:
            case 7:
            case 8:
            {
                //..... About Us  &&  Terms and condition  &&  Privacy policy
                
                MIAMHPCMSViewController *cmsVC = [[MIAMHPCMSViewController alloc] initWithNibName:@"MIAMHPCMSViewController" bundle:nil];
                cmsVC.cmsType = indexPath.row == 6 ? AboutUs : indexPath.row == 7 ? TermsAndCondition : PrivacyPolicy;
                cmsVC.isFromSideMenu = YES;
                [appDelegate openMenuViewcontroller:cmsVC  animated:YES];
            }
                break;
                
            case 9:
            {
                //..... Help and Support
                
//                [appDelegate trackTheEventWithName:CEventHelp&SupportClicked];
                
                dispatch_async(GCDMainThread, ^{
                    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleActionSheet title:@"" message:CChooseOption firstButton:CUserGuide firstHandler:^(UIAlertAction *action)
                     {
                         MIUserGuideViewController *userGuideVC = [[MIUserGuideViewController alloc] initWithNibName:@"MIUserGuideViewController" bundle:nil];
                         [appDelegate openMenuViewcontroller:userGuideVC  animated:YES];
                         
                     } secondButton:CEmailUs secondHandler:^(UIAlertAction *action)
                     {
                         [self helpAndSupportEmail];
                         
                     } inView:self];
                    
                });
            }
                break;
                
            case 10:
            {
                //..... Logout
                
                dispatch_async(GCDMainThread, ^{
                    
                    NSString *logoutMessage = CMessageLogout;
                    NSArray *unsyncedDraft = [TBLDraft fetch:[NSPredicate predicateWithFormat:@"iSynced == 0"] sortedBy:nil];
                    if (unsyncedDraft.count > 0) logoutMessage = CMessageNoSyncLogout;
                    
                    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:logoutMessage firstButton:@"No" firstHandler:nil secondButton:@"Yes" secondHandler:^(UIAlertAction *action)
                     {
                         //..... Logout Event
//                         [appDelegate trackTheEventWithName:CEventLogout];
                         
                         [appDelegate logoutUser];
                     } inView:self];
                });
                
            }
                break;
                
            default:
                break;
        }
    }
}




# pragma mark
# pragma mark - MFMailComposeViewController Delegate

- (void)helpAndSupportEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@""];
        [controller setMessageBody:@"" isHTML:YES];
        [controller setToRecipients:[NSArray arrayWithObjects:CHelpAndSupportEmail,nil]];
        [[UIApplication topMostController] presentViewController:controller animated:YES completion:NULL];
    }
    else
        [CustomAlertView iOSAlert:@"" withMessage:CMessageMailNotSupport onView:self];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}





# pragma mark
# pragma mark - TWTSideMenuViewControllerDelegate

- (void)sideMenuViewControllerWillOpenMenu:(TWTSideMenuViewController *)sideMenuViewController
{
    totaldraftCount = appDelegate.loginUser.draft_count + [[TBLDraft fetch:[NSPredicate predicateWithFormat:@"iSynced == 0"] sortedBy:nil] count];
    [tblVLeftPanel reloadData];
}

- (void)sideMenuViewControllerWillCloseMenu:(TWTSideMenuViewController *)sideMenuViewController
{
}

@end
