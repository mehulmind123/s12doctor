//
//  MISelectLanguageViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    employer,
    localAuthority,
    languageSelection,
    specialities
    
}  selectionType;


typedef void(^SelectLanguageBlock)(NSArray *arrSelected, NSArray *arrTotalLanguage);

@interface MISelectLanguageViewController : ParentViewController
{
    IBOutlet UITableView *tblVLanguage;
    IBOutlet UILabel *lblNoData;
}


@property (nonatomic, strong) NSArray *arrSelectedValue;
@property (nonatomic, strong) NSArray *arrNewLanguage;
@property (nonatomic, strong) NSString *regionID;
@property (nonatomic, assign) selectionType selectionType;
@property (nonatomic, copy) SelectLanguageBlock selectLanguageBlock;



- (IBAction)btnDoneClicked:(id)sender;

@end
