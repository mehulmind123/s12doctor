//
//  MISelectLanguageViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MISelectLanguageViewController.h"

#import "MICommonPopup.h"

#import "MISelectLanguageTableViewCell.h"

@interface MISelectLanguageViewController ()
{
    NSMutableArray *arrSelection;
    NSMutableArray *arrSelected;
    NSMutableArray *arrAddedLanguage;
}
@end

@implementation MISelectLanguageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    arrSelected = [[NSMutableArray alloc] initWithArray:_arrSelectedValue];
    arrSelection = [NSMutableArray new];
    arrAddedLanguage = [[NSMutableArray alloc] initWithArray:_arrNewLanguage];
    
    switch (_selectionType)
    {
        
        case employer:
        {
            self.title = @"Select Employer";
            
            [self startLoadingAnimationInView:self.view];
            
            [[APIRequest request] employerList:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
             {
                 [self stopLoadingAnimationInView:self.view];
                 
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     NSArray *arrResponseData = [responseObject valueForKey:CJsonData];
                     
                     if (arrResponseData.count > 0)
                     {
                         [arrSelection addObjectsFromArray:arrResponseData];
                         [tblVLanguage reloadData];
                     }
                 }
                 
                 [self displayNoData];
                 
             }];
        }
            break;
            
        case localAuthority:
        {
            self.title = @"Select Local Authority";
            
            [self startLoadingAnimationInView:self.view];
            
            [[APIRequest request] localAuthorityListWithLoginType:appDelegate.loginUser.login_type andType:CLAAll andRegionID:_regionID completed:^(id responseObject, NSError *error)
             {
                 [self stopLoadingAnimationInView:self.view];
                 
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     NSArray *arrResponseData = [responseObject valueForKey:CJsonData];
                     
                     if (arrResponseData.count > 0)
                     {
                         [arrSelection addObjectsFromArray:arrResponseData];
                         [tblVLanguage reloadData];
                     }
                 }
                 
                 [self displayNoData];
                 
             }];
        }
            break;
            
        case languageSelection:
        {
            self.title = @"Select Languages";
            
            [self startLoadingAnimationInView:self.view];
            
            [[APIRequest request] languageList:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
             {
                 [self stopLoadingAnimationInView:self.view];
                 
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     NSArray *arrResponseData = [responseObject valueForKey:CJsonData];
                     
                     if (arrResponseData.count > 0)
                     {
                         //..... BAR BUTTON
                         
                         self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add"] style:UIBarButtonItemStylePlain target:self action:@selector(btnAddLanguageClick)];
                         
                         [arrSelection addObjectsFromArray:arrResponseData];
                         if (arrAddedLanguage.count > 0)
                            [arrSelection addObjectsFromArray:arrAddedLanguage];
                         [tblVLanguage reloadData];
                     }
                 }
             }];
        }
            break;
            
        case specialities:
        {
            self.title = @"Select Specialties";
            
            [self startLoadingAnimationInView:self.view];
            
            [[APIRequest request] specialtiesList:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
            {
                [self stopLoadingAnimationInView:self.view];
                
                if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                {
                    NSArray *arrResponseData = [responseObject valueForKey:CJsonData];
                    
                    if (arrResponseData.count > 0)
                    {
                        [arrSelection addObjectsFromArray:arrResponseData];
                        [tblVLanguage reloadData];
                    }
                }
                
                [self displayNoData];
                
            }];
        }
            break;
    }
    
    
    //..... REGISTER CELL
    
    [tblVLanguage registerNib:[UINib nibWithNibName:@"MISelectLanguageTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISelectLanguageTableViewCell"];
    [tblVLanguage setContentInset:UIEdgeInsetsMake(10, 0, 0, 0)];
    [tblVLanguage setAllowsMultipleSelection:YES];
    [tblVLanguage setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)displayNoData
{
    if (arrSelection.count > 0)
    {
        lblNoData.hidden = YES;
        tblVLanguage.hidden = NO;
    }
    else
    {
        lblNoData.hidden = NO;
        tblVLanguage.hidden = YES;
    }
}






# pragma mark
# pragma mark - Action Events

- (void)btnAddLanguageClick
{
    MICommonPopup *addLanguagePopup = [MICommonPopup customCommonPopup];
    [addLanguagePopup.btnSubmit setTitle:@"Add" forState:UIControlStateNormal];
    [addLanguagePopup.txtCommonField setSecureTextEntry:NO];
    
    [addLanguagePopup.btnSubmit touchUpInsideClicked:^{
       
        if (![addLanguagePopup.txtCommonField.text isBlankValidationPassed])
        {
            [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageBlankLanguageName buttonTitle:@"OK" handler:nil inView:self];
        }
        else
        {
            [arrSelection addObject:@{@"language_name":addLanguagePopup.txtCommonField.text}];
            [arrSelected addObject:@{@"language_name":addLanguagePopup.txtCommonField.text}];
            [arrAddedLanguage addObject:@{@"language_name":addLanguagePopup.txtCommonField.text}];
            [tblVLanguage reloadData];
            [self dismissPopUp:addLanguagePopup];
        }
    }];
    
    [self presentPopUp:addLanguagePopup from:PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

- (IBAction)btnDoneClicked:(id)sender
{
    NSArray *result;
    
    result = arrSelected;
    
    if (_selectionType == employer || _selectionType == localAuthority || _selectionType == specialities)
    {
        if (self.block)
            self.block(result, nil);
    }
    else
    {
        if (_selectLanguageBlock)
            _selectLanguageBlock (arrSelected, arrAddedLanguage);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}





# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSelection.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MISelectLanguageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MISelectLanguageTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    //..... SET DATA
    
    NSDictionary *dictData = arrSelection[indexPath.row];
    
    switch (_selectionType)
    {
        case employer:    //..... EMPLOYER
        {
            cell.lblLanguageName.text = [dictData stringValueForJSON:@"organisation_name"];
            
            if ([[arrSelected valueForKeyPath:@"organisation_id"] containsObject:[dictData numberForJson:@"organisation_id"]])
            {
                cell.selected = YES;
                [tblVLanguage selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
            break;
        }
            
        case localAuthority:    //..... LOCAL AUTHORITY
        {
            cell.lblLanguageName.text = [dictData stringValueForJSON:@"authority_name"];
            
            if ([[arrSelected valueForKeyPath:@"authority_id"] containsObject:[dictData numberForJson:@"authority_id"]])
            {
                cell.selected = YES;
                [tblVLanguage selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
            break;
        }
            
        case languageSelection:     //..... LANGUAGE SELECTION
        {
            cell.lblLanguageName.text = [dictData stringValueForJSON:@"language_name"];
            
            if ([[arrSelected valueForKeyPath:@"language_name"] containsObject:[dictData stringValueForJSON:@"language_name"]])
            {
                cell.selected = YES;
                [tblVLanguage selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
            break;
        }
            
        case specialities:      //..... SPECIALTIES
        {
            cell.lblLanguageName.text = [dictData stringValueForJSON:@"specialties_name"];
            
            if ([[arrSelected valueForKeyPath:@"specialties_id"] containsObject:[dictData numberForJson:@"specialties_id"]])
            {
                cell.selected = YES;
                [tblVLanguage selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
            break;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![arrSelected containsObject:arrSelection[indexPath.row]])
        [arrSelected addObject:arrSelection[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([arrSelected containsObject:arrSelection[indexPath.row]])
        [arrSelected removeObject:arrSelection[indexPath.row]];
}

@end
