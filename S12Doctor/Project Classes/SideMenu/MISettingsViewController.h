//
//  MISettingsViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISettingsViewController : ParentViewController
{
//    IBOutlet UISwitch *switchGPS;
    IBOutlet UIImageView *imgVGPS;
    IBOutlet UILabel *lblGpsMsg;
    IBOutlet UISwitch *switchNotification;
}


- (IBAction)notificationSwitchValueChange:(id)sender;

@end
