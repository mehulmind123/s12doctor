//
//  MISettingsViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/19/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MISettingsViewController.h"

@interface MISettingsViewController ()

@end

@implementation MISettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UserDefaultLocationChange object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark -
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Settings";
    
    CGFloat fSize = 0.6;
    switchNotification.transform = CGAffineTransformMakeScale(fSize, fSize);
    [switchNotification setOn:[[NSNumber numberWithBool:appDelegate.loginUser.notification_reminder] isEqual:@1] ? YES : NO];
    
    lblGpsMsg.text = CMessageLocationSetting;
    
    [self displayGpsStatus];
    
    //.....Change Location settings
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationUpdated) name:UserDefaultLocationChange object:nil];
}





# pragma mark -
# pragma mark - Action Events

- (void)displayGpsStatus
{
    if ((![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]  == kCLAuthorizationStatusDenied))
        [imgVGPS setImage:[UIImage imageNamed:@"gps_disabled"]];
    else
        [imgVGPS setImage:[UIImage imageNamed:@"gps_enabled"]];
}

- (void)locationUpdated
{
    [self displayGpsStatus];
}






# pragma mark -
# pragma mark - Switch Value Change

- (void)switchValueChange
{
    [[APIRequest request] settingsWithNotification:switchNotification.isOn ? 1 : 0 andLoginType:appDelegate.loginUser.login_type completion:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
         {
             appDelegate.loginUser.notification_reminder = switchNotification.isOn ? 1 : 0;
             [[[Store sharedInstance] mainManagedObjectContext] save];
         }
     }];
}

- (IBAction)notificationSwitchValueChange:(id)sender
{
    [self switchValueChange];
}

@end
