//
//  MIUsefulInfoViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "ParentViewController.h"
#import <WebKit/WebKit.h>

@interface MIUsefulInfoViewController : ParentViewController<WKUIDelegate,WKNavigationDelegate>
{
    IBOutlet UITableView *tblUsefulInfo;
    IBOutlet UILabel *lblNoData;
}
@end
