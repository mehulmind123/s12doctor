//
//  MIUsefulInfoViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 7/20/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIUsefulInfoViewController.h"
#import "MIUsefulInfoTableViewCell.h"
#import "MIUserInfoDetailsTableViewCell.h"


#define contactNumber

@interface MIUsefulInfoViewController ()
{
    NSMutableArray *arrUseFulInfo;
    UIRefreshControl *refreshControl;
    int iOffset;
}
@end

@implementation MIUsefulInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.title = @"Useful Information";
    
    if (![AFNetworkReachabilityManager sharedManager].reachable)
        [appDelegate toastAlertWithMessage:CMessageOffline showDone:NO];
    
    
    //..... REGISTER CELL
    
    [tblUsefulInfo registerNib:[UINib nibWithNibName:@"MIUsefulInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIUsefulInfoTableViewCell"];
    
    [tblUsefulInfo registerNib:[UINib nibWithNibName:@"MIUserInfoDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIUserInfoDetailsTableViewCell"];
    
    [tblUsefulInfo setContentInset:UIEdgeInsetsMake(14, 0, 8, 0)];
    [tblUsefulInfo setEstimatedRowHeight:50];
    
    
    //..... UIREFRESH CONTROL
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = ColorBlue_1371b9;
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblUsefulInfo addSubview:refreshControl];
    
    
    //..... LOAD DATA
    
    arrUseFulInfo = [NSMutableArray new];
    [self getUsefulInfoDataFromLocal];
    [self pullToRefresh];
}

- (NSMutableAttributedString*)setAttributedText:(NSString *)boldString normalString:(NSString*)normalString
{
    NSString *string = [NSString stringWithFormat:@"%@ %@",boldString,normalString];
    
    int fontSize = Is_iPhone_5 ? 11 : 12;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedText addAttributes: @{NSFontAttributeName:CFontAvenirLTStd55Roman (fontSize), NSForegroundColorAttributeName:ColorBlack_4f5d60} range: NSMakeRange(0, string.length)];
    [attributedText addAttributes: @{NSFontAttributeName:CFontAvenirLTStd85Heavy(fontSize), NSForegroundColorAttributeName:ColorBlack_4f5d60} range: [string rangeOfString:boldString]];
    
    return attributedText;
}




# pragma mark
# pragma mark - API Function

- (void)getUsefulInfoDataFromLocal
{
    NSArray *arrData = [TBLS12UsefulInfo fetchAllObjects];
    
    [arrUseFulInfo removeAllObjects];
    [arrUseFulInfo addObjectsFromArray:arrData];
    [tblUsefulInfo reloadData];
}

- (void)pullToRefresh
{
    iOffset = 0;
    [self loadUsefulInformationListFromServer:(arrUseFulInfo.count == 0)];
}

- (void)loadUsefulInformationListFromServer:(BOOL)showLoadingAnimation
{
//    if (iOffset == 0 && showLoadingAnimation)
//        [self startLoadingAnimationInView:self.view];
    
    [[APIRequest request] usefulInformationWithOffset:iOffset andLoginType:appDelegate.loginUser.login_type completed:^(id responseObject, NSError *error)
    {
        [refreshControl endRefreshing];
//        [self stopLoadingAnimationInView:self.view];
        
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            NSArray *arrResponseData = [responseObject valueForKey:CJsonData];
            
            if (arrResponseData.count > 0)
            {
                if (iOffset == 0)
                    [TBLS12UsefulInfo deleteAllObjects];
                iOffset = [[responseObject valueForKey:CJsonMeta] intForKey:CJsonNewOffset];
                [[APIRequest request] saveS12UsefulInfo:[responseObject valueForKey:CJsonData]];
                [self getUsefulInfoDataFromLocal];
            }
        }
        
        if (arrUseFulInfo.count == 0)
        {
            [lblNoData setHidden:NO];
            [tblUsefulInfo setHidden:YES];
        }
        
    }];
}






# pragma mark
# pragma mark - UITableview Delegate And Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrUseFulInfo.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *dict = arrUseFulInfo[section];
    
    NSInteger countCCGs = [[dict valueForKey:@"ccgs"] count];
    NSInteger countAMHPs = [[dict valueForKey:@"amhps"] count];
    
    return countCCGs + countAMHPs + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TBLS12UsefulInfo *objUsefulInfo = arrUseFulInfo[indexPath.section];
    
    if(!indexPath.row)
    {
        //..... First cell of all section
        
        static NSString *identifier = @"MIUsefulInfoTableViewCell";
        MIUsefulInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        [cell.lblTitle setText:objUsefulInfo.local_authority];
        
        if ([objUsefulInfo.contact_number isBlankValidationPassed])
            cell.lblContactNo.attributedText = [self setAttributedText:@"Contact Number:" normalString:objUsefulInfo.contact_number];
        else
            cell.lblContactNo.attributedText = [self setAttributedText:@"Contact Number:" normalString:CNotAvailable];
        
        if ([objUsefulInfo.address isBlankValidationPassed])
            cell.lblAddress.attributedText = [self setAttributedText:@"Address:" normalString:objUsefulInfo.address];
        else
            cell.lblAddress.attributedText = [self setAttributedText:@"Address:" normalString:CNotAvailable];
        
        
        //..... Clicked events
        
        [cell.btnContact touchUpInsideClicked:^{
            
            if ([objUsefulInfo.contact_number isBlankValidationPassed])
                [appDelegate callMobileNo:objUsefulInfo.contact_number];
            
        }];
        
        
        //..... Load More
        
        if (indexPath.row == (arrUseFulInfo.count - 1))
            [self loadUsefulInformationListFromServer:NO];
        
        return cell;
    }
    else
    {
        NSArray *arrCCGs = [objUsefulInfo valueForKey:@"ccgs"];
        NSArray *arrAMHPs = [objUsefulInfo valueForKey:@"amhps"];
        
        static NSString *identifier = @"MIUserInfoDetailsTableViewCell";
        MIUserInfoDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        cell.vwContent.layer.cornerRadius = 0;
        
        if (indexPath.row <= arrAMHPs.count)
        {
            // Configure for AMHPs
            
            NSInteger index = indexPath.row - 1;
            NSDictionary *dictAMHPs = arrAMHPs[index];
            
            [cell.vwTopSeprator setHidden:index];
            [cell.vwBottomSeprator setHidden:(index == arrAMHPs.count-1)];
            
            cell.lblTitle.text = !index ? @"AMHPs:" : @"";
            [cell.lblTitle hideByHeight:index];
            
            [cell.lblAddress setText:@""];
            
            if ([[dictAMHPs stringValueForJSON:@"amhp_name"] isBlankValidationPassed])
                cell.lblName.attributedText = [self setAttributedText:@"Name:" normalString:[dictAMHPs stringValueForJSON:@"amhp_name"]];
            else
                cell.lblName.attributedText = [self setAttributedText:@"Name:" normalString:CNotAvailable];
            
            
            if ([[dictAMHPs stringValueForJSON:@"amhp_contact_number"] isBlankValidationPassed])
                cell.lblContactNo.attributedText = [self setAttributedText:@"Contact Number:" normalString:[dictAMHPs stringValueForJSON:@"amhp_contact_number"]];
            else
                cell.lblContactNo.attributedText = [self setAttributedText:@"Contact Number:" normalString:CNotAvailable];
            
            
            //..... Clicked events
            
            [cell.btnContact touchUpInsideClicked:^{
                
                if ([[dictAMHPs stringValueForJSON:@"amhp_contact_number"] isBlankValidationPassed])
                    [appDelegate callMobileNo:[dictAMHPs stringValueForJSON:@"amhp_contact_number"]];
                
            }];
            
        }
        else
        {
            // Configure for CCGs
            
            NSInteger index = indexPath.row - (1 + arrAMHPs.count);
            NSDictionary *dictCCGS = arrCCGs[index];
            
            [cell.vwTopSeprator setHidden:index];
            [cell.vwBottomSeprator setHidden:(index == arrCCGs.count - 1)];
            
            if (index == arrCCGs.count - 1)
                cell.vwContent.layer.cornerRadius = 5;
            
            cell.lblTitle.text = !index ? @"CCGs:" : @"";
            [cell.lblTitle hideByHeight:index];
            
            if ([[dictCCGS stringValueForJSON:@"ccg_name"] isBlankValidationPassed])
                cell.lblName.attributedText = [self setAttributedText:@"Name:" normalString:[dictCCGS stringValueForJSON:@"ccg_name"]];
            else
                cell.lblName.attributedText = [self setAttributedText:@"Name:" normalString:CNotAvailable];
            
            if ([[dictCCGS stringValueForJSON:@"ccg_contact_number"] isBlankValidationPassed])
                cell.lblContactNo.attributedText = [self setAttributedText:@"Contact Number:" normalString:[dictCCGS stringValueForJSON:@"ccg_contact_number"]];
            else
                cell.lblContactNo.attributedText = [self setAttributedText:@"Contact Number:" normalString:CNotAvailable];
            
            if ([[dictCCGS stringValueForJSON:@"ccg_address"] isBlankValidationPassed])
                cell.lblAddress.attributedText = [self setAttributedText:@"Address:" normalString:[dictCCGS stringValueForJSON:@"ccg_address"]];
            else
                cell.lblAddress.attributedText = [self setAttributedText:@"Address:" normalString:CNotAvailable];
            
            
            //..... Clicked events
            
            [cell.btnContact touchUpInsideClicked:^{
                
                if ([[dictCCGS stringValueForJSON:@"ccg_contact_number"] isBlankValidationPassed])
                    [appDelegate callMobileNo:[dictCCGS stringValueForJSON:@"ccg_contact_number"]];
                
            }];
            
        }
        
        //..... Load More
        if (indexPath.row == (arrUseFulInfo.count - 1))
            [self loadUsefulInformationListFromServer:NO];
        
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CScreenWidth, 10)];
    footer.tag = -100;
    footer.backgroundColor = [UIColor clearColor];
    
    
    UIView *shadow = [[UIView alloc] initWithFrame:CGRectMake((CScreenWidth - (CScreenWidth * 0.888)) / 2, -3, CScreenWidth * 0.888, 3)];
    shadow.backgroundColor = ColorBlue_1371b9;
    shadow.layer.shadowColor = ColorBlack.CGColor;
    shadow.layer.shadowOffset = CGSizeZero;
    shadow.layer.shadowRadius = 3;
    shadow.layer.shadowOpacity = 0.3;
    shadow.layer.masksToBounds = NO;
    
    
    [footer addSubview:shadow];
    [footer setClipsToBounds:YES];

    return footer;
}

@end
