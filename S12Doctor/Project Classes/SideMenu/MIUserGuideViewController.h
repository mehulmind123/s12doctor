//
//  MIUserGuideViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 31/10/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIUserGuideViewController : ParentViewController
{
    IBOutlet UIWebView *webviewUserGuide;
}
@end
