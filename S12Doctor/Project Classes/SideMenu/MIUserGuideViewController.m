//
//  MIUserGuideViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 31/10/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIUserGuideViewController.h"

@interface MIUserGuideViewController ()

@end

@implementation MIUserGuideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"User Guide";
    
    NSString *path = [[NSBundle mainBundle] pathForResource:appDelegate.loginUser.login_type == 1 ? @"S12 App Doctor User Guide" : @"S12 App AMHP User Guide" ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [webviewUserGuide loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
