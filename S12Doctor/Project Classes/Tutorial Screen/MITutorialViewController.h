//
//  MITutorialViewController.h
//  S12Doctor
//
//  Created by S12 Solutions on 12/1/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITutorialViewController : UIViewController
{
    IBOutlet UIButton *btnDone;
    IBOutlet UICollectionView *collVTutorial;
    IBOutlet UIPageControl *pageControl;
}
@end
