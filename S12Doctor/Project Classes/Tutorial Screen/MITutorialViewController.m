//
//  MITutorialViewController.m
//  S12Doctor
//
//  Created by S12 Solutions on 12/1/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MITutorialViewController.h"
#import "MITutorialCollectionViewCell.h"
#import "MILoginViewController.h"
#import "MIAMHPLoginViewController.h"

@interface MITutorialViewController ()
{
    NSArray *arrTutorialData;
}
@end

@implementation MITutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if (Is_iPhone_X)
        [btnDone setConstraintConstant:25 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    
    btnDone.hidden = YES;
    
    if ([appDelegate.isFromS12 isEqual: @1])        //..... S12
    {
        arrTutorialData = [[NSArray alloc] initWithObjects:
                           @{@"title":CMessageTutorial, @"description":CMessageS12Tutorial1Desc, @"image":@"", @"imageDesc":@""},
                           @{@"title":@"", @"description":@"", @"image":@"S12_home_tutorial.png", @"imageDesc":CMessageS12Tutorial2},
                           @{@"title":@"", @"description":@"", @"image":@"S12_edit_profile_tutorial.png", @"imageDesc":CMessageS12Tutorial3},
                           @{@"title":@"", @"description":@"", @"image":@"S12_settings_tutorial.png", @"imageDesc":CMessageS12Tutorial4},
                           @{@"title":@"", @"description":@"", @"image":@"AMHP_Location_tutorial.png", @"imageDesc":CMessageS12Tutorial5},nil];
        
        self.view.backgroundColor = ColorBlue_1371b9;
    }
    else        //..... AMHP
    {
        arrTutorialData = [[NSArray alloc] initWithObjects:
                           @{@"title":CMessageTutorial, @"description":CMessageTutorial1Desc, @"image":@"", @"imageDesc":@""},
                           @{@"title":@"", @"description":@"", @"image":@"AMHP_addressbook_menu_tutorial.png", @"imageDesc":CMessageAMHPTutorial2},
                           @{@"title":@"", @"description":@"", @"image":@"AMHP_addressbook_tutorial.png", @"imageDesc":CMessageAMHPTutorial3},
                           @{@"title":@"", @"description":@"", @"image":@"AMHP_Location_tutorial.png", @"imageDesc":CMessageAMHPTutorial4},
                           @{@"title":CMessageAMHPTutorial5, @"description":CMessageTutorial5Desc, @"image":@"", @"imageDesc":@""}, nil];
        
        self.view.backgroundColor = ColorGreen_47C0B6;
    }
    
    pageControl.numberOfPages = arrTutorialData.count;
    
    [collVTutorial registerNib:[UINib nibWithNibName:@"MITutorialCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MITutorialCollectionViewCell"];
}






# pragma mark
# pragma mark - Action Events

- (IBAction)btnDoneClicked:(id)sender
{
    if ([appDelegate.isFromS12 isEqual:@1])
    {
        [CUserDefaults setValue:[NSNumber numberWithBool:YES] forKey:UserDefaultTutorialS12];
        [CUserDefaults synchronize];
        
        MILoginViewController *s12LoginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
        [self.navigationController pushViewController:s12LoginVC animated:YES];
    }
    else
    {
        [CUserDefaults setValue:[NSNumber numberWithBool:YES] forKey:UserDefaultTutorialAMHP];
        [CUserDefaults synchronize];
        
        MIAMHPLoginViewController *amhpLoginVC = [[MIAMHPLoginViewController alloc] initWithNibName:@"MIAMHPLoginViewController" bundle:nil];
        [self.navigationController pushViewController:amhpLoginVC animated:YES];
    }
}

- (IBAction)pageChange:(UIPageControl *)sender
{
    [collVTutorial scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:sender.currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
}






# pragma mark
# pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrTutorialData.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth, CScreenHeight);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MITutorialCollectionViewCell";
    MITutorialCollectionViewCell *cell = [collVTutorial dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    //..... Set Data .....
    
    NSDictionary *dictTutorial = [arrTutorialData objectAtIndex:indexPath.row];
    
    cell.viewTutorialImg.hidden = NO;
    cell.viewTutorialText.hidden = YES;
    
    if ([appDelegate.isFromS12 isEqual:@1])
    {
        if (indexPath.item == 0)
        {
            cell.viewTutorialText.hidden = NO;
            cell.viewTutorialImg.hidden = YES;
        }
    }
    else
    {
        if (indexPath.item == 0 || indexPath.item == 4)
        {
            cell.viewTutorialText.hidden = NO;
            cell.viewTutorialImg.hidden = YES;
            
            if (indexPath.row == 4)
                cell.lblDescription.textAlignment = NSTextAlignmentLeft;
        }
    }
    
    if (indexPath.item == 0)
    {
        cell.lblDescription.text = [dictTutorial valueForKey:@"description"];
        
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:cell.lblDescription.text];
        NSRange range = [cell.lblDescription.text rangeOfString:@"support@s12solutions.com"];
        [string addAttribute:NSFontAttributeName value:CFontAvenirLTStd95Balck(15) range:range];
        cell.lblDescription.attributedText = string;
    }
    else
    {
        cell.lblDescription.text = [dictTutorial valueForKey:@"description"];
    }
    
    cell.imgVTutorial.image = [UIImage imageNamed:[dictTutorial valueForKey:@"image"]];
    cell.lblTutorial.text = [dictTutorial valueForKey:@"imageDesc"];
    cell.lblTitle.text = [dictTutorial valueForKey:@"title"];
    
    return cell;
}






# pragma mark
# pragma mark - UIScrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    int page = round(collVTutorial.contentOffset.x / collVTutorial.bounds.size.width);
    [pageControl setCurrentPage:page];
    if (page == 4)
        btnDone.hidden = NO;
    else
        btnDone.hidden = YES;
}

@end
