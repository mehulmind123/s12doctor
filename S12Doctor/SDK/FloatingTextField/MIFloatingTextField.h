//
//  MIFloatingTextField.h
//  Zubba
//
//  Created by S12 Solutions on 02/05/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface MIFloatingTextField : UITextField
/**
 * Change the Placeholder text color. Default is Light Gray Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *placeHolderColor;


/**
 * Change the Placeholder text color when selected. Default is Black Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *selectedPlaceHolderColor;


/**
 * Change the Bottom line color. Default is Light Gray Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *lineColor;


/**
 * Change the bottom line color when selected. Default is Black Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *selectedLineColor;


/**
 * Change the error line color. Default is Red Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *errorLineColor;


/**
 * UITextField need to check content validation while editing.
 */
@property (nonatomic, assign) IBInspectable BOOL validateContent;


/**
 * UITextField validation incorrect image.
 */
@property (nonatomic, strong) IBInspectable UIImage *imageIncorrectValidation;


/**
 * UITextField validation correct image.
 */
@property (nonatomic, strong) IBInspectable UIImage *imageCorrectValidation;



- (instancetype)init;
- (instancetype)initWithFrame:(CGRect)frame;
- (void)setTextFieldValidate:(BOOL)isValidate;

@end
