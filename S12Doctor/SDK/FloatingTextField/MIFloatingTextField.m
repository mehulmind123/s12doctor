//
//  MIFloatingTextField.m
//  Zubba
//
//  Created by S12 Solutions on 02/05/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIFloatingTextField.h"
#import "Constants.h"

#define AnimationDuration   0.2
#define RightViewSize       CGSizeMake(15, 11)

@interface MIFloatingTextField ()
@property (nonatomic, assign) CGRect initialFrame;
@property (nonatomic, assign) BOOL isValidationCorrect;

@property (nonatomic, strong) UIView *bottomLineView;
@property (nonatomic, strong) UILabel *labelPlaceholder;
@property (nonatomic, strong) UIButton *buttonValidation;

@property (nonatomic, strong) NSLayoutConstraint *cnLeftPlaceholder;
@property (nonatomic, strong) NSLayoutConstraint *cnRightPlaceholder;
@property (nonatomic, strong) NSLayoutConstraint *cnTopPlaceholder;
@property (nonatomic, strong) NSLayoutConstraint *cnBottomPlaceholder;
@end

@implementation MIFloatingTextField

- (instancetype)init
{
    self = [super init];
    if (self) [self initialization];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initialization];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initialization];
    return self;
}

- (void)didMoveToSuperview
{
    //....Add bottom line view.
    [self addBottomLineView];
    
    //....Add placeholder.
    [self addPlaceholderLabel];
    
    //....Add validation view.
    [self addValidationView];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initialization
{
    //....
    self.clipsToBounds = true;
    self.tintColor = _selectedPlaceHolderColor;
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    
    if (Is_iPhone_4 || Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 2)];
    else if(Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 2)];
    
    
    //...Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidBeginEditing:) name:UITextFieldTextDidBeginEditingNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidEndEditing:) name:UITextFieldTextDidEndEditingNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:) name:UITextFieldTextDidChangeNotification object:self];
    
    
    
    //1. Placeholder Color.
    if (_placeHolderColor == nil)
        _placeHolderColor = ColorBlack_4f5d60;
    
    //2. Placeholder Color When Selected.
    if (_selectedPlaceHolderColor == nil)
    {
        _selectedPlaceHolderColor = ColorDarkGray_ededf1;
        self.tintColor = _selectedPlaceHolderColor;
    }
    
    //3. Bottom line Color.
    if (_lineColor == nil)
        _lineColor = [UIColor lightGrayColor];
    
    //4. Bottom line Color When Selected.
    if (_selectedLineColor == nil)
        _selectedLineColor = [UIColor clearColor];
    
    //5. Bottom line error Color.
    if (_errorLineColor == nil)
        _errorLineColor = [UIColor clearColor];
    
    [self setValue:self.placeHolderColor forKeyPath:@"_placeholderLabel.textColor"];
}


#pragma mark -
#pragma mark - Override

- (void)drawRect:(CGRect)rect
{
    CGRect frame = CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame), CGRectGetWidth(rect), CGRectGetHeight(rect));
    if (CGRectEqualToRect(_initialFrame, CGRectZero)) _initialFrame = frame;
    [self setFrame:frame];
    [self floatPlaceHolderAnimated:NO];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGFloat leftPadding = self.leftView?self.leftView.frame.size.width:0;
    CGFloat rightPadding = self.rightView?self.rightView.frame.size.width:0;
    CGFloat height = bounds.size.height * 50 / 100;
    
    return CGRectMake(leftPadding, bounds.size.height - height, bounds.size.width - leftPadding - rightPadding, height);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGFloat leftPadding = self.leftView?self.leftView.frame.size.width:0;
    CGFloat rightPadding = self.rightView?self.rightView.frame.size.width:0;
    CGFloat height = bounds.size.height * 50 / 100;
    
    return CGRectMake(leftPadding, bounds.size.height - height, bounds.size.width - leftPadding - rightPadding, height);
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    CGFloat leftPadding = self.leftView?self.leftView.frame.size.width:0;
    CGFloat rightPadding = self.rightView?self.rightView.frame.size.width:0;
    CGFloat height = bounds.size.height * 50 / 100;
    
    return CGRectMake(leftPadding, bounds.size.height - height, bounds.size.width - leftPadding - rightPadding, height);
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self floatPlaceHolderAnimated:NO];
}

- (void)setPlaceholder:(NSString *)placeholder
{
    [super setPlaceholder:placeholder];
    [self addPlaceholderLabel];
    [self floatPlaceHolderAnimated:NO];
}

- (void)setValidateContent:(BOOL)validateContent
{
    _validateContent = validateContent;
    [self addValidationView];
}

- (void)setImageCorrectValidation:(UIImage *)imageCorrectValidation
{
    _imageCorrectValidation = imageCorrectValidation;
    [self addValidationView];
}

- (void)setImageIncorrectValidation:(UIImage *)imageIncorrectValidation
{
    _imageIncorrectValidation = imageIncorrectValidation;
    [self addValidationView];
}




#pragma mark -
#pragma mark - Private Methods

- (void)addBottomLineView
{
    if (_bottomLineView) [_bottomLineView removeFromSuperview];
    
    _bottomLineView = [[UIView alloc] initWithFrame:CGRectZero];
    [_bottomLineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_bottomLineView setBackgroundColor:_lineColor];
    [self addSubview:_bottomLineView];
    
    [_bottomLineView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:1]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
}

- (void)addPlaceholderLabel
{
    if (self.placeholder && self.placeholder.length > 0)
    {
        if (_labelPlaceholder) [_labelPlaceholder removeFromSuperview];
        
        _labelPlaceholder = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelPlaceholder setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_labelPlaceholder setTextAlignment:self.textAlignment];
        [_labelPlaceholder setTextColor:_placeHolderColor];
        [_labelPlaceholder setFont:self.font];
        [_labelPlaceholder setText:self.placeholder];
        [_labelPlaceholder setHidden:YES];
        [self addSubview:_labelPlaceholder];
        
        _cnLeftPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
        
        _cnRightPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
        
        _cnTopPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        
        _cnBottomPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        
        [self addConstraints:@[_cnLeftPlaceholder, _cnRightPlaceholder, _cnTopPlaceholder, _cnBottomPlaceholder]];
    }
}

- (void)addValidationView
{
    if (_validateContent)
    {
        if (_buttonValidation) [_buttonValidation removeFromSuperview];
        
        _buttonValidation = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, RightViewSize.width, RightViewSize.height)];
        [_buttonValidation setImage:_imageIncorrectValidation forState:UIControlStateNormal];
        [_buttonValidation setImage:_imageCorrectValidation forState:UIControlStateSelected];
        [_buttonValidation setUserInteractionEnabled:NO];
        [_buttonValidation setHidden:(self.text.length == 0)];
        [_buttonValidation setSelected:_isValidationCorrect];
        
        [self setRightView:_buttonValidation];
        [self setRightViewMode:UITextFieldViewModeAlways];
    }
}

- (void)floatPlaceHolderAnimated:(BOOL)animated
{
    BOOL isSelected = (self.text.length == 0)?self.isFirstResponder:YES;
    
    CGFloat constBottom = _cnBottomPlaceholder.constant;
    NSString *_placeholder;
    UIFont *_font;
    
    if (isSelected)
    {
        if (_validateContent && self.text.length > 0 && !_isValidationCorrect)
            _bottomLineView.backgroundColor = _errorLineColor;
        else
            _bottomLineView.backgroundColor = _selectedLineColor;
        //    _bottomLineView.backgroundColor = self.isFirstResponder?_selectedLineColor:_lineColor;
        
        _labelPlaceholder.textColor = _selectedPlaceHolderColor;
        _font = [UIFont fontWithName:self.font.fontName size:roundf(self.font.pointSize * 85 / 100)];
        _labelPlaceholder.font = _font;
        _placeholder = @"";
        
        constBottom = - (self.bounds.size.height * 50 / 100);
    }
    else
    {
        if (_validateContent && self.text.length > 0 && !_isValidationCorrect)
            _bottomLineView.backgroundColor = _errorLineColor;
        else
        {
            _bottomLineView.backgroundColor = _lineColor;
            _buttonValidation.hidden = YES;
        }
        
        _labelPlaceholder.textColor = _placeHolderColor;
        _font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize];
        _placeholder = _labelPlaceholder.text;
        
        constBottom = 0;
        
        [self setValue:self.placeHolderColor forKeyPath:@"_placeholderLabel.textColor"];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:animated?AnimationDuration:0 animations:^{
            [_cnBottomPlaceholder setConstant:constBottom];
            [self layoutIfNeeded];
            
            if (isSelected) {
                [_labelPlaceholder setHidden:NO];
                [super setPlaceholder:_placeholder];
            }
        } completion:^(BOOL finished) {
            [_labelPlaceholder setFont:_font];
            
            if (!isSelected) {
                [_labelPlaceholder setHidden:YES];
                [super setPlaceholder:_placeholder];
            }
        }];
    });
}





#pragma mark -
#pragma mark - UITextField Notification.

- (void)textFieldDidBeginEditing:(NSNotification *)notification
{
    [self floatPlaceHolderAnimated:YES];
}

- (void)textFieldDidEndEditing:(NSNotification *)notification
{
    [self floatPlaceHolderAnimated:YES];
}

- (void)textFieldTextDidChange:(NSNotification *)notification
{
    //...Check validation.
    if (_validateContent)
    {
        if (self.text.length == 0) {
            _buttonValidation.hidden = YES;
            _bottomLineView.backgroundColor = self.selectedLineColor;
        } else {
            _buttonValidation.hidden = NO;
            _bottomLineView.backgroundColor = (_buttonValidation.isSelected)?self.selectedLineColor:self.errorLineColor;
        }
    }
}





#pragma mark -
#pragma mark - Validation

- (void)setTextFieldValidate:(BOOL)isValidate
{
    if (_validateContent)
    {
        [self setIsValidationCorrect:isValidate];
        [_buttonValidation setSelected:isValidate];
        [_buttonValidation setHidden:(self.text.length == 0)];
        
        if (isValidate)
            _bottomLineView.backgroundColor = self.selectedLineColor;
        else
            _bottomLineView.backgroundColor = self.errorLineColor;
    }
}
@end
