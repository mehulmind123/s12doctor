//
//  MIFloatingTextView.h
//  Zubba
//
//  Created by S12 Solutions on 16/05/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

/******* NOTE ******
 *
 * Take UITextView height 50% of actual height you want.
 *
 */

IB_DESIGNABLE
@interface MIFloatingTextView : UITextView
/**
 * Change the Placeholder text color. Default is Light Gray Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *placeHolderColor;


/**
 * Change the Placeholder text color when selected. Default is Black Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *selectedPlaceHolderColor;


/**
 * Change the Bottom line color. Default is Light Gray Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *lineColor;


/**
 * Change the bottom line color when selected. Default is Black Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *selectedLineColor;


/**
 * Change the error line color. Default is Red Color.
 */
@property (nonatomic, strong) IBInspectable UIColor *errorLineColor;


/**
 * Set placeholder text.
 */
@property (nonatomic, strong) IBInspectable NSString *placeholder;


/**
 * UITextView need to check content validation while editing.
 */
@property (nonatomic, assign) IBInspectable BOOL validateContent;


/**
 * UITextView validation incorrect image.
 */
@property (nonatomic, strong) IBInspectable UIImage *imageIncorrectValidation;


/**
 * UITextView validation correct image.
 */
@property (nonatomic, strong) IBInspectable UIImage *imageCorrectValidation;



- (instancetype)init;
- (instancetype)initWithFrame:(CGRect)frame;
- (void)setTextViewValidate:(BOOL)isValidate;

@end
