//
//  MIFloatingTextView.m
//  Zubba
//
//  Created by S12 Solutions on 16/05/17.
//  Copyright S12 Solutions Limited. All rights reserved.
//

#import "MIFloatingTextView.h"
#import "Constants.h"

#define AnimationDuration   0.2
#define RightViewSize       CGSizeMake(15, 20)

@interface MIFloatingTextView ()
@property (nonatomic, assign) CGRect initialFrame;
@property (nonatomic, assign) BOOL isValidationCorrect;


@property (nonatomic, strong) UIView *bottomLineView;
@property (nonatomic, strong) UILabel *labelPlaceholder;
@property (nonatomic, strong) UIButton *buttonValidation;

@property (nonatomic, strong) NSLayoutConstraint *cnLeftPlaceholder;
@property (nonatomic, strong) NSLayoutConstraint *cnRightPlaceholder;
@property (nonatomic, strong) NSLayoutConstraint *cnTopPlaceholder;
@property (nonatomic, strong) NSLayoutConstraint *cnBottomPlaceholder;

@property (nonatomic, strong) NSLayoutConstraint *cnHeightSelf;
@end

@implementation MIFloatingTextView

- (instancetype)init
{
    /******* NOTE ******
     *
     * Take UITextView height 50% of actual height you want.
     *
     */
    
    self = [super init];
    if (self) [self initialization];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    /******* NOTE ******
     *
     * Take UITextView height 50% of actual height you want.
     *
     */
    
    self = [super initWithFrame:frame];
    if (self) [self initialization];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    /******* NOTE ******
     *
     * Take UITextView height 50% of actual height you want.
     *
     */
    
    self = [super initWithCoder:aDecoder];
    if (self) [self initialization];
    return self;
}

- (void)didMoveToSuperview
{
    if (CGRectEqualToRect(_initialFrame, CGRectZero)) _initialFrame = self.frame;
    
    //....Add bottom line view.
    [self addBottomLineView];
    
    //....Add placeholder.
    [self addPlaceholderLabel];
    
    //....Add validation view.
    [self addValidationView];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}





#pragma mark -
#pragma mark - Override

- (void)drawRect:(CGRect)rect
{
    CGRect frame = CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame), CGRectGetWidth(rect), CGRectGetHeight(rect));
    [self setFrame:frame];
    [self floatPlaceHolderAnimated:NO];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self floatPlaceHolderAnimated:NO];
}

- (void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = placeholder;
    [self addPlaceholderLabel];
    [self floatPlaceHolderAnimated:NO];
}

- (void)setValidateContent:(BOOL)validateContent
{
    _validateContent = validateContent;
    if ([self respondsToSelector:@selector(textContainerInset)]) {
        self.textContainerInset = UIEdgeInsetsMake(0.f, -self.textContainer.lineFragmentPadding, 0.f, _validateContent?RightViewSize.width - (CScreenWidth*8/375):0);
        //...Here 8 is right padding of txtView according to device width.
    }
    [self addValidationView];
}

- (void)setImageCorrectValidation:(UIImage *)imageCorrectValidation
{
    _imageCorrectValidation = imageCorrectValidation;
    [self addValidationView];
}

- (void)setImageIncorrectValidation:(UIImage *)imageIncorrectValidation
{
    _imageIncorrectValidation = imageIncorrectValidation;
    [self addValidationView];
}


#pragma mark -
#pragma mark - Private Methods

- (void)initialization
{
    self.scrollEnabled = NO;
    self.clipsToBounds = true;
    self.tintColor = _selectedPlaceHolderColor;
    self.backgroundColor = [UIColor clearColor];
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    
    
    //...
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?(self.font.pointSize - 2):(Is_iPhone_6_PLUS)?(self.font.pointSize + 2):self.font.pointSize;
    self.font = [UIFont fontWithName:self.font.fontName size:fontSize];
    
    
    //...
    if ([self respondsToSelector:@selector(textContainerInset)]) {
        self.textContainerInset = UIEdgeInsetsMake(0.f, -self.textContainer.lineFragmentPadding, 0.f, _validateContent?RightViewSize.width - (CScreenWidth*8/375):0);
        //...Here 8 is right padding of txtView according to device width.
    }
    
    
    //...Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidBeginEditing:) name:UITextViewTextDidBeginEditingNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidEndEditing:) name:UITextViewTextDidEndEditingNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextDidChange:) name:UITextViewTextDidChangeNotification object:self];
    
    
    
    //1. Placeholder Color.
    if (_placeHolderColor == nil)
        _placeHolderColor = [UIColor lightGrayColor];
    
    //2. Placeholder Color When Selected.
    if (_selectedPlaceHolderColor == nil)
        _selectedPlaceHolderColor = [UIColor blackColor];
    
    //3. Bottom line Color.
    if (_lineColor == nil)
        _lineColor = [UIColor lightGrayColor];
    
    //4. Bottom line Color When Selected.
    if (_selectedLineColor == nil)
        _selectedLineColor = [UIColor blackColor];
    
    //5. Bottom line error Color.
    if (_errorLineColor == nil)
        _errorLineColor = [UIColor redColor];
}

- (void)addBottomLineView
{
    if (_bottomLineView) [_bottomLineView removeFromSuperview];
    
    _bottomLineView = [[UIView alloc] initWithFrame:CGRectZero];
    [_bottomLineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_bottomLineView setBackgroundColor:_lineColor];
    [self.superview insertSubview:_bottomLineView aboveSubview:self];
    
    [_bottomLineView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:1]];
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:_bottomLineView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
}

- (void)addPlaceholderLabel
{
    if (_placeholder && _placeholder.length > 0)
    {
        if (_labelPlaceholder) [_labelPlaceholder removeFromSuperview];
        
        _labelPlaceholder = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelPlaceholder setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_labelPlaceholder setTextAlignment:self.textAlignment];
        [_labelPlaceholder setTextColor:_placeHolderColor];
        [_labelPlaceholder setFont:self.font];
        [_labelPlaceholder setText:_placeholder];
        [self.superview insertSubview:_labelPlaceholder belowSubview:self];
        
        _cnLeftPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
        
        _cnRightPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
        
        _cnTopPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        
        _cnBottomPlaceholder = [NSLayoutConstraint constraintWithItem:_labelPlaceholder attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        
        [self.superview addConstraints:@[_cnLeftPlaceholder, _cnRightPlaceholder, _cnTopPlaceholder, _cnBottomPlaceholder]];
    }
}

- (void)addValidationView
{
    if (_validateContent)
    {
        if (_buttonValidation) [_buttonValidation removeFromSuperview];
        
        _buttonValidation = [[UIButton alloc] initWithFrame:CGRectZero];
        [_buttonValidation setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_buttonValidation setImage:_imageIncorrectValidation forState:UIControlStateNormal];
        [_buttonValidation setImage:_imageCorrectValidation forState:UIControlStateSelected];
        [_buttonValidation setUserInteractionEnabled:NO];
        [_buttonValidation setHidden:(self.text.length == 0)];
        [_buttonValidation setSelected:_isValidationCorrect];
        [self.superview insertSubview:_buttonValidation aboveSubview:self];
        
        [_buttonValidation addConstraint:[NSLayoutConstraint constraintWithItem:_buttonValidation attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:RightViewSize.width]];
        
        [_buttonValidation addConstraint:[NSLayoutConstraint constraintWithItem:_buttonValidation attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:RightViewSize.height]];
        
        [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:_buttonValidation attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
        
        [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:_buttonValidation attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-15]];
    }
}

- (void)floatPlaceHolderAnimated:(BOOL)animated
{
    BOOL isSelected = (self.text.length == 0)?self.isFirstResponder:YES;
    
    CGFloat constTop = _cnTopPlaceholder.constant;
    CGFloat constBottom = _cnBottomPlaceholder.constant;
    UIFont *_font;
    
    if (isSelected)
    {
        if (_validateContent && self.text.length > 0 && !_isValidationCorrect)
            _bottomLineView.backgroundColor = _errorLineColor;
        else
            _bottomLineView.backgroundColor = _selectedLineColor;
        //    _bottomLineView.backgroundColor = self.isFirstResponder?_selectedLineColor:_lineColor;
        
        _labelPlaceholder.textColor = _selectedPlaceHolderColor;
        _font = [UIFont fontWithName:self.font.fontName size:roundf(self.font.pointSize * 85 / 100)];
        _labelPlaceholder.font = _font;
        
        constTop = - _initialFrame.size.height;
        constBottom = 0;
    }
    else
    {
        if (_validateContent && self.text.length > 0 && !_isValidationCorrect)
            _bottomLineView.backgroundColor = _errorLineColor;
        else
        {
            _bottomLineView.backgroundColor = _lineColor;
            _buttonValidation.hidden = YES;
        }
        
        _labelPlaceholder.textColor = _placeHolderColor;
        _font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize];
        
        constTop = 0;
        constBottom = self.frame.size.height;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (animated) {
            [UIView animateWithDuration:AnimationDuration animations:^{
                [_cnTopPlaceholder setConstant:constTop];
                [_cnBottomPlaceholder setConstant:constBottom];
                [_labelPlaceholder.superview layoutIfNeeded];
            } completion:^(BOOL finished) {
                [_labelPlaceholder setFont:_font];
            }];
        } else {
            [_cnTopPlaceholder setConstant:constTop];
            [_cnBottomPlaceholder setConstant:constBottom];
            [_labelPlaceholder.superview layoutIfNeeded];
            [_labelPlaceholder setFont:_font];
        }
    });
}





#pragma mark -
#pragma mark - UITextView Notification.

- (void)textViewDidBeginEditing:(NSNotification *)notification
{
    [self floatPlaceHolderAnimated:YES];
}

- (void)textViewDidEndEditing:(NSNotification *)notification
{
    [self floatPlaceHolderAnimated:YES];
}

- (void)textViewTextDidChange:(NSNotification *)notification
{
    //...Increase self height accroding to content.
    CGSize newSize = [self sizeThatFits:CGSizeMake(self.frame.size.width, MAXFLOAT)];
    if (_cnHeightSelf == nil) {
        _cnHeightSelf = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:0];
        [self addConstraint:_cnHeightSelf];
    }
    _cnHeightSelf.constant = fmaxf(_initialFrame.size.height, newSize.height + (CScreenWidth*8/375));
//    _cnBottomPlaceholder.constant = - (_cnHeightSelf.constant);
    
    
    
    //...Check validation.
    if (_validateContent)
    {
        if (self.text.length == 0) {
            _buttonValidation.hidden = YES;
            _bottomLineView.backgroundColor = self.selectedLineColor;
        } else {
            _buttonValidation.hidden = NO;
            _bottomLineView.backgroundColor = (_buttonValidation.isSelected)?self.selectedLineColor:self.errorLineColor;
        }
    }
}





#pragma mark -
#pragma mark - Validation

- (void)setTextViewValidate:(BOOL)isValidate
{
    if (_validateContent)
    {
        [self setIsValidationCorrect:isValidate];
        [_buttonValidation setSelected:isValidate];
        [_buttonValidation setHidden:(self.text.length == 0)];
        
        if (isValidate)
            _bottomLineView.backgroundColor = self.selectedLineColor;
        else
            _bottomLineView.backgroundColor = self.errorLineColor;
    }
}

@end
